/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.sin.tsr;

public final class R {
    public static final class attr {
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int font=0x7f010000;
    }
    public static final class color {
        public static final int article_title=0x7f05000c;
        public static final int articlecolor=0x7f05000b;
        public static final int back=0x7f050017;
        public static final int black=0x7f050004;
        public static final int blue=0x7f050008;
        public static final int blueback=0x7f05000a;
        public static final int bottom_text=0x7f050016;
        public static final int cachecolor=0x7f05000d;
        public static final int daily_color=0x7f050018;
        public static final int dark_gray=0x7f050015;
        public static final int date_color=0x7f050003;
        public static final int first_line=0x7f05001c;
        public static final int gold=0x7f050009;
        public static final int gray=0x7f050005;
        public static final int green=0x7f050014;
        public static final int market_color=0x7f05001a;
        public static final int market_selcted_color=0x7f05001b;
        public static final int orange=0x7f050000;
        public static final int red=0x7f050012;
        public static final int second_line=0x7f05001d;
        public static final int selectedgray=0x7f050006;
        public static final int slate_blue=0x7f050011;
        public static final int text_color=0x7f050019;
        public static final int third_line=0x7f05001e;
        public static final int trans_blue=0x7f050013;
        public static final int trans_gray=0x7f05000f;
        public static final int trans_red=0x7f050010;
        public static final int trans_white=0x7f05000e;
        public static final int transparent=0x7f050002;
        public static final int unselectedgray=0x7f050007;
        public static final int white=0x7f050001;
    }
    public static final class drawable {
        public static final int back=0x7f020000;
        public static final int back_button=0x7f020001;
        public static final int back_selected=0x7f020002;
        public static final int bt_click=0x7f020003;
        public static final int chart_button=0x7f020004;
        public static final int click_bt=0x7f020005;
        public static final int click_bt_selected=0x7f020006;
        public static final int click_button=0x7f020007;
        public static final int day_bt=0x7f020008;
        public static final int day_bt_1=0x7f020009;
        public static final int day_bt_2=0x7f02000a;
        public static final int day_bt_3=0x7f02000b;
        public static final int day_bt_4=0x7f02000c;
        public static final int day_bt_5=0x7f02000d;
        public static final int day_bt_6=0x7f02000e;
        public static final int day_bt_click=0x7f02000f;
        public static final int day_button=0x7f020010;
        public static final int day_button_1=0x7f020011;
        public static final int day_button_2=0x7f020012;
        public static final int day_button_3=0x7f020013;
        public static final int day_button_4=0x7f020014;
        public static final int day_button_5=0x7f020015;
        public static final int day_button_6=0x7f020016;
        public static final int gloss_button_press=0x7f020017;
        public static final int gloss_button_unpress=0x7f020018;
        public static final int gloss_gray_button=0x7f020019;
        public static final int gloss_topbar=0x7f02001a;
        public static final int home=0x7f02001b;
        public static final int home_selected=0x7f02001c;
        public static final int home_unselected=0x7f02001d;
        public static final int ic_launcher=0x7f02001e;
        public static final int icon=0x7f02001f;
        public static final int icon_list=0x7f020020;
        public static final int input_bg=0x7f020021;
        public static final int list_selector=0x7f020022;
        public static final int mail_icon=0x7f020023;
        public static final int market_button=0x7f020024;
        public static final int markets=0x7f020025;
        public static final int markets_line=0x7f020026;
        public static final int markets_selected=0x7f020027;
        public static final int markets_unselected=0x7f020028;
        public static final int new_gloss_button=0x7f020029;
        public static final int new_gloss_button_left_right=0x7f02002a;
        public static final int new_gloss_button_left_right_unpress=0x7f02002b;
        public static final int new_gloss_button_right=0x7f02002c;
        public static final int new_gloss_button_right_unpress=0x7f02002d;
        public static final int new_gloss_button_unpress=0x7f02002e;
        public static final int news=0x7f02002f;
        public static final int news_selected=0x7f020030;
        public static final int news_unselected=0x7f020031;
        public static final int passicon=0x7f020032;
        public static final int search=0x7f020033;
        public static final int search_button=0x7f020034;
        public static final int search_icon=0x7f020035;
        public static final int shape=0x7f020036;
        public static final int shapewhite=0x7f020037;
        public static final int splash=0x7f020038;
        public static final int tab1=0x7f020039;
        public static final int tab2=0x7f02003a;
        public static final int tab3=0x7f02003b;
        public static final int tab_selected=0x7f02003c;
        public static final int tap_bg=0x7f02003d;
        public static final int test_graph=0x7f02003e;
        public static final int test_graph1=0x7f02003f;
        public static final int test_img=0x7f020040;
        public static final int text_area=0x7f020041;
        public static final int text_area_lower=0x7f020042;
        public static final int text_area_rounded=0x7f020043;
        public static final int text_area_upper=0x7f020044;
        public static final int title_bar=0x7f020045;
        public static final int top_bar=0x7f020046;
        public static final int top_bar_gray_02=0x7f020047;
        public static final int top_search_bg=0x7f020048;
        public static final int top_search_bg_1=0x7f020049;
        public static final int topsearch=0x7f02004a;
        public static final int topsearch_selected=0x7f02004b;
        public static final int user_icon=0x7f02004c;
    }
    public static final class id {
        public static final int ImageView01=0x7f08000b;
        public static final int ImageView02=0x7f080031;
        public static final int ImageView03=0x7f08002f;
        public static final int LinearLayout1=0x7f08003b;
        public static final int MyTextView1=0x7f080015;
        public static final int RelativeLayout1=0x7f080041;
        public static final int ScrollView1=0x7f080012;
        public static final int TextView03=0x7f08002d;
        public static final int TextView04=0x7f080019;
        public static final int TextView05=0x7f08001b;
        public static final int TextView08=0x7f08001d;
        public static final int TextView09=0x7f08001f;
        public static final int back_bt=0x7f080043;
        public static final int bt_1d=0x7f080027;
        public static final int bt_1m=0x7f080029;
        public static final int bt_1y=0x7f08002b;
        public static final int bt_5d=0x7f080028;
        public static final int bt_5y=0x7f08002c;
        public static final int bt_6m=0x7f08002a;
        public static final int bt_layout=0x7f080026;
        public static final int bt_list_more=0x7f080046;
        public static final int chartview=0x7f080025;
        public static final int detail_layout=0x7f080039;
        public static final int details=0x7f08004a;
        public static final int frameLayout1=0x7f080033;
        public static final int graph_layout=0x7f080023;
        public static final int groupItem=0x7f080000;
        public static final int imageView1=0x7f080005;
        public static final int imageView2=0x7f08000d;
        public static final int iv_group_list_bg_divider=0x7f080001;
        public static final int join_bt=0x7f080010;
        public static final int linearLayout1=0x7f080034;
        public static final int list_layout=0x7f08004d;
        public static final int list_tweet=0x7f08002e;
        public static final int log_bt=0x7f08000f;
        public static final int login_layout=0x7f08000a;
        public static final int logout_bt=0x7f080009;
        public static final int lvGroup=0x7f080002;
        public static final int market_list=0x7f08003a;
        public static final int price_view_layout=0x7f080014;
        public static final int real_graph=0x7f080024;
        public static final int search_bt=0x7f080008;
        public static final int search_layout=0x7f080004;
        public static final int searchbt_layout=0x7f080007;
        public static final int show_layout=0x7f080021;
        public static final int show_txt=0x7f080022;
        public static final int stockChartView1=0x7f080040;
        public static final int textView1=0x7f080017;
        public static final int texttitle=0x7f080042;
        public static final int top_search=0x7f080011;
        public static final int topsearch_bt=0x7f080044;
        public static final int txt_confirm=0x7f080032;
        public static final int txt_current=0x7f080016;
        public static final int txt_mail=0x7f080030;
        public static final int txt_pass=0x7f08000e;
        public static final int txt_search=0x7f080006;
        public static final int txt_user=0x7f08000c;
        public static final int txt_verify=0x7f08004b;
        public static final int verify_bt=0x7f08004c;
        public static final int view_asia=0x7f080038;
        public static final int view_close=0x7f08001c;
        public static final int view_daily=0x7f080018;
        public static final int view_eu=0x7f080036;
        public static final int view_high=0x7f08001e;
        public static final int view_img=0x7f080049;
        public static final int view_list_detail=0x7f08003f;
        public static final int view_list_img=0x7f08003c;
        public static final int view_list_price=0x7f08003e;
        public static final int view_list_publish=0x7f080045;
        public static final int view_list_title=0x7f08003d;
        public static final int view_low=0x7f080020;
        public static final int view_no=0x7f08004f;
        public static final int view_open=0x7f08001a;
        public static final int view_publish=0x7f080048;
        public static final int view_stock=0x7f080013;
        public static final int view_title=0x7f080047;
        public static final int view_uk=0x7f080037;
        public static final int view_us=0x7f080035;
        public static final int view_welcome=0x7f080003;
        public static final int view_yahoo_list=0x7f08004e;
        public static final int webView1=0x7f080050;
    }
    public static final class layout {
        public static final int fragment_agreeableness_step1=0x7f030000;
        public static final int group_item_view=0x7f030001;
        public static final int group_list=0x7f030002;
        public static final int home=0x7f030003;
        public static final int home_search=0x7f030004;
        public static final int joinus=0x7f030005;
        public static final int main=0x7f030006;
        public static final int markets=0x7f030007;
        public static final int markets_list=0x7f030008;
        public static final int splash=0x7f030009;
        public static final int test=0x7f03000a;
        public static final int topbar=0x7f03000b;
        public static final int tweet_list=0x7f03000c;
        public static final int tweet_news_details=0x7f03000d;
        public static final int verify=0x7f03000e;
        public static final int yahoo_list=0x7f03000f;
        public static final int yahoo_news=0x7f030010;
        public static final int yahoo_news_details=0x7f030011;
    }
    public static final class raw {
        public static final int dark=0x7f040000;
        public static final int gray=0x7f040001;
    }
    public static final class string {
        public static final int Dialog=0x7f060003;
        public static final int app_name=0x7f060001;
        public static final int hello=0x7f060000;
        public static final int tsr=0x7f060002;
    }
    public static final class style {
        public static final int button=0x7f070000;
    }
    public static final class styleable {
        /** Attributes that can be used with a CustomFont.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #CustomFont_font com.sin.tsr:font}</code></td><td></td></tr>
           </table>
           @see #CustomFont_font
         */
        public static final int[] CustomFont = {
            0x7f010000
        };
        /**
          <p>This symbol is the offset where the {@link com.sin.tsr.R.attr#font}
          attribute's value can be found in the {@link #CustomFont} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.sin.tsr:font
        */
        public static final int CustomFont_font = 0;
    };
}
