package com.sin.tsr.shared;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;

public class SPUserInfo {

	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	public static boolean newsrefreshflg = false;
	Context context;

	//Warning!!!
	//the string is totally follow the string return from web service
	public static final String USERNAME = "UserName";
	public static final String PASSWORD = "Password";
	public static final String EMAIL = "Email";
	public static int logged = 0;

	public SPUserInfo(Context _context) {

		context = _context;

		preferences = _context.getSharedPreferences("TSR Data",
				Context.MODE_PRIVATE);
		editor = preferences.edit();

	}
	public String getUserInfo(String sKey) {
		return preferences.getString(sKey, "");
	}

	public void setUserInfo(String sKey, String sValue) {
		editor.putString(sKey, sValue);
	}

	public void commit() {
		editor.commit();
	}
	
	public void clear() {
		editor.clear();
		commit();
	}

}
