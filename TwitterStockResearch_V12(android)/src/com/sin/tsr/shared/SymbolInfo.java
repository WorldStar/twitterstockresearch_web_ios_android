package com.sin.tsr.shared;

public class SymbolInfo {
	public String[] symbol = new String[3];
	public String[] title = new String[3];
	public void setUS(){
		symbol[0] = "^dji";
		symbol[1] = "^GSPC";
		symbol[2] = "^IXIC";
		title[0] = "DOW Jones";
		title[1] = "   S&P   ";
		title[2] = "NASDAQ";
	}
	public void setUK(){
		symbol[0] = "^FTSE";
		symbol[1] = "^FTMC";
		symbol[2] = "^FTAI";
		title[0] = "FTSE 100";
		title[1] = "FTSE 250";
		title[2] = "  AIM  ";
	}
	public void setEurope(){
		symbol[0] = "^GDAXI";
		symbol[1] = "^FCHI";
		symbol[2] = "^IBEX";
		title[0] = " DAX ";
		title[1] = "CAC 40";
		title[2] = " IBEX";
	}
	public void setAsia(){
		symbol[0] = "^N225";
		symbol[1] = "^HSI";
		symbol[2] = "000001.SS";
		title[0] = "  Nikkei 225 ";
		title[1] = "  Hang Seng  ";
		title[2] = "SSE Composite";
	}
}
