package com.sin.tsr.utils;

import android.content.Context;
import android.util.DisplayMetrics;

public class PxDpConverter {
	public int DpToPx(Context context, int dp) {
	    DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
	    int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));       
	    return px;
	}
	public int PxToDp(Context context, int px) {
	    DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
	    int dp = Math.round(px * ( DisplayMetrics.DENSITY_DEFAULT / displayMetrics.xdpi));       
	    return dp;
	}
}
