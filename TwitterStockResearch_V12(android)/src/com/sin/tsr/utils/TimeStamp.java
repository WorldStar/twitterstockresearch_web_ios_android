package com.sin.tsr.utils;

import java.util.Date;

public class TimeStamp {
	public String GetDateFromStamp(long timestamp,int flg){
		String date = "";
		Date d = new Date(timestamp);
		switch(flg){
		case 1:
			// only 1d
			date = d.getHours() +":" + d.getMinutes();
			break;
		case 2:
			// only 5d
			date = "" + d.getDay();
			break;
		case 3:
			// only 1m
			date = "" + d.getDay();
			break;
		case 4:
			// only 6m
			int m = d.getMonth() + 1;
			date = "" + m;
			break;
		case 5:
			// only 1y
			int m1 = d.getMonth() + 1;			
			date = "" + d.getYear()+"/"+m1;
			break;
		case 6:
			// only 5y
			int m2 = d.getMonth() + 1;			
			date = "" + d.getYear()+"/"+m2;
			break;
		default:
			// only 5y
			int m3 = d.getMonth() + 1;			
			date = "" + d.getYear()+"/"+m3+"/"+d.getDay() + " " + d.getHours() +":"+ d.getMinutes()+":"+d.getSeconds();
			break;
		}
		return date;
	}
}
