package com.sin.tsr.lib;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.sin.tsr.news.news;
import com.sin.tsr.shared.YahooNewsInfo;


public class PullXML {
	public static List<YahooNewsInfo> YahooNewsMain (String string)
	throws XmlPullParserException, IOException
	{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		xpp.setInput( new StringReader(string));
		int eventType = xpp.getEventType();
		String tag = "", stag = "";
		String value = "";
		List<YahooNewsInfo> resultMaps = new ArrayList<YahooNewsInfo>();
		int i = 0;
		YahooNewsInfo yn1 = new YahooNewsInfo();
		boolean save = false, save1 = false, save2 = false;
		while (eventType != XmlPullParser.END_DOCUMENT) {
			
			if(eventType == XmlPullParser.START_TAG) {
				
				tag = xpp.getName();
				
				if(tag.compareTo("channel") == 0)
					save = true;
				if(tag.compareTo("item") == 0){
					yn1 = new YahooNewsInfo();
					save1 = true;
				}
				if(tag.compareTo("channel") == 0)
					save2 = true;

				
			} else if(eventType == XmlPullParser.TEXT) {
				
				value = xpp.getText();
				
				if (value == null)
					value = "";
				
				//only save the value under DocumentElement
				
				if (save2){
					
					//if already have the same key, put it into array
					
					if(tag.equals("title")){
						YahooNewsInfo yn = new YahooNewsInfo();
						yn.title = value;
						resultMaps.add(yn);
					}
				}else if (save){
					if(save1){
					//if already have the same key, put it into array
						
						if(tag.equals("title")){
							yn1.title = value;
						}else if(tag.equals("link")){
								yn1.link = value;
						}else if(tag.equals("description")){
							yn1.desc = value;
						}else if(tag.equals("guid")){
							yn1.guid = value;
						}
						if(tag.equals("pubDate")){
							yn1.date = value;
						}
					}
				}
				
			} else if(eventType == XmlPullParser.END_TAG) {

				if(xpp.getName().compareTo("channel") == 0){
					save = false;					
				}
				if(xpp.getName().compareTo("copyright") == 0){
					save2 = false;					
				}
				if(xpp.getName().compareTo("item") == 0){
					resultMaps.add(yn1);
					i++;
					save1 = false;					
				}
			} 
			
			eventType = xpp.next();
		}
		
		
		return resultMaps;
	}
	public static List<HashMap<String,String>> main1 (String string)
			throws XmlPullParserException, IOException
	{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		xpp.setInput( new StringReader(string));
		int eventType = xpp.getEventType();
		String tag = "";
		String value = "";
		List<HashMap<String, String>> resultMaps = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> temp = new HashMap<String, String>();
		
		boolean save = false;
		int i = 0;
		while (eventType != XmlPullParser.END_DOCUMENT) {
			
			if(eventType == XmlPullParser.START_TAG) {
				
				tag = xpp.getName();
				
				if(tag.compareTo("DocumentElement") == 0)
					save = true;
				
			} else if(eventType == XmlPullParser.TEXT) {
				
				value = xpp.getText();
				
				if (value == null)
					value = "";
				
				//only save the value under DocumentElement
				if (save){
					
					//if already have the same key, put it into array
					if (temp.containsKey(tag)){
						
						resultMaps.add(temp);
						temp = new HashMap<String, String>();
						
					}
					temp.put(tag, value);
				}
				
			} else if(eventType == XmlPullParser.END_TAG) {

				if(xpp.getName().compareTo("DocumentElement") == 0){
					save = false;
					i++;
				}
			} 
			
			eventType = xpp.next();
		}
		
		//add last element if not empty
		if (!temp.isEmpty()){
			resultMaps.add(temp);
			
		}
		
		return resultMaps;
	}
}
