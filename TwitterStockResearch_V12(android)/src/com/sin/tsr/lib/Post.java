package com.sin.tsr.lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import android.util.Log;

public class Post {
	public String CallWebService(String url, String soapAction, String envelope) {
		final DefaultHttpClient httpClient = new DefaultHttpClient();
		// request parameters
		HttpParams params = httpClient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, 20000);
		HttpConnectionParams.setSoTimeout(params, 25000);
		// set parameter
		HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

		// POST the envelope
		HttpPost httppost = new HttpPost(url);
		// add headers
		if (soapAction.equals("1")) {
			httppost.setHeader("soapaction", "");
			httppost.setHeader("Content-Type", "text/xml; charset=ISO-8859-1");
		} else {
			httppost.setHeader("soapaction", soapAction);
			httppost.setHeader("Content-Type", "text/xml; charset=utf-8");
			// httppost.setHeader("Content-Type", "text/xml;");
		}

		String responseString = "";
		try {

			// the entity holds the request
			HttpEntity entity = new StringEntity(envelope);
			Log.d("content", envelope.toString());
			httppost.setEntity(entity);

			// Response handler
			ResponseHandler<String> rh = new ResponseHandler<String>() {
				// invoked when client receives response
				@Override
				public String handleResponse(HttpResponse response)
						throws ClientProtocolException, IOException {

					// get response entity
	
					HttpEntity entity = response.getEntity();
					InputStream inputStream = entity.getContent();
					
					BufferedReader br = new BufferedReader(
							new InputStreamReader(inputStream, "UTF-8"));

					StringBuilder sb = new StringBuilder();

					String line;
					while ((line = br.readLine()) != null) {
						sb.append(line);
					}
					// convert stream to string
					//String ok = EntityUtils.toString(entity,
					//		HTTP.PLAIN_TEXT_TYPE);
					String ok = sb.toString();
					Log.d("result", ok);
					// read the response as byte array
					StringBuffer out = new StringBuffer();
					// byte[] b = EntityUtils.toByteArray(entity);
					byte[] b = ok.getBytes();
					// write the response byte array to a string buffer
					out.append(new String(b, 0, b.length));
					return out.toString();
				}
			};

			responseString = httpClient.execute(httppost, rh);

		} catch (Exception e) {
			responseString = "";
		}

		// close the connection
		httpClient.getConnectionManager().shutdown();
		// Log.d("response string", responseString + responseString.length());
		return responseString;
	}
}
