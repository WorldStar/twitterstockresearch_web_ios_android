package com.sin.tsr.lib;

import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.util.Calendar;
import android.content.Context;
import android.os.Environment; 
public class logFile
{
	private String m_strLogFileFolderPath = "";
	private String m_strLogFileName = "MerchantLogFile";
	public void logFile(){
		
	}
	public void confirmDir(String filename)
	{
		// SD 카드가 있으면 SD 카드 경로, 없을 경우는 그냥 Root
		boolean bSDCardExist = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
		if (bSDCardExist == true)
		{
		m_strLogFileFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		}
		else
		{
		m_strLogFileFolderPath = "";
		}
		m_strLogFileFolderPath = m_strLogFileFolderPath + "/MoolaLog";
		File dir = new File(m_strLogFileFolderPath);
		if(!dir.exists()){
			dir.mkdir();
		}
		m_strLogFileName = filename + ".txt";
	}
	public void write(String filename, String strMessage)
	{
		confirmDir(filename);
		String _strMessage = strMessage;
		if ( (strMessage == null) || (strMessage.length() == 0) )
			return;  
		//_strMessage = getCurrentTime() + ":::" + _strMessage;
		File file = new File(m_strLogFileFolderPath + "/" + m_strLogFileName);
		if(file.exists()){
			file.delete();
		}
		RandomAccessFile access = null;
		try
		{
			access = new RandomAccessFile(file, "rw");
			access.seek(file.length());			
			if (access != null)
			{
				access.write(_strMessage.getBytes());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (access != null)
				{
					access.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
 
	private static String getCurrentTime()
	{
		Calendar calendar = Calendar.getInstance();
		String strTime = String.format("%d-%d-%d %d:%d:%d", calendar.get(Calendar.YEAR),

				calendar.get(Calendar.MONTH) + 1,
				calendar.get(Calendar.DAY_OF_MONTH),
				calendar.get(Calendar.HOUR_OF_DAY),
				calendar.get(Calendar.MINUTE),
				calendar.get(Calendar.SECOND));
		return strTime;
	}
	private static String getCurrentDate()
	{
		Calendar calendar = Calendar.getInstance();
		String strTime = String.format("%d-%d-%d", calendar.get(Calendar.YEAR),

				calendar.get(Calendar.MONTH) + 1,
				calendar.get(Calendar.DAY_OF_MONTH)
				);
		return strTime;
	}
}