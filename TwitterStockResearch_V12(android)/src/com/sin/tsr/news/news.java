package com.sin.tsr.news;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.sin.tsr.ConnectionDetector;
import com.sin.tsr.R;
import com.sin.tsr.TSRMainActivity;
import com.sin.tsr.TabGroupActivity;
import com.sin.tsr.autocomplete.AutoCompleteForDetails;
import com.sin.tsr.home.search_details;
import com.sin.tsr.lib.PullXML;
import com.sin.tsr.lib.ReturnMessage;
import com.sin.tsr.shared.RestoreNews;
import com.sin.tsr.shared.SPUserInfo;
import com.sin.tsr.shared.YahooNewsInfo;
import com.sin.tsr.textview.MyTextView;
import com.sin.tsr.textview.MyTextViewBold;
import com.sin.tsr.webservice.CheckInternetAccess;
import com.sin.tsr.webservice.WebService;

public class news extends Activity{
	Typeface font1,font2;
	String symbol = null;
	LinearLayout top_search;
	AutoCompleteForDetails searchtext;
	MyTextView view_no;
	MyTextView view_title;
	List<YahooNewsInfo> result = new ArrayList<YahooNewsInfo>();
	ListView viewlist;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.yahoo_news);

		font1= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-MdCn.ttf");
		font2= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-Cn.ttf");

		LinearLayout backbt = (LinearLayout)findViewById(R.id.back_bt);
		backbt.setVisibility(View.GONE);
		backbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((TabGroupActivity)getParent()).onBackPressed();
			}
		});
		LinearLayout topserarchbt = (LinearLayout)findViewById(R.id.topsearch_bt);
		topserarchbt.setVisibility(View.VISIBLE);
		top_search = (LinearLayout)findViewById(R.id.top_search);
		top_search.setVisibility(View.GONE);
		topserarchbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				top_search.setVisibility(View.VISIBLE);
			}
		});
		view_no = (MyTextView)findViewById(R.id.view_no);
		view_title = (MyTextView)findViewById(R.id.view_title);
		searchtext = (AutoCompleteForDetails)findViewById(R.id.txt_search);
		viewlist = (ListView)findViewById(R.id.view_yahoo_list);
	}
	@Override
	public void onResume(){
		this.symbol = TSRMainActivity.symbol;
		if(TSRMainActivity.symbol == null || TSRMainActivity.symbol.equals("")){
			top_search.setVisibility(View.VISIBLE);
			view_no.setVisibility(View.VISIBLE);
			
		}else{
			top_search.setVisibility(View.GONE);
			view_no.setVisibility(View.VISIBLE);
			new GetNewsDataTask().execute();
			SPUserInfo.newsrefreshflg = false;
		}

		if(SPUserInfo.logged == 0){
			ReturnMessage.showAlertDialog(getParent(), "Please log in and try again");
			searchtext.setEnabled(false);
		}
		searchtext.setOnKeyListener(new View.OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
			            (keyCode == KeyEvent.KEYCODE_ENTER)) {
					String symbol1 = searchtext.getText().toString();
					view_title.setText("");
					//search_details.this.symbol = symbol;
					TSRMainActivity.symbol = symbol1;
					symbol = TSRMainActivity.symbol;
					new GetNewsDataTask().execute();
					SPUserInfo.newsrefreshflg = false;
					getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
					return true;
				}
				return false;
			}
		});
		searchtext.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				if(position < searchtext.getList().size()){
					String s = searchtext.getList().get(position);
					String[] s1 = s.split(",");
					String symbol1 = s1[0];
					view_title.setText(s);
					//search_details.this.symbol = symbol;
					TSRMainActivity.symbol = symbol1;
					searchtext.setText(s);
					symbol = TSRMainActivity.symbol;
					new GetNewsDataTask().execute();
					SPUserInfo.newsrefreshflg = false;
					getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
					//InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					//imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
				}
			}
			
		});
			//InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			//imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		super.onResume();
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(getParent()).setTitle("TSR")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}
	private class GetNewsDataTask extends AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetNewsDataTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result != null){
				if(!_result.equals("")){
					try{
						if(_result.contains("Not Found")){
							Alert("Please input stock symbol and try again.");
						}else{
							List<YahooNewsInfo> resultMaps = new ArrayList<YahooNewsInfo>();
							resultMaps = PullXML.YahooNewsMain(_result);
							view_title.setText(resultMaps.get(0).title);
							
							resultMaps.remove(0);
							result.clear();
							result.addAll(resultMaps);
							//RestoreNews news = new RestoreNews();
							//news.setNews(result);
							view_no.setVisibility(View.GONE);
							YahooNewsListAdapter homeadapter = new YahooNewsListAdapter(
									getParent(), R.layout.yahoo_list, result);
							viewlist.setAdapter(homeadapter);
							viewlist.setSmoothScrollbarEnabled(true);
							viewlist.setOnItemClickListener(new OnItemClickListener(){

								@Override
								public void onItemClick(AdapterView<?> arg0,
										View arg1, int position, long arg3) {
									// TODO Auto-generated method stub
									String link = result.get(position+1).link;
									Intent intent = new Intent(getParent(), News_details.class);
									TabGroupActivity parentActivity = (TabGroupActivity) getParent();
									intent.putExtra("link", link);
									parentActivity.startChildActivity("detail", intent);
								}
								
							});
							homeadapter.notifyDataSetChanged();
							
						}
					}catch(Exception e){
						Alert("Can not get stock's news data from yahoo. Please try again.");
					}
				}else{
					Alert("Can not get stock's news data from yahoo. please try again.");
				}
			}else{
				Alert("Failed service. Please try again.");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			 MyDialog = ProgressDialog.show(getParent(), "",
	                    "Loading... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(news.this);
	        if (check.checkNetwork()) {
	        	WebService service= new WebService();
				result = service.YahooNewsConnect (URLEncoder.encode(symbol));
	        }
			return result;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Overrides the default implementation for KeyEvent.KEYCODE_BACK 
	 * so that all systems call onBackPressed().
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			((TabGroupActivity)getParent()).onBackPressed();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	public class YahooNewsListAdapter extends ArrayAdapter<YahooNewsInfo> {

		private List<YahooNewsInfo> items;
		Context con;

		public YahooNewsListAdapter(Context context, int textViewResourceId,
				List<YahooNewsInfo> objects) {

			super(context, textViewResourceId, objects);
			this.items = objects;
			this.con = context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
				throws IndexOutOfBoundsException, IllegalStateException {
			// TODO Auto-generated method stub
			View v = convertView;
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			final YahooNewsInfo item = items.get(position);
			if (v == null) {
				v = vi.inflate(R.layout.yahoo_list, null);
			}
			MyTextView title = (MyTextView) v
					.findViewById(R.id.view_list_title);
			MyTextView desc = (MyTextView) v
					.findViewById(R.id.view_list_detail);
			MyTextView date = (MyTextView) v
					.findViewById(R.id.view_list_publish);
			title.setText(item.title);
			String s = item.desc;
			if(s.length() > 60){
				s = s.substring(0, 59);
			}
			desc.setText(s);
			date.setText(item.date);
			return v;
		}
	}

}
