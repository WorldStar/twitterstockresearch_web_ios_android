package com.sin.tsr.news;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
import org.stockchart.StockChartActivity;
import org.stockchart.StockChartView;
import org.stockchart.core.Area;
import org.stockchart.core.Appearance.Gradient;
import org.stockchart.series.LinearSeries;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.sin.tsr.ConnectionDetector;
import com.sin.tsr.R;
import com.sin.tsr.TSRMainActivity;
import com.sin.tsr.TabGroupActivity;
import com.sin.tsr.autocomplete.AutoComplete;
import com.sin.tsr.autocomplete.AutoCompleteForDetails;
import com.sin.tsr.shared.ChartInfo;
import com.sin.tsr.shared.CurrentInfo;
import com.sin.tsr.shared.SPUserInfo;
import com.sin.tsr.textview.MyButton;
import com.sin.tsr.textview.MyTextView;
import com.sin.tsr.textview.MyTextViewBold;
import com.sin.tsr.webservice.CheckInternetAccess;
import com.sin.tsr.webservice.WebService;

public class News_details extends Activity{
	Typeface font1,font2;
	boolean connection;
	EditText editverify;
	String link = "";
	String datetime = "1d";
	AutoComplete txtsearh;
	AutoCompleteForDetails searchtext;
	MyTextView txt_current;
	MyTextView view_daily;
	MyTextView view_open;
	MyTextView view_close;
	MyTextView view_high;
	MyTextView view_low;
	LinearLayout real_graph;
	MyButton bt1, bt2, bt3, bt4, bt5, bt6;
	int length = 0;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.yahoo_news_details);
		Intent intent = getIntent();
		link = intent.getStringExtra("link");
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-MdCn.ttf");
		font2= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-Cn.ttf");	
		LinearLayout backbt = (LinearLayout)findViewById(R.id.back_bt);
		backbt.setVisibility(View.VISIBLE);
		backbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((TabGroupActivity)getParent()).onBackPressed();
			}
		});
		SPUserInfo.newsrefreshflg = true;
		LinearLayout topserarchbt = (LinearLayout)findViewById(R.id.topsearch_bt);
		topserarchbt.setVisibility(View.GONE);
		WebView mWeb = (WebView)findViewById(R.id.webView1);
		WebSettings settings = mWeb.getSettings();
        settings.setJavaScriptEnabled(true);
        // the init state of progress dialog
        final ProgressDialog mProgress = ProgressDialog.show(getParent(), "Loading", "Please wait for a moment...", true);
        mProgress.show();
        // add a WebViewClient for WebView, which actually handles loading data from web
        mWeb.setWebViewClient(new WebViewClient() {
            // load url
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
 
            // when finish loading page
            public void onPageFinished(WebView view, String url) {
                if(mProgress.isShowing()) {
                    mProgress.dismiss();
                }
            }
        });
        // set url for webview to load
        mWeb.loadUrl(link);
	}
	@Override
	public void onResume(){
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		super.onResume();
	}
}
