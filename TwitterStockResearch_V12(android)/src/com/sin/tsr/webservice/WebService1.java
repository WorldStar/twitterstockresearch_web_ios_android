package com.sin.tsr.webservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sin.tsr.shared.ChartInfo;
import com.sin.tsr.shared.CurrentInfo;
import com.sin.tsr.shared.CurrentInfo1;

import android.content.Context;
import android.util.Log;

public class WebService1 {
	//public static String TSR_URL = "http://192.168.0.107/NewStockTwits/json/";
	public static String TSR_URL = "https://www.twitterstockresearch.com/json/";
	//public static String CURRENT_URL1 = "http://192.168.0.107/NewStockTwits/json/";
	//public static String CURRENT_URL2 = "_yql.php";
	public static String CURRENT_URL1 = "https://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.quotes%20where%20symbol%20in%20%28%22";
	public static String CURRENT_URL2 = "%22%29%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
	//public static String CHART_URL1 = "http://192.168.0.107/NewStockTwits/json/";
	//public static String CHART_URL2 = "_";
	//public static String CHART_URL3 = ".html";
	public static String CHART_URL1 = "https://chartapi.finance.yahoo.com/instrument/1.0/";
	public static String CHART_URL2 = "/chartdata;type=quote;range=";
	public static String CHART_URL3 = "/csv";
	//public static String YAHOO_NEWS_URL1 = "http://192.168.0.107/NewStockTwits/json/";
	//public static String YAHOO_NEWS_URL2 = "_headline.xml";
    public static String YAHOO_NEWS_URL1 = "https://feeds.finance.yahoo.com/rss/2.0/headline?s=";
	public static String YAHOO_NEWS_URL2 = "&region=US&lang=en-US";
	
	public static int GLOBAL = 2;//local, 2=global
	
   public String tsrConnect(String filename, JSONObject data){

	   // Prepare JSON to send by setting the entity
	   try {
		   URL url = new URL(TSR_URL + filename);

		   HttpParams httpParameters = new BasicHttpParams();
		   int timeoutConnection = 30000;
		   HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		   int timeoutSocket = 50000;
		   HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		   HttpClient httpClient = getHTTPClient(httpParameters);
		   //HttpClient httpClient = createHttpClient(); //HTTPS Connect
		   HttpPost httpPost = new HttpPost(url.toURI());
		   httpPost.setEntity(new StringEntity(data.toString(), "UTF-8"));

		   // Set up the header types needed to properly transfer JSON
		   httpPost.setHeader("Content-Type", "application/json");
		   //httpPost.setHeader("Accept-Encoding", "application/json");
		   //httpPost.setHeader("Accept-Language", "en-US");

		   // Execute POST
		   HttpResponse response = httpClient.execute(httpPost);
		   HttpEntity entity = response.getEntity();
	        // If the response does not enclose an entity, there is no need
	        // to worry about connection release
		   String result = "";
	        if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	            result = convertStreamToString(instream);

	                     

	            // Closing the input stream will trigger connection release
	            instream.close();
	        }
		   return result;
	  } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	   return null;
   }

   public String currentConnect(String symbol){

	   // Prepare JSON to send by setting the entity
	   try {
		   URL url = new URL(CURRENT_URL1 + symbol + CURRENT_URL2);

		   HttpParams httpParameters = new BasicHttpParams();
		   int timeoutConnection = 30000;
		   HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		   int timeoutSocket = 50000;
		   HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		   HttpClient httpClient = getHTTPClient(httpParameters);
		   //HttpClient httpClient = createHttpClient(); //HTTPS Connect
		   HttpPost httpPost = new HttpPost(url.toURI());

		   // Set up the header types needed to properly transfer JSON
		   httpPost.setHeader("Content-Type", "application/json");
		   //httpPost.setHeader("Accept-Encoding", "application/json");
		   //httpPost.setHeader("Accept-Language", "en-US");

		   // Execute POST
		   HttpResponse response = httpClient.execute(httpPost);
		   HttpEntity entity = response.getEntity();
	        // If the response does not enclose an entity, there is no need
	        // to worry about connection release
		   String result = "";
	        if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	            result = convertStreamToString(instream);

	            //if(GLOBAL == 1){
	            	//result = currentdata;
	            //}

	            // Closing the input stream will trigger connection release
	            instream.close();
	        }
		   return result;
	  } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	   return null;
   }

   public List<ChartInfo> chartConnect(String symbol, String format){

	   // Prepare JSON to send by setting the entity
	   try {
		   URL url = new URL(CHART_URL1 + symbol + CHART_URL2 + format + CHART_URL3);

		   HttpParams httpParameters = new BasicHttpParams();
		   int timeoutConnection = 30000;
		   HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		   int timeoutSocket = 50000;
		   HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		   HttpClient httpClient = getHTTPClient(httpParameters);
		   //HttpClient httpClient = createHttpClient(); //HTTPS Connect
		   HttpPost httpPost = new HttpPost(url.toURI());

		   // Set up the header types needed to properly transfer JSON
		   //httpPost.setHeader("Content-Type", "application/json");
		   //httpPost.setHeader("Accept-Encoding", "application/json");
		   //httpPost.setHeader("Accept-Language", "en-US");

		   // Execute POST
		   HttpResponse response = httpClient.execute(httpPost);
		   HttpEntity entity = response.getEntity();
	        // If the response does not enclose an entity, there is no need
	        // to worry about connection release
		   //String[] result = null;
		   List<ChartInfo> result = new ArrayList<ChartInfo>();
	        if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	            result = convertStreamToStringForChart(instream);

	            // Closing the input stream will trigger connection release
	            instream.close();
	        }
		   return result;
	  } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	   return null;
   }

   public List<ChartInfo> chartConnect1(String symbol, String format){

	   // Prepare JSON to send by setting the entity
	   try {
		   URL url = new URL(CHART_URL1 + symbol + CHART_URL2 + format + CHART_URL3);

		   HttpParams httpParameters = new BasicHttpParams();
		   int timeoutConnection = 30000;
		   HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		   int timeoutSocket = 50000;
		   HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		   HttpClient httpClient = getHTTPClient(httpParameters);
		   //HttpClient httpClient = createHttpClient(); //HTTPS Connect
		   HttpPost httpPost = new HttpPost(url.toURI());

		   // Set up the header types needed to properly transfer JSON
		   //httpPost.setHeader("Content-Type", "application/json");
		   //httpPost.setHeader("Accept-Encoding", "application/json");
		   //httpPost.setHeader("Accept-Language", "en-US");

		   // Execute POST
		   HttpResponse response = httpClient.execute(httpPost);
		   HttpEntity entity = response.getEntity();
	        // If the response does not enclose an entity, there is no need
	        // to worry about connection release
		   //String[] result = null;
		   List<ChartInfo> result = new ArrayList<ChartInfo>();
	        if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	            result = convertStreamToStringForChart1(instream);

	            // Closing the input stream will trigger connection release
	            instream.close();
	        }
		   return result;
	  } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	   return null;
   }
   public List<ChartInfo> chartMarketConnect(String symbol, String format){

	   // Prepare JSON to send by setting the entity
	   try {
		   URL url = new URL(CHART_URL1 + symbol + CHART_URL2 + format + CHART_URL3);

		   HttpParams httpParameters = new BasicHttpParams();
		   int timeoutConnection = 30000;
		   HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		   int timeoutSocket = 50000;
		   HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		   HttpClient httpClient = getHTTPClient(httpParameters);
		   //HttpClient httpClient = createHttpClient(); //HTTPS Connect
		   HttpPost httpPost = new HttpPost(url.toURI());

		   // Set up the header types needed to properly transfer JSON
		   //httpPost.setHeader("Content-Type", "application/json");
		   //httpPost.setHeader("Accept-Encoding", "application/json");
		   //httpPost.setHeader("Accept-Language", "en-US");

		   // Execute POST
		   HttpResponse response = httpClient.execute(httpPost);
		   HttpEntity entity = response.getEntity();
	        // If the response does not enclose an entity, there is no need
	        // to worry about connection release
		   //String[] result = null;
		   List<ChartInfo> result = new ArrayList<ChartInfo>();
	        if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	            result = convertStreamToStringForMarketChart(instream);

	            // Closing the input stream will trigger connection release
	            instream.close();
	        }
		   return result;
	  } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	   return null;
   }
   public List<ChartInfo> chartMarketConnect1(String symbol, String format){

	   // Prepare JSON to send by setting the entity
	   try {
		   URL url = new URL(CHART_URL1 + symbol + CHART_URL2 + format + CHART_URL3);

		   HttpParams httpParameters = new BasicHttpParams();
		   int timeoutConnection = 30000;
		   HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		   int timeoutSocket = 50000;
		   HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		   HttpClient httpClient = getHTTPClient(httpParameters);
		   //HttpClient httpClient = createHttpClient(); //HTTPS Connect
		   HttpPost httpPost = new HttpPost(url.toURI());

		   // Set up the header types needed to properly transfer JSON
		   //httpPost.setHeader("Content-Type", "application/json");
		   //httpPost.setHeader("Accept-Encoding", "application/json");
		   //httpPost.setHeader("Accept-Language", "en-US");

		   // Execute POST
		   HttpResponse response = httpClient.execute(httpPost);
		   HttpEntity entity = response.getEntity();
	        // If the response does not enclose an entity, there is no need
	        // to worry about connection release
		   //String[] result = null;
		   List<ChartInfo> result = new ArrayList<ChartInfo>();
	        if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	            result = convertStreamToStringForMarketChart1(instream);

	            // Closing the input stream will trigger connection release
	            instream.close();
	        }
		   return result;
	  } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	   return null;
   }
   public String YahooNewsConnect(String symbol){

	   // Prepare JSON to send by setting the entity
	   try {
		   URL url = new URL(YAHOO_NEWS_URL1 + symbol + YAHOO_NEWS_URL2);

		   HttpParams httpParameters = new BasicHttpParams();
		   int timeoutConnection = 30000;
		   HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		   int timeoutSocket = 50000;
		   HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		   HttpClient httpClient = getHTTPClient(httpParameters);
		   //HttpClient httpClient = createHttpClient(); //HTTPS Connect
		   HttpGet httpPost = new HttpGet(url.toURI());

		   // Set up the header types needed to properly transfer JSON
		   //httpPost.setHeader("Content-Type", "application/json");
		   //httpPost.setHeader("Accept-Encoding", "application/json");
		   //httpPost.setHeader("Accept-Language", "en-US");

		   // Execute POST
		   HttpResponse response = httpClient.execute(httpPost);
		   HttpEntity entity = response.getEntity();
	        // If the response does not enclose an entity, there is no need
	        // to worry about connection release
		   //String[] result = null;
		   String result = null;
	        if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	            result = convertStreamToString(instream);

	            // Closing the input stream will trigger connection release
	            instream.close();
	        }
		   return result;
	  } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	   return null;
   }
   public static String convertStreamToString(InputStream is) {
	    /*
	     * To convert the InputStream to String we use the BufferedReader.readLine()
	     * method. We iterate until the BufferedReader return null which means
	     * there's no more data to read. Each line will appended to a StringBuilder
	     * and returned as String.
	     */
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();

	    String line = null;
	    try {
	        while ((line = reader.readLine()) != null) {
	            sb.append(line + "\n");
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb.toString();
   }
   public static List<ChartInfo> convertStreamToStringForChart(InputStream is) {
	    /*
	     * To convert the InputStream to String we use the BufferedReader.readLine()
	     * method. We iterate until the BufferedReader return null which means
	     * there's no more data to read. Each line will appended to a StringBuilder
	     * and returned as String.
	     */
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    //StringBuilder sb = new StringBuilder();
	    List<ChartInfo> sb = new ArrayList<ChartInfo>();
	    String line = null;
	    boolean flg = false;
	    try {
	        while ((line = reader.readLine()) != null) {
	        	if(flg){
	        		ChartInfo cinfo = new ChartInfo();
	        		try{
	        			String[] s= line.split(",");
		        		cinfo.stamp = s[0];
		        		cinfo.open = s[4];
		        		sb.add(cinfo);
	        		}catch(Exception e){
	        			return sb;
	        		}
	        	}
	        	if(line.contains("volume:")){
	        		flg = true;
	        	}	            
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb;
  }
   public static List<ChartInfo> convertStreamToStringForChart1(InputStream is) {
	    /*
	     * To convert the InputStream to String we use the BufferedReader.readLine()
	     * method. We iterate until the BufferedReader return null which means
	     * there's no more data to read. Each line will appended to a StringBuilder
	     * and returned as String.
	     */
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    //StringBuilder sb = new StringBuilder();
	    List<ChartInfo> sb = new ArrayList<ChartInfo>();
	    String line = null;
	    String line1 = "";
	    boolean flg = false;
	    try {
	        while ((line = reader.readLine()) != null) {
	        	if(flg){
	        		ChartInfo cinfo = new ChartInfo();
	        		try{
	        			String[] s= line.split(",");
		        		cinfo.stamp = s[0];
		        		cinfo.open = s[4];
		        		sb.add(cinfo);
	        		}catch(Exception e){
	        			return sb;
	        		}
	        	}
	        	if(line.contains("volume:")){
	        		flg = true;
	        	}	         
	        	if(line.contains("previous_close")){
	        		String[] s= line.split(":");
	        		CurrentInfo1.close = s[1];
	        	}        
	        	if(line.contains("open:")){
	        		String[] s= line.split(",");
	        		CurrentInfo1.open = s[1];
	        	}     
	        	if(line.contains("high:")){
	        		String[] s= line.split(",");
	        		CurrentInfo1.high = s[1];
	        	}
	        	if(line.contains("low:")){
	        		String[] s= line.split(",");
	        		String[] s1= s[0].split(":");
	        		CurrentInfo1.low = s1[1];
	        	}
	        	line1 = line;
	        }
	        String[] s= line1.split(",");
	        if(s.length > 4){
	        	CurrentInfo1.cprice = s[4];
	        	float m = Float.parseFloat(CurrentInfo1.cprice) - Float.parseFloat(CurrentInfo1.close);
	        	String s1 = ""+m;
	        	int idx = 0;
	        	
	        	m = m / Float.parseFloat(CurrentInfo1.close) * 100;
	        	String s2 = "" + m;
	        	if((idx = s2.indexOf(".")) !=-1){
	        		String s3 = s2.substring(0, idx);
	        		String s4 = s2.substring(idx+1, s2.length());
	        		if(s4.length() > 2){
	        			s4 = s4.substring(0,2);
	        		}
	        		s2 = s3 + "." + s4 + "%";
	        	}else{
	        		s2 = s2 + "%";
	        	}
	        	if((idx = s1.indexOf(".")) !=-1){
	        		String s3 = s1.substring(0, idx);
	        		String s4 = s1.substring(idx+1, s1.length());
	        		if(s4.length() > 2){
	        			s4 = s4.substring(0,2);
	        		}
	        		s1 = s3 + "." + s4;
	        	}
	        	CurrentInfo1.dchange = s1 + "(" + s2 + ")";
	        	
	        }
	        
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb;
 }

   public static List<ChartInfo> convertStreamToStringForMarketChart(InputStream is) {
	    /*
	     * To convert the InputStream to String we use the BufferedReader.readLine()
	     * method. We iterate until the BufferedReader return null which means
	     * there's no more data to read. Each line will appended to a StringBuilder
	     * and returned as String.
	     */
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    //StringBuilder sb = new StringBuilder();
	    List<ChartInfo> sb = new ArrayList<ChartInfo>();
	    String line = null;
	    String pclose = "0";
	    boolean flg = false;
	    try {
	        while ((line = reader.readLine()) != null) {
	        	if(flg){
	        		ChartInfo cinfo = new ChartInfo();
	        		try{
	        			String[] s= line.split(",");
		        		cinfo.stamp = s[0];
		        		String open = s[4];
		        		if(!pclose.equals("0")){
		        			try{
		        				double f = Double.parseDouble(open) - Double.parseDouble(pclose);
		        				cinfo.close = "" + f;
		        				double g = (f / Double.parseDouble(pclose)) * 100;
		        				String m = "" + g;
		        				int idx = 0;
		        				if((idx = m.indexOf(".")) != -1){
		        					String m1 = m.substring(0,idx);
		        					String m2 = m.substring(idx+1, m.length());
		        					if(m2.length() > 2){
		        						m2 = m2.substring(0,2) +"." + m2.substring(2, m2.length());
		        						double ms = Double.parseDouble(m2);
		        						int mm = (int)ms;
		        						cinfo.open = m1 +"." + mm;
		        					}else{
		        						cinfo.open = m1+"."+m2;
		        					}
		        				}else{
		        					cinfo.open = m;
		        				}
				        		//cinfo.open = s[4];
				        		sb.add(cinfo);
		        			}catch(Exception e){
		        				
		        			}
		        		}
	        		}catch(Exception e){
	        			return sb;
	        		}
	        	}
	        	if(line.contains("volume:")){
	        		flg = true;
	        	}
	        	if(line.contains("previous_close")){
	        		String[] sv= line.split(":");
	        		pclose = sv[1];
	        	}
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb;
  }
   public static List<ChartInfo> convertStreamToStringForMarketChart1(InputStream is) {
	    /*
	     * To convert the InputStream to String we use the BufferedReader.readLine()
	     * method. We iterate until the BufferedReader return null which means
	     * there's no more data to read. Each line will appended to a StringBuilder
	     * and returned as String.
	     */
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    //StringBuilder sb = new StringBuilder();
	    List<ChartInfo> sb = new ArrayList<ChartInfo>();
	    String line = null;
	    String line1 = "";
	    String pclose = "0";
	    boolean flg = false;
	    try {
	        while ((line = reader.readLine()) != null) {
	        	if(flg){
	        		ChartInfo cinfo = new ChartInfo();
	        		try{
	        			String[] s= line.split(",");
		        		cinfo.stamp = s[0];
		        		String open = s[4];
		        		if(!pclose.equals("0")){
		        			try{
		        				double f = Double.parseDouble(open) - Double.parseDouble(pclose);
		        				cinfo.close = "" + f;
		        				double g = (f / Double.parseDouble(pclose)) * 100;
		        				String m = "" + g;
		        				int idx = 0;
		        				if((idx = m.indexOf(".")) != -1){
		        					String m1 = m.substring(0,idx);
		        					String m2 = m.substring(idx+1, m.length());
		        					if(m2.length() > 2){
		        						m2 = m2.substring(0,2) +"." + m2.substring(2, m2.length());
		        						double ms = Double.parseDouble(m2);
		        						int mm = (int)ms;
		        						cinfo.open = m1 +"." + mm;
		        					}else{
		        						cinfo.open = m1+"."+m2;
		        					}
		        				}else{
		        					cinfo.open = m;
		        				}
				        		//cinfo.open = s[4];
				        		sb.add(cinfo);
		        			}catch(Exception e){
		        				
		        			}
		        		}
	        		}catch(Exception e){
	        			return sb;
	        		}
	        	}
	        	if(line.contains("volume:")){
	        		flg = true;
	        	}
	        	if(line.contains("previous_close")){
	        		String[] sv= line.split(":");
	        		pclose = sv[1];
	        	}
	        	
	        	
	        	if(line.contains("previous_close")){
	        		String[] s= line.split(":");
	        		CurrentInfo1.close = s[1];
	        	}  
	        	line1 = line;
	        	
	        }
	        String[] s= line1.split(",");
	        if(s.length > 4){
	        	CurrentInfo1.cprice = s[4];
	        	float m = Float.parseFloat(CurrentInfo1.cprice) - Float.parseFloat(CurrentInfo1.close);
	        	String s1 = ""+m;
	        	int idx = 0;
	        	
	        	m = m / Float.parseFloat(CurrentInfo1.close) * 100;
	        	String s2 = "" + m;
	        	if((idx = s2.indexOf(".")) !=-1){
	        		String s3 = s2.substring(0, idx);
	        		String s4 = s2.substring(idx+1, s2.length());
	        		if(s4.length() > 2){
	        			s4 = s4.substring(0,2);
	        		}
	        		s2 = s3 + "." + s4 + "%";
	        	}else{
	        		s2 = s2 + "%";
	        	}
	        	if((idx = s1.indexOf(".")) !=-1){
	        		String s3 = s1.substring(0, idx);
	        		String s4 = s1.substring(idx+1, s1.length());
	        		if(s4.length() > 2){
	        			s4 = s4.substring(0,2);
	        		}
	        		s1 = s3 + "." + s4;
	        	}
	        	CurrentInfo1.volume = s1;
	        	CurrentInfo1.dchange = s1 + "(" + s2 + ")";
	        	
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb;
 }
   

   public String connectforchart(String filename, String params){

	   // Prepare JSON to send by setting the entity
	   try {
		   URL url = new URL(CHART_URL1 + filename);

		   HttpParams httpParameters = new BasicHttpParams();
		   int timeoutConnection = 30000;
		   HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		   int timeoutSocket = 50000;
		   HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		   HttpClient httpClient = new DefaultHttpClient(httpParameters);
		   HttpPost httpPost = new HttpPost(url.toURI());

		   // Set up the header types needed to properly transfer JSON
		   httpPost.setHeader("Content-Type", "application/json");
		   //httpPost.setHeader("Accept-Encoding", "application/json");
		   //httpPost.setHeader("Accept-Language", "en-US");

		   // Execute POST
		   HttpResponse response = httpClient.execute(httpPost);
		   HttpEntity entity = response.getEntity();
	        // If the response does not enclose an entity, there is no need
	        // to worry about connection release
		   String result = "";
	        if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	            result = convertStreamToString(instream);

	                     

	            // Closing the input stream will trigger connection release
	            instream.close();
	        }
		   return result;
	  } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	   return null;
   }
   private HttpClient getHTTPClient(HttpParams params){
	   HttpClient httpClient = null;
	   if(GLOBAL == 1){
		   httpClient = new DefaultHttpClient(params);
	   }else{
		   httpClient = createHttpClient(); //HTTPS Connect
	   }
	   return httpClient;
   }
   private HttpClient createHttpClient()
   {
	    HttpParams params = new BasicHttpParams();
	    HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
	    HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
	    HttpProtocolParams.setUseExpectContinue(params, true);

	   int timeoutConnection = 30000;
	   HttpConnectionParams.setConnectionTimeout(params, timeoutConnection);
	   int timeoutSocket = 50000;
	   HttpConnectionParams.setSoTimeout(params, timeoutSocket);
	    SchemeRegistry schReg = new SchemeRegistry();
	    schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	    schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
	    ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);

	    return new DefaultHttpClient(conMgr, params);
	}

   public CurrentInfo downloadQuoteViaHTTP (Context con, String symbol) {
	   Log.d("download", "downloadFileViaHTTP...");
	   String baseDir = con.getApplicationContext().getFilesDir().getAbsolutePath();

	   try {
	      //this is the Yahoo url
		  String urlFile = "http://192.168.0.107/NewStockTwits/json/quotes.csv";
	      //String urlFile = "http://download.finance.yahoo.com/d/quotes.csv?s=" + symbol + "&f=l1&e=.csv";
	      URL url = new URL(urlFile);
	      //create the new connection 
	      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
	      urlConnection.setRequestMethod("GET");
	      urlConnection.connect();

	      //pointer to the downloaded file path
	      String localFileName = baseDir + "/" + "quotes.csv";
	      //this is the actual downloaded file
	      File MyFilePtrDest = new File(localFileName);

	      //this will be used in reading the data from the Internet
	      InputStream inputStream = urlConnection.getInputStream();

	      //this will be used to write the downloaded data into the file we created
	      FileOutputStream fileOutput = new FileOutputStream(MyFilePtrDest);

	      byte[] buffer = new byte[1024];
	      int bufferLength = 0; //used to store a temporary size of the buffer

	      //write buffer contents to file
	      while ((bufferLength = inputStream.read(buffer)) > 0 ) {
	         //add the data in the buffer to the file in the file output stream (the file on the sd card
	         fileOutput.write(buffer, 0, bufferLength);
	      }

	      inputStream.close();
	      //close the output stream when done
	      fileOutput.flush();
	      fileOutput.close();
	      urlConnection.disconnect();
	   }
	   catch (IOException e) {
	      //data were not found
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	   }
	   try {
         // open the file for reading
         InputStream instream = new FileInputStream(baseDir + "/" + "quotes.csv");
         // if file the available for reading
         if (instream != null) {
            // prepare the file for reading
            InputStreamReader inputreader = new InputStreamReader(instream);
            BufferedReader buffreader = new BufferedReader(inputreader);
            //read the line
            
            int i = 0;
            String line = null;
            //while((line = buffreader.readLine()) != null){
            	
            	//i++;
            //}
            CurrentInfo cinfo = new CurrentInfo();
            if((line = buffreader.readLine()) != null){	            	
            	String[] s = line.split(",");
            	cinfo.cprice = s[1];
            	cinfo.date = s[2];
            	cinfo.time = s[3];
            	cinfo.dchange = s[4];
            	cinfo.open = s[5];
            	cinfo.high = s[6];
            	cinfo.low = s[7];
            	cinfo.volume = s[8];
            }
            //Log.d("download", "fileLine = " + fileLine);
            instream.close();
            return cinfo;
         }
      }
      catch (Exception ex) {
    	  
      }
	   return null;
	} 
    public static String currentdata = "{'query':{'count':1,'created':'2014-12-18T06:41:31Z','lang':'ko-KR','results':{'quote':{'symbol':'DOW','Ask':null,'AverageDailyVolume':'12448500','Bid':null,'AskRealtime':'49.97','BidRealtime':'43.25','BookValue':'18.618','Change_PercentChange':'+1.84 - +4.31%','Change':'+1.84','Commission':null,'Currency':'USD','ChangeRealtime':'+1.84','AfterHoursChangeRealtime':'N/A - N/A','DividendShare':'1.43','LastTradeDate':'12/17/2014','TradeDate':null,'EarningsShare':'3.045','ErrorIndicationreturnedforsymbolchangedinvalid':null,'EPSEstimateCurrentYear':'2.95','EPSEstimateNextYear':'3.28','EPSEstimateNextQuarter':'0.86','DaysLow':'42.85','DaysHigh':'44.93','YearLow':'41.45','YearHigh':'54.97','HoldingsGainPercent':'- - -','AnnualizedGain':null,'HoldingsGain':null,'HoldingsGainPercentRealtime':'N/A - N/A','HoldingsGainRealtime':null,'MoreInfo':'cn','OrderBookRealtime':null,'MarketCapitalization':'51.999B','MarketCapRealtime':null,'EBITDA':'8.005B','ChangeFromYearLow':'+3.10','PercentChangeFromYearLow':'+7.48%','LastTradeRealtimeWithTime':'N/A - <b>44.55</b>','ChangePercentRealtime':'N/A - +4.31%','ChangeFromYearHigh':'-10.42','PercebtChangeFromYearHigh':'-18.96%','LastTradeWithTime':'Dec 17 - <b>44.55</b>','LastTradePriceOnly':'44.55','HighLimit':null,'LowLimit':null,'DaysRange':'42.85 - 44.93','DaysRangeRealtime':'N/A - N/A','FiftydayMovingAverage':'48.936','TwoHundreddayMovingAverage':'50.9848','ChangeFromTwoHundreddayMovingAverage':'-6.4348','PercentChangeFromTwoHundreddayMovingAverage':'-12.62%','ChangeFromFiftydayMovingAverage':'-4.386','PercentChangeFromFiftydayMovingAverage':'-8.96%','Name':'Dow Chemical Comp','Notes':null,'Open':'43.00','PreviousClose':'42.71','PricePaid':null,'ChangeinPercent':'+4.31%','PriceSales':'0.86','PriceBook':'2.29','ExDividendDate':'Sep 26','PERatio':'14.03','DividendPayDate':'Jan 30','PERatioRealtime':null,'PEGRatio':'1.17','PriceEPSEstimateCurrentYear':'14.48','PriceEPSEstimateNextYear':'13.02','Symbol':'DOW','SharesOwned':null,'ShortRatio':'1.90','LastTradeTime':'4:00pm','TickerTrend':'&nbsp;==+-=+&nbsp;','OneyrTargetPrice':'54.30','Volume':'17215958','HoldingsValue':null,'HoldingsValueRealtime':null,'YearRange':'41.45 - 54.97','DaysValueChange':'- - +4.31%','DaysValueChangeRealtime':'N/A - N/A','StockExchange':'NYSE','DividendYield':'3.35','PercentChange':'+4.31%'}}}}";
}
