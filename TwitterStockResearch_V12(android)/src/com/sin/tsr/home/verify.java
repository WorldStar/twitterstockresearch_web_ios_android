package com.sin.tsr.home;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.sin.tsr.ConnectionDetector;
import com.sin.tsr.R;
import com.sin.tsr.TSRMainActivity;
import com.sin.tsr.TabGroupActivity;
import com.sin.tsr.shared.SPUserInfo;
import com.sin.tsr.textview.MyButton;
import com.sin.tsr.webservice.CheckInternetAccess;
import com.sin.tsr.webservice.WebService;

public class verify extends Activity{
	Typeface font1,font2;
	boolean connection;
	EditText editverify;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.verify);
		Intent intent = getIntent();
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-MdCn.ttf");
		font2= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-Cn.ttf");
		LinearLayout backbt = (LinearLayout)findViewById(R.id.back_bt);
		backbt.setVisibility(View.VISIBLE);
		backbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(verify.this, TSRMainActivity.class);
	            verify.this.startActivity(intent);
				finish();
			}
		});
		LinearLayout topserarchbt = (LinearLayout)findViewById(R.id.topsearch_bt);
		topserarchbt.setVisibility(View.GONE);
			Alert(intent.getStringExtra("msg"));
		

		editverify = (EditText)findViewById(R.id.txt_verify);
		editverify.setTypeface(font2);
		MyButton verifybt = (MyButton)findViewById(R.id.verify_bt);
		verifybt.setTypeface(font2);
		verifybt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(editverify.getText().toString().equals("")){
					Alert("Please input verify code.");
					return;
				}
				Verify_Click();
			}
		});
		
		
	}

	@Override
	public void onResume(){
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		super.onResume();
	}
	public void Verify_Click(){
		if(editverify.getText().toString().equals("")){
			Alert("Please insert verify code.");
			return;
		}
		CheckInternetAccess check = new CheckInternetAccess(verify.this);
        if (check.checkNetwork()) {
        	new VerifyTask().execute();
        }else{
        	Alert("Please check your network.");
        }
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(verify.this).setTitle("TSR")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}
	private class VerifyTask extends AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public VerifyTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result != null){
				if(!_result.equals("")){
					try {
						JSONObject data = new JSONObject(_result);
			            SPUserInfo info = new SPUserInfo(verify.this);
						if(data.getString("registered").equals("success")){
							Alert("Successfully register. You can search stock information.");
							SPUserInfo.logged = 1;

				            info.setUserInfo(SPUserInfo.USERNAME, data.getString("username"));
				            info.setUserInfo(SPUserInfo.PASSWORD, data.getString("userpass"));
				            info.setUserInfo(SPUserInfo.EMAIL, data.getString("usermail"));
				            Intent intent = new Intent(verify.this, TSRMainActivity.class);
				            verify.this.startActivity(intent);
				            finish();
							//Intent intent = new Intent(getParent(), Home.class);
			        		//TabGroupActivity parentActivity = (TabGroupActivity) getParent();
			        		//parentActivity.startChildActivity("HomeMenu",intent);
			        		//parentActivity.onBackPressed();
						}else if(data.getString("registered").equals("failed")){
			            	Alert("Failed response from server. Please try again.");
			            }else{
			            	Alert(data.getString("registered"));
			            }

				       
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Alert("Failed parse for JSON data.");
					}
				}else{
					Alert("Failed response from server. Please try again.");
				}
			}else{
				Alert("Failed service for register. Please try again.");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			 MyDialog = ProgressDialog.show(verify.this, "",
	                    "Loading... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
	        	WebService service= new WebService();
				result = service.tsrConnect("user_request.php", AddJSONData());
			return result;
		}
	}
	public JSONObject AddJSONData(){
		   JSONObject jdata = new JSONObject();
		   try {
			   SPUserInfo info = new SPUserInfo(verify.this);
			   jdata.put("username", info.getUserInfo(SPUserInfo.USERNAME));
			   jdata.put("usermail", info.getUserInfo(SPUserInfo.EMAIL));
			   jdata.put("userpass", info.getUserInfo(SPUserInfo.PASSWORD));
			   jdata.put("verify", editverify.getText().toString());
		   } catch (JSONException e) {
			// TODO Auto-generated catch block
			   e.printStackTrace();
		   }
		   return jdata;
	 }

}
