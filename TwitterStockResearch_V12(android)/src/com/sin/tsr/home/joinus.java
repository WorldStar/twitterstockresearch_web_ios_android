package com.sin.tsr.home;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.sin.tsr.ConnectionDetector;
import com.sin.tsr.R;
import com.sin.tsr.TSRMainActivity;
import com.sin.tsr.TabGroupActivity;
import com.sin.tsr.shared.SPUserInfo;
import com.sin.tsr.textview.MyButton;
import com.sin.tsr.webservice.CheckInternetAccess;
import com.sin.tsr.webservice.WebService;

public class joinus extends Activity{
	Typeface font1,font2;
	boolean connection;
	String name = "";
	String pass = "";
	String email = "";
	String confirmpass = "";
	EditText edituser;
	EditText editemail;
	EditText editpass;
	EditText editconfirmpass;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.joinus);

		font1= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-MdCn.ttf");
		font2= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-Cn.ttf");
		LinearLayout backbt = (LinearLayout)findViewById(R.id.back_bt);
		backbt.setVisibility(View.VISIBLE);
		backbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Intent intent = new Intent(joinus.this, TSRMainActivity.class);
				 joinus.this.startActivity(intent);
				finish();
			}
		});
		LinearLayout topserarchbt = (LinearLayout)findViewById(R.id.topsearch_bt);
		topserarchbt.setVisibility(View.GONE);
		
		edituser = (EditText)findViewById(R.id.txt_user);
		edituser.setTypeface(font2);
		editemail = (EditText)findViewById(R.id.txt_mail);
		editemail.setTypeface(font2);
		editpass = (EditText)findViewById(R.id.txt_pass);
		editpass.setTypeface(font2);
		editconfirmpass = (EditText)findViewById(R.id.txt_confirm);
		editconfirmpass.setTypeface(font2);
		MyButton joinbt = (MyButton)findViewById(R.id.join_bt);
		joinbt.setTypeface(font2);
		joinbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Join_Click();
			}
		});
		//tvPageTitle.setTypeface(font1);
		 
		
	}

	@Override
	public void onResume(){
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		super.onResume();
	}
	public void Join_Click(){
		if(edituser.getText().toString().equals("")){
			Alert("Please insert user name.");
			return;
		}
		if(editemail.getText().toString().equals("")){
			Alert("Please insert user email.");
			return;
		}
		if(editpass.getText().toString().equals("")){
			Alert("Please insert user password.");
			return;
		}
		if(editconfirmpass.getText().toString().equals("")){
			Alert("Please insert user password.");
			return;
		}
		if(!editconfirmpass.getText().toString().equals(editpass.getText().toString())){
			Alert("Please match user password.");
			return;
		}
		CheckInternetAccess check = new CheckInternetAccess(joinus.this);
        if (check.checkNetwork()) {
    		new GetUserTask().execute();
        }else{
        	Alert("Please check your network.");
        }
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(joinus.this).setTitle("TSR")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}
	private class GetUserTask extends AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetUserTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result != null){
				if(!_result.equals("")){
					try {
						// A Simple JSONObject Creation
			            JSONObject json=new JSONObject(_result);	   
						// A Simple JSONObject Parsing
			            //JSONArray nameArray=json.names();
			            //JSONArray valArray=json.toJSONArray(nameArray);
			            SPUserInfo info = new SPUserInfo(joinus.this);
			            if(json.getString("registered").equals("success")){
				            info.setUserInfo(SPUserInfo.USERNAME, json.getString("username"));
				            info.setUserInfo(SPUserInfo.PASSWORD, json.getString("userpass"));
				            info.setUserInfo(SPUserInfo.EMAIL, json.getString("usermail"));
				            info.commit();
				            

							
							Intent intent = new Intent(joinus.this, verify.class);
							intent.putExtra("msg", "Successfully register. You can get verify code from your mail. Please verify.");
							joinus.this.startActivity(intent);
							finish();
			        		//TabGroupActivity parentActivity = (TabGroupActivity) getParent();
			        		//parentActivity.onBackPressed();
			        		//parentActivity.startChildActivity("Verify",intent);
			            }else if(json.getString("registered").equals("failed")){
			            	Alert("Failed response from server. Please try again.");
			            }else{
			            	Alert(json.getString("registered"));
			            }
				       
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Alert("Failed parse for JSON data.");
					}
				}else{
					Alert("Failed response from server. Please try again.");
				}
			}else{
				Alert("Failed service for register. Please try again.");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			 MyDialog = ProgressDialog.show(joinus.this, "",
	                    "Registering User... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(joinus.this);
	        if (check.checkNetwork()) {
	        	WebService service= new WebService();
				result = service.tsrConnect("user_request.php", AddJSONData());
	        }
			return result;
		}
	}
	public JSONObject AddJSONData(){
	   JSONObject jdata = new JSONObject();
	   try {
		   jdata.put("username", edituser.getText().toString());
		   jdata.put("usermail", editemail.getText().toString());
		   jdata.put("userpass", editpass.getText().toString());
	   } catch (JSONException e) {
		// TODO Auto-generated catch block
		   e.printStackTrace();
	   }
	   return jdata;
   }

}
