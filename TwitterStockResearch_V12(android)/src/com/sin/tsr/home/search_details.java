package com.sin.tsr.home;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONException;
import org.json.JSONObject;
import org.stockchart.StockChartActivity;
import org.stockchart.StockChartView;
import org.stockchart.core.Area;
import org.stockchart.core.Appearance.Gradient;
import org.stockchart.series.LinearSeries;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

import com.google.gson.Gson;
import com.sin.tsr.ConnectionDetector;
import com.sin.tsr.R;
import com.sin.tsr.TSRMainActivity;
import com.sin.tsr.TabGroupActivity;
import com.sin.tsr.autocomplete.AutoComplete;
import com.sin.tsr.autocomplete.AutoCompleteForDetails;
import com.sin.tsr.shared.ChartInfo;
import com.sin.tsr.shared.CurrentInfo;
import com.sin.tsr.shared.CurrentInfo1;
import com.sin.tsr.shared.SPUserInfo;
import com.sin.tsr.shared.StaticInfo;
import com.sin.tsr.shared.YahooNewsInfo;
import com.sin.tsr.textview.MyButton;
import com.sin.tsr.textview.MyTextView;
import com.sin.tsr.textview.MyTextViewBold;
import com.sin.tsr.tweet.Authenticated;
import com.sin.tsr.tweet.Search;
import com.sin.tsr.tweet.SearchResults;
import com.sin.tsr.tweet.Searches;
import com.sin.tsr.tweet.Tweet;
import com.sin.tsr.tweet.Twitter;
import com.sin.tsr.utils.ExpandableHeightGridView;
import com.sin.tsr.webservice.CheckInternetAccess;
import com.sin.tsr.webservice.WebService;
import com.squareup.picasso.Picasso;

public class search_details extends Activity{
	Typeface font1,font2;
	boolean connection;
	EditText editverify;
	String symbol = "";
	String datetime = "1d";
	AutoComplete txtsearh;
	AutoCompleteForDetails searchtext;
	MyTextView txt_current;
	MyTextView view_daily;
	MyTextView view_open;
	MyTextView view_close;
	MyTextView view_high;
	MyTextView view_low;
	LinearLayout real_graph;
	MyButton bt1, bt2, bt3, bt4, bt5, bt6;
	ExpandableHeightGridView newslist;
	int length = 0;
	int currentfindflg = 0;
	String flg = "0";
	List<ChartInfo> clist = new ArrayList<ChartInfo>();
	LinearLayout show_layout;
	LinearLayout price_layout;
	final static String LOG_TAG = "TSR";
	final static String ScreenName = "TSRScreen";
	String Key = "howb20GutmTtvUnM1qcKyHATE";
	String Secret = "IcXOqOSBFkBwPyKxJNkOGayfSwtPdxi3pqNgPiw8q8Oo6Tw8wI";
	MyTextViewBold view_title;
	boolean scrollflg = false;
	ScrollView scrollview;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_search);
		Intent intent = getIntent();
		symbol = intent.getStringExtra("symbol");
		flg = intent.getStringExtra("flg");
		final String title = intent.getStringExtra("title");
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-MdCn.ttf");
		font2= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-Cn.ttf");	
		LinearLayout backbt = (LinearLayout)findViewById(R.id.back_bt);
		backbt.setVisibility(View.VISIBLE);
		backbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((TabGroupActivity)getParent()).onBackPressed();
			}
		});
		LinearLayout topserarchbt = (LinearLayout)findViewById(R.id.topsearch_bt);
		topserarchbt.setVisibility(View.VISIBLE);
		final LinearLayout top_search = (LinearLayout)findViewById(R.id.top_search);
		top_search.setVisibility(View.GONE);
		topserarchbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				top_search.setVisibility(View.VISIBLE);
			}
		});
		scrollview = (ScrollView)findViewById(R.id.ScrollView1);
		view_title = (MyTextViewBold)findViewById(R.id.view_stock);
		searchtext = (AutoCompleteForDetails)findViewById(R.id.txt_search);
		searchtext.setOnKeyListener(new View.OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
			            (keyCode == KeyEvent.KEYCODE_ENTER)) {
					datetime = "1d";
					Selected_Bt(1);
					symbol = searchtext.getText().toString();
					search_details.this.symbol = symbol;
					TSRMainActivity.symbol = symbol;
					view_title.setText("");
					//searchtext.setText(s);
					new GetCurrentDataTask().execute();	
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
					
					show_layout.setVisibility(View.GONE);
					price_layout.setVisibility(View.VISIBLE);
					return true;
				}
				return false;
			}
		});
		searchtext.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				if(position < searchtext.getList().size()){
					datetime = "1d";
					Selected_Bt(1);
					String s = searchtext.getList().get(position);
					String[] s1 = s.split(",");
					String symbol = s1[0];
					view_title.setText(s);
					search_details.this.symbol = symbol;
					TSRMainActivity.symbol = symbol;
					searchtext.setText(s);
					new GetCurrentDataTask().execute();	
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
					
					show_layout.setVisibility(View.GONE);
					price_layout.setVisibility(View.VISIBLE);
				}
			}
			
		});
		view_title.setText(title);
		price_layout = (LinearLayout)findViewById(R.id.price_view_layout);
		price_layout.setVisibility(View.VISIBLE);
		show_layout = (LinearLayout)findViewById(R.id.show_layout);
		show_layout.setVisibility(View.GONE);
		real_graph = (LinearLayout)findViewById(R.id.real_graph);
		txt_current = (MyTextView)findViewById(R.id.txt_current);
		view_daily = (MyTextView)findViewById(R.id.view_daily);
		view_open = (MyTextView)findViewById(R.id.view_open);
		view_close = (MyTextView)findViewById(R.id.view_close);
		view_high = (MyTextView)findViewById(R.id.view_high);
		view_low = (MyTextView)findViewById(R.id.view_low);
		bt1 = (MyButton)findViewById(R.id.bt_1d);
		bt1.setSelected(true);
		bt1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("1d")){
					datetime = "1d";
					Selected_Bt(1);
					currentfindflg = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		bt2 = (MyButton)findViewById(R.id.bt_5d);
		bt2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("5d")){
					datetime = "5d";
					Selected_Bt(2);
					currentfindflg = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		bt3 = (MyButton)findViewById(R.id.bt_1m);
		bt3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("1m")){
					datetime = "1m";
					Selected_Bt(3);
					currentfindflg = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		bt4 = (MyButton)findViewById(R.id.bt_6m);
		bt4.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("6m")){
					datetime = "6m";
					Selected_Bt(4);
					currentfindflg = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		bt5 = (MyButton)findViewById(R.id.bt_1y);
		bt5.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("1y")){
					datetime = "1y";
					Selected_Bt(5);
					currentfindflg = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		bt6 = (MyButton)findViewById(R.id.bt_5y);
		bt6.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("5y")){
					datetime = "5y";
					Selected_Bt(6);
					currentfindflg = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		newslist = (ExpandableHeightGridView)findViewById(R.id.list_tweet);
		new GetCurrentDataTask().execute();
	}
	@Override
	public void onResume(){
		//InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		//imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		super.onResume();
	}
	public void Selected_Bt(int id){
		bt1.setSelected(false);
		bt2.setSelected(false);
		bt3.setSelected(false);
		bt4.setSelected(false);
		bt5.setSelected(false);
		bt6.setSelected(false);
		switch(id){
		case 1:
			bt1.setSelected(true);
			break;
		case 2:
			bt2.setSelected(true);
			break;
		case 3:
			bt3.setSelected(true);
			break;
		case 4:
			bt4.setSelected(true);
			break;
		case 5:
			bt5.setSelected(true);
			break;
		case 6:
			bt6.setSelected(true);
			break;
		}
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(getParent()).setTitle("TSR")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}
	private Context getDialogContext() {
	    Context context;
	    if (getParent() != null) context = getParent();
	    else context = this;
	    return context;
	}
	private class GetCurrentDataTask extends AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetCurrentDataTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result != null){
				if(!_result.equals("")){
					try{
						if(WebService.GLOBAL == 1){
							_result = _result.substring(1, _result.length()-2);
							_result = _result.replace("\\", "");
						}
						JSONObject json=new JSONObject(_result);
						String data = new JSONObject(new JSONObject(json.getString("query")).getString("results")).getString("quote");
						JSONObject json1=new JSONObject(data);
						txt_current.setText(json1.getString("LastTradePriceOnly"));
						String s = json1.getString("ChangeRealtime");
						if(s.contains("+")){
							view_daily.setTextColor(getParent().getResources().getColor(R.color.green));
						}else{
							view_daily.setTextColor(getParent().getResources().getColor(R.color.daily_color));							
						}
						view_daily.setText(json1.getString("ChangeRealtime") + " (" +json1.getString("ChangeinPercent") + ")");
						view_open.setText(json1.getString("Open"));
						view_close.setText(json1.getString("PreviousClose"));
						view_high.setText(json1.getString("DaysHigh"));
						view_low.setText(json1.getString("DaysLow"));
						price_layout.setVisibility(View.VISIBLE);
						if(view_title.getText().toString().equals("")){
							String s1 = json1.getString("Name");
							view_title.setText(s1);
						}
						currentfindflg = 0;
					}catch(Exception e){
						currentfindflg = 1;
						//Alert("Can not get current's stock data from yahoo. please try again.");
					}
				}else{
					currentfindflg = 1;
					//Alert("Can not get stock's real data from yahoo. please try again.");
				}
			}else{
				currentfindflg = 1;
				//Alert("Failed service. Please try again.");
			}
			new GetChartDataTask().execute();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			StaticInfo.autoflg = false;
			StaticInfo.autoflg1 = false;
				 MyDialog = ProgressDialog.show(getDialogContext(), "",
		                    "Real Data Loading... ", false);
		            MyDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(search_details.this);
	        if (check.checkNetwork()) {
	        	WebService service= new WebService();
				result = service.currentConnect (URLEncoder.encode(symbol));
	        }
			return result;
		}
	}
	private class GetChartDataTask extends AsyncTask<Void, Void, List<ChartInfo>> {
		ProgressDialog MyDialog;
       public GetChartDataTask() {
       }
		@Override
		protected void onPostExecute(List<ChartInfo> _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result != null){
				if(_result.size() > 0){
					clist.clear();
					clist.addAll(_result);
					ChartDraw();
					if(currentfindflg == 1){
						txt_current.setText(CurrentInfo1.cprice);
						String s = CurrentInfo1.dchange;
						if(s.contains("-")){
							view_daily.setTextColor(getParent().getResources().getColor(R.color.daily_color));
						}else{
							view_daily.setTextColor(getParent().getResources().getColor(R.color.green));							
						}
						view_daily.setText(CurrentInfo1.dchange);
						view_open.setText(CurrentInfo1.open);
						view_close.setText(CurrentInfo1.close);
						view_high.setText(CurrentInfo1.high);
						view_low.setText(CurrentInfo1.low);
						price_layout.setVisibility(View.VISIBLE);
						if(view_title.getText().toString().equals("")){
							
							view_title.setText(symbol);
						}
					}
				}else{
					Alert("Can not get stock's chart data from yahoo. please try again.");
					if(currentfindflg == 1){
						show_layout.setVisibility(View.VISIBLE);
						price_layout.setVisibility(View.GONE);
					}
				}
			}else{
				if(currentfindflg == 1){
					show_layout.setVisibility(View.VISIBLE);
					price_layout.setVisibility(View.GONE);
				}
				Alert("Failed stock's chart service. Please try again.");
			}
			//new GetChartDataTask().execute();
			if(datetime.equals("1d") && WebService.GLOBAL == 2)
				downloadSearches();
			//downloadTweets();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
				 MyDialog = ProgressDialog.show(getDialogContext(), "",
		                    "Chart Data Loading... ", false);
		            MyDialog.setCancelable(false);
		}

		@Override
		protected List<ChartInfo> doInBackground(Void... params) {	
			
			List<ChartInfo> result = new ArrayList<ChartInfo>();
			CheckInternetAccess check = new CheckInternetAccess(search_details.this);
	        if (check.checkNetwork()) {
	        	WebService service= new WebService();
	        	if(currentfindflg == 1){
	        		result = service.chartConnect1(URLEncoder.encode(symbol), datetime);
	        	}else{
	        		result = service.chartConnect(URLEncoder.encode(symbol), datetime);
	        	}
	        }
			return result;
		}
	}
	public void ChartDraw(){
		StockChartView chartview = (StockChartView)findViewById(R.id.chartview);//new StockChartView(search_details.this);
		chartview.postInvalidate();
		chartview.invalidate();
		chartview.init();
		chartview.reset();
		LinearSeries chartSeries = new LinearSeries();
		chartSeries.getAppearance().setOutlineColor(0x800000ff);
		chartSeries.getAppearance().setOutlineWidth(1.0f);
		chartSeries.getAppearance().setPrimaryFillColor(0x80999999);
		chartSeries.getAppearance().setSecondaryFillColor(0x22999999);
		chartSeries.getAppearance().setGradient(Gradient.LINEAR_VERTICAL);
		chartSeries.setName("chart");
		Area a = chartview.addArea();
		a.getSeries().add(chartSeries);
		List<String> xvalue = new ArrayList<String>();
		for(int i = 0; i < clist.size(); i++){
			ChartInfo cinfo = clist.get(i);
			xvalue.add(cinfo.stamp);
			chartSeries.addPoint(Double.parseDouble(cinfo.open));
		}
		chartSeries.wherechart = 1;
		if(datetime.equals("1d")){
			chartSeries.daytime = 1;
		}else if(datetime.equals("5d")){
			chartSeries.daytime = 2;
		}else if(datetime.equals("1m")){
			chartSeries.daytime = 3;
		}else if(datetime.equals("6m")){
			chartSeries.daytime = 4;
		}else if(datetime.equals("1y")){
			chartSeries.daytime = 5;
		}else if(datetime.equals("5y")){
			chartSeries.daytime = 6;
		}
		
		chartSeries.clist = xvalue;
		//real_graph.removeAllViews();
		//real_graph.addView(chartview, new LayoutParams(LayoutParams.FILL_PARENT, 250));
		chartview.refreshDrawableState();
		
	}

	// download twitter searches after first checking to see if there is a network connection
		public void downloadSearches() {
			ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

			if (networkInfo != null && networkInfo.isConnected()) {
				new DownloadTwitterTask().execute(symbol);
			} else {
				Log.v(LOG_TAG, "No network connection available.");
			}
		}

		// Uses an AsyncTask to download data from Twitter
		private class DownloadTwitterTask extends AsyncTask<String, Void, String> {
			final static String TwitterTokenURL = "https://api.twitter.com/oauth2/token";
			final static String TwitterSearchURL = "https://api.twitter.com/1.1/search/tweets.json?q=";
			ProgressDialog MyDialog;
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				
					// MyDialog = ProgressDialog.show(getDialogContext(), "",
			        //            "Loading... ", false);
			        //    MyDialog.setCancelable(false);
			}
			@Override
			protected String doInBackground(String... searchTerms) {
				String result = null;

				if (searchTerms.length > 0) {
					result = getSearchStream(searchTerms[0]);
				}
				return result;
			}

			// onPostExecute convert the JSON results into a Twitter object (which is an Array list of tweets
			@Override
			protected void onPostExecute(String result) {
				//MyDialog.dismiss();
				if(result != null){
					if(result.length() > 0){
						Searches searches = jsonToSearches(result);

						// lets write the results to the console as well
						for (Search search : searches) {
							Log.i(LOG_TAG, search.getText());
						}

						// send the tweets to the adapter for rendering
						TweetNewsListAdapter adapter = new TweetNewsListAdapter(getParent(), R.layout.tweet_list, searches);
						newslist.setAdapter(adapter);
						newslist.setExpanded(true);
						newslist.setVerticalScrollBarEnabled(true);
						newslist.setSmoothScrollbarEnabled(true);
						
						adapter.notifyDataSetChanged();
						scrollflg = false;
						newslist.setOnScrollListener(new OnScrollListener(){

							@Override
							public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
								// TODO Auto-generated method stub
								if(!scrollflg){
									scrollview.scrollTo(0, 0);
									new Thread()
									  {
										  public void run()
										  {
											 try {
												Thread.sleep(1000);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											 getParent().runOnUiThread(new Runnable() {
												
												@Override
												public void run() {
													// TODO Auto-generated method stub
													scrollview.scrollTo(0, 0);
												}
											});
										  }
									  }.start();
									scrollflg = true;
								}
							}

							@Override
							public void onScrollStateChanged(AbsListView view, int scrollState) {
								// TODO Auto-generated method stub
								
							}
							
						});
					}else{
						Alert("Can not get stock's tweet news from twitter. Please try again.");
					}
				}else{
					Alert("Failed stock's tweet service. Please try again.");
				}
			}

			// converts a string of JSON data into a SearchResults object
			private Searches jsonToSearches(String result) {
				Searches searches = null;
				if (result != null && result.length() > 0) {
					try {
						Gson gson = new Gson();
						// bring back the entire search object
						SearchResults sr = gson.fromJson(result, SearchResults.class);
						// but only pass the list of tweets found (called statuses)
						searches = sr.getStatuses();
					} catch (IllegalStateException ex) {
						// just eat the exception for now, but you'll need to add some handling here
					}
				}
				return searches;
			}

			// convert a JSON authentication object into an Authenticated object
			private Authenticated jsonToAuthenticated(String rawAuthorization) {
				Authenticated auth = null;
				if (rawAuthorization != null && rawAuthorization.length() > 0) {
					try {
						Gson gson = new Gson();
						auth = gson.fromJson(rawAuthorization, Authenticated.class);
					} catch (IllegalStateException ex) {
						// just eat the exception for now, but you'll need to add some handling here
					}
				}
				return auth;
			}

			private String getResponseBody(HttpRequestBase request) {
				StringBuilder sb = new StringBuilder();
				try {

					DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
					HttpResponse response = httpClient.execute(request);
					int statusCode = response.getStatusLine().getStatusCode();
					String reason = response.getStatusLine().getReasonPhrase();

					if (statusCode == 200) {

						HttpEntity entity = response.getEntity();
						InputStream inputStream = entity.getContent();

						BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
						String line = null;
						while ((line = bReader.readLine()) != null) {
							sb.append(line);
						}
					} else {
						sb.append(reason);
					}
				} catch (UnsupportedEncodingException ex) {
				} catch (ClientProtocolException ex1) {
				} catch (IOException ex2) {
				}
				return sb.toString();
			}

			private String getStream(String url) {
				String results = null;

				// Step 1: Encode consumer key and secret
				try {
					// URL encode the consumer key and secret
					String urlApiKey = URLEncoder.encode(Key, "UTF-8");
					String urlApiSecret = URLEncoder.encode(Secret, "UTF-8");

					// Concatenate the encoded consumer key, a colon character, and the encoded consumer secret
					String combined = urlApiKey + ":" + urlApiSecret;

					// Base64 encode the string
					String base64Encoded = Base64.encodeToString(combined.getBytes(), Base64.NO_WRAP);

					// Step 2: Obtain a bearer token
					HttpPost httpPost = new HttpPost(TwitterTokenURL);
					httpPost.setHeader("Authorization", "Basic " + base64Encoded);
					httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
					httpPost.setEntity(new StringEntity("grant_type=client_credentials"));
					String rawAuthorization = getResponseBody(httpPost);
					Authenticated auth = jsonToAuthenticated(rawAuthorization);

					// Applications should verify that the value associated with the
					// token_type key of the returned object is bearer
					if (auth != null && auth.token_type.equals("bearer")) {

						// Step 3: Authenticate API requests with bearer token
						HttpGet httpGet = new HttpGet(url);

						// construct a normal HTTPS request and include an Authorization
						// header with the value of Bearer <>
						httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
						httpGet.setHeader("Content-Type", "application/json");
						// update the results with the body of the response
						results = getResponseBody(httpGet);
					}
				} catch (UnsupportedEncodingException ex) {
				} catch (IllegalStateException ex1) {
				} catch (Exception ex1) {
				}
				return results;
			}

			private String getSearchStream(String searchTerm) {
				String results = null;
				try {
					String encodedUrl = URLEncoder.encode(searchTerm, "UTF-8");
					results = getStream(TwitterSearchURL + encodedUrl);
				} catch (UnsupportedEncodingException ex) {
				} catch (IllegalStateException ex1) {
				} catch (Exception ex1) {
				}
				return results;
			}
		}
		public class TweetNewsListAdapter extends ArrayAdapter<Search> {

			private List<Search> items;
			Context con;

			public TweetNewsListAdapter(Context context, int textViewResourceId,
					List<Search> objects) {

				super(context, textViewResourceId, objects);
				this.items = objects;
				this.con = context;
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent)
					throws IndexOutOfBoundsException, IllegalStateException {
				// TODO Auto-generated method stub
				View v = convertView;
				LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
				final Search item = items.get(position);
				if (v == null) {
					v = vi.inflate(R.layout.tweet_list, null);
				}
				MyTextView title = (MyTextView) v
						.findViewById(R.id.view_list_title);
				MyTextView desc = (MyTextView) v
						.findViewById(R.id.view_list_detail);
				ImageView img = (ImageView) v
						.findViewById(R.id.view_list_img);
				title.setText(item.getUser().getName() + "@" + item.getUser().getScreenName());
				desc.setText(item.getText());
				
				Picasso.with(con).load(item.getUser().getProfileImageUrl()).resize(70, 70)
				.centerCrop().placeholder(R.drawable.user_icon)
				.error(R.drawable.user_icon).into(img);
				return v;
			}
		}
}
