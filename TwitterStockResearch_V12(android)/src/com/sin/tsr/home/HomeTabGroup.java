package com.sin.tsr.home;

import com.sin.tsr.TabGroupActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;


public class HomeTabGroup extends TabGroupActivity{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		startChildActivity("HomeMenu", new Intent(this,Home.class));
	}
}