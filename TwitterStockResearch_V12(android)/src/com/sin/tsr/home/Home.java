package com.sin.tsr.home;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.sin.tsr.ConnectionDetector;
import com.sin.tsr.R;
import com.sin.tsr.TSRMainActivity;
import com.sin.tsr.TabGroupActivity;
import com.sin.tsr.autocomplete.AutoComplete;
import com.sin.tsr.autocomplete.GroupAdapter;
import com.sin.tsr.shared.SPUserInfo;
import com.sin.tsr.shared.StaticInfo;
import com.sin.tsr.textview.MyButton;
import com.sin.tsr.textview.MyTextViewBold;
import com.sin.tsr.webservice.CheckInternetAccess;
import com.sin.tsr.webservice.WebService;

public class Home extends Activity{
	Typeface font1,font2;
	boolean connection;
	String name = "";
	String pass = "";
	String email = "";
	EditText username;
	EditText editpass;
	MyButton searchbt;
	MyButton logoutbt;
	AutoComplete search;
	MyButton joinbt;
	MyButton logbt;
	private PopupWindow popupWindow;  
	LinearLayout search_bt_layout, search_layout, log_layout;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);

		font1= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-MdCn.ttf");
		font2= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-Cn.ttf");
		SPUserInfo info = new SPUserInfo(this);
		name = info.getUserInfo(SPUserInfo.USERNAME);
		pass = info.getUserInfo(SPUserInfo.PASSWORD);
		email = info.getUserInfo(SPUserInfo.EMAIL);
		LinearLayout backbt = (LinearLayout)findViewById(R.id.back_bt);
		backbt.setVisibility(View.GONE);
		LinearLayout topserarchbt = (LinearLayout)findViewById(R.id.topsearch_bt);
		topserarchbt.setVisibility(View.GONE);
		
		search_bt_layout = (LinearLayout)findViewById(R.id.searchbt_layout);
		search_layout = (LinearLayout)findViewById(R.id.search_layout);
		log_layout = (LinearLayout)findViewById(R.id.login_layout);
		searchbt = (MyButton)findViewById(R.id.search_bt);
		searchbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String symbol = search.getText().toString();
				TSRMainActivity.symbol = symbol;
				
				Intent intent = new Intent(getParent(), search_details.class);
				TabGroupActivity parentActivity = (TabGroupActivity) getParent();
				intent.putExtra("symbol", symbol);
				intent.putExtra("title", "");
				intent.putExtra("flg", "0");
				parentActivity.startChildActivity("search", intent);
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
			}
		});
		logoutbt = (MyButton)findViewById(R.id.logout_bt);
		logoutbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SPUserInfo.logged = 0;
				search_bt_layout.setVisibility(View.GONE);
				search.setEnabled(false);
				SPUserInfo info = new SPUserInfo(Home.this);
		            info.setUserInfo(SPUserInfo.PASSWORD, "");
		            info.setUserInfo(SPUserInfo.EMAIL, "");
		            info.commit();
		            //Alert("Successful. You can search stock information.");
					log_layout.setVisibility(View.VISIBLE);
					
				//ShowPopUp(v);
			}
		});
		//searchbt.setTypeface(font2);
		if(SPUserInfo.logged == 0){
			search_bt_layout.setVisibility(View.GONE);
		}else{
			search_bt_layout.setVisibility(View.VISIBLE);
		}
		search = (AutoComplete)findViewById(R.id.txt_search);
		search.setTypeface(font2);
		username = (EditText)findViewById(R.id.txt_user);
		username.setTypeface(font2);
		editpass = (EditText)findViewById(R.id.txt_pass);
		editpass.setTypeface(font2);
		if(!email.equals("")){
			username.setText(email);
			editpass.setText(pass);
		}
		logbt = (MyButton)findViewById(R.id.log_bt);
		logbt.setTypeface(font2);
		logbt.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(username.getText().toString().equals("")){
					Alert("Please input your email.");
					return;
				}
				if(editpass.getText().toString().equals("")){
					Alert("Please input your password.");
					return;
				}
				CheckInternetAccess check = new CheckInternetAccess(Home.this);
		        if (check.checkNetwork()) {
					new GetUserLoginTask().execute();
		        }else{
		        	Alert("Please check your network.");
		        }
			}
		});
		joinbt = (MyButton)findViewById(R.id.join_bt);
		joinbt.setTypeface(font2);
		joinbt.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(getParent(), joinus.class);
				Home.this.startActivity(intent);
				finish();
        		//TabGroupActivity parentActivity = (TabGroupActivity) getParent();
        		//parentActivity.startChildActivity("Join",intent);
        		
			}
		});
		//tvPageTitle.setTypeface(font1);
		 //new GetUserTask().execute();
		
	}
	@Override
	public void onResume(){

		StaticInfo.autoflg = false;
		if(SPUserInfo.logged == 0){
			search.setEnabled(false);
		}else{
			search.setEnabled(true);
		}

		if(SPUserInfo.logged == 0){
			if(email != null && !email.equals("")){
				if(pass != null && !pass.equals("")){
					CheckInternetAccess check = new CheckInternetAccess(Home.this);
			        if (check.checkNetwork()) {
						new GetUserLoginTask().execute();
			        }else{
			        	Alert("Please check your network.");
			        }
				}
			}
		}else{
			log_layout.setVisibility(View.GONE);
			search_bt_layout.setVisibility(View.VISIBLE);
			
		}
		//InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		//imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		super.onResume();
	}
	/*public void ShowPopUp(View parent){
			ListView lv_group = null;
			popupWindow = null;
	        if (popupWindow == null) {  
	            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
	  
	            View view = layoutInflater.inflate(R.layout.group_list, null);  
	  
	            lv_group = (ListView) view.findViewById(R.id.lvGroup);
	            List<String> groups = new ArrayList<String>();  
	            groups.add("1111111111111111111111111");  
	            groups.add("1111111111111111111111111");  
	            groups.add("1111111111111111111111111");  
	            groups.add("1111111111111111111111111");  
	            groups.add("1111111111111111111111111");  
	            groups.add("1111111111111111111111111");  
	            groups.add("1111111111111111111111111");  
	  
	            GroupAdapter groupAdapter = new GroupAdapter(this, groups);  
	            lv_group.setAdapter(groupAdapter);  
	            popupWindow = new PopupWindow(view, 300, 350);  
	        }  
	  
	        popupWindow.setFocusable(true);  
	        popupWindow.setOutsideTouchable(true);  
	        popupWindow.setBackgroundDrawable(getParent().getResources().getDrawable(R.color.blue));  
	        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);  
	        
	        int xPos = windowManager.getDefaultDisplay().getWidth() / 2  
	                - popupWindow.getWidth() / 2;  
	        Log.i("coder", "xPos:" + xPos);  
	  
	        popupWindow.showAsDropDown(parent, xPos, 0);  
	  
	        lv_group.setOnItemClickListener(new OnItemClickListener() {  
	  
	            @Override  
	            public void onItemClick(AdapterView<?> adapterView, View view,  
	                    int position, long id) {  
	  
	                Toast.makeText(Home.this,  
	                        ""+position, 1000)  
	                        .show();  
	  
	                if (popupWindow != null) {  
	                    popupWindow.dismiss(); 
	                }  
	            }  
	        });  
	    }  
	    */
	public void Alert(String msg) {
		new AlertDialog.Builder(getParent()).setTitle("TSR")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}
	private class GetUserLoginTask extends AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;
       public GetUserLoginTask() {
       }
		@Override
		protected void onPostExecute(String _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result != null){
				if(!_result.equals("")){
					try {
						// A Simple JSONObject Creation
			            JSONObject json=new JSONObject(_result);	   
						// A Simple JSONObject Parsing
			            //JSONArray nameArray=json.names();
			            //JSONArray valArray=json.toJSONArray(nameArray);
			            SPUserInfo info = new SPUserInfo(Home.this);
			            if(json.getString("registered").equals("success")){
				            info.setUserInfo(SPUserInfo.PASSWORD, json.getString("userpass"));
				            info.setUserInfo(SPUserInfo.EMAIL, json.getString("username"));
				            info.commit();
				            SPUserInfo.logged = 1;
				            //Alert("Successful. You can search stock information.");
							searchbt.setVisibility(View.VISIBLE);
							search.setEnabled(true);
							log_layout.setVisibility(View.GONE);
							search_bt_layout.setVisibility(View.VISIBLE);
							search.setEnabled(true);
			            }else if(json.getString("registered").equals("failed")){
			            	Alert("Failed response from server. Please try again.");
			            }else{
			            	if(json.getString("registered").contains("not verified")){
			            		//Alert(json.getString("registered"));
			            		
			            		Intent intent = new Intent(getParent(), verify.class);
			            		intent.putExtra("msg", json.getString("registered"));
			            		Home.this.startActivity(intent);
			            		finish();
			            		//TabGroupActivity parentActivity = (TabGroupActivity) getParent();
			            		//parentActivity.startChildActivity("Verify",intent);
			            	}else{
				            	Alert(json.getString("registered"));
			            	}
			            }
				       
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Alert("Failed parse for JSON data.");
					}
				}else{
					Alert("Failed response from server. Please try again.");
				}
			}else{
				Alert("Failed. Please check network and try again.");
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			 MyDialog = ProgressDialog.show(getParent(), "",
	                    "Loading... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {	
			
			String result = null;
			CheckInternetAccess check = new CheckInternetAccess(Home.this);
	        if (check.checkNetwork()) {
	        	WebService service= new WebService();
				result = service.tsrConnect("user_login.php", AddJSONData());
				//result = service.connectforchart("daychart.html", "");
	        }
			return result;
		}
	}
	public JSONObject AddJSONData(){
		   JSONObject jdata = new JSONObject();
		   try {
			   jdata.put("username", username.getText().toString());
			   jdata.put("userpass", editpass.getText().toString());
		   } catch (JSONException e) {
			// TODO Auto-generated catch block
			   e.printStackTrace();
		   }
		   return jdata;
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Overrides the default implementation for KeyEvent.KEYCODE_BACK 
	 * so that all systems call onBackPressed().
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			((TabGroupActivity)getParent()).onBackPressed();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

}
