package com.sin.tsr.markets;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.stockchart.StockChartView;
import org.stockchart.core.Area;
import org.stockchart.core.Appearance.Gradient;
import org.stockchart.series.LinearSeries;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.sin.tsr.ConnectionDetector;
import com.sin.tsr.R;
import com.sin.tsr.TSRMainActivity;
import com.sin.tsr.TabGroupActivity;
import com.sin.tsr.autocomplete.AutoCompleteForDetails;
import com.sin.tsr.home.search_details;
import com.sin.tsr.lib.Helper;
import com.sin.tsr.lib.ReturnMessage;
import com.sin.tsr.news.news.YahooNewsListAdapter;
import com.sin.tsr.shared.ArrayChartInfo;
import com.sin.tsr.shared.ChartInfo;
import com.sin.tsr.shared.CurrentInfo1;
import com.sin.tsr.shared.SPUserInfo;
import com.sin.tsr.shared.SymbolInfo;
import com.sin.tsr.shared.YahooNewsInfo;
import com.sin.tsr.textview.MyButton;
import com.sin.tsr.textview.MyTextView;
import com.sin.tsr.webservice.CheckInternetAccess;
import com.sin.tsr.webservice.WebService;

public class markets extends Activity{
	Typeface font1,font2;
	boolean connection;
	SharedPreferences settings;
	String CID = "";
	ScrollView  scWhy;//scIntro,
	Button btRegister, btWhy;
	AutoCompleteForDetails searchtext;
	String datetime = "1d";
	String symbol;
	MyButton bt1, bt2, bt3, bt4, bt5, bt6;
	int market_id = 1;
	MyTextView view_us, view_eu, view_uk, view_asia;
	ListView marketlist;
	List<String[]> symbollist = new ArrayList<String[]>();
	List<ArrayChartInfo> clist = new ArrayList<ArrayChartInfo>();
	int[] currentfindflg = new int[3];
	int[] colorflg = new int[3];
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.markets);
		
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-MdCn.ttf");
		font2= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-Cn.ttf");
		 settings = getSharedPreferences("TSR", Context.MODE_PRIVATE);
		 LinearLayout backbt = (LinearLayout)findViewById(R.id.back_bt);
		backbt.setVisibility(View.GONE);
		backbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((TabGroupActivity)getParent()).onBackPressed();
			}
		});
		LinearLayout topserarchbt = (LinearLayout)findViewById(R.id.topsearch_bt);
		topserarchbt.setVisibility(View.GONE);
		final LinearLayout top_search = (LinearLayout)findViewById(R.id.top_search);
		top_search.setVisibility(View.GONE);
		topserarchbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				top_search.setVisibility(View.VISIBLE);
			}
		});
		searchtext = (AutoCompleteForDetails)findViewById(R.id.txt_search);
		searchtext.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				if(position < searchtext.getList().size()){
					String s = searchtext.getList().get(position);
					String[] s1 = s.split(",");
					String symbol1 = s1[0];
					symbol = symbol1;
					TSRMainActivity.symbol = symbol1;
					searchtext.setText(s);
					Intent intent = new Intent(getParent(), search_details.class);
					TabGroupActivity parentActivity = (TabGroupActivity) getParent();
					intent.putExtra("symbol", symbol1);
					intent.putExtra("title", s);
					intent.putExtra("flg", "1");
					parentActivity.startChildActivity("searchDetails1", intent);
					getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
					//InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					//imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
				}
			}
			
		});
		bt1 = (MyButton)findViewById(R.id.bt_1d);
		bt1.setSelected(true);
		bt1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("1d") && SPUserInfo.logged == 1){
					datetime = "1d";
					Selected_Bt(1);
					for(int i = 0; i < 3; i++)
						currentfindflg[i] = 1;
					symbollist.clear();
					new GetChartDataTask().execute();
				}
			}
		});
		bt2 = (MyButton)findViewById(R.id.bt_5d);
		bt2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("5d") && SPUserInfo.logged == 1){
					datetime = "5d";
					Selected_Bt(2);
					for(int i = 0; i < 3; i++)
						currentfindflg[i] = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		bt3 = (MyButton)findViewById(R.id.bt_1m);
		bt3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("1m") && SPUserInfo.logged == 1){
					datetime = "1m";
					Selected_Bt(3);
					for(int i = 0; i < 3; i++)
						currentfindflg[i] = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		bt4 = (MyButton)findViewById(R.id.bt_6m);
		bt4.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("3m") && SPUserInfo.logged == 1){
					datetime = "3m";
					Selected_Bt(4);
					for(int i = 0; i < 3; i++)
						currentfindflg[i] = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		bt5 = (MyButton)findViewById(R.id.bt_1y);
		bt5.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("6m") && SPUserInfo.logged == 1){
					datetime = "6m";
					Selected_Bt(5);
					for(int i = 0; i < 3; i++)
						currentfindflg[i] = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		bt6 = (MyButton)findViewById(R.id.bt_5y);
		bt6.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!datetime.equals("1y") && SPUserInfo.logged == 1){
					datetime = "1y";
					Selected_Bt(6);
					for(int i = 0; i < 3; i++)
						currentfindflg[i] = 0;
					new GetChartDataTask().execute();
				}
			}
		});
		view_us = (MyTextView)findViewById(R.id.view_us);
		view_us.setSelected(true);
		view_us.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(market_id != 1 && SPUserInfo.logged == 1){
					market_id = 1;
					datetime = "1d";
					Selected_Bt(1);
					Select_View(market_id);
					for(int i = 0; i < 3; i++)
						currentfindflg[i] = 1;
					symbollist.clear();
					new GetChartDataTask().execute();
				}
			}
		});
		view_eu = (MyTextView)findViewById(R.id.view_eu);
		view_eu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(market_id != 2 && SPUserInfo.logged == 1){
					market_id = 2;
					datetime = "1d";
					Selected_Bt(1);
					Select_View(market_id);
					for(int i = 0; i < 3; i++)
						currentfindflg[i] = 1;
					symbollist.clear();
					new GetChartDataTask().execute();
				}
			}
		});
		view_uk = (MyTextView)findViewById(R.id.view_uk);
		view_uk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(market_id != 3 && SPUserInfo.logged == 1){
					market_id = 3;
					datetime = "1d";
					Selected_Bt(1);
					Select_View(market_id);
					for(int i = 0; i < 3; i++)
						currentfindflg[i] = 1;
					symbollist.clear();
					new GetChartDataTask().execute();
				}
			}
		});
		view_asia = (MyTextView)findViewById(R.id.view_asia);
		view_asia.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(market_id != 4 && SPUserInfo.logged == 1){
					market_id = 4;
					datetime = "1d";
					Selected_Bt(1);
					Select_View(market_id);
					for(int i = 0; i < 3; i++)
						currentfindflg[i] = 1;
					symbollist.clear();
					new GetChartDataTask().execute();
				}
			}
		});
		marketlist = (ListView)findViewById(R.id.market_list);
		//tvPageTitle.setTypeface(font1);
	}

	@Override
	public void onResume(){
		if(SPUserInfo.logged == 1){
			for(int i = 0; i < 3; i++)
				currentfindflg[i] = 1;
			symbollist.clear();
			new GetChartDataTask().execute();
		}else{
			ReturnMessage.showAlertDialog(getParent(), "Please log in and try again");
		}
		//InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		//imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		super.onResume();
	}
	public void Select_View(int id){
		view_us.setSelected(false);
		view_eu.setSelected(false);
		view_uk.setSelected(false);
		view_asia.setSelected(false);
		switch(id){
		case 1:
			view_us.setSelected(true);
			break;
		case 2:
			view_eu.setSelected(true);
			break;
		case 3:
			view_uk.setSelected(true);
			break;
		case 4:
			view_asia.setSelected(true);
			break;
		}
	}
	public void Selected_Bt(int id){
		bt1.setSelected(false);
		bt2.setSelected(false);
		bt3.setSelected(false);
		bt4.setSelected(false);
		bt5.setSelected(false);
		bt6.setSelected(false);
		switch(id){
		case 1:
			bt1.setSelected(true);
			break;
		case 2:
			bt2.setSelected(true);
			break;
		case 3:
			bt3.setSelected(true);
			break;
		case 4:
			bt4.setSelected(true);
			break;
		case 5:
			bt5.setSelected(true);
			break;
		case 6:
			bt6.setSelected(true);
			break;
		}
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(getParent()).setTitle("TSR")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}

	private class GetCurrentDataTask extends AsyncTask<Void, Void, String[]> {
		ProgressDialog MyDialog;
       public GetCurrentDataTask() {
       }
		@Override
		protected void onPostExecute(String[] _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			if(_result != null){
				if(_result.length > 0){
					try{
						SymbolInfo sinfo = new SymbolInfo();						
						switch(market_id){
			        	case 1:
			        		sinfo.setUS();
			        		break;
			        	case 2:
			        		sinfo.setEurope();
			        		break;
			        	case 3:
			        		sinfo.setUK();
			        		break;
			        	case 4:
			        		sinfo.setAsia();
			        		break;
			        	}
						symbollist.clear();
						for(int i = 0; i < 3; i++){	
							String[] s = new String[5];
							try{
								JSONObject json=new JSONObject(_result[i]);
								String data = new JSONObject(new JSONObject(json.getString("query")).getString("results")).getString("quote");
								JSONObject json1=new JSONObject(data);
								s[0] = sinfo.title[i];
								s[1] = json1.getString("LastTradePriceOnly");
								s[2] = json1.getString("ChangeRealtime") + " (" +json1.getString("ChangeinPercent") + ")";
								s[3] = json1.getString("ChangeRealtime");
								symbollist.add(s);
								currentfindflg[i] = 0;
							}catch(Exception e){
								currentfindflg[i] = 1;
							}
							
						}
						
					}catch(Exception e){
						for(int i = 0; i < 3; i++){	
							currentfindflg[i] = 1;
						}
						//Alert("Can not get stock's real data from yahoo. please try again.");
					}
				}else{
					for(int i = 0; i < 3; i++){	
						currentfindflg[i] = 1;
					}
					//Alert("Can not get stock's real data from yahoo. please try again.");
				}
			}else{
				for(int i = 0; i < 3; i++){	
					currentfindflg[i] = 1;
				}
				//Alert("Failed service. Please try again.");
			}
			new GetChartDataTask().execute();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			 MyDialog = ProgressDialog.show(getParent(), "",
	                    "Real Data Loading... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected String[] doInBackground(Void... params) {	
			
			String[] result = new String[3];
			CheckInternetAccess check = new CheckInternetAccess(markets.this);
	        if (check.checkNetwork()) {
	        	WebService service= new WebService();
	        	SymbolInfo sinfo = new SymbolInfo();
	        	String symbol1 = "";
	        	switch(market_id){
	        	case 1:
	        		sinfo.setUS();
	        		for(int i = 0; i < 3; i++){
	        			symbol1 = sinfo.symbol[i];
	        			try {
							result[i] = service.currentConnect (URLEncoder.encode(symbol1, "utf-8"));
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	        		}
	        		break;
	        	case 2:
	        		sinfo.setEurope();
	        		for(int i = 0; i < 3; i++){
	        			symbol1 = sinfo.symbol[i];
	        			try {
							result[i] = service.currentConnect (URLEncoder.encode(symbol1, "utf-8"));
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	        		}
	        		break;
	        	case 3:
	        		sinfo.setUK();
	        		for(int i = 0; i < 3; i++){
	        			symbol1 = sinfo.symbol[i];
	        			try {
							result[i] = service.currentConnect (URLEncoder.encode(symbol1, "utf-8"));
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	        		}
	        		break;
	        	case 4:
	        		sinfo.setAsia();
	        		for(int i = 0; i < 3; i++){
	        			symbol1 = sinfo.symbol[i];
	        			try {
							result[i] = service.currentConnect (URLEncoder.encode(symbol1, "utf-8"));
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	        		}
	        		break;
	        	}
	        }
			return result;
		}
	}
	

	private class GetChartDataTask extends AsyncTask<Void, Void, List<ArrayChartInfo>> {
		ProgressDialog MyDialog;
       public GetChartDataTask() {
       }
		@Override
		protected void onPostExecute(List<ArrayChartInfo> _result) {
			super.onPostExecute(_result);
			MyDialog.dismiss();
			List<ArrayChartInfo>  result = new ArrayList<ArrayChartInfo>();
			if(_result != null){
				if(_result.size() > 0){
					for(int i = 0; i <_result.size(); i++){
						if(_result.get(i).arraylist.size() > 0){
							ArrayChartInfo info = new ArrayChartInfo();
							info.arraylist = _result.get(i).arraylist;
							result.add(info);
							colorflg[i] = 1;
						}else{
							colorflg[i] = 0;
						}
					}
					if(result.size() > 0){
						clist.clear();
						clist.addAll(result);
						SymbolListAdapter homeadapter = new SymbolListAdapter(
								getParent(), R.layout.markets_list, symbollist);
						marketlist.setAdapter(homeadapter);
						marketlist.setSmoothScrollbarEnabled(true);
						Helper.getListViewSize(marketlist);
						homeadapter.notifyDataSetChanged();
						ChartDraw();
					}else{
						Alert("Can not get stock's chart data from yahoo. please try again.");
					}
				}else{
					Alert("Can not get stock's chart data from yahoo. please try again.");
				}
			}else{
				Alert("Failed stock's chart service. Please try again.");
			}
			//new GetChartDataTask().execute();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			for(int i = 0; i < 3; i++){
				colorflg[i] = 0;
			}
			 MyDialog = ProgressDialog.show(getParent(), "",
	                    "Chart Data Loading... ", false);
	            MyDialog.setCancelable(false);
		}

		@Override
		protected List<ArrayChartInfo> doInBackground(Void... params) {	
			
			List<ArrayChartInfo> result = new ArrayList<ArrayChartInfo>();
			CheckInternetAccess check = new CheckInternetAccess(markets.this);
	        if (check.checkNetwork()) {
	        	WebService service= new WebService();
				SymbolInfo sinfo = new SymbolInfo();
	        	String symbol1 = "";
	        	switch(market_id){
	        	case 1:
	        		sinfo.setUS();
	        		
	        		for(int i = 0; i < 3; i++){
	        			symbol1 = sinfo.symbol[i];
	        			ArrayChartInfo list = new ArrayChartInfo();
	        			//if(symbol1.contains("^") && WebService.GLOBAL == 1){
	        				//symbol1 = symbol1.substring(1,symbol1.length());
	        			//}else{
	        				symbol1 = URLEncoder.encode(symbol1);
	        			//}
	        			try {
	        				if(currentfindflg[i] == 1){
	        					list.arraylist = service.chartMarketConnect1(symbol1, datetime);//URLEncoder.encode(symbol1, "utf-8")
	        					
	        					String[] s = new String[4];
	        					s[0] = sinfo.title[i];
	        					s[1] = CurrentInfo1.cprice; 
	        					s[2] = CurrentInfo1.dchange;
	        					s[3] = CurrentInfo1.volume;
	        					CurrentInfo1.cprice = "";
	        					CurrentInfo1.dchange = "";
	        					CurrentInfo1.volume = "";
	        					symbollist.add(s);
	        				}else{
	        					list.arraylist = service.chartMarketConnect(symbol1, datetime);//URLEncoder.encode(symbol1, "utf-8")
	        				}	        				
	        				result.add(list);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	        		}
	        		break;
	        	case 2:
	        		sinfo.setEurope();
	        		for(int i = 0; i < 3; i++){
	        			symbol1 = sinfo.symbol[i];
	        			symbol1 = URLEncoder.encode(symbol1);
	        			ArrayChartInfo list = new ArrayChartInfo();
	        			try {
	        				if(currentfindflg[i] == 1){
	        					list.arraylist = service.chartMarketConnect1(symbol1, datetime);//URLEncoder.encode(symbol1, "utf-8")
	        					
	        					String[] s = new String[4];
	        					s[0] = sinfo.title[i];
	        					s[1] = CurrentInfo1.cprice;
	        					s[2] = CurrentInfo1.dchange;
	        					s[3] = CurrentInfo1.volume;
	        					CurrentInfo1.cprice = "";
	        					CurrentInfo1.dchange = "";
	        					CurrentInfo1.volume = "";
	        					symbollist.add(s);
	        				}else{
	        					list.arraylist = service.chartMarketConnect(symbol1, datetime);
	        				}
	        				result.add(list);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	        		}
	        		break;
	        	case 3:
	        		sinfo.setUK();
	        		for(int i = 0; i < 3; i++){
	        			symbol1 = sinfo.symbol[i];
	        			symbol1 = URLEncoder.encode(symbol1);
	        			ArrayChartInfo list = new ArrayChartInfo();
	        			try {

	        				if(currentfindflg[i] == 1){
	        					list.arraylist = service.chartMarketConnect1(symbol1, datetime);//URLEncoder.encode(symbol1, "utf-8")
	        					
	        					String[] s = new String[4];
	        					s[0] = sinfo.title[i];
	        					s[1] = CurrentInfo1.cprice;
	        					s[2] = CurrentInfo1.dchange;
	        					s[3] = CurrentInfo1.volume;
	        					CurrentInfo1.cprice = "";
	        					CurrentInfo1.dchange = "";
	        					CurrentInfo1.volume = "";
	        					symbollist.add(s);
	        				}else{
	        					list.arraylist = service.chartMarketConnect(symbol1, datetime);
	        				}
	        				result.add(list);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	        		}
	        		break;
	        	case 4:
	        		sinfo.setAsia();
	        		for(int i = 0; i < 3; i++){
	        			symbol1 = sinfo.symbol[i];
	        			symbol1 = URLEncoder.encode(symbol1);
	        			ArrayChartInfo list = new ArrayChartInfo();
	        			try {
	        				if(currentfindflg[i] == 1){
	        					list.arraylist = service.chartMarketConnect1(symbol1, datetime);//URLEncoder.encode(symbol1, "utf-8")
	        					
	        					String[] s = new String[4];
	        					s[0] = sinfo.title[i];
	        					s[1] = CurrentInfo1.cprice;
	        					s[2] = CurrentInfo1.dchange;
	        					s[3] = CurrentInfo1.volume;
	        					CurrentInfo1.cprice = "";
	        					CurrentInfo1.dchange = "";
	        					CurrentInfo1.volume = "";
	        					symbollist.add(s);
	        				}else{
	        					list.arraylist = service.chartMarketConnect(symbol1, datetime);
	        				}
	        				result.add(list);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	        		}
	        		break;
	        	}
	        }
			return result;
		}
	}
	public void ChartDraw(){
		StockChartView chartview = (StockChartView)findViewById(R.id.chartview);//new StockChartView(search_details.this);
		chartview.postInvalidate();
		chartview.invalidate();
		chartview.init();
		chartview.reset();
		LinearSeries chartSeries1 = new LinearSeries();
		LinearSeries chartSeries2 = new LinearSeries();
		LinearSeries chartSeries3 = new LinearSeries();
		for(int i = 0; i < clist.size(); i ++){
			switch(i){
			case 0:
				chartSeries1.getAppearance().setOutlineWidth(1.0f);
				if(colorflg[0] == 1){
					chartSeries1.getAppearance().setOutlineColor(getParent().getResources().getColor(R.color.first_line));//
					chartSeries1.getAppearance().setPrimaryFillColor(0x449a6154);
					chartSeries1.getAppearance().setSecondaryFillColor(0x449a6154);
				}else{
					if(colorflg[1] == 1){
						chartSeries1.getAppearance().setOutlineColor(getParent().getResources().getColor(R.color.second_line));//
						chartSeries1.getAppearance().setPrimaryFillColor(0x444a769d);
						chartSeries1.getAppearance().setSecondaryFillColor(0x444a769d);
						colorflg[1] = 0;
					}else{
						chartSeries1.getAppearance().setOutlineColor(getParent().getResources().getColor(R.color.third_line));//
						chartSeries1.getAppearance().setPrimaryFillColor(0x44e7c47d);
						chartSeries1.getAppearance().setSecondaryFillColor(0x44e7c47d);
						colorflg[2] = 0;
					}
				}
				chartSeries1.getAppearance().setGradient(Gradient.LINEAR_VERTICAL);
				chartSeries1.setName("chart1");
				break;
			case 1:
				chartSeries2.getAppearance().setOutlineWidth(1.0f);
				if(colorflg[1] == 1){
					chartSeries2.getAppearance().setOutlineColor(getParent().getResources().getColor(R.color.second_line));//

					chartSeries2.getAppearance().setPrimaryFillColor(0x444a769d);
					chartSeries2.getAppearance().setSecondaryFillColor(0x444a769d);
				}else{
					chartSeries2.getAppearance().setOutlineColor(getParent().getResources().getColor(R.color.third_line));//
					chartSeries2.getAppearance().setPrimaryFillColor(0x44e7c47d);
					chartSeries2.getAppearance().setSecondaryFillColor(0x44e7c47d);
					
				}
				chartSeries2.getAppearance().setGradient(Gradient.LINEAR_VERTICAL);
				chartSeries2.setName("chart2");
				break;
			case 2:
				chartSeries3.getAppearance().setOutlineColor(getParent().getResources().getColor(R.color.third_line));//
				chartSeries3.getAppearance().setOutlineWidth(1.0f);
				chartSeries3.getAppearance().setPrimaryFillColor(0x44e7c47d);
				chartSeries3.getAppearance().setSecondaryFillColor(0x44e7c47d);
				chartSeries3.getAppearance().setGradient(Gradient.LINEAR_VERTICAL);
				chartSeries3.setName("chart3");
				break;
			}
		}
		Area a = chartview.addArea();
		switch(clist.size()){
		case 1:
			a.getSeries().add(chartSeries1);
			break;
		case 2:
			a.getSeries().add(chartSeries1);
			a.getSeries().add(chartSeries2);
			break;
		case 3:
			a.getSeries().add(chartSeries1);
			a.getSeries().add(chartSeries2);
			a.getSeries().add(chartSeries3);
			break;
		}
		int k = 0;
		for(int i = 0; i < clist.size() ; i++){
			if(i == 0){
				k = clist.get(i).arraylist.size();
			}
			if(i > 0){
				if(k > clist.get(i).arraylist.size()){
					k = clist.get(i).arraylist.size();
				}
			}
		}
		for(int i = 0; i< clist.size(); i++){
			int diff = clist.get(i).arraylist.size() - k;
			if(diff > 0){
				List<ChartInfo> cinfo = new ArrayList<ChartInfo>();
				cinfo.addAll(clist.get(i).arraylist);
				clist.get(i).arraylist.clear();
				for(int j = cinfo.size() -1; j >= cinfo.size() - k;j--){
					clist.get(i).arraylist.add(cinfo.get(j));
				}
			}
		}
		for(int i = 0; i < clist.size(); i++){
			switch(i){
			case 0:
				List<String> xvalue = new ArrayList<String>();
				for(int j = 0; j < clist.get(i).arraylist.size();j++){
					ChartInfo cinfo = clist.get(i).arraylist.get(j);
					xvalue.add(cinfo.stamp);
					chartSeries1.addPoint(Double.parseDouble(cinfo.open));
				}
				chartSeries1.wherechart = 2;
				if(datetime.equals("1d")){
					chartSeries1.daytime = 1;
				}else if(datetime.equals("5d")){
					chartSeries1.daytime = 2;
				}else if(datetime.equals("1m")){
					chartSeries1.daytime = 3;
				}else if(datetime.equals("6m")){
					chartSeries1.daytime = 4;
				}else if(datetime.equals("1y")){
					chartSeries1.daytime = 5;
				}else if(datetime.equals("5y")){
					chartSeries1.daytime = 6;
				}
				chartSeries1.clist = xvalue;	
				break;
			case 1:
				List<String> xvalue1 = new ArrayList<String>();
				for(int j = 0; j < clist.get(i).arraylist.size();j++){
					ChartInfo cinfo = clist.get(i).arraylist.get(j);
					xvalue1.add(cinfo.stamp);
					chartSeries2.addPoint(Double.parseDouble(cinfo.open));
				}
				chartSeries2.wherechart = 2;
				if(datetime.equals("1d")){
					chartSeries2.daytime = 1;
				}else if(datetime.equals("5d")){
					chartSeries2.daytime = 2;
				}else if(datetime.equals("1m")){
					chartSeries2.daytime = 3;
				}else if(datetime.equals("6m")){
					chartSeries2.daytime = 4;
				}else if(datetime.equals("1y")){
					chartSeries2.daytime = 5;
				}else if(datetime.equals("5y")){
					chartSeries2.daytime = 6;
				}
				chartSeries2.clist = xvalue1;	
				break;
			case 2:
				List<String> xvalue2 = new ArrayList<String>();
				for(int j = 0; j < clist.get(i).arraylist.size();j++){
					ChartInfo cinfo = clist.get(i).arraylist.get(j);
					xvalue2.add(cinfo.stamp);
					chartSeries3.addPoint(Double.parseDouble(cinfo.open));
				}
				chartSeries3.wherechart = 2;
				if(datetime.equals("1d")){
					chartSeries3.daytime = 1;
				}else if(datetime.equals("5d")){
					chartSeries3.daytime = 2;
				}else if(datetime.equals("1m")){
					chartSeries3.daytime = 3;
				}else if(datetime.equals("6m")){
					chartSeries3.daytime = 4;
				}else if(datetime.equals("1y")){
					chartSeries3.daytime = 5;
				}else if(datetime.equals("5y")){
					chartSeries3.daytime = 6;
				}
				chartSeries3.clist = xvalue2;	
				break;
			}		
		}
		//real_graph.removeAllViews();
		//real_graph.addView(chartview, new LayoutParams(LayoutParams.FILL_PARENT, 250));
		chartview.refreshDrawableState();
		
	}
	public class SymbolListAdapter extends ArrayAdapter<String[]> {

		private List<String[]> items;
		Context con;

		public SymbolListAdapter(Context context, int textViewResourceId,
				List<String[]> objects) {

			super(context, textViewResourceId, objects);
			this.items = objects;
			this.con = context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
				throws IndexOutOfBoundsException, IllegalStateException {
			// TODO Auto-generated method stub
			View v = convertView;
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			final String[] item = items.get(position);
			if (v == null) {
				v = vi.inflate(R.layout.markets_list, null);
			}
			MyTextView title = (MyTextView) v
					.findViewById(R.id.view_list_title);
			MyTextView desc = (MyTextView) v
					.findViewById(R.id.view_list_price);
			MyTextView date = (MyTextView) v
					.findViewById(R.id.view_list_detail);
			title.setText(item[0]);
			if(!item[1].equals("") && !item[2].equals("")){

				desc.setText(item[1]);
				date.setText(item[2]);
			}
			switch(position){
			case 0:
				title.setTextColor(getParent().getResources().getColor(R.color.first_line));
				desc.setTextColor(getParent().getResources().getColor(R.color.first_line));
				if(item[3].contains("-")){
					date.setTextColor(getParent().getResources().getColor(R.color.first_line));
				}else{
					date.setTextColor(getParent().getResources().getColor(R.color.green));
				}
				break;
			case 1:
				title.setTextColor(getParent().getResources().getColor(R.color.second_line));
				desc.setTextColor(getParent().getResources().getColor(R.color.second_line));
				if(item[3].contains("-")){
					date.setTextColor(getParent().getResources().getColor(R.color.second_line));
				}else{
					date.setTextColor(getParent().getResources().getColor(R.color.green));
				}
				break;
			case 2:
				title.setTextColor(getParent().getResources().getColor(R.color.third_line));
				desc.setTextColor(getParent().getResources().getColor(R.color.third_line));
				if(item[3].contains("-")){
					date.setTextColor(getParent().getResources().getColor(R.color.third_line));
				}else{
					date.setTextColor(getParent().getResources().getColor(R.color.green));
				}
				break;
			}
			return v;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Overrides the default implementation for KeyEvent.KEYCODE_BACK 
	 * so that all systems call onBackPressed().
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			((TabGroupActivity)getParent()).onBackPressed();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}
}
