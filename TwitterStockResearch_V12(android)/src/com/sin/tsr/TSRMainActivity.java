package com.sin.tsr;

import org.stockchart.series.LinearSeries;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import com.sin.tsr.home.HomeTabGroup;
import com.sin.tsr.markets.MarketsTabGroup;
import com.sin.tsr.news.NewsTabGroup;


public class TSRMainActivity extends TabActivity implements OnTabChangeListener{
	/** Called when the activity is first created. */
	public static TabHost tabHost;
	public ImageView ivPrev;
	public ImageView ivNext;
	Typeface font1,font2;
	public static String symbol = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-MdCn.ttf");
		font2= Typeface.createFromAsset(getAssets(),
				"fonts/HelveticaNeueLTStd-Cn.ttf");
		TextView tv= new TextView(this);

		tv.setTypeface(font2);
		tv.setTextColor(getResources().getColor(R.color.text_color));
		tv.setText("NEWS");
		tabHost = getTabHost();
		tabHost.setOnTabChangedListener(this);
		tabHost.addTab(tabHost.newTabSpec("tab1")
				.setIndicator(tv.getText(), getResources().getDrawable(R.drawable.tab1))
				.setContent(new Intent(this, NewsTabGroup.class)));
		tv.setText("HOME");
		tabHost.addTab(tabHost.newTabSpec("tab2")
				.setIndicator(tv.getText(), getResources().getDrawable(R.drawable.tab2))
				.setContent(new Intent(this, HomeTabGroup.class)));
		tv.setText("MARKETS");
		tabHost.addTab(tabHost.newTabSpec("tab3")
				.setIndicator(tv.getText(),getResources().getDrawable(R.drawable.tab3))//.tabicon_notifications
				.setContent(new Intent(this, MarketsTabGroup.class)));
		
		tabHost.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (getTabHost().getCurrentTabTag().equals("tab1")) { 
				}
			}
		});
		for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)  
		{   
			//tabHost.getTabWidget().getChildAt(i).setBackgroundColor(getBaseContext().getResources().getColor(R.color.unselectedgray));  
		}  

		//tabHost.getTabWidget().getChildAt(0).setBackgroundColor(getBaseContext().getResources().getColor(R.color.selectedgray));  

		Intent intent = getIntent();

		String emailmark = intent.getStringExtra("active");
		if (intent.getBooleanExtra("ShowNotification", false)){
			tabHost.setCurrentTab(1);
		}else if(emailmark !=null && emailmark.equals("email")){
			tabHost.setCurrentTab(2);
		}else if(emailmark !=null && emailmark.equals("info")){
			tabHost.setCurrentTab(3);
		}else if(emailmark !=null && emailmark.equals("profile")){
			tabHost.setCurrentTab(1);
		}else if(emailmark !=null && emailmark.equals("pro")){
			tabHost.setCurrentTab(1);
		}else if(emailmark !=null && emailmark.equals("serve")){
			tabHost.setCurrentTab(2);
		}else {
			tabHost.setCurrentTab(1);
		}
	    for(int i=0;i<tabHost.getTabWidget().getChildCount();i++) 
	    {
	        TextView tv1 = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
	        tv1.setTextColor(getResources().getColor(R.color.text_color));
	    } 
		
	}
	public void onTabChanged(String tabId) {
		for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)  
		{  
//			tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#8A4117"));  
			//tabHost.getTabWidget().getChildAt(i).setBackgroundColor(getBaseContext().getResources().getColor(R.color.unselectedgray));
			
			switch(i){
			case 0:
				tabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.news_unselected);
				break;
			case 1:
				tabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.home_unselected);
				break;
			case 2:
				tabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.markets_unselected);
				break;
			}
		}  
		//InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
       // imm.hideSoftInputFromWindow(tabHost.getApplicationWindowToken(), 0);
		if(tabId.equals("tab1")){
			tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundResource(R.drawable.tab_selected);
		}else if(tabId.equals("tab2")){
			LinearSeries.wherechart = 1;
			tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundResource(R.drawable.tab_selected);  
			
		}else if(tabId.equals("tab3")){
			LinearSeries.wherechart = 2;
			tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundResource(R.drawable.tab_selected);
		}
		//tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(getBaseContext().getResources().getColor(R.color.selectedgray));  
	
		InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
        
	}
	
	/*if(tabId == 1){
		
	}*/
}