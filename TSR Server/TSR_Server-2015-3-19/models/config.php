<?php

define('SITE_URL', 'https://www.twitterstockresearch.com'); //https://www.twitterstockresearch.com
define('WEB_ROOT', $_SERVER['DOCUMENT_ROOT'] . "");
define('BASIC_URL', "http://" . $_SERVER['HTTP_HOST'] . "");
define('FAILED', 10000);
define('SUCCESS', 10001);
define('SQLFAILED', 10002);

// define parameters
define('PARAM_GOTO', 'VXo5MzJ3QUNpSUVJcXJBaElQYmpwUT09'); // goto
define('PARAM_PAGE', 'VXo5OWlBQU9ic2t2dHJtWWtBY1c5Zz09'); // page
define('PARAM_RUN',  'VXorNVlnQU1sNmc4MkI2Vlp6M3dHUT09'); // b5fc5 : user
define('PARAM_TYPE',  'VXovWTBnQUlpK2x5SzVLTHB0Z010QT09'); // 34b9c : admin

// post parameters
define('POST_SIGN_IN', 20001);
define('POST_SIGN_OUT', 20002);
define('POST_REGISTER', 20003);
define('POST_CONFIRM_EMAIL', 20004);
define('POST_SAVE_PROFILE', 20005);
define('POST_CHANGE_PASS', 20006);
define('POST_RESET_PASS', 20007);

// registration email
define('REG_MAIL_TITLE', "Welcome to StockTwits!");

define('REG_MAIL_CONTENTS', SITE_URL . "/post.php?b5fc5=2");

// msgs
define('SENT_CONFIRM_MAIL', "An email has been sent to your email address to verify.<br>Please verify your email address and enjoy!");
define('SENT_CONFIRM_MAIL_FAILED', "Your mail address can't be confirmed.<br>Try again with aother email address.");
define('CONFIRM_PASSWORD', "Your password changed. Please check your email.");
define('EMAIL_CHANGED', "Your email has been changed.<br>An email has been sent to your new email address to verify.<br>Please verify your email address and enjoy!");
define('DUPLICATED_USERNAME', "The user name is already in use.<br>Please try with other user name.");
define('DUPLICATED_EMAIL', "The email is already in use.<br>Please try with other email.");
define('UNCONFIRMED_EMAIL', "The email is not verified yet.<br>Please check your mail box and verify it by clicking the link.");
define('WRONG_EMAIL', "User name or email is wrong. Pleae try again.<br>If you didn't verify your email yet, please kindly verify it now.");
define('WRONG_PASSWORD', "Password is wrong. Pleae try again.");
define('EMAIL_CONFIRM_SUCCESS', "Your email address has been confirmed.<br>Please log in and edit your profile to fill up all infomation needed for service.");
define('EMAIL_CONFIRM_FAILED', "The link was expired. Try to log in now.");

session_start();

require_once("db_settings.php"); //Require DB connection

// secure module
define("__SECURE_MODULE_BASE_DIR__",  "/home/twitters/public_html/stsemds"); // linux
//define("__SECURE_MODULE_BASE_DIR__", "D:/xampp/htdocs/newstocktwits/stsemds");

$error_msg = true;
if (!isset($_SESSION['neededToShowError']))
{
    $error_msg = false;
}
else
{
    $error_msg = $_SESSION['neededToShowError'];
}

if ($error_msg === false)
{
    include_once(__SECURE_MODULE_BASE_DIR__ . "/referee.php");
}

require_once("encrypt/security_module.php");
require_once("encrypt/secure.php");
require_once("encrypt/encrpt.php");
require_once("encrypt/decrpt.php");
require_once("encrypt/encrpt2.php");
require_once("encrypt/decrpt2.php");
require_once("encrypt/crypto.php");
require_once("encrypt/xtea.php");

//Pages to require
require_once("class.exhandler.php");
require_once("class.navigator.php");
require_once("class.stockchart.php");
require_once("class.usermng.php");
require_once("class.profile.php");
require_once("class.syslogs.php");

require_once("class.header.php");
require_once("class.home.php");
require_once("class.footer.php");

function secureLog($username, $action, $content, $result)
{
    $logClass = new SysLogs(0, $username);
    $logClass->addSysLog($action, $content, $result);
}

function getParameter($param, $default_value = "")
{
    if (isset($_REQUEST[$param]) && $_REQUEST[$param] !== NULL)
    {
        return trim($_REQUEST[$param]);
    }

    return $default_value;
}

function getTotalCount($table_name, $where = "")
{
    global $mysqli, $db_table_prefix;

    if ($where !== "")
    {
        $where = " WHERE " . $where;
    }

    $stmt = $mysqli->prepare("SELECT count(*) as count FROM " . $table_name . " " . $where);
    $stmt->execute();
    $stmt->bind_result($count);
    $stmt->fetch();
    $stmt->close();
    return ($count);
}

// Current page, Total page, Count per page, URL
function getNavigationPaging($write_pages, $cur_page, $total_page, $url, $add = "")
{
    $str = "";
    if ($cur_page > 1)
    {
//        $str .= "<a href='" . $url . "1{$add}'>first</a>";
        $href = 'href="' . $url . '1' . $add . '"';
        $str .= '<span class="lowercase"><a ' . $href . ' name="pagination" class=" cm-history" >&laquo;&nbsp;first</a></span>';
    }

    $start_page = ( ( (int) ( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page)
    {
        $end_page = $total_page;
    }

    if ($start_page > 1)
    {
//        $href .= "href='" . $url . ($start_page - 1) . "{$add}'";
        $href = 'href="' . $url . ($start_page - 1) . $add . '"';
        $str .= '<span class="lowercase"><a ' . $href . ' name="pagination" class=" cm-history" >&laquo;&nbsp;previous</a></span>';
    }

    if ($total_page > 1)
    {
        for ($k = $start_page; $k <= $end_page; $k++)
        {
            if ($cur_page !== $k)
            {
//                $str .= " &nbsp;<a href='$url$k{$add}'><span>$k</span></a>";
                $href = 'href="' . $url . $k . $add . '"';
                $str .= '<a name = "pagination" ' . $href . ' rel = "' . $k . '" rev = "pagination_contents">' . $k . '</a>';
            }
            else
            {
//                $str .= " &nbsp;<b>$k</b> ";
                $str .= '<span class = "strong">' . $k . '</span>';
            }
        }
    }

    if ($total_page > $end_page)
    {
//        $str .= " &nbsp;<a href='" . $url . ($end_page + 1) . "{$add}'>next</a>";
        $href = 'href="' . $url . ($end_page + 1) . $add . '"';
        $str .= '<span class="lowercase"><a ' . $href . ' name="pagination" class=" cm-history" >next&nbsp;&raquo;</a></span>';
    }

    if ($cur_page < $total_page)
    {
//$str .= "[<a href='$url" . ($cur_page+1) . "'>next</a>]";
//        $str .= " &nbsp;<a href='$url$total_page{$add}'>last</a>";
        $href = 'href="' . $url . $total_page . $add . '"';
        $str .= '<span class="lowercase"><a ' . $href . ' name="pagination" class=" cm-history" >last&nbsp;&raquo;</a></span>';
    }

    $str .= "";

    return $str;
}

?>
