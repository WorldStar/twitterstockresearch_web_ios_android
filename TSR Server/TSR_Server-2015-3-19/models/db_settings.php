<?php

//Database Information
$db_host = "103.6.196.120"; //Host address (103.6.196.120)
$db_name = "twitters_DB"; //Name of Database
$db_user = "twitters_user"; //Name of database user (twitters_user)
$db_pass = "4Y2rnqr9J4"; //Password for database user (4Y2rnqr9J4)

GLOBAL $errors;
GLOBAL $successes;

$errors = array();
$successes = array();

/* Create a new mysqli object with database connection parameters */
$mysqli = new mysqli($db_host, $db_user, $db_pass, $db_name);
GLOBAL $mysqli;

if (mysqli_connect_errno())
{
    echo "Connection Failed: " . mysqli_connect_errno();
    exit();
}

if (!$mysqli->set_charset("utf8"))
{
    //printf("Error loading character set utf8: %s\n", $mysqli->error);
}
else
{
    //printf("Current character set: %s\n", $mysqli->character_set_name());
}

//Direct to install directory, if it exists
if (is_dir("install/"))
{
    header("Location: install/");
    die();
}
?>