<?php

class SysLogs
{

    private $log_id;
    private $user_name;

    function __construct($logid = 0, $username = "")
    {
        // Sanitize
        $this->log_id = $logid;
        $this->user_name = trim($username);
    }

    public function addSysLog($action, $content, $result)
    {
        global $mysqli;

        $stmt = $mysqli->prepare("INSERT INTO sys_logs (
                                    user_name,
                                    action,
                                    content,
                                    result,
                                    reg_stamp
                                    )
                                    VALUES (
                                    ?,?,?,?,'" . time() . "'
                                    )");

        $stmt->bind_param("sssi", $this->user_name, $action, $content, $result);
        $stmt->execute();
        $stmt->close();
    }

    //Retrieve information for all roles
    public function fetchAllSysLogs($page, $count, $sort_by, $sort_order, $search_start, $search_end)
    {
        global $mysqli;

        $where = "WHERE id != '0'";
        if ($search_start !== "" && $search_end !== "")
        {
//    $where .= " AND reg_stamp >= '" . strtotime($search_start) . "'";
//    $where .= " AND reg_stamp <= '" . strtotime($search_end . " 24:00:00") . "'";
            $where .= " AND reg_stamp >= '" . getSecondTime($search_start . " 00:00:00") . "'";
            $where .= " AND reg_stamp <= '" . getSecondTime($search_end . " 23:59:60") . "'";
        }
        else
        {
            if ($search_start !== "")
            {
//        $where .= " AND reg_stamp >= '" . strtotime($search_start . " 00:00:00") . "'";
//        $where .= " AND reg_stamp <= '" . strtotime($search_start . " 24:00:00") . "'";
                $where .= " AND reg_stamp >= '" . getSecondTime($search_start . " 00:00:00") . "'";
                $where .= " AND reg_stamp <= '" . getSecondTime($search_start . " 23:59:60") . "'";
            }
            if ($search_end !== "")
            {
//        $where .= " AND reg_stamp >= '" . strtotime($search_end . " 00:00:00") . "'";
//        $where .= " AND reg_stamp <= '" . strtotime($search_end . " 24:00:00") . "'";
                $where .= " AND reg_stamp >= '" . strtotime($search_end . " 00:00:00") . "'";
                $where .= " AND reg_stamp <= '" . strtotime($search_end . " 23:59:60") . "'";
            }
        }

//        $query = "SELECT 
//                    id,
//                    user_name,
//                    action,
//                    content,
//                    result,
//                    reg_stamp
//                    FROM " . $db_table_prefix . "logs " . $where . " ORDER BY " . $sort_by . " " . $sort_order . " LIMIT $page, $count";
//        echo $query;

        $stmt = $mysqli->prepare("SELECT 
                    id,
                    user_name,
                    action,
                    content,
                    result,
                    reg_stamp
                    FROM sys_logs " . $where . " ORDER BY " . $sort_by . " " . $sort_order . " LIMIT $page, $count");
        $stmt->execute();
        $stmt->bind_result($id, $user_name, $action, $content, $result, $reg_stamp);

        $row = array();
        while ($stmt->fetch())
        {
            $row[] = array('id' => $id, 'user_name' => $user_name, 'action' => $action, 'content' => $content, 'result' => $result, 'reg_stamp' => $reg_stamp);
        }

        $stmt->close();
        return ($row);
    }

}

?>