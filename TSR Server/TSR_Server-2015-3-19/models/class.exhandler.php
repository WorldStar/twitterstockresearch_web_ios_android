<?php

class ExceptionHandler
{

    public function getErrMsg()
    {
        return $_SESSION['err_msg'];
    }

    public static function showMsgDiag()
    {
        /*
         * <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
		  <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		  <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
         */
//        echo '
//		   <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
//		  <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
//		  <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
//			<script>
//			$(function() {
//				$("#dialog" ).dialog({
//				  width: 500,
//				  autoOpen: false,
//				  show: {
//					effect: "blind",
//					duration: 500
//				  },
//				  hide: {
//					effect: "blind",
//					duration: 500
//				  }
//				}); 
//				$( "#dialog" ).dialog( "open" );
//			});
//			</script>
//			
//			<div id="dialog" title="Note: TwitterStockResearch ">
//			' . $_SESSION['err_msg'] . '
//			</div>
//		';
        echo "
                <script src='https://code.jquery.com/jquery-1.9.1.js'></script>
		<link rel='stylesheet' href='https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css' />
		<script src='https://code.jquery.com/ui/1.10.3/jquery-ui.js'></script>
                <script>
                    $(function() {
                      $( '#dialog-message' ).dialog({
                        modal: true,
                        width: 'auto',
                        height: 'auto',
                        resizable: false,
                        maxHeight: 800,
                        buttons: {
                          Ok: function() {
                            $( this ).dialog( 'close' );
                          }
                        }
                      });
                    });
                </script>
                
                <div id='dialog-message' title='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Note: TwitterStockResearch'>
                    <div class='cnfx_content'>
                        <span class='dialog_icon icon_info_b' style='float:left; margin:0 7px 50px 0;'></span>
                        <div class='dialog_f_c'>
                            " . $_SESSION['err_msg'] . "
                        </div>
                    </div>
                </div>
             ";

        $_SESSION['neededToShowError'] = false;
    }

    public static function needToShowErrMsg($msg)
    {
        $_SESSION['err_msg'] = $msg;

        $_SESSION['neededToShowError'] = true;
    }

    public static function neededToShowMsg()
    {
        if (isset($_SESSION['neededToShowError']))
        {
            return $_SESSION['neededToShowError'];
        }
    }

}

?>
