<?php

class Header
{

    public static function show()
    {
        $xtea = new XTEA(NULL);
        $profile_link = PARAM_GOTO . "=" . base64_encode($xtea->encrypt('profile'));
        $login_link = PARAM_RUN . "=" . base64_encode($xtea->encrypt(POST_SIGN_OUT));
        $signout_link = PARAM_RUN . "=" . base64_encode($xtea->encrypt(POST_SIGN_OUT));

        if (isset($_SESSION['2376fa']))
        {
            echo '
                <div class="row-fluid header-container">
                     <div style="float:left; margin-left:70px;">
                             <a href="' . SITE_URL . '" target="_self" title="tdameritrade"><img src="img/logo.png"/></a>
                     </div>  
                     <div style="float:right; margin-right:80px;">  
                            <a href="javascript:;" title=""></a>
                            <a href="' . SITE_URL . '/index.php?' . $profile_link . '" target="_self" title="Edit your profile">My Profile</a>
                            <a href="' . SITE_URL . '/post.php?' . $signout_link . '" target="_self" title="Sign out">Signout</a>
                     </div>  
                </div>
		';
        }
        else
        {
            echo '
                <div class="row-fluid header-container">
                     <div style="float:left; margin-left:70px;">
                             <a href="' . SITE_URL . '" target="_self" title="tdameritrade"><img src="img/logo.png"/></a>
                     </div>  
                     <div style="float:right; margin-right:80px;">  
                             <a href="' . SITE_URL . '/post.php?' . $login_link . '" target="_self" title="Sign in">Log in</a>
                     </div>  
                </div>
                ';
        }
    }

}

?>