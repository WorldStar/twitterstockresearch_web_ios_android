<?php

require_once("seed.php");

class Crypto
{

    private $serverEncoding = 'UTF-8';
    private $innerEncoding = 'UTF-8';
    private $block = 16;
    private $pbUserKey = array(49, -97, 101, -52, 57, 97, 49, 97, -49, 101, 98, 49, 50, -48, 55, 50); // 사용자키
    private $IV = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16); // 초기화 벡터

    public function __contruct()
    {
        
    }

    /**
     * SEED128 + CBC + PKCS#5
     *
     * @param string $str
     *
     * @return string Encrypted string of hex type
     */
    public function encrypt($str)
    {
        //$str = iconv($this->serverEncoding, $this->innerEncoding, $str);
        $planBytes = array_slice(unpack('c*', $str), 0);
        if (count($planBytes) == 0)
        {
            return $str;
        }

        $seed = new Seed();
        $seed->SeedRoundKey($pdwRoundKey, $this->pbUserKey);

        $planBytesLength = count($planBytes);
        $start = 0;
        $end = 0;
        $cipherBlockBytes = array();
        $cbcBlockBytes = array();
        $this->arraycopy($this->IV, 0, $cbcBlockBytes, 0, $this->block);
        $ret = null;
        while ($end < $planBytesLength)
        {
            $end = $start + $this->block;
            if ($end > $planBytesLength)
            {
                $end = $planBytesLength;
            }

            $this->arraycopy($planBytes, $start, $cipherBlockBytes, 0, $end - $start);

            $nPad = $this->block - ($end - $start);
            for ($i = ($end - $start); $i < $this->block; $i++)
            {
                $cipherBlockBytes[$i] = $nPad;
            }

            $this->xor16($cipherBlockBytes, $cbcBlockBytes, $cipherBlockBytes);
            $seed->SeedEncrypt($cipherBlockBytes, $pdwRoundKey, $encryptCbcBlockBytes);
            $this->arraycopy($encryptCbcBlockBytes, 0, $cbcBlockBytes, 0, $this->block);

            foreach ($encryptCbcBlockBytes as $encryptedString)
            {
                $ret .= bin2hex(chr($encryptedString));
            }
            $start = $end;
        }
        return $ret;
    }

    /**
     * SEED128 + CBC + PKCS#5
     *
     * @param string $str
     *
     * @return string Return decrypted string.
     */
    public function decrypt($str)
    {
        $planBytes = array();
        for ($i = 0; $i < strlen($str); $i += 2)
        {
            $planBytes[] = $this->convertMinus128(hexdec(substr($str, $i, 2)));
        }

        if (count($planBytes) == 0)
        {
            return $str;
        }

        $seed = new Seed();
        $seed->SeedRoundKey($pdwRoundKey, $this->pbUserKey);

        $planBytesLength = count($planBytes);
        $start = 0;
        $isEnd = false;
        $cipherBlockBytes = array();
        $cbcBlockBytes = array();
        $thisEE = array();
        $this->arraycopy($this->IV, 0, $cbcBlockBytes, 0, $this->block);

        while (!$isEnd)
        {
            if ($start + $this->block >= $planBytesLength)
            {
                $isEnd = true;
            }

            $this->arraycopy($planBytes, $start, $cipherBlockBytes, 0, $this->block);
            $seed->SeedDecrypt($cipherBlockBytes, $pdwRoundKey, $ee);
            $this->xor16($thisEE, $cbcBlockBytes, $ee);
            $thisEE = $this->convertMinus128($thisEE);

            $this->arraycopy($thisEE, 0, $planBytes, $start, $this->block);
            $this->arraycopy($cipherBlockBytes, 0, $cbcBlockBytes, 0, $this->block);
            $start += $this->block;
        }
        //$rst = iconv($this->innerEncoding, $this->serverEncoding, call_user_func_array("pack", array_merge(array("c*"), $planBytes)));
        $rst = call_user_func_array("pack", array_merge(array("c*"), $planBytes));
        return $this->pkcs5Unpad($rst);
    }

    /**
     * Java arraycopy
     *
     * @param array $src Source array.
     * @param integer $srcPos Start position of source array.
     * @param array $dest Destination array.
     * @param integer $destPos Start position of destination array.
     * @param integer $length Integer to count the arrays of..
     *
     * @return array Return destination source array.
     */
    public function arraycopy($src, $srcPos, &$dest, $destPos, $length)
    {
        for ($i = $srcPos; $i < $srcPos + $length; $i++)
        {
            $dest[$destPos] = $src[$i];
            $destPos++;
        }
    }

    /**
     * XOR
     *
     * @param array $t1
     * @param array $x1
     * @param array $x2
     *
     * @return array
     */
    public function xor16(&$t, $x1, $x2)
    {
        $t[0] = $x1[0] ^ $x2[0];
        $t[1] = $x1[1] ^ $x2[1];
        $t[2] = $x1[2] ^ $x2[2];
        $t[3] = $x1[3] ^ $x2[3];
        $t[4] = $x1[4] ^ $x2[4];
        $t[5] = $x1[5] ^ $x2[5];
        $t[6] = $x1[6] ^ $x2[6];
        $t[7] = $x1[7] ^ $x2[7];
        $t[8] = $x1[8] ^ $x2[8];
        $t[9] = $x1[9] ^ $x2[9];
        $t[10] = $x1[10] ^ $x2[10];
        $t[11] = $x1[11] ^ $x2[11];
        $t[12] = $x1[12] ^ $x2[12];
        $t[13] = $x1[13] ^ $x2[13];
        $t[14] = $x1[14] ^ $x2[14];
        $t[15] = $x1[15] ^ $x2[15];
    }

    private function convertMinus128($bytes)
    {
        if (PHP_INT_SIZE > 4)
        { // 64비트가 아닌 경우 그대로 출력
            return $bytes;
        }

        if (is_array($bytes))
        {
            $ret = array();
            foreach ($bytes as $val)
            {
                $ret[] = (($val + 128) % 256) - 128;
            }
            return $ret;
        }
        return (($bytes + 128) % 256) - 128;
    }

    private function convertModulo256($hex)
    {
        if (is_array($hex))
        {
            $ret = array();
            foreach ($hex as $val)
            {
                $ret[] = ($val % 256) - 128;
            }
            return $ret;
        }
        return ($hex % 256) - 128;
    }

    /**
     * Padding
     *
     * @param array $xx
     *
     * @return int
     */
    private function unPddingCntPKCS7($xx)
    {
        $xxxxx = $xx[count($xx) - 1];

        if ($xxxxx > 16)
        {
            return 0;
        }

        if ($xxxxx < 0)
        {
            return 0;
        }

        for ($i = (16 - $xxxxx); $i < count($xx); $i++)
        {
            if ($xx[$i] != $xxxxx)
            {
                return 0;
            }
        }
        return $xxxxx;
    }

    /**
     * Little Endian
     *
     * @param int $int
     *
     * @return boolean
     */
    private function isLittleEndian($int)
    {
        $p = pack('S', $int);

        return $int === current(unpack('v', $p));
    }

    /**
     * Biginteger
     *
     * @param bigint $unsignedBigEndianInteger
     *
     * @return array
     */
    private function unpackBigInteger($unsignedBigEndianInteger)
    {
        $unpack = unpack('N', $unsignedBigEndianInteger);

        return $unpack;
    }

    public function pkcs5Pad($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    public function pkcs5Unpad($text)
    {
        $pad = ord($text {strlen($text) - 1});
        if ($pad > strlen($text))
            return $text;
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad)
            return $text;
        return substr($text, 0, - 1 * $pad);
    }

}

?>