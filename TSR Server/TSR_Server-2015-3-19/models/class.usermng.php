<?php

//require_once "/home/twitters/php/Mail.php"; // linux
//require_once 'Mail.php'; // windows

require_once 'PHPMailer/PHPMailerAutoload.php';

class UserManager
{

    //private $mysql;
    private $usr_tb = "user";
    private $reg_tb = "reged_user";
    public $convert;

    function __construct()
    {
//        $this->mysql = new MySQL(MY_HOST, MY_USER, MY_PASS);
//        $this->mysql->connect(MY_DB);
        $this->convert = new Security();
    }

    public function register($userinfo)
    {
        $secureUsername = $this->convert->myEncrypt($userinfo['usernamesignup']);
        $secureEmail = $this->convert->myEncrypt($userinfo['emailsignup']);

        // check the duplication for user table
        if ($this->existUserName($secureUsername))
        {
            return DUPLICATED_USERNAME;
        }

        if ($this->existUserEmail($secureEmail))
        {
            return DUPLICATED_EMAIL;
        }

        // check the duplication for reg_user table
        if ($this->existRegedUserName($secureUsername))
        {
            return DUPLICATED_USERNAME;
        }

        if ($this->existRegedUserEmail($secureEmail))
        {
            return DUPLICATED_EMAIL;
        }

        // send email
        $reg_mail_url = SITE_URL . "/post.php?b5fc5=2&c45b=" . urlencode(base64_encode($secureEmail));
        $reg_mail_url_tag = "<a href='" . $reg_mail_url . "'>" . $reg_mail_url . "</a>";
        $subject = "Confirmation email";
        $reg_mail_content = "Welcome to Twitter Stock Research!<br><br>Our site is designed in a crisp, clean format that allows users to view and compare stock charts with recent tweets and news articles. <br><br>Please click on the below link to activate your account.<br><br>" . $reg_mail_url_tag . "<br><br>If you did not sign up for this account, please contact us at support@twitterstockresearch.com <br><br>Best Regards,<br>Customer Support";

        $res = $this->sendMail($userinfo['emailsignup'], $subject, $reg_mail_content);

        if ($res === SUCCESS)
        {
            // store into the reged_user table
            $regedUserInfo = array('uname' => $secureUsername, 'email' => $secureEmail, 'pwd' => $this->convert->myEncrypt($userinfo['passwordsignup']), 'fname' => '', 'email_2' => '', 'addr' => '');
            $res = $this->addRegedUser($regedUserInfo);

            if ($res === SUCCESS)
            {
                $action = "User Register (success)";
                $content = "User: " . $userinfo['usernamesignup'] . "; email: " . $userinfo['emailsignup'];
                secureLog($secureUsername, $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);
            }
            else
            {
                $action = "User Register (failed)";
                $content = "User: " . $userinfo['usernamesignup'] . "; email: " . $userinfo['emailsignup'];
                secureLog($secureUsername, $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);
            }

            return $res;
        }
        else
        {
            $res .= SENT_CONFIRM_MAIL_FAILED;
            return $res;
        }
    }
     public function mobile_register($userinfo)
    {
        $secureUsername = $this->convert->myEncrypt($userinfo['usernamesignup']);
        $secureEmail = $this->convert->myEncrypt($userinfo['emailsignup']);

        // check the duplication for user table
        if ($this->existUserName($secureUsername))
        {
            return DUPLICATED_USERNAME;
        }

        if ($this->existUserEmail($secureEmail))
        {
            return DUPLICATED_EMAIL;
        }

        // check the duplication for reg_user table
        if ($this->existRegedUserName($secureUsername))
        {
            return DUPLICATED_USERNAME;
        }

        if ($this->existRegedUserEmail($secureEmail))
        {
            return DUPLICATED_EMAIL;
        }
        // send email
        $reg_mail_url = SITE_URL . "/post.php?b5fc5=2&c45b=" . urlencode(base64_encode($secureEmail));
        $reg_mail_url_tag = "<a href='" . $reg_mail_url . "'>" . $reg_mail_url . "</a>";
		
        $subject = "verify code for TSR";
		$verifycode = substr($secureUsername, 10, 5);
        $reg_mail_content = "Welcome to Twitter Stock Research!<br><br>Our site is designed in a crisp, clean format that allows users to view and compare stock charts with recent tweets and news articles. <br><br>Please use the below verify code to activate your account.<br><br>" . $verifycode . "<br><br>If you did not sign up for this account, please contact us at support@twitterstockresearch.com <br><br>Best Regards,<br>Customer Support";

        $res = $this->sendMail($userinfo['emailsignup'], $subject, $reg_mail_content);
		//$res = SUCCESS;
        if ($res === SUCCESS)
        {
            // store into the reged_user table
            $regedUserInfo = array('uname' => $secureUsername, 'email' => $secureEmail, 'pwd' => $this->convert->myEncrypt($userinfo['passwordsignup']), 'fname' => '', 'email_2' => '', 'addr' => '', 'v_code' => $verifycode);
            
   
			$res = $this->addRegedMobileUser($regedUserInfo);

            if ($res === SUCCESS)
            {
                $action = "User Register (success)";
                $content = "User: " . $userinfo['usernamesignup'] . "; email: " . $userinfo['emailsignup'];
                secureLog($secureUsername, $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);
            }
            else
            {
                $action = "User Register (failed)";
                $content = "User: " . $userinfo['usernamesignup'] . "; email: " . $userinfo['emailsignup'];
                secureLog($secureUsername, $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);
            }

            return $res;
        }
        else
        {
            $res .= SENT_CONFIRM_MAIL_FAILED;
            return $res;
        }
    }
    public function forgotPassword($userinfo)
    {
        // check the mail
        $secureEmail = $this->convert->myEncrypt($userinfo['emailforgot']);

        if (!$this->existUserEmail($secureEmail))
        {
            if (!$this->existUserEmail2($secureEmail))
            {
                // log
                $action = "User Password (forget)";
                $content = "Email: " . $userinfo['emailforgot'] . ": Email address can't be confirmed";
                secureLog($this->convert->myEncrypt($userinfo['emailforgot']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

                return SENT_CONFIRM_MAIL_FAILED;
            }
        }

        // send email
        $reg_mail_content = SITE_URL . "/post.php?b5fc5=2&c45b=" . urlencode(base64_encode($secureEmail));
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()';
        $randomString = '';
        for ($i = 0; $i < 10; $i++)
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        $subject = "Password Reset";
        $reg_mail_content = "Greetings from Twitter Stock Research!<br><br>A password reset was requested for your user account.  Please log in using following password and change your password.  <br><br>Your password: " . $randomString . "<br>If you did not request this password reset, please contact us at support@twitterstockresearch.com  <br>Best Regards,<br>Customer Support";
        $res = $this->sendMail($userinfo['emailforgot'], $subject, $reg_mail_content);

        if ($res === SUCCESS)
        {
            // log
            $action = "User Password (forget)";
            $content = "Email: " . $userinfo['emailforgot'] . ": Send confirm email";
            secureLog($this->convert->myEncrypt($userinfo['emailforgot']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);

            // store new pass into the user table
            $res = $this->updateUserPassword($this->convert->myEncrypt($randomString), $secureEmail);

            // log
            $action = "User Password (forget)";
            $content = "Email: " . $userinfo['emailforgot'] . ": Change password";
            secureLog($this->convert->myEncrypt($userinfo['emailforgot']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), $res === SUCCESS ? 1 : 0);

            return $res;
        }
        else
        {
            // log
            $action = "User Password (forget)";
            $content = "Email: " . $userinfo['emailforgot'] . ": Email address can't be confirmed";
            secureLog($this->convert->myEncrypt($userinfo['emailforgot']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

            $res .= SENT_CONFIRM_MAIL_FAILED;
            return $res;
        }
    }

    public function reRegister($userinfo)
    {
        $secureEmail = $this->convert->myEncrypt($userinfo['emailsignup']);

        // check the duplication
        if ($this->existUserEmail($secureEmail))
        {
            return DUPLICATED_EMAIL;
        }

        if ($this->existRegedUserEmail($secureEmail))
        {
            return DUPLICATED_EMAIL;
        }

        // send email
        $reg_mail_content = SITE_URL . "/post.php?b5fc5=2&c45b=" . urlencode(base64_encode($this->convert->myEncrypt($userinfo['emailsignup'])));
        $subject = "Confirmation email";
        $reg_mail_content = "Welcome to Twitter Stock Research!<br><br>Our site is designed in a crisp, clean format that allows users to view and compare stock charts with recent tweets and news articles. <br><br>Please click on the below link to activate your account.<br><br>" . $reg_mail_content . "<br><br>If you did not sign up for this account, please contact us at support@twitterstockresearch.com <br><br>Best Regards,<br>Customer Support";
        $res = $this->sendMail($userinfo['emailsignup'], $subject, $reg_mail_content);

        if ($res === SUCCESS)
        {
            // store into the reged_user table
            $newRegedUserInfo = array('uname' => $this->convert->myEncrypt($userinfo['usernamesignup']), 'email' => $this->convert->myEncrypt($userinfo['emailsignup']), 'pwd' => $userinfo['passwordsignup'], 'fname' => $this->convert->myEncrypt($userinfo['fname']), 'email_2' => $this->convert->myEncrypt($userinfo['email_2']), 'addr' => $this->convert->myEncrypt($userinfo['addr']));
            $res = $this->addRegedUser($newRegedUserInfo);
            return $res;
        }
        else
        {
            $res .= SENT_CONFIRM_MAIL_FAILED;
            return $res;
        }
    }

    public function confirmEmail($confirm)
    {
        $para = $this->convert->myDecrypt(base64_decode(urldecode($confirm['c45b'])));
        $securePara = $this->convert->myEncrypt($para);

        if (!$this->existRegedUserEmail($securePara))
        {
            $action = "User Confirm (failed)";
            $content = "User: " . $para . "; The link was expired.";
            secureLog($securePara, $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

            return EMAIL_CONFIRM_FAILED;
        }

        // if exist email, then add user from reged user
        $regUserDetails = $this->fetchRegedUserDetails(NULL, $this->convert->myEncrypt($para));
        $res = $this->addUser($regUserDetails);

        if ($res)
        {
            $action = "User Add (success)";
            $content = "User: " . $para . "; Email confirmed.";
            secureLog($securePara, $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);

            $this->deleteRegedUser($this->convert->myEncrypt($para));

            return $this->loginWithout($para);
        }

        return EMAIL_CONFIRM_FAILED;
    }

    private function loginWithout($email)
    {
        $userDetails = $this->fetchUserDetails(NULL, $this->convert->myEncrypt($email));
        if (sizeof($userDetails) === 0)
        {
            return WRONG_EMAIL;
        }

        // log
        $action = "User Login (session)";
        $content = "User: " . $email . ": Logged in";
        secureLog($this->convert->myEncrypt($email), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);

        $_SESSION['2376fa'] = base64_encode($this->convert->myEncrypt($userDetails['uid']));

        return SUCCESS;
    }
    public function mobile_login($array)
    {
        $email = $this->convert->myEncrypt($array['username']);
        $pwd = $this->convert->myEncrypt($array['password']);

        if ($this->existRegedUserEmail($email))
        {
            // log
            $action = "User Login (failed)";
            $content = "User: " . $array['username'] . ": The email is not verified yet.";
            secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);
			if ($this->existUserEmail($email))
			{
				//return DUPLICATED_EMAIL;
			}else{
				return UNCONFIRMED_EMAIL;
			}
        }

        if (!$this->existUserEmail($email) && !$this->existUserName($email))
        {
            // log
            $action = "User Login (failed)";
            $content = "User: " . $array['username'] . ": User name or email is wrong.";
            secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

            return WRONG_EMAIL;
        }

        global $mysqli;

        $stmt = $mysqli->prepare("SELECT uid
                    FROM " . $this->usr_tb . "
                    WHERE (
                    email = ?
                    OR
                    uname = ?)
                    AND
                    pwd = '" . $pwd . "'
                    LIMIT 1");
        $stmt->bind_param("ss", $email, $pwd);
        $stmt->execute();
        $stmt->bind_result($uid);

        $row = array();
        while ($stmt->fetch())
        {
            $row = array('uid' => $uid);
        }

        $stmt->close();

        if (sizeof($row) === 0)
        {
            // log
            $action = "User Login (failed)";
            $content = "User: " . $array['username'] . ": Password is wrong.";
            secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

            return WRONG_PASSWORD;
        }

        $_SESSION['2376fa'] = base64_encode($this->convert->myEncrypt($row['uid']));

        // log
        $action = "User Login (session)";
        $content = "User: " . $array['username'] . ": Logged in";
        secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);

        return SUCCESS;
    }
    public function addMobileUser($newUser)
    {
        $res = SUCCESS;
		$secureEmail = $this->convert->myEncrypt($newUser['emailsignup']);

        // check the duplication for user table
        
        if ($this->existUserEmail($secureEmail))
        {
            return DUPLICATED_EMAIL;
        }
        global $mysqli;
        $column = "v_code";
        $data = $newUser['userverify'];

        $stmt = $mysqli->prepare("SELECT 
                    ruid,
                    uname,
                    email,
                    pwd,
                    fname,                              
                    email_2,
                    addr,
					v_code
                    FROM " . $this->reg_tb . "
                    WHERE
                    $column = ?
                    LIMIT 1");
        $stmt->bind_param("s", $data);

        $stmt->execute();
        $stmt->bind_result($uid, $uname, $email, $pwd, $fname, $email_2, $addr, $v_code);

        $row = array();
        while ($stmt->fetch())
        {
            $row = array('uid' => $uid, 'uname' => $uname, 'email' => $email, 'pwd' => $pwd, 'fname' => $fname, 'email_2' => $email_2, 'addr' => $addr, 'v_code' => $v_code);
			
        }

        $stmt->close();

		if(sizeof($row) > 0){
			$stmt = $mysqli->prepare("INSERT INTO " . $this->usr_tb . " (
                                    uname,
                                    email,
                                    pwd
                                    )
                                    VALUES (
                                    ?,?,?
                                    )");

			$stmt->bind_param("sss", $row['uname'], $row['email'], $row['pwd']);

			if (!$stmt->execute())
			{
				$res = SQLFAILED;
			}

			$stmt->close();

			return $res;
		}else{
		   return FAILED;
		}
		
		
        
    }
    public function addRegedMobileUser($newUser)
    {
        $res = SUCCESS;

        global $mysqli;

        $stmt = $mysqli->prepare("INSERT INTO " . $this->reg_tb . " (
                                    uname,
                                    email,
                                    pwd,
                                    fname,
                                    email_2,
                                    addr,
				   v_code
                                    )
                                    VALUES (
                                    ?,?,?,?,?,?,?
                                    )");

        $stmt->bind_param("sssssss", $newUser['uname'], $newUser['email'], $newUser['pwd'], $newUser['fname'], $newUser['email_2'], $newUser['addr'],$newUser['v_code']);
        if (!$stmt->execute())
        {
            $res = SQLFAILED;
        }
        $stmt->close();

        return $res;
    }
    public function login($array)
    {
        $email = $this->convert->myEncrypt($array['username']);
        $pwd = $this->convert->myEncrypt($array['password']);

        if ($this->existRegedUserEmail($email))
        {
            // log
            $action = "User Login (failed)";
            $content = "User: " . $array['username'] . ": The email is not verified yet.";
            secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

            return UNCONFIRMED_EMAIL;
        }

        if (!$this->existUserEmail($email) && !$this->existUserName($email))
        {
            // log
            $action = "User Login (failed)";
            $content = "User: " . $array['username'] . ": User name or email is wrong.";
            secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

            return WRONG_EMAIL;
        }

        global $mysqli;

        $stmt = $mysqli->prepare("SELECT uid
                    FROM " . $this->usr_tb . "
                    WHERE (
                    email = ?
                    OR
                    uname = ?)
                    AND
                    pwd = '" . $pwd . "'
                    LIMIT 1");
        $stmt->bind_param("ss", $email, $email);
        $stmt->execute();
        $stmt->bind_result($uid);

        $row = array();
        while ($stmt->fetch())
        {
            $row = array('uid' => $uid);
        }

        $stmt->close();

        if (sizeof($row) === 0)
        {
            // log
            $action = "User Login (failed)";
            $content = "User: " . $array['username'] . ": Password is wrong.";
            secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

            return WRONG_PASSWORD;
        }

        $_SESSION['2376fa'] = base64_encode($this->convert->myEncrypt($row['uid']));

        // log
        $action = "User Login (session)";
        $content = "User: " . $array['username'] . ": Logged in";
        secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);

        return SUCCESS;
    }

    public function loginAd($array)
    {
        $email = $this->convert->myEncrypt($array['username']);
        $pwd = $this->convert->myEncrypt($array['password']);

        global $mysqli;

        $stmt = $mysqli->prepare("SELECT *
                    FROM leon
                    WHERE
                    name = ?
                    LIMIT 1");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_returns = $stmt->num_rows;
        $stmt->close();

        if ($num_returns === 0)
        {
            // Non-verified
            $_SESSION['8ab9'] = ($_SESSION['8ab9'] === '' ? 0 : (intval($_SESSION['8ab9']) + 1));

            // log
            $action = "Admin Login (failed)";
            $content = "User: " . $array['username'] . ": User name or email is wrong.";
            secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

            return WRONG_EMAIL;
        }

        $stmt = $mysqli->prepare("SELECT auid
                    FROM leon
                    WHERE
                    name = ?
                    AND
                    pass = '" . $pwd . "'
                    LIMIT 1");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->bind_result($auid);

        $row = array();
        while ($stmt->fetch())
        {
            $row = array('auid' => $auid);
        }

        $stmt->close();

        if (sizeof($row) === 0)
        {
            // Non-registered
            $_SESSION['8ab9'] = ($_SESSION['8ab9'] === '' ? 0 : (intval($_SESSION['8ab9']) + 1));

            // log
            $action = "Admin Login (failed)";
            $content = "User: " . $array['username'] . ": Password is wrong.";
            secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

            return WRONG_PASSWORD;
        }

        $_SESSION['3d91n7s'] = base64_encode($this->convert->myEncrypt($row['auid']));

        // log
        $action = "Admin Login (session)";
        $content = "User: " . $array['username'] . ": Logged in";
        secureLog($this->convert->myEncrypt($array['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);

        return SUCCESS;
    }

    private function sendMail($to, $subject, $message)
    {
        $mail = new PHPMailer;

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'ssl://hobbit.mschosting.com';  // Specify main and backup server
        $mail->Port = 465;
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'twitters@twitterstockresearch.com';                            // SMTP username
        $mail->Password = '4Y2rnqr9J4';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

        $mail->From = 'twitters@twitterstockresearch.com';
        $mail->FromName = 'StockTwits';

        $mail->addAddress($to);

        $mail->Subject = $subject;
        $mail->Body = $message; // html
        //$mail->AltBody = $message; // text

        $mail->isHTML(true);

        if (!$mail->send())
        {
            $error = 'Message could not be sent.';
            $error .= 'Mailer Error: ' . $mail->ErrorInfo;
            return $error;
        }

        return SUCCESS;
    }

    public function sendMailAll($uids, $subject = '', $message = '')
    {
        $ret = SUCCESS;
        
        $crypt = new Security();

        $userDetails = $this->fetchAllUserDetails($uids);

        if (sizeof($userDetails) > 0)
        {
            $mail = new PHPMailer;

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'ssl://hobbit.mschosting.com';  // Specify main and backup server
            $mail->Port = 465;
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'twitters@twitterstockresearch.com';                            // SMTP username
            $mail->Password = '4Y2rnqr9J4';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

            $mail->From = 'twitters@twitterstockresearch.com';
            $mail->FromName = 'StockTwits';

            $toArryStr = "";
            $email = "";
            foreach ($userDetails as $userInfo)
            {
                $email = $crypt->myDecrypt($userInfo['email']);
                $mail->addAddress($email);
                if ($toArryStr === "")
                {
                    $toArryStr .= $email;
                }
                else
                {
                    $toArryStr .= ("," . $email);
                }
            }

            $mail->Subject = $subject;
            $mail->Body = $message; // html
            //$mail->AltBody = $message; // text

            $mail->isHTML(true);

            if (!$mail->send())
            {
                $error = 'Message could not be sent.';
                $error .= 'Mailer Error: ' . $mail->ErrorInfo;
                $ret = $error;
            }

            // log
            $action = "Admin User Management (send mail)";
            $content = "Email addresses: " . $toArryStr;
            secureLog($this->convert->myEncrypt("admin"), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), ($ret === SUCCESS ? 1 : 0));
        }

        return $ret;
    }

    public function signout()
    {
        $uid = $this->convert->myDecrypt(base64_decode($_SESSION['2376fa']));
        $userDetails = $this->fetchUserDetailsFromUID($uid);

        // log
        if (sizeof($userDetails) > 0)
        {
            $action = "User Logout (session)";
            $content = "User: " . $this->convert->myDecrypt($userDetails['uname']) . "; logged out.";
            secureLog($userDetails['uname'], $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);
        }

        unset($_SESSION['2376fa']);
        unset($_SESSION['guest']);
    }

    public function signoutAd()
    {
        unset($_SESSION['3d91n7s']);

        // log
        $action = "Admin Logout (session)";
        $content = "Administrator: " . "logged out.";
        secureLog($this->convert->myEncrypt("admin"), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);
    }

    /*
      public function getProfile($uid)
      {
      $sql = "SELECT * FROM " . $this->usr_tb . " WHERE uid='" . $this->convert->myDecrypt(base64_decode($uid)) . "'";
      $this->mysql->query($sql) or die($this->mysql->getErrors());

      if ($this->mysql->rowsReturned() === 0)
      { // Non-registered
      echo $uid;
      die('-Wrong UID!');
      }

      return $this->mysql->fetchArray();
      }
     */

    public function saveProfile($profile)
    {
        global $mysqli;

        $uid = $profile['uid']; //$this->convert->myDecrypt(base64_decode($profile['uid']));
        $secureEmail = $this->convert->myEncrypt($profile['email']);

        if ($this->existUserEmailForUpdate($uid, $secureEmail))
        {
            // update user info
            $new_reg = array(
                'uid' => $uid,
                'uname' => $this->convert->myEncrypt($profile['username']),
                'email' => $this->convert->myEncrypt($profile['email']),
                'fname' => $this->convert->myEncrypt($profile['fullname']),
                'email_2' => $this->convert->myEncrypt($profile['email2']),
                'addr' => $this->convert->myEncrypt($profile['addr'])
            );

            $res = $this->updateUser($new_reg);

            // log
            $action = "User Profile (update)";
            $content = "User: " . $profile['username'] . "; " . $profile['email'];
            secureLog($this->convert->myEncrypt($profile['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), ($res === SUCCESS ? 1 : 0));

            return $res;
        }
        else
        {
            // email is update, so check duplication and re register
            if ($this->existUserEmail($secureEmail))
            {
                // log
                $action = "User Profile (update)";
                $content = "User: " . $profile['username'] . "; " . $profile['email'] . " is already in use.";
                secureLog($this->convert->myEncrypt($profile['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), ($res === SUCCESS ? 1 : 0));

                return DUPLICATED_EMAIL;
            }

            $oldUserDetails = $this->fetchUserDetailsFromUID($profile['uid']);

            $new_reg = array(
                'uid' => $uid,
                'usernamesignup' => $profile['username'],
                'emailsignup' => $profile['email'],
                'passwordsignup' => $oldUserDetails['pwd'],
                'fname' => $profile['fullname'],
                'email_2' => $profile['email2'],
                'addr' => $profile['addr']
            );

            if ($res = $this->reRegister($new_reg) === SUCCESS)
            {
                // log
                $action = "User Profile (re register)";
                $content = "User: " . $profile['username'] . "; " . $profile['email'] . ": Email has been changed";
                secureLog($this->convert->myEncrypt($profile['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);

                $this->deleteUser($uid, FALSE);

                // log
                $action = "User Profile (delete)";
                $content = "User: " . $profile['username'] . "; " . $profile['username'] . " is deleted.";
                secureLog($this->convert->myEncrypt($profile['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);

                $this->signout();
                return EMAIL_CHANGED;
            }
            else
            {
                // log
                $action = "User Profile (Failed re register)";
                $content = "User: " . $profile['username'] . "; " . $profile['email'];
                secureLog($this->convert->myEncrypt($profile['username']), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);

                return $res;
            }
        }

        return SUCCESS;
    }

    /*
      public function remove($uid)
      {
      $sql = "DELETE FROM " . $this->usr_tb . " WHERE uid=" . $uid;
      $this->mysql->query($sql) or die($this->mysql->getErrors());

      return SUCCESS;
      }
     */

    public function deleteUser($uid, $isLog = TRUE)
    {
        $userDetails = $this->fetchUserDetailsFromUID($uid);
        $uname = $this->convert->myDecrypt($userDetails['uname']);

        global $mysqli;
        $i = 0;
        $stmt = $mysqli->prepare("DELETE FROM " . $this->usr_tb . " 
                    WHERE uid = ?");
        $stmt->bind_param("i", $uid);
        $stmt->execute();
        $stmt->close();

        // log
        if ($isLog)
        {
            $action = "Admin User Management (delete user)";
            $content = "User: " . $uname . "; " . $uname . " is deleted.";
            secureLog($this->convert->myEncrypt("admin"), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);
        }

        return $i;
    }

    public function listUsers($page, $interval)
    {
        $xtea = new XTEA(NULL);
        $page_link = PARAM_TYPE . "=" . base64_encode($xtea->encrypt('1')) . "&" . PARAM_PAGE . "=";

        $uids = array();
        $userData = $this->fetchAllUserDetails($uids, $page, $interval);

        echo '
            <div class="CSS_Table_Example" id="userTable">
                    <table >
                            <tr> 
                                    <td width="4%">No.</td>
                                    <td width="18%" >Full name</td>
                                    <td width="16%">User name</td>
                                    <td width="32%">Primary email</td>
                                    <td width="32%">Secondary email</td>
                                    <td width="30px">
                                            <input type="checkbox" name="allcheck" id="allcheck" class="tf">
                                            <label for="allcheck"><span></span></label>
                                    </td>
                            </tr>
            ';

        if (sizeof($userData) === 0)
        {
            echo '
                <tr>
                        <td colspan="6" class="msg">
                        -- There\'s no user! --
                        </td>
                </tr>
                ';
        }
        else
        {
            $start = ($page - 1) * $interval;
            $i = $start;
            foreach ($userData as $row)
            {
                $i ++;
                if ($row['fname'] === null || $row['fname'] === '')
                {
                    $fname = "";
                }
                else
                {
                    $fname = $this->convert->myDecrypt($row['fname']);
                }
                if ($row['email_2'] === null || $row['email_2'] === '')
                {
                    $email_2 = "";
                }
                else
                {
                    $email_2 = $this->convert->myDecrypt($row['email_2']);
                }
                echo '  
                    <tr>
                        <td>' . $i . '</td>
                        <td>' . $fname . '</td>
                        <td id="uname' . $row['uid'] . '">' . $this->convert->myDecrypt($row['uname']) . '</td>
                        <td>' . $this->convert->myDecrypt($row['email']) . '</td>
                        <td>' . $email_2 . '</td>
                        <td>
                                <input class="tf" type="checkbox" name="check' . ($i - $start) . '" id="check' . ($i - $start) . '" value="' . $row['uid'] . '">
                                <label for="check' . ($i - $start) . '"><span></span></label>
                        </td>
                    </tr>
                    ';
            }
        }

        echo '
                    </table>
                </div>
            ';

        $total = $this->getTotalCount($this->usr_tb);
        $total_pages = intval($total / $interval);
        if ($total_pages > 0 && ($total % $interval) > 0)
        {
            $total_pages = $total_pages + 1;
        }

        echo '
		<div style="height:50px;"></div>
		<div class="page_control">
			<ul id="userTableController">
            ';

        if ($page > 5)
            echo '
                    <li><a class="btn btn-info custom-font12" href="index.php?">' . $page_link . base64_encode($xtea->encrypt(1)) . '</a></li><li><a href="javascript:;">...</a></li>
                ';

        for ($i = $page - 3; $i < $page + 4; $i ++)
        {
            if ($i < 1)
            {
                continue;
            }
            if ($i > $total_pages)
            {
                break;
            }

            if ($i === $page)
            {
                echo '
			<li><a class="btn btn-success custom-font12" href="index.php?' . $page_link . base64_encode($xtea->encrypt($i)) . '">' . $i . '</a></li>
                    ';
            }
            else
            {
                echo '
			<li><a class="btn btn-info custom-font12" href="index.php?' . $page_link . base64_encode($xtea->encrypt($i)) . '">' . $i . '</a></li>
                    ';
            }
        }

        if ($page < $total_pages - 4)
        {
            echo '
                    <li><a href="javascript:;">...</a></li><li><a class="btn btn-info custom-font12" href="index.php?' . $page_link . base64_encode($xtea->encrypt($total_pages)) . '">' . ((int) $total_pages) . '</a></li>
		';
        }

        echo '
			</ul> 
			
			<input type="button" class="btn btn-info custom-font12" value="DELETE" id="btnDeleteUser" name="btnDeleteUser" />
			<input type="button" class="btn btn-info custom-font12" value="SEND MAIL" id="btnSendMail" name="btnSendMail" />
		</div>
		
		<form action="post.php" method="post" id="user_mng_frm">
			<input type="hidden" name="34b9c" value="1">
			<input type="hidden" name="page" value="' . $page . '">
			<input type="hidden" name="seled_uids" id="seled_uids">
			<input type="hidden" name="req" id="req">
		</form>
            ';
    }

    public function listLogs($page, $interval, $search_start = "", $search_end = "")
    {
        $xtea = new XTEA(NULL);
        $page_link = PARAM_TYPE . "=" . base64_encode($xtea->encrypt('4')) . "&" . PARAM_PAGE . "=";

        // Fetch information for all users
        $logsClass = new SysLogs();
        $logData = $logsClass->fetchAllSysLogs(($page - 1) * $interval, $interval, "reg_stamp", "desc", $search_start, $search_end);

        echo '
            <div class="CSS_Table_Example" id="userTable">
                    <table >
                            <tr> 
                                    <td width="3%">&nbsp;</td>
                                    <td width="15%">Time</td>
                                    <td width="15%" >User</td>
                                    <td width="20%">Type</td>
                                    <td width="47%">Content</td>
                            </tr>
            ';

        if (sizeof($logData) === 0)
        {
            echo '
                <tr>
                        <td colspan="6" class="msg">
                        -- There\'s no log! --
                        </td>
                </tr>
                ';
        }
        else
        {
            $start = ($page - 1) * $interval;
            $i = $start;
            foreach ($logData as $row)
            {
                $result = '<img src="../img/notification_icon_n.png" />';
                if (intval($row['result']) === 0)
                {
                    $result = '<img src="../img/notification_icon_w.png" />';
                }

                $username = $this->convert->myDecrypt($row['user_name']);
                $action = $this->convert->myDecrypt($row['action']);
                $content = $this->convert->myDecrypt($row['content']);

                echo '  
                    <tr>
                        <td>' . $result . '</td>
                        <td>' . date("j M, Y h:i:s A", $row['reg_stamp']) . '</td>
                        <td>' . $username . '</td>
                        <td>' . $action . '</td>
                        <td>' . $content . '</td>
                    </tr>
                    ';
            }
        }

        echo '
                    </table>
                </div>
            ';

        $total = $this->getTotalCount("sys_logs");
        $total_pages = intval($total / $interval);
        if (($total % $interval) > 0)
        {
            $total_pages = $total_pages + 1;
        }

        echo '
		<div style="height:30px;"></div>
		<div class="page_control">
			<ul id="userTableController">
            ';

        if ($page > 5)
            echo '
                    <li><a class="btn btn-info custom-font12" href="index.php?' . $page_link . base64_encode($xtea->encrypt('4')) . '">1</a></li><li><a href="javascript:;">...</a></li>
                ';

        for ($i = $page - 3; $i < $page + 4; $i ++)
        {
            if ($i < 1)
            {
                continue;
            }
            if ($i > $total_pages)
            {
                break;
            }

            if ($i === $page)
            {
                echo '
			<li><a class="btn btn-success custom-font12" href="index.php?' . $page_link . base64_encode($xtea->encrypt($i)) . '">' . $i . '</a></li>
                    ';
            }
            else
            {
                echo '
			<li><a class="btn btn-info custom-font12" href="index.php?' . $page_link . base64_encode($xtea->encrypt($i)) . '">' . $i . '</a></li>
                    ';
            }
        }

        if ($page < $total_pages - 4)
        {
            echo '
                    <li><a href="javascript:;">...</a></li><li><a class="btn btn-info custom-font12" href="index.php?' . $page_link . base64_encode($xtea->encrypt($total_pages)) . '">' . ((int) $total_pages) . '</a></li>
		';
        }

        echo '
			</ul> 
		</div>
		
		<form action="post.php" method="post" id="user_mng_frm">
			<input type="hidden" name="34b9c" value="1">
			<input type="hidden" name="page" value="' . $page . '">
			<input type="hidden" name="seled_uids" id="seled_uids">
			<input type="hidden" name="req" id="req">
		</form>
            ';
    }

    public function changeAdminPass($array)
    {
        $securePass = $this->convert->myEncrypt($array['passwordsignup']);
        $auid = $this->convert->myDecrypt(base64_decode($_SESSION['3d91n7s']));

        $res = SUCCESS;
        global $mysqli;

        $stmt = $mysqli->prepare("UPDATE leon
                    SET 
                    pass = ?
                    WHERE
                    auid = ?");
        $stmt->bind_param("si", $securePass, $auid);

        if ($stmt->execute())
        {
            $res = SUCCESS;
        }
        else
        {
            $res = SQLFAILED;
        }

        $stmt->close();

        // log
        $action = "Admin Password (change)";
        if ($res === SUCCESS)
        {
            $content = "Admin: admin; Changed password.";
            secureLog($this->convert->myEncrypt("admin"), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);
        }
        else
        {
            $content = "Admin: " . "admin; Failed changed password.";
            secureLog($this->convert->myEncrypt("admin"), $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);
        }

        return $res;
    }

    public function changeUserPass($userinfo)
    {
        $securePass = $this->convert->myEncrypt($userinfo['passwordsignup']);
        $uid = $this->convert->myDecrypt(base64_decode($_SESSION['2376fa']));

        $userDetails = $this->fetchUserDetailsFromUID($uid);

        $res = $this->updateUserPassword($securePass, $userDetails['email']);

        // log
        $action = "User Password (change)";
        if ($res === SUCCESS)
        {
            $content = "Email: " . $this->convert->myDecrypt($userDetails['email']) . ": Changed password.";
            secureLog($userDetails['fname'], $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);
        }
        else
        {
            $content = "Email: " . $this->convert->myDecrypt($userDetails['email']) . ": Failed changed password.";
            secureLog($userDetails['fname'], $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 0);
        }
        return $res;
    }

    public function fetchAllUserDetails($uids, $page = 0, $interval = 0)
    {
        global $mysqli;

        $where = "";
        foreach ($uids as $uid)
        {
            if ($uid === "")
            {
                continue;
            }

            if ($where === "")
            {
                $where .= "uid = '" . $uid . "'";
            }
            else
            {
                $where .= " OR uid = '" . $uid . "'";
            }
        }

        if ($where !== "")
        {
            $where = " WHERE " . $where;
        }

        $start = ($page - 1) * $interval;

        $limit = "";
        if ($interval !== 0)
        {
            $limit = " LIMIT " . $start . ", " . $interval;
        }

        $stmt = $mysqli->prepare("SELECT 
                    uid,
                    uname,
                    email,
                    pwd,
                    fname,                              
                    email_2,
                    addr
                    FROM " . $this->usr_tb . $where . " ORDER BY uid DESC" . $limit);
        $stmt->execute();
        $stmt->bind_result($uid, $uname, $email, $pwd, $fname, $email_2, $addr);

        $row = array();
        while ($stmt->fetch())
        {
            $row[] = array('uid' => $uid, 'uname' => $uname, 'email' => $email, 'pwd' => $pwd, 'fname' => $fname, 'email_2' => $email_2, 'addr' => $addr);
        }

        $stmt->close();
        return ($row);
    }

    public function fetchUserDetails($uname, $email = NULL, $email2 = NULL)
    {
        global $mysqli;

        if ($uname !== NULL)
        {
            $column = "uname";
            $data = $uname;
        }
        else if ($email !== NULL)
        {
            $column = "email";
            $data = $email;
        }
        else if ($email2 !== NULL)
        {
            $column = "email_2";
            $data = $email;
        }

        $stmt = $mysqli->prepare("SELECT 
                    uid,
                    uname,
                    email,
                    pwd,
                    fname,                              
                    email_2,
                    addr
                    FROM " . $this->usr_tb . "
                    WHERE
                    $column = ?
                    LIMIT 1");
        $stmt->bind_param("s", $data);

        $stmt->execute();
        $stmt->bind_result($uid, $uname, $email, $pwd, $fname, $email_2, $addr);

        $row = array();
        while ($stmt->fetch())
        {
            $row = array('uid' => $uid, 'uname' => $uname, 'email' => $email, 'pwd' => $pwd, 'fname' => $fname, 'email_2' => $email_2, 'addr' => $addr);
        }

        $stmt->close();
        return ($row);
    }

    public function fetchUserDetailsFromUID($uid)
    {
        global $mysqli;

        $stmt = $mysqli->prepare("SELECT 
                    uid,
                    uname,
                    email,
                    pwd,
                    fname,                              
                    email_2,
                    addr
                    FROM " . $this->usr_tb . "
                    WHERE
                    uid = ?
                    LIMIT 1");
        $stmt->bind_param("i", $uid);

        $stmt->execute();
        $stmt->bind_result($uid, $uname, $email, $pwd, $fname, $email_2, $addr);

        $row = array();
        while ($stmt->fetch())
        {
            $row = array('uid' => $uid, 'uname' => $uname, 'email' => $email, 'pwd' => $pwd, 'fname' => $fname, 'email_2' => $email_2, 'addr' => $addr);
        }

        $stmt->close();
        return ($row);
    }

    public function addUser($newUser)
    {
        $res = SUCCESS;

        global $mysqli;

        $stmt = $mysqli->prepare("INSERT INTO " . $this->usr_tb . " (
                                    uname,
                                    email,
                                    pwd,
                                    fname,
                                    email_2,
                                    addr
                                    )
                                    VALUES (
                                    ?,?,?,?,?,?
                                    )");

        $stmt->bind_param("ssssss", $newUser['uname'], $newUser['email'], $newUser['pwd'], $newUser['fname'], $newUser['email_2'], $newUser['addr']);

        if (!$stmt->execute())
        {
            $res = SQLFAILED;
        }

        $stmt->close();

        return $res;
    }

    public function updateUser($userInfo)
    {
        $res = SUCCESS;

        global $mysqli;

        $stmt = $mysqli->prepare("UPDATE " . $this->usr_tb . "
                    SET 
                    uname = ?,
                    email = ?,
                    fname = ?,
                    email_2 = ?,
                    addr = ?
                    WHERE
                    uid = ?");
        $stmt->bind_param("sssssi", $userInfo['uname'], $userInfo['email'], $userInfo['fname'], $userInfo['email_2'], $userInfo['addr'], $userInfo['uid']);

        if ($stmt->execute())
        {
            $res = SUCCESS;
        }
        else
        {
            $res = SQLFAILED;
        }

        $stmt->close();

        return $res;
    }

    public function updateUserPassword($pwd, $email)
    {
        $res = SUCCESS;

        global $mysqli;

        $stmt = $mysqli->prepare("UPDATE " . $this->usr_tb . "
                    SET 
                    pwd = ?
                    WHERE
                    email = ?");
        $stmt->bind_param("ss", $pwd, $email);

        if ($stmt->execute())
        {
            $res = SUCCESS;
        }
        else
        {
            $res = SQLFAILED;
        }

        $stmt->close();

        return $res;
    }

    public function existUserName($uname)
    {
        $userDetails = $this->fetchUserDetails($uname, NULL);
        if (sizeof($userDetails) > 0)
        {
            return true;
        }

        return false;
    }

    public function existUserEmail($email)
    {
        $userDetails = $this->fetchUserDetails(NULL, $email);
        if (sizeof($userDetails) > 0)
        {
            return true;
        }

        return false;
    }

    public function existUserEmailForUpdate($uid, $email)
    {
        global $mysqli;
        $stmt = $mysqli->prepare("SELECT uid 
                    FROM " . $this->usr_tb . "
                    WHERE
                    uid = ?
                    AND
                    email = ?
                    LIMIT 1");
        $stmt->bind_param("is", $uid, $email);
        $stmt->execute();
        $stmt->store_result();
        $num_returns = $stmt->num_rows;
        $stmt->close();

        if ($num_returns > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function existUserEmail2($email)
    {
        $userDetails = $this->fetchUserDetails(NULL, NULL, $email);
        if (sizeof($userDetails) > 0)
        {
            return true;
        }

        return false;
    }

    public function fetchRegedUserDetails($uname, $email)
    {
        global $mysqli;

        if ($uname !== NULL)
        {
            $column = "uname";
            $data = $uname;
        }
        else if ($email !== NULL)
        {
            $column = "email";
            $data = $email;
        }

        $stmt = $mysqli->prepare("SELECT 
                    ruid,
                    uname,
                    email,
                    pwd,
                    fname,                              
                    email_2,
                    addr
                    FROM " . $this->reg_tb . "
                    WHERE
                    $column = ?
                    LIMIT 1");
        $stmt->bind_param("s", $data);

        $stmt->execute();
        $stmt->bind_result($uid, $uname, $email, $pwd, $fname, $email_2, $addr);

        $row = array();
        while ($stmt->fetch())
        {
            $row = array('uid' => $id, 'uname' => $uname, 'email' => $email, 'pwd' => $pwd, 'fname' => $fname, 'email_2' => $email_2, 'addr' => $addr);
        }

        $stmt->close();

        return ($row);
    }

    public function addRegedUser($newUser)
    {
        $res = SUCCESS;

        global $mysqli;

        $stmt = $mysqli->prepare("INSERT INTO " . $this->reg_tb . " (
                                    uname,
                                    email,
                                    pwd,
                                    fname,
                                    email_2,
                                    addr
                                    )
                                    VALUES (
                                    ?,?,?,?,?,?
                                    )");

        $stmt->bind_param("ssssss", $newUser['uname'], $newUser['email'], $newUser['pwd'], $newUser['fname'], $newUser['email_2'], $newUser['addr']);

        if (!$stmt->execute())
        {
            $res = SQLFAILED;
        }

        $stmt->close();

        return $res;
    }

    public function deleteRegedUser($email)
    {
        global $mysqli;
        $i = 0;
        $stmt = $mysqli->prepare("DELETE FROM " . $this->reg_tb . " 
                    WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->close();

        $action = "User Delete (success)";
        $content = "User: " . $this->convert->myDecrypt($email) . "; Email deleted from temp.";
        secureLog($email, $this->convert->myEncrypt($action), $this->convert->myEncrypt($content), 1);

        return $i;
    }

    public function existRegedUserName($uname)
    {
        $userDetails = $this->fetchRegedUserDetails($uname, NULL);
        if (sizeof($userDetails) > 0)
        {
            return true;
        }

        return false;
    }

    public function existRegedUserEmail($email)
    {
        $userDetails = $this->fetchRegedUserDetails(NULL, $email);
        if (sizeof($userDetails) > 0)
        {
            return true;
        }

        return false;
    }

    function getTotalCount($table_name, $where = "")
    {
        global $mysqli, $db_table_prefix;

        if ($where !== "")
        {
            $where = " WHERE " . $where;
        }

        $stmt = $mysqli->prepare("SELECT count(*) as count FROM " . $table_name . " " . $where);
        $stmt->execute();
        $stmt->bind_result($count);
        $stmt->fetch();
        $stmt->close();
        return ($count);
    }

}

?>