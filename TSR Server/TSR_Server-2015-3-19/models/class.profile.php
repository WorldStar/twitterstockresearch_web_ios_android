<?php

class Profile
{

    //protected $usermng;
    protected $uid;
    //protected $info;
    protected $fname;
    protected $uname;
    protected $email;
    protected $email2;
    protected $addr;

    public function __construct()
    {
        $convert = new Security();
        $this->uid = $convert->myDecrypt(base64_decode($_SESSION['2376fa']));

        $this->fname = '';
        $this->uname = '';
        $this->email = '';
        $this->email2 = '';
        $this->addr = '';

        $userManager = new UserManager();

        $userDetails = $userManager->fetchUserDetailsFromUID($this->uid);
        if (sizeof($userDetails) > 0)
        {
            if ($userDetails['fname'] !== null && $userDetails['fname'] !== "")
            {
                $this->fname = $userManager->convert->myDecrypt($userDetails['fname']);
            }

            if ($userDetails['uname'] !== null && $userDetails['uname'] !== "")
            {
                $this->uname = $userManager->convert->myDecrypt($userDetails['uname']);
            }

            if ($userDetails['email'] !== null && $userDetails['email'] !== "")
            {
                $this->email = $userManager->convert->myDecrypt($userDetails['email']);
            }

            if ($userDetails['email_2'] !== null && $userDetails['email_2'] !== "")
            {
                $this->email2 = $userManager->convert->myDecrypt($userDetails['email_2']);
            }

            if ($userDetails['addr'] !== null && $userDetails['addr'] !== "")
            {
                $this->addr = $userManager->convert->myDecrypt($userDetails['addr']);
            }
        }
    }

    public function show($goto)
    {
        $xtea = new XTEA(NULL);
        $tochpass_link = PARAM_GOTO . "=" . base64_encode($xtea->encrypt('tochpass'));
        $toprofile_link = PARAM_GOTO . "=" . base64_encode($xtea->encrypt('profile'));

        echo '
			<div class="container back-white">
				<div class="container-fluid" style="text-align:center">
						<div style="height:20px"></div>
						<!--------------------------------------------------------------->
						<!-- Login Form -->
						<section>				
							<div id="container_demo" >
								<a class="hiddenanchor" id="toregister"></a>
								<a class="hiddenanchor" id="tologin"></a>
								<div id="wrapper">
									<div id="login" class="animate form">';
        //if (!isset($_REQUEST['toregister']))
        if ($goto === 'profile')
        {
            echo '<form  action="post.php" autocomplete="on" method="post"> 
											<input name="b5fc5" value="5" type="hidden">
											<input name="uid" value="' . $this->uid . '" type="hidden">
											<h1> Profile </h1> 
											<div> 
												<label for="fullname" class="uname" data-icon="F">Your full name</label>
												<input id="fullname" name="fullname" required type="text" value="' . $this->fname . '" placeholder="Full name" />
											</div>
											<div> 
												<label for="username" class="uname" data-icon="U">Your user name</label>
												<input id="username" name="username" required readonly type="text" value="' . $this->uname . '" />
											</div>
											<div> 
												<label for="email" class="youmail" data-icon="E" > Your email</label>
												<input id="email" name="email" required type="email" value="' . $this->email . '"placeholder="Email"/> 
											</div>
											<div> 
												<label for="email2" class="youmail" data-icon="E" > Your secondary email</label>
												<input id="email2" name="email2" type="email" value="' . $this->email2 . '" placeholder="Email"/> 
											</div>
<!--											<div> 
												<label for="addr" class="uname" data-icon="A" > Your address</label>
												<input id="addr" name="addr" required type="text" value="' . $this->addr . '" placeholder="Address"/> 
											</div>
-->											<p class="signin button"> 
												<input type="submit" value="Save"/> 
												<input type="button" value="Back" onClick="history.back();"/> 
											</p>
											<p class="change_link">
												<a href="index.php?' . $tochpass_link . '">Change Password</a>
											</p>
										</form>';
        }
        //else if (isset($_REQUEST['toregister']))
        else if ($goto === 'tochpass')
        {
            echo '<form id="changepass_frm" action="post.php" autocomplete="on" method="post" onSubmit="return null;"> 
											<input name="b5fc5" value="6" type="hidden">
											<h1> Change Password </h1> 
											<div> 
												<label for="oldpassword" class="youpasswd" data-icon="P" > Current password</label>
												<input id="oldpassword" name="oldpassword" required type="password" placeholder="Current Password"/> 
											</div>
											<div> 
												<label for="passwordsignup" class="youpasswd" data-icon="NP"> New password </label>
												<input id="passwordsignup" name="passwordsignup" required type="password" placeholder="New Password"/>
											</div>
											<div> 
												<label for="passwordsignup_confirm" class="youpasswd" data-icon="CP">Please confirm your password </label>
												<input id="passwordsignup_confirm" name="passwordsignup_confirm" required type="password"  placeholder="Confirm Password"/>
											</div>
											<p class="signin button"> 
												<input id="btnSignup" type="button" value="Save" onClick="verify_changepass();"/> 
											</p>
											<p class="change_link">  
												<a href="index.php?' . $toprofile_link . '" class="to_register"> Edit profile </a>
											</p>
										</form>';
        }
        echo '</div>
								</div>
							</div>  
						</section>
<!--						<div style="height:30px"></div>
-->					</div>
				</div>
		';
    }

}

?>