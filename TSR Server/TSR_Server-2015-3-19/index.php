<?php
require_once 'models/config.php';

/*
  $query = array('val' => 1, 'test' => 2, 'key' => 'value');
  $string = http_build_query( $query );
 */
$goto = "home";
$page = 0;

$xtea = new XTEA(NULL);

if (isset($_GET['VXo5MzJ3QUNpSUVJcXJBaElQYmpwUT09']) && $_GET['VXo5MzJ3QUNpSUVJcXJBaElQYmpwUT09'] != null)
{
    $goto = $_GET['VXo5MzJ3QUNpSUVJcXJBaElQYmpwUT09'];

    $goto = $xtea->decrypt(base64_decode($goto));
//    if ($goto === NULL || $goto === "")
//    {
//        $username = "guest";
//        $action = "Request: failed";
//        $content = "Parameter missing";
//        secureLog($username, $action, $content, 0);
//    }
}

//echo $goto;
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>TwitterStockResearch</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-responsive.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
        <!--        <link rel="stylesheet" type="text/css" href="syntaxhighlighter/styles/shCoreDefault.css" />
                <link rel="stylesheet" type="text/css" href="syntaxhighlighter/styles/shThemejqPlot.css" />-->
        <link href="css/bootstrap-combined.no-icons.css" rel="stylesheet">
        <link href="css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/style.css">

        <!-- Login -->
        <link rel="stylesheet" type="text/css" href="css/login/style.css" />
        <link rel="stylesheet" type="text/css" href="css/login/animate-custom.css" />
        <!----------->

        <!-- Dialog -->
        <link rel="stylesheet" href="css/jquery-ui.css" />
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.js"></script>
        
        <script src="js/excanvas.js"></script>
        <script src="js/jquery.jqplot.js"></script>

        <script src="js/plugins/jqplot.dateAxisRenderer.js"></script>
        <script src="js/plugins/jqplot.ohlcRenderer.js"></script>
        <script src="js/plugins/jqplot.highlighter.js"></script>

<!--       <script src="syntaxhighlighter/scripts/shCore.js"></script>
       <script src="syntaxhighlighter/scripts/shBrushJScript.js"></script>
       <script src="syntaxhighlighter/scripts/shBrushXml.js"></script>-->

        <script src="js/bootstrap.js"></script>
        <script src="js/yui.js"></script>

<!--        <script src="js/ys_autocomplete.js"></script>-->

        <?php
        //if (!isset($_REQUEST['profile']))
        if ($goto !== 'profile')
        //if(isset($_POST['txtSymbol']))
        {
            echo '
		<script src="js/yql_process.js"></script>
                <script src="js/chart_module.js"></script>
                 ';
        }
        ?>

    </head>
    <body>
        <?php
        if (ExceptionHandler::neededToShowMsg())
        {
            ExceptionHandler::showMsgDiag();
        }

        Header::show();

        if (empty($_POST) && !isset($_SESSION['2376fa']))
        {
            $_SESSION['l94jso28dn'] = "";
            //Home::show();
            Home::show($goto);
        }
        else
        {

            if (isset($_SESSION['l94jso28dn']))
            {
                unset($_SESSION['l94jso28dn']);
                Navigator::redirect("index.php");
            }

//	print_r($_SESSION);die(isset($_GET['profile']));
            //if (isset($_GET['profile']))
            if ($goto === 'profile' || $goto === 'tochpass')
            {
                $profileview = new Profile();
                $profileview->show($goto);
            }
            else
            {
//                $symbol = 'YHOO';
//                if(isset($_POST['txtSymbol']))
//                {
//                    $symbol = $_POST['txtSymbol'];
//                    $_SESSION['guest'] = TRUE; // guest searched once
//                }
//                StockChart::show($symbol);
                
                $symbol = 'YHOO';
                if (isset($_POST['txtSymbol']))
                {
                    $symbol = $_POST['txtSymbol'];
                }

                if (isset($_SESSION['2376fa']))
                {
                    StockChart::show($symbol);
                }
                else
                {
                    if (isset($_SESSION['guest']))
                    {
                        $_SESSION['err_msg'] = "Please kindly log in to continue.";
                        $_SESSION['neededToShowError'] = true;
        
                        ExceptionHandler::showMsgDiag();
                        Home::show($goto);
                    }
                    else
                    {
                        $_SESSION['guest'] = TRUE;
                        StockChart::show($symbol);
                    }
                }
            }
        }

        Footer::show();
        ?>
        
        <script src="js/ys_autocomplete.js"></script>
        
    </body>
</html>
