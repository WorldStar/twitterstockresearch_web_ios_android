<?php
include_once("admin_lib.php");
check_authorized();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" HREF="style.css" type="text/css" title="style">
    </head>
    <body>
        <?php include_once("admin_title.php"); ?>
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
            <tr> 
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr bgcolor="#CACACA">
                            <td width="100%" height="80" colspan="2">
                                <?php include_once("admin_top.php"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="160" bgcolor="#f3f3f3">
                                <?php include_once("admin_menu.php"); ?>
                            </td>
                            <td width="100%" bgcolor="#f3f3f3" valign="top">
                                <br><br>
                                <div id="main_column" class="clear">
                                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr valign="top">
                                            <td width="100%">
                                                <table width="100%" height="100%" cellspacing="20" cellpadding="0" border="0">
                                                    <tr valign="top">
                                                        <td width="100%" style="line-height:100%" nowrap>

                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Administrator Account</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table width="800" cellspacing="10" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="100%" style="line-height:160%" nowrap>
                                                                        <b>Note: The administrator account is very important to the security management. Please note that you are managing.</b><br>
                                                                        Username and password are both changed, and password finding function is not supported.<br>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <?php
                                                            $policy['admin_id'] = "";
                                                            $policy['admin_password'] = "";

                                                            $policy['admin_id'] = & $_SECURE_POLICY['CONFIG']['ADMIN']['ID'];
                                                            $policy['admin_password'] = & $_SECURE_POLICY['CONFIG']['ADMIN']['PASSWORD'];

                                                            $print['admin_id'] = base64_decode($policy['admin_id']);
                                                            $print['admin_password'] = base64_decode($policy['admin_password']);
                                                            ?>
                                                            <br>
                                                            <table cellspacing="2" cellpadding="6" border="0" align="center">
                                                                <form action="admin_account_submit.php?mode=CONFIG_ACCOUNT" method="post">
                                                                    <tr>
                                                                        <th width="120" height="30" align="right">Admin ID</th>
                                                                        <td colspan="3"><input type="text" name="admin_id" class="input-text" size="30" maxlength="32" value="<?php echo $print['admin_id'] ?>"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120" height="30" align="right">Password</th>
                                                                        <td><input type="password" name="admin_password" class="input-text" size="30" maxlength="32"></td>
                                                                        <th width="120" height="30" align="right">Confirm password</th>
                                                                        <td><input type="password" name="admin_repassword" class="input-text" size="30" maxlength="32"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120" height="30" align="right">Old password</th>
                                                                        <td colspan="3"><input type="password" name="admin_old_password" class="input-text" size="30" maxlength="32"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td colspan="3">
                                                                            <table width="100%" height="50" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:120%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>ID - min: 4, max 16 characters.
                                                                                                <li>Password - min 8, max 32 characters.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                            </table>
                                                            <br>
                                                            <table width="475" height="35" cellspacing="0" cellpadding="0" border="0">
                                                                <tr valign="top">
                                                                    <td width="210">&nbsp;</td>
                                                                    <td width="300">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="submit" value="Conform">
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                    return false;">
                                                                        </span>
                                                                        <br><br>
                                                                    </td>
                                                                </tr>
                                                                </form>
                                                            </table>
                                                            <?php ?>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr bgcolor="#A0A0A0">
                            <td width="100%" height="50" colspan="2" align="center">
                                <?php include_once("admin_bottom.php"); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
