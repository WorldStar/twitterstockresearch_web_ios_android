<?php
include_once("admin_lib.php");
check_authorized();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" HREF="style.css" type="text/css" title="style">
    </head>
    <body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="#D0D0D0">
        <?php include_once("admin_title.php"); ?>
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
            <tr> 
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tr bgcolor="#f3f3f3">
                            <td width="100%" height="80" colspan="2">
                                <?php include_once("admin_top.php"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="160" bgcolor="#f3f3f3">
                                <?php include_once("admin_menu.php"); ?>
                            </td>
                            <td width="100%" bgcolor="#f3f3f3" valign="top">
                                <br><br>
                                <div id="main_column" class="clear">
                                    <table width="800" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tr valign="top">
                                            <td width="100%">
                                                <table width="100%" height="100%" cellspacing="20" cellpadding="0" border="0" align="center">
                                                    <tr valign="top">
                                                        <td width="100%">

                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">View policy</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>

                                                            <br>
                                                            <table width="790" cellspacing="10" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="100%" style="line-height:160%" nowrap>
                                                                        <b>View: Show tree and form.</b><br>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <table width="100%" cellspacing="0" cellpadding="5" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <table width="780" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000" align="center">
                                                                            <tr>
                                                                                <td bgcolor="#FFFFFF" style="line-height:100%">
                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <?php echo print_policy_tree(); ?>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <table width="780" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000" align="center">
                                                                            <tr>
                                                                                <td bgcolor="#FFFFFF" style="line-height:100%">
                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <?php echo print_policy(); ?>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="2" bgcolor="#000000" colspan="2"></td>
            </tr>
            <tr bgcolor="#A0A0A0">
                <td width="100%" height="50" colspan="2" align="center">
                    <?php include_once("admin_bottom.php"); ?>
                </td>
            </tr>
        </table>
    </body>
</html>
