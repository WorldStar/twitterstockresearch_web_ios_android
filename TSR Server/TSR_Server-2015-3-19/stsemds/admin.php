<?php
include_once("admin_lib.php");
check_authorized();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" HREF="style.css" type="text/css" title="style">        
    </head>
    <body>
        <?php include_once("admin_title.php"); ?>
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
            <tr> 
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr bgcolor="#CACACA">
                            <td width="100%" height="80" colspan="2">
                                <?php include_once("admin_top.php"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr>                            
                            <td width="160" bgcolor="#f3f3f3">
                                <?php include_once("admin_menu.php"); ?>
                            </td>
                            <td width="100%" bgcolor="#f3f3f3" valign="top">
                                <br><br>
                                <div id="main_column" class="clear">
                                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr valign="top">
                                            <td width="100%">
                                                <table width="100%" height="100%" cellspacing="20" cellpadding="0" border="0">
                                                    <tr valign="top">
                                                        <td width="100%" style="line-height:160%" nowrap>

                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Menu Description</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>

                                                            <table width="800" cellspacing="2" cellpadding="5" border="0">
                                                                <tr valign="top">
        <!--                                                            <td width="100" height="100%" bgcolor="#D8D8D8" style="line-height:160%" nowrap>
                                                                        1. <a href="admin.php#home">Home</a><br>
                                                                        2. <a href="admin.php#account">Account</a><br>
                                                                        3. <a href="admin.php#config">Configure</a><br>
                                                                        4. <a href="admin.php#policy">Policy</a><br>
                                                                        5. <a href="admin.php#advance">Advanced</a><br>
                                                                        6. <a href="admin.php#log">Logs</a><br>
                                                                        7. <a href="admin.php#policyview">View Policy</a><br>
                                                                        8. <a href="admin.php#backup">Backup</a><br>
                                                                    </td>-->
                                                                    <td>
                                                                        <table width="100%" height="100%">
                                                                            <tr>
                                                                                <td bgcolor="#FFFFFF">
                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <b>Home</b><br><br>
                                                                                            Move main page.
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <b>Account</b><br><br>
                                                                                            Manage administrator's account.<br><br>
                                                                                            <font color="red">Note: Password set to 8 or more.</font>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <b>Configure</b><br><br>
                                                                                            Set the main setting for secure.
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <b>Policy Settings</b><br><br>
                                                                                            This is applied to the actual policy.<br>
                                                                                            This applies to all pages, not set in the Advanced Settings section.<br><br>
                                                                                            
                                                                                            A. Login Limits settings<br>
                                                                                            B. DOS (Denial-of-service) policy settings<br>
                                                                                            C. SQL Injection policy settings<br>
                                                                                            D. XSS policy settings<br>
                                                                                            E. BAD WORD  policy settings<br>
                                                                                            F. BAD TAG  policy settings<br>
                                                                                            G. IP Address  policy settings<br>
                                                                                            H. FILE  policy settings
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <b>Advanced Settings</b><br><br>
                                                                                            This is to set the policy of each page.<br>
                                                                                            This is applied over the common policy.<br><br>
                                                                                            A. Add / Remove the pages<br>
                                                                                            B. Add / Remove the page-specific variables
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <b>Log Management</b><br><br>
                                                                                            This manage the detected logs by secure module.<br><br>
                                                                                            A. Set for logging<br>
                                                                                            B. Set the logging method<br>
                                                                                            D. Set the number of log list
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <b>View Policies</b><br><br>
                                                                                            Show the current policy settings.<br><br>
                                                                                            A. Show by tree view<br>
                                                                                            B. Show by saved form
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <b>Backup</b><br><br>
                                                                                            Back up the current settings and can be stored in PC.<br><br>
                                                                                            A. View the policy information file<br>
                                                                                            B. Download the policy file
                                                                                        </div>
                                                                                    </div>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table></div>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr bgcolor="#A0A0A0">
                            <td width="100%" height="50" colspan="2" align="center">
                                <?php include_once("admin_bottom.php"); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </body>
</html>
