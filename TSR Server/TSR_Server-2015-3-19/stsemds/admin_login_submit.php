<?php

$exception['check_authorized'] = TRUE;

include_once("admin_lib.php");
require_once '../models/config.php';

init_page();

clear_submit();

$user_type = 2;
$nowsec = time();
$rip = $_SERVER['REMOTE_ADDR'];
$wanted = $_SERVER['REQUEST_URI'];

// check login count
//$res = check_login_count($rip, $user_type);
//if ($res !== TRUE)
//{
//    html_msgback("You have exceeded the login counts. Please try again in a few minutes.");
//}

if (isset($_POST['admin_id']))
    $submit['admin_id'] = $_POST['admin_id'];

if (isset($_POST['admin_password']))
    $submit['admin_password'] = $_POST['admin_password'];

$check['submit'] = array("admin_id", "admin_password",);
check_submit($check['submit']);

$check['admin_id_length'] = strlen($submit['admin_id']);
check_length("Admin ID", $check['admin_id_length'], 4, 16);

$check['admin_password_length'] = strlen($submit['admin_password']);
check_length("Password", $check['admin_password_length'], 3, 32);

$policy['admin_id'] = "";
$policy['admin_password'] = "";

if (isset($_SECURE_POLICY['CONFIG']['ADMIN']['ID']))
    $policy['admin_id'] = $_SECURE_POLICY['CONFIG']['ADMIN']['ID'];

if (isset($_SECURE_POLICY['CONFIG']['ADMIN']['PASSWORD']))
    $policy['admin_password'] = $_SECURE_POLICY['CONFIG']['ADMIN']['PASSWORD'];

// BASE64
$policy['admin_id'] = base64_decode($policy['admin_id']);
$policy['admin_password'] = base64_decode($policy['admin_password']);

$submit['admin_password'] = md5(md5($submit['admin_password']));

if ($submit['admin_id'] != $policy['admin_id'] ||
        $submit['admin_password'] != $policy['admin_password'])
{
    // insert new login
    save_login_access("recent", $nowsec, $rip, $wanted, $user_type);

    if ($submit['admin_id'] != $policy['admin_id'])
        html_msgback("Incorrect admin ID.");

    if ($submit['admin_password'] != $policy['admin_password'])
        html_msgback("Incorrect password.");
}

$auth['remote_addr'] = getenv("REMOTE_ADDR");
$auth['user_agent'] = getenv("HTTP_USER_AGENT");

$auth['key'] = "auth_token_" . $auth['remote_addr'];
$auth['value'] = md5($auth['user_agent']);

$_SESSION[$auth['key']] = $auth['value'];

delete_login_info($rip, $user_type);
        
html_msgmove("Successfully login.", "admin.php");

exit;

function save_login_access($table_name, $nowsec, $rip, $wanted, $type)
{
    global $mysqli;
    $stmt = $mysqli->prepare("INSERT INTO " . $table_name . " (
                                    tstamp,
                                    remoteaddr,
                                    calledfor,
                                    type
                                    )
                                    VALUES (
                                    ?,?,?,?
                                    )");

    $stmt->bind_param("sssi", $nowsec, $rip, $wanted, $type);
    $stmt->execute();
    $stmt->close();
}
/*
function get_login_count($rip, $tstamp, $type)
{
    global $mysqli;
    $stmt = $mysqli->prepare("SELECT 
                    COUNT(tstamp)
                    FROM recent
                    WHERE 
                    type = ? AND 
                    remoteaddr = ? AND
                    tstamp > ?");
    $stmt->bind_param("isi", $type, $rip, $tstamp);
    $stmt->execute();
    $stmt->bind_result($count);

    $row = array();
    while ($stmt->fetch())
    {
        $row = array('balloon' => $count);
    }
    $stmt->close();

    return intval($row['balloon']);
}
*/
function delete_login_info($rip, $type)
{
    global $mysqli;
    $stmt = $mysqli->prepare("DELETE 
                    FROM recent
                    WHERE 
                    type = ? AND 
                    remoteaddr = ?");
    $stmt->bind_param("is", $type, $rip);
    $stmt->execute();
    $stmt->bind_result($count);
    $stmt->close();
}

function check_login_count($rip, $type)
{
    global $_SECURE_POLICY;

    // get dos policy
    $policy['login'] = "";
    if (isset($_SECURE_POLICY['POLICY']['LOGIN']))
        $policy['login'] = & $_SECURE_POLICY['POLICY']['LOGIN'];

    /* apply */
    $policy['login_bool'] = base64_decode($policy['login']['BOOL']);
    $policy['peroid'] = base64_decode($policy['login']['PERIOD']);
    $policy['hurdle'] = base64_decode($policy['login']['SECADMIN']);

    if ($policy['login_bool'] === "FALSE")
        return TRUE;

    // How many requests in the test time period
    $nowsec = time();
    $keeptime = intval($policy['peroid']);
    $hurdle = intval($policy['hurdle']);

    $nn = $nowsec - $keeptime;
    $balloon = get_login_count($rip, $nn, $type);

    // $hurdle pages per $keeptime seconds - aggressive!
    if ($balloon >= $hurdle)
    {
        return "You have exceeded the login counts.<br>Please try again in a few minutes.";
    }

    return TRUE;
}

?>
