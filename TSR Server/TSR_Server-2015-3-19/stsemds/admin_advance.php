<?php
include_once("admin_lib.php");
check_authorized();

init_page();

//$page['mode']:
if (isset($_GET) && isset($_GET['mode']))
    $page['mode'] = $_GET['mode'];


$error_msg['page_name'] = "Page name must be at least 4 characters.";
$error_msg['page_view'] = "Run the first page view.";
$error_msg['delete_confirm'] = "You delete really?";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" HREF="style.css" type="text/css" title="style">
        <script language=javascript>
            <!--
          function page_view()
            {
                var len = document.page_insert_form.page_name.value.length;
                if (len < 4) {
                    alert("<?php echo $error_msg['page_name'] ?>");
                    return;
                }
                window.open(document.page_insert_form.page_name.value, "page_view");
                document.page_insert_form.page_view_bool.value = 1;
            }

            function page_view2(url)
            {
                window.open(url, "page_view");
            }

            function page_view_check_submit()
            {
                if (document.page_insert_form.page_view_bool.value == "0") {
                    alert("<?php echo $error_msg['page_view'] ?>");
                    return false;
                }
                document.page_insert_form.submit();
            }

            function page_delete_submit(page_name)
            {
                ret = confirm("<?php echo $error_msg['delete_confirm'] ?>");
                if (!ret)
                    return;
                location.href = 'admin_advance_submit.php?mode=PAGE_DELETE&page_name=' + page_name;
            }
//-->
        </script>
    </head>
    <body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="#D0D0D0">
        <?php include_once("admin_title.php"); ?>
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
            <tr> 
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tr bgcolor="#CACACA">
                            <td width="100%" height="80" colspan="2">
                                <?php include_once("admin_top.php"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="160" bgcolor="#f3f3f3">
                                <?php include_once("admin_menu.php"); ?>
                            </td>
                            <td width="100%" bgcolor="#f3f3f3" valign="top">
                                <br><br>
                                <div id="main_column" class="clear">
                                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tr valign="top">
                                            <td width="100%">
                                                <table width="100%" height="100%" cellspacing="20" cellpadding="0" border="0" align="center">
                                                    <tr valign="top">
                                                        <td width="100%">
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Page Lists</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>

                                                            <br>
                                                            <table width="790" cellspacing="10" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="100%" style="line-height:160%" nowrap>
                                                                        <b>Note: The pages that there is not registered be applied the default policy settings.</b><br>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <table width="790" border="0" align="center">
                                                                <tr>
                                                                    <td align="right">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <a href="admin_advance.php?mode=INSERT#insert_form">
                                                                                <input type="button" class="cm-confirm cm-process-items" value="Add page">
                                                                            </a>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <table width="780" cellspacing="1" cellpadding="3" border="0" align="center">
                                                                <tr height="30">
                                                                    <th width="50" bgcolor="#ddf3fa">No</th>
                                                                    <th width="360" bgcolor="#ddf3fa">Page Name</th>
                                                                    <th width="80" bgcolor="#ddf3fa">Param Count</th>
                                                                    <th width="100" bgcolor="#ddf3fa">Apply Status</th>
                                                                    <th width="100" bgcolor="#ddf3fa">Apply Base</th>
                                                                    <th width="50" bgcolor="#ddf3fa">Modify</th>
                                                                    <th width="50" bgcolor="#ddf3fa">Delete</th>
                                                                </tr>
                                                                <?php
                                                                if (isset($_SECURE_POLICY['ADVANCE']['PAGE']))
                                                                    $policy['page_list'] = & $_SECURE_POLICY['ADVANCE']['PAGE'];
                                                                else
                                                                    $policy['page_list'] = array();

                                                                $print['page_count'] = count($policy['page_list']);
                                                                $print['page_num'] = 1;

                                                                foreach ($policy['page_list'] as $key => $value)
                                                                {
                                                                    $policy['page'] = $policy['page_list'][$key];

                                                                    $print['page_name'] = $key;
                                                                    $print['page_bool'] = "<font color=\"red\"><b>Block</b></font>";
                                                                    $print['page_base'] = "<font color=\"red\"><b>Black List</b></font>";

                                                                    if (isset($policy['page']['BOOL']) && base64_decode($policy['page']['BOOL']) == "TRUE")
                                                                        $print['page_bool'] = "<font color=\"blue\"><b>Allow</b></font>";

                                                                    if (isset($policy['page']['ALLOW']) && base64_decode($policy['page']['ALLOW']) == "TRUE")
                                                                        $print['page_base'] = "<font color=\"blue\"><b>White list</b></font>";

                                                                    if (isset($policy['page']['LIST']))
                                                                        $print['page_var_num'] = count($policy['page']['LIST']);
                                                                    else
                                                                        $print['page_var_num'] = "--";
                                                                    ?>
                                                                    <tr height="25" bgcolor="#FFFFFF">
                                                                        <td align="center">
                                                                            <a name="<?php echo $print['page_num'] ?>"></a><?php echo $print['page_num'] ?>
                                                                        </td>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td><?php echo $print['page_name'] ?></td>
                                                                                    <td><a href="admin_advance_detail.php?page_name=<?php echo $print['page_name'] ?>&pos=<?php echo $print['page_num'] - 1 ?>"><button>Set Policy</button></a></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td align="center"><?php echo $print['page_var_num'] ?></td>
                                                                        <td align="center"><?php echo $print['page_bool'] ?></td>
                                                                        <td align="center"><?php echo $print['page_base'] ?></td>
                                                                        <td align="center"><a href="admin_advance.php?page_name=<?php echo $print['page_name'] ?>&mode=MODIFY#<?php echo $print['page_num'] - 1 ?>"><button>Modify</button></a></td>
                                                                        <td align="center"><button onclick="page_delete_submit('<?php echo $print['page_name'] ?>');
                                                                                return false;">Delete</button></td>
                                                                    </tr>
                                                                    <?php
                                                                    $print['page_num'] ++;

                                                                    if (isset($page['mode']) && $page['mode'] == "MODIFY")
                                                                    {
                                                                        $submit['page_name'] = $_GET['page_name'];

                                                                        if ($print['page_name'] != $submit['page_name'])
                                                                            continue;

                                                                        $print['page_bool_true_check'] = "checked";
                                                                        $print['page_bool_false_check'] = "";
                                                                        $print['page_base_allow_check'] = "checked";
                                                                        $print['page_base_deny_check'] = "";

                                                                        if (isset($policy['page']['BOOL']) && base64_decode($policy['page']['BOOL']) == "TRUE")
                                                                            $print['page_bool_true_check'] = "checked";
                                                                        else
                                                                            $print['page_bool_false_check'] = "checked";

                                                                        if (isset($policy['page']['ALLOW']) && base64_decode($policy['page']['ALLOW']) == "TRUE")
                                                                            $print['page_base_allow_check'] = "checked";
                                                                        else
                                                                            $print['page_base_deny_check'] = "checked";
                                                                        ?>
                                                                        <tr>
                                                                            <td colspan="7" align="center">
                                                                                <br>
                                                                                <form name="page_modify_form" action="admin_advance_submit.php?mode=PAGE_MODIFY" method="post">
                                                                                    <table width="700" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                                        <tr>
                                                                                            <th width="200" height="30" rowspan="4" bgcolor="#ddf3fa" align="right">Page Management</th>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th width="150" height="30" bgcolor="#ddf3fa">Page Name</th>
                                                                                            <td width="300">
                                                                                                <input type="hidden" name="page_name" value="<?php echo $print['page_name'] ?>">
                                                                                                <table width="300" height="25" cellspacing="0" cellpadding="2" border="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <input type="text" size="50" class="input-text-disabled" value="<?php echo $print['page_name'] ?>" disabled>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td width="200">
                                                                                                <span class="cm-button-main cm-process-items">
                                                                                                    <input type="button" class="cm-confirm cm-process-items" value="View Page" onclick="page_view2('<?php echo $print['page_name'] ?>');">
                                                                                                </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th width="150" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                                            <td width="480" colspan="2">
                                                                                                <input type="radio" name="page_bool" value="true" <?php echo $print['page_bool_true_check'] ?>>YES (default)
                                                                                                <input type="radio" name="page_bool" value="allow" <?php echo $print['page_bool_false_check'] ?>>NO
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th width="150" height="30" bgcolor="#ddf3fa">Apply Base</th>
                                                                                            <td width="480" colspan="2">
                                                                                                <input type="radio" name="page_base" value="allow" <?php echo $print['page_base_allow_check'] ?>>White List (default)
                                                                                                <input type="radio" name="page_base" value="deny" <?php echo $print['page_base_deny_check'] ?>>Black List
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <br><br>
                                                                                    <table width="475" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                        <tr>
                                                                                            <td width="175">&nbsp;</td>
                                                                                            <td width="300">
                                                                                                <span class="cm-button-main cm-process-items">
                                                                                                    <input type="submit" value="Confirm">
                                                                                                </span>
                                                                                                <span class="cm-button-main cm-process-items">
                                                                                                    <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="history.back(-1);
                                                                                                            return false;">
                                                                                                </span>
                                                                                                <br>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </form>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <tr>
                                                                        <td colspan="7"><hr color="#ddf3fa"></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>                                                                        
                                                            </table>
                                                            <table width="790" height="40">
                                                                <tr>
                                                                    <td width="100%" height="100%" align="center">Set total <b><?php echo $print['page_count'] ?></b> pages.</td>
                                                                </tr>
                                                            </table>
                                                            <?php
                                                            if (isset($page['mode']) && $page['mode'] == "INSERT")
                                                            {
                                                                ?>
                                                                <br>
                                                                <a name="insert_form"></a>
                                                                <form name="page_insert_form" action="admin_advance_submit.php?mode=PAGE_INSERT" method="post" onsubmit="page_view_check_submit();
                                                                        return false;">
                                                                    <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                        <tr>
                                                                            <td width="5"></td>
                                                                            <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                                <p class="mainbox-title">Add new page</p>
                                                                            </td>
                                                                            <td width="5"></td>
                                                                        </tr>
                                                                    </table>

                                                                    <br>
                                                                    <table width="790" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                        <tr>
                                                                            <th width="150" height="30" rowspan="6" bgcolor="#ddf3fa" align="right">Page Management</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th width="100" height="30" bgcolor="#ddf3fa">Page Name</th>
                                                                            <td width="480">
                                                                                <input type="text" name="page_name" size="50" class="input-text">
                                                                                <input type="hidden" name="page_view_bool" value="0">
                                                                                <span class="cm-button-main cm-process-items">
                                                                                    <input type="button" class="cm-confirm cm-process-items" value="View Page" onclick="page_view();">
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th width="100"></th>
                                                                            <td width="410" colspan="2">
                                                                                <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                    <tr>
                                                                                        <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                            <div class="statistics-box inventory">
                                                                                                <div class="statistics-body">
                                                                                                    <b>ex: http://host<font color="red">/path</font></b>, &nbsp;&nbsp;&nbsp;Here <font color="red"><b>/path</b></font> is the page name.
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th width="100" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                            <td width="480">
                                                                                <input type="radio" name="page_bool" value="true" checked>YES (default)
                                                                                <input type="radio" name="page_bool" value="false">NO
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th width="100"></th>
                                                                            <td width="410" colspan="2">
                                                                                <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                    <tr>
                                                                                        <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                            <div class="statistics-box inventory">
                                                                                                <div class="statistics-body">
                                                                                                    <li>YES - This allows access to the file.<br>
                                                                                                    <li>NO - Unconditional access to this file will be blocked.
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th width="100" height="30" bgcolor="#ddf3fa">Apply Base</th>
                                                                            <td width="480">
                                                                                <input type="radio" name="page_base" value="allow" checked>White List (default)
                                                                                <input type="radio" name="page_base" value="deny">Black List
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <br>
                                                                    <table width="475" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                        <tr>
                                                                            <td width="175">&nbsp;</td>
                                                                            <td width="300"><br>
                                                                                <span class="cm-button-main cm-process-items">
                                                                                    <input type="submit" value="Confirm">
                                                                                </span>
                                                                                <span class="cm-button-main cm-process-items">
                                                                                    <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="history.back(-1);
                                                                                            return false;">
                                                                                </span>
                                                                                <br><br>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </form>
                                                                <?php
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr bgcolor="#A0A0A0">
                            <td width="100%" height="50" colspan="2" align="center">
                                <?php include_once("admin_bottom.php"); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
