<?php

include_once("admin_lib.php");
check_authorized();

init_page();

//$page['mode']
if (isset($_GET) && isset($_GET['mode']))
    $page['mode'] = $_GET['mode'];

if ($page['mode'] == "POLICY_SQL_INJECTION")
{
    clear_submit();

    if (isset($_POST['policy_sql_injection']))
        $submit['policy_sql_injection'] = $_POST['policy_sql_injection'];

    if (isset($_POST['policy_sql_injection_list']))
        $submit['policy_sql_injection_list'] = $_POST['policy_sql_injection_list'];

    $check['submit'] = array("policy_sql_injection", "policy_sql_injection_list",);
    check_submit($check['submit']);

    if ($submit['policy_sql_injection'] == "true")
        $_SECURE_POLICY['POLICY']['SQL_INJECTION']['BOOL'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['POLICY']['SQL_INJECTION']['BOOL'] = base64_encode("FALSE");

    if (isset($_SECURE_POLICY['POLICY']['SQL_INJECTION']['LIST']))
        unset($_SECURE_POLICY['POLICY']['SQL_INJECTION']['LIST']);

    $index = 0;

    $string = $submit['policy_sql_injection_list'];
    $string = trim($string);

    $token = strtok($string, ", \n\t");
    while ($token)
    {
        $token = trim($token);

        if (get_magic_quotes_gpc())
            $token = stripslashes($token);

        if (strlen($token) > 0)
            $_SECURE_POLICY['POLICY']['SQL_INJECTION']['LIST'][$index++] = base64_encode($token);

        $token = strtok(", \n\t");
    }

    if (!$index)
    {
        $_SECURE_POLICY['POLICY']['SQL_INJECTION']['BOOL'] = base64_encode("FALSE");
        $_SECURE_POLICY['POLICY']['SQL_INJECTION']['LIST']['0'] = "";

        html_msg("SQL_INJECTION policy no exist.");
    }

    write_policy();

    html_msgmove("SQL_INJECTION policy modified.", "admin_policy.php#sql_injection");

    exit;
}
/* SQL_INJECTION */

/* XSS */
if ($page['mode'] == "POLICY_XSS")
{
    clear_submit();

    if (isset($_POST['policy_xss']))
        $submit['policy_xss'] = $_POST['policy_xss'];

    if (isset($_POST['policy_xss_list']))
        $submit['policy_xss_list'] = $_POST['policy_xss_list'];

    $check['submit'] = array("policy_xss", "policy_xss_list",);
    check_submit($check['submit']);

    $_SECURE_POLICY['POLICY']['XSS']['BOOL'] = base64_encode("FALSE");

    if ($submit['policy_xss'] == "true")
        $_SECURE_POLICY['POLICY']['XSS']['BOOL'] = base64_encode("TRUE");

    if (isset($_SECURE_POLICY['POLICY']['XSS']['LIST']))
        unset($_SECURE_POLICY['POLICY']['XSS']['LIST']);

    $index = 0;

    $string = $submit['policy_xss_list'];
    $string = trim($string);

    $token = strtok($string, ", \n\t");
    while ($token)
    {
        $token = trim($token);

        // magic_quotes_gpc()
        if (get_magic_quotes_gpc())
            $token = stripslashes($token);

        if (strlen($token) > 0)
            $_SECURE_POLICY['POLICY']['XSS']['LIST'][$index++] = base64_encode($token);

        $token = strtok(", \n\t");
    }

    if (!$index)
    {
        $_SECURE_POLICY['POLICY']['XSS']['BOOL'] = base64_encode("FALSE");
        $_SECURE_POLICY['POLICY']['XSS']['LIST']['0'] = "";

        html_msg("XSS policy no exist.");
    }

    write_policy();

    html_msgmove("XSS policy modified.", "admin_policy.php#xss");

    exit;
}
/* XSS */

/* WORD */
if ($page['mode'] == "POLICY_WORD")
{
    clear_submit();

    if (isset($_POST['policy_word']))
        $submit['policy_word'] = $_POST['policy_word'];

    if (isset($_POST['policy_word_list']))
        $submit['policy_word_list'] = $_POST['policy_word_list'];

    $check['submit'] = array("policy_word", "policy_word_list",);
    check_submit($check['submit']);

    $_SECURE_POLICY['POLICY']['WORD']['BOOL'] = base64_encode("FALSE");

    if ($submit['policy_word'] == "true")
        $_SECURE_POLICY['POLICY']['WORD']['BOOL'] = base64_encode("TRUE");

    if (isset($_SECURE_POLICY['POLICY']['WORD']['LIST']))
        unset($_SECURE_POLICY['POLICY']['WORD']['LIST']);

    $index = 0;

    $string = $submit['policy_word_list'];
    $string = trim($string);

    $token = strtok($string, ", \n\t");
    while ($token)
    {
        $token = trim($token);

        // magic_quotes_gpc()
        if (get_magic_quotes_gpc())
            $token = stripslashes($token);

        if (strlen($token) > 0)
            $_SECURE_POLICY['POLICY']['WORD']['LIST'][$index++] = base64_encode($token);

        $token = strtok(", \n\t");
    }

    if (!$index)
    {
        $_SECURE_POLICY['POLICY']['WORD']['BOOL'] = base64_encode("FALSE");
        $_SECURE_POLICY['POLICY']['WORD']['LIST']['0'] = "";

        html_msg("WORD policy no exist.");
    }

    write_policy();

    html_msgmove("WORD policy modified.", "admin_policy.php#word");

    exit;
}
/* WORD */

/* TAG */
if ($page['mode'] == "POLICY_TAG")
{
    clear_submit();

    if (isset($_POST['policy_tag']))
        $submit['policy_tag'] = $_POST['policy_tag'];

    if (isset($_POST['policy_tag_list']))
        $submit['policy_tag_list'] = $_POST['policy_tag_list'];

    $check['submit'] = array("policy_tag", "policy_tag_list",);
    check_submit($check['submit']);

    $_SECURE_POLICY['POLICY']['TAG']['BOOL'] = base64_encode("FALSE");

    if ($submit['policy_tag'] == "true")
        $_SECURE_POLICY['POLICY']['TAG']['BOOL'] = base64_encode("TRUE");

    if (isset($_SECURE_POLICY['POLICY']['TAG']['LIST']))
        unset($_SECURE_POLICY['POLICY']['TAG']['LIST']);

    $index = 0;

    $string = $submit['policy_tag_list'];
    $string = trim($string);

    $token = strtok($string, ", \n\t");
    while ($token)
    {
        $token = trim($token);

        // magic_quotes_gpc()
        if (get_magic_quotes_gpc())
            $token = stripslashes($token);

        if (strlen($token) > 0)
            $_SECURE_POLICY['POLICY']['TAG']['LIST'][$index++] = base64_encode($token);

        $token = strtok(", \n\t");
    }

    if (!$index)
    {
        $_SECURE_POLICY['POLICY']['TAG']['BOOL'] = base64_encode("FALSE");
        $_SECURE_POLICY['POLICY']['TAG']['LIST']['0'] = "";

        html_msg("TAG policy no exist.");
    }

    // write policy info
    write_policy();

    html_msgmove("TAG policy modified.", "admin_policy.php#tag");

    exit;
}
/* TAG */

/* IP */
if ($page['mode'] == "POLICY_IP")
{
    clear_submit();

    if (isset($_POST['policy_ip']))
        $submit['policy_ip'] = $_POST['policy_ip'];

    if (isset($_POST['policy_ip_base']))
        $submit['policy_ip_base'] = $_POST['policy_ip_base'];

    if (isset($_POST['policy_ip_list']))
        $submit['policy_ip_list'] = $_POST['policy_ip_list'];

    $check['submit'] = array("policy_ip", "policy_ip_base", "policy_ip_list",);
    check_submit($check['submit']);

    $_SECURE_POLICY['POLICY']['IP']['BOOL'] = base64_encode("FALSE");

    if ($submit['policy_ip'] == "true")
        $_SECURE_POLICY['POLICY']['IP']['BOOL'] = base64_encode("TRUE");

    $_SECURE_POLICY['POLICY']['IP']['ALLOW'] = base64_encode("FALSE");
    $_SECURE_POLICY['POLICY']['IP']['DENY'] = base64_encode("FALSE");

    if ($submit['policy_ip_base'] == "allow")
        $_SECURE_POLICY['POLICY']['IP']['ALLOW'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['POLICY']['IP']['DENY'] = base64_encode("TRUE");

    if (isset($_SECURE_POLICY['POLICY']['IP']['LIST']))
        unset($_SECURE_POLICY['POLICY']['IP']['LIST']);

    $index = 0;

    $string = $submit['policy_ip_list'];
    $string = trim($string);

    $token = strtok($string, ", \n\t");
    while ($token)
    {
        $token = trim($token);

        if (get_magic_quotes_gpc())
            $token = stripslashes($token);

        if (strlen($token) > 0)
            $_SECURE_POLICY['POLICY']['IP']['LIST'][$index++] = base64_encode($token);

        $token = strtok(", \n\t");
    }

    if (!$index)
    {
        $_SECURE_POLICY['POLICY']['IP']['BOOL'] = base64_encode("FALSE");
        $_SECURE_POLICY['POLICY']['IP']['LIST']['0'] = "";

        html_msg("IP policy no exist.");
    }

    write_policy();

    html_msgmove("IP policy modified.", "admin_policy.php#ip");

    exit;
}
/* IP */

/* FILE */
if ($page['mode'] == "POLICY_FILE")
{
    clear_submit();

    if (isset($_POST['policy_filename']))
        $submit['policy_filename'] = $_POST['policy_filename'];

    if (isset($_POST['policy_filename_base']))
        $submit['policy_filename_base'] = $_POST['policy_filename_base'];

    if (isset($_POST['policy_filename_list']))
        $submit['policy_filename_list'] = $_POST['policy_filename_list'];

    if (isset($_POST['policy_filetype']))
        $submit['policy_filetype'] = $_POST['policy_filetype'];

    if (isset($_POST['policy_filetype_base']))
        $submit['policy_filetype_base'] = $_POST['policy_filetype_base'];

    if (isset($_POST['policy_filetype_list']))
        $submit['policy_filetype_list'] = $_POST['policy_filetype_list'];

    if (isset($_POST['policy_filesize']))
        $submit['policy_filesize'] = $_POST['policy_filesize'];

    if (isset($_POST['policy_filesize_min_size']))
        $submit['policy_filesize_min_size'] = $_POST['policy_filesize_min_size'];

    if (isset($_POST['policy_filesize_max_size']))
        $submit['policy_filesize_max_size'] = $_POST['policy_filesize_max_size'];

    // check exceptions
    $check['submit'] = array("policy_filename", "policy_filename_base", "policy_filename_list",
        "policy_filetype", "policy_filetype_base", "policy_filetype_list",
        "policy_filesize", "policy_filesize_min_size", "policy_filesize_max_size",);
    check_submit($check['submit']);

    if (!strlen($submit['policy_filesize_min_size']))
        html_msgback("Input the min size of file.");

    if (!strlen($submit['policy_filesize_max_size']))
        html_msgback("input the max file size.");

    $_SECURE_POLICY['POLICY']['FILENAME']['BOOL'] = base64_encode("FALSE");

    if ($submit['policy_filename'] == "true")
        $_SECURE_POLICY['POLICY']['FILENAME']['BOOL'] = base64_encode("TRUE");

    $_SECURE_POLICY['POLICY']['FILENAME']['ALLOW'] = base64_encode("FALSE");
    $_SECURE_POLICY['POLICY']['FILENAME']['DENY'] = base64_encode("FALSE");

    if ($submit['policy_filename_base'] == "allow")
        $_SECURE_POLICY['POLICY']['FILENAME']['ALLOW'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['POLICY']['FILENAME']['DENY'] = base64_encode("TRUE");

    if (isset($_SECURE_POLICY['POLICY']['FILENAME']['LIST']))
        unset($_SECURE_POLICY['POLICY']['FILENAME']['LIST']);

    $index = 0;

    $string = $submit['policy_filename_list'];
    $string = trim($string);

    $token = strtok($string, ", \n\t");
    while ($token)
    {
        $token = trim($token);

        // magic_quotes_gpc()
        if (get_magic_quotes_gpc())
            $token = stripslashes($token);

        if (strlen($token) > 0)
            $_SECURE_POLICY['POLICY']['FILENAME']['LIST'][$index++] = base64_encode($token);

        $token = strtok(", \n\t");
    }

    if (!$index)
    {
        $_SECURE_POLICY['POLICY']['FILENAME']['BOOL'] = base64_encode("FALSE");
        $_SECURE_POLICY['POLICY']['FILENAME']['LIST']['0'] = "";

        html_msg("FILENAME policy no exist.");
    }

    $_SECURE_POLICY['POLICY']['FILETYPE']['BOOL'] = base64_encode("FALSE");

    if ($submit['policy_filetype'] == "true")
        $_SECURE_POLICY['POLICY']['FILETYPE']['BOOL'] = base64_encode("TRUE");

    $_SECURE_POLICY['POLICY']['FILETYPE']['ALLOW'] = base64_encode("FALSE");
    $_SECURE_POLICY['POLICY']['FILETYPE']['DENY'] = base64_encode("FALSE");

    if ($submit['policy_filetype_base'] == "allow")
        $_SECURE_POLICY['POLICY']['FILETYPE']['ALLOW'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['POLICY']['FILETYPE']['DENY'] = base64_encode("TRUE");

    if (isset($_SECURE_POLICY['POLICY']['FILETYPE']['LIST']))
        unset($_SECURE_POLICY['POLICY']['FILETYPE']['LIST']);

    $index = 0;

    $string = $submit['policy_filetype_list'];
    $string = trim($string);

    $token = strtok($string, ", \n\t");
    while ($token)
    {
        $token = trim($token);

        // magic_quotes_gpc()
        if (get_magic_quotes_gpc())
            $token = stripslashes($token);

        if (strlen($token) > 0)
            $_SECURE_POLICY['POLICY']['FILETYPE']['LIST'][$index++] = base64_encode($token);

        $token = strtok(", \n\t");
    }

    if (!$index)
    {
        $_SECURE_POLICY['POLICY']['FILETYPE']['BOOL'] = base64_encode("FALSE");
        $_SECURE_POLICY['POLICY']['FILETYPE']['LIST']['0'] = "";

        html_msg("FILETYPE policy no exist.");
    }

    $_SECURE_POLICY['POLICY']['FILESIZE']['BOOL'] = base64_encode("FALSE");

    if ($submit['policy_filesize'] == "true")
        $_SECURE_POLICY['POLICY']['FILESIZE']['BOOL'] = base64_encode("TRUE");

    $min_size = $submit['policy_filesize_min_size'];
    $max_size = $submit['policy_filesize_max_size'];

    if (isset($_SECURE_POLICY['POLICY']['FILESIZE']['MIN_SIZE']))
        unset($_SECURE_POLICY['POLICY']['FILESIZE']['MIN_SIZE']);

    if (isset($_SECURE_POLICY['POLICY']['FILESIZE']['MAX_SIZE']))
        unset($_SECURE_POLICY['POLICY']['FILESIZE']['MAX_SIZE']);

    $min_size = trim($min_size);
    $max_size = trim($max_size);

    $_SECURE_POLICY['POLICY']['FILESIZE']['MIN_SIZE'] = base64_encode($min_size);
    $_SECURE_POLICY['POLICY']['FILESIZE']['MAX_SIZE'] = base64_encode($max_size);

    write_policy();

    html_msgmove("FILENAME policy modified.", "admin_policy.php#file");

    exit;
}

/* DOS */
if ($page['mode'] == "POLICY_DOS")
{
    clear_submit();

    if (isset($_POST['policy_dos']))
        $submit['policy_dos'] = $_POST['policy_dos'];
    
    if (isset($_POST['policy_dos_peroid']))
        $submit['policy_dos_peroid'] = $_POST['policy_dos_peroid'];

    if (isset($_POST['policy_dos_hurdle']))
        $submit['policy_dos_hurdle'] = $_POST['policy_dos_hurdle'];
    
    if ($submit['policy_dos'] == "true")
        $_SECURE_POLICY['POLICY']['DOS']['BOOL'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['POLICY']['DOS']['BOOL'] = base64_encode("FALSE");
    
    $period = $submit['policy_dos_peroid'];
    $hurdle = $submit['policy_dos_hurdle'];

    if (isset($_SECURE_POLICY['POLICY']['DOS']['PERIOD']))
        unset($_SECURE_POLICY['POLICY']['DOS']['PERIOD']);

    if (isset($_SECURE_POLICY['POLICY']['DOS']['HURDLE']))
        unset($_SECURE_POLICY['POLICY']['DOS']['HURDLE']);

    $period = trim($period);
    $hurdle = trim($hurdle);

    $_SECURE_POLICY['POLICY']['DOS']['PERIOD'] = base64_encode($period);
    $_SECURE_POLICY['POLICY']['DOS']['HURDLE'] = base64_encode($hurdle);
    
    write_policy();

    html_msgmove("DOS policy modified.", "admin_policy.php#dos");

    exit;
}
/* DOS */

/* LOGIN */
if ($page['mode'] == "POLICY_LOGIN")
{
    clear_submit();

    if (isset($_POST['policy_login']))
        $submit['policy_login'] = $_POST['policy_login'];
    
    if (isset($_POST['policy_login_peroid']))
        $submit['policy_login_peroid'] = $_POST['policy_login_peroid'];

    if (isset($_POST['policy_secadmin_hurdle']))
        $submit['policy_secadmin_hurdle'] = $_POST['policy_secadmin_hurdle'];
    
    if (isset($_POST['policy_admin_hurdle']))
        $submit['policy_admin_hurdle'] = $_POST['policy_admin_hurdle'];
    
    if (isset($_POST['policy_user_hurdle']))
        $submit['policy_user_hurdle'] = $_POST['policy_user_hurdle'];
    
    if ($submit['policy_login'] == "true")
        $_SECURE_POLICY['POLICY']['LOGIN']['BOOL'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['POLICY']['LOGIN']['BOOL'] = base64_encode("FALSE");
    
    $period = $submit['policy_login_peroid'];
    $secadmin_hurdle = $submit['policy_secadmin_hurdle'];   
    $admin_hurdle = $submit['policy_admin_hurdle'];
    $user_hurdle = $submit['policy_user_hurdle'];

    if (isset($_SECURE_POLICY['POLICY']['LOGIN']['PERIOD']))
        unset($_SECURE_POLICY['POLICY']['LOGIN']['PERIOD']);

    if (isset($_SECURE_POLICY['POLICY']['LOGIN']['SECADMIN']))
        unset($_SECURE_POLICY['POLICY']['LOGIN']['SECADMIN']);
    
    if (isset($_SECURE_POLICY['POLICY']['LOGIN']['ADMIN']))
        unset($_SECURE_POLICY['POLICY']['LOGIN']['ADMIN']);
    
    if (isset($_SECURE_POLICY['POLICY']['LOGIN']['USER']))
        unset($_SECURE_POLICY['POLICY']['LOGIN']['USER']);

    $period = trim($period);
    $secadmin_hurdle = trim($secadmin_hurdle);   
    $admin_hurdle = trim($admin_hurdle);
    $user_hurdle = trim($user_hurdle);

    $_SECURE_POLICY['POLICY']['LOGIN']['PERIOD'] = base64_encode($period);
    $_SECURE_POLICY['POLICY']['LOGIN']['SECADMIN'] = base64_encode($secadmin_hurdle);
    $_SECURE_POLICY['POLICY']['LOGIN']['ADMIN'] = base64_encode($admin_hurdle);
    $_SECURE_POLICY['POLICY']['LOGIN']['USER'] = base64_encode($user_hurdle);
    
    write_policy();

    html_msgmove("Login limits modified.", "admin_policy.php#login");

    exit;
}
/* LOGIN */
?>
