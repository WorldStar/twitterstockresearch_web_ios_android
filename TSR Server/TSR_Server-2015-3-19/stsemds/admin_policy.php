<?php
include_once("admin_lib.php");
check_authorized();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" HREF="style.css" type="text/css" title="style">
    </head>
    <body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="#D0D0D0">
        <?php include_once("admin_title.php"); ?>
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
            <tr> 
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr bgcolor="#CACACA">
                            <td width="100%" height="80" colspan="2">
                                <?php include_once("admin_top.php"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="160" bgcolor="#f3f3f3">
                                <?php include_once("admin_menu.php"); ?>
                            </td>
                            <td width="100%" bgcolor="#f3f3f3" valign="top">
                                <br><br>
                                <div id="main_column" class="clear">
                                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr valign="top">
                                            <td width="100%">
                                                <table width="100%" height="100%" cellspacing="20" cellpadding="0" border="0">
                                                    <tr valign="top">
                                                        <td width="100%">

                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Policy settings</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>

                                                            <br>
                                                            <table width="790" cellspacing="10" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="100%" style="line-height:160%" nowrap>
                                                                        <b>Note: Be careful with the settings apply whether beware error.</b><br>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <br>
                                                            <?php
                                                            /* LOGIN */
                                                            $print['login_bool_true_check'] = "";
                                                            $print['login_bool_false_check'] = "";
                                                            
                                                            // LOGIN
                                                            $policy['login_bool'] = & $_SECURE_POLICY['POLICY']['LOGIN']['BOOL'];
                                                            $policy['login_period'] = & $_SECURE_POLICY['POLICY']['LOGIN']['PERIOD']; 
                                                            $policy['secadmin_hurdle'] = & $_SECURE_POLICY['POLICY']['LOGIN']['SECADMIN'];
                                                            $policy['admin_hurdle'] = & $_SECURE_POLICY['POLICY']['LOGIN']['ADMIN'];
                                                            $policy['user_hurdle'] = & $_SECURE_POLICY['POLICY']['LOGIN']['USER'];                                                           

                                                            $policy['login_bool'] = base64_decode($policy['login_bool']);
                                                            $print['login_period'] = base64_decode($policy['login_period']); 
                                                            $print['secadmin_hurdle'] = base64_decode($policy['secadmin_hurdle']);
                                                            $print['admin_hurdle'] = base64_decode($policy['admin_hurdle']);
                                                            $print['user_hurdle'] = base64_decode($policy['user_hurdle']);

                                                            if ($policy['login_bool'] == "TRUE")
                                                                $print['login_bool_true_check'] = "checked";
                                                            else
                                                                $print['login_bool_false_check'] = "checked";
                                                            
                                                            if($print['login_period'] === "")
                                                                $print['login_period'] = "1200";
                                                            if($print['secadmin_hurdle'] === "")
                                                                $print['secadmin_hurdle'] = "3";
                                                            if($print['admin_hurdle'] === "")
                                                                $print['admin_hurdle'] = "3";
                                                            if($print['user_hurdle'] === "")
                                                                $print['user_hurdle'] = "3";
                                                            ?>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 1px solid #9fa8b3;background-color: #ddf3fa;" align="center">
                                                                        <p class="field-title">Login Limits</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <form action="admin_policy_submit.php?mode=POLICY_LOGIN" method="post">
                                                                <table width="780" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                    <tr>
                                                                        <th width="90" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td>
                                                                            <input type="radio" name="policy_login" value="true" <?php echo $print['login_bool_true_check'] ?>>YES
                                                                            <input type="radio" name="policy_login" value="false" <?php echo $print['login_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120"></th>
                                                                        <td width="410" colspan="2">
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>YES - Run the detect in excess of login.
                                                                                                <li>NO&nbsp; - Do not run the detect in excess of login.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Detection period</th>
                                                                        <td>
                                                                            <input type="text" name="policy_login_peroid" class="input-text" size="20" value="<?php echo $print['login_period'] ?>"> (Unit: Second)
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Login Hurdle for Security Admin</th>
                                                                        <td>
                                                                            <input type="text" name="policy_secadmin_hurdle" class="input-text" size="20" value="<?php echo $print['secadmin_hurdle'] ?>"> (Logins in the time period)
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Login Hurdle for Site Admin</th>
                                                                        <td>
                                                                            <input type="text" name="policy_admin_hurdle" class="input-text" size="20" value="<?php echo $print['admin_hurdle'] ?>"> (Logins in the time period)
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Login Hurdle for Site User</th>
                                                                        <td>
                                                                            <input type="text" name="policy_user_hurdle" class="input-text" size="20" value="<?php echo $print['user_hurdle'] ?>"> (Logins in the time period)
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <br>
                                                                <table width="790" height="50" cellspacing="0" cellpadding="0" border="0">
                                                                    <tr valign="top">
                                                                        <td width="140">&nbsp;</td>
                                                                        <td width="670" align="left">
                                                                            <span class="cm-button-main cm-process-items">
                                                                                <input type="submit" value="Conform">
                                                                            </span>
                                                                            <span class="cm-button-main cm-process-items">
                                                                                <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                        return false;">
                                                                            </span>
                                                                            <br><br>
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </form>
                                                            <br>
                                                            <?php
                                                            /* DOS */
                                                            $print['dos_bool_true_check'] = "";
                                                            $print['dos_bool_false_check'] = "";
                                                            
                                                            // DOS
                                                            $policy['dos_bool'] = & $_SECURE_POLICY['POLICY']['DOS']['BOOL'];
                                                            $policy['dos_period'] = & $_SECURE_POLICY['POLICY']['DOS']['PERIOD'];
                                                            $policy['dos_hurdle'] = & $_SECURE_POLICY['POLICY']['DOS']['HURDLE'];

                                                            $print['dos_bool'] = base64_decode($policy['dos_bool']);
                                                            $print['dos_period'] = base64_decode($policy['dos_period']);
                                                            $print['dos_hurdle'] = base64_decode($policy['dos_hurdle']);

                                                            if ($print['dos_bool'] == "TRUE")
                                                                $print['dos_bool_true_check'] = "checked";
                                                            else
                                                                $print['dos_bool_false_check'] = "checked";
                                                            
                                                            if($print['dos_period'] === "")
                                                                $print['dos_period'] = "120";
                                                            if($print['dos_hurdle'] === "")
                                                                $print['dos_hurdle'] = "50";
                                                            ?>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 1px solid #9fa8b3;background-color: #ddf3fa;" align="center">
                                                                        <p class="field-title">DOS (Denial-of-service)</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <form action="admin_policy_submit.php?mode=POLICY_DOS" method="post">
                                                                <table width="780" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                    <tr>
                                                                        <th width="90" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td>
                                                                            <input type="radio" name="policy_dos" value="true" <?php echo $print['dos_bool_true_check'] ?>>YES
                                                                            <input type="radio" name="policy_dos" value="false" <?php echo $print['dos_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120"></th>
                                                                        <td width="410" colspan="2">
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>YES - Run the DOS detection.
                                                                                                <li>NO&nbsp; - Do not run the DOS detection.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Detection period</th>
                                                                        <td>
                                                                            <input type="text" name="policy_dos_peroid" class="input-text" size="20" value="<?php echo $print['dos_period'] ?>"> (Unit: Second)
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Hurdle</th>
                                                                        <td>
                                                                            <input type="text" name="policy_dos_hurdle" class="input-text" size="20" value="<?php echo $print['dos_hurdle'] ?>"> (Requests in the time period)
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <br>
                                                                <table width="790" height="50" cellspacing="0" cellpadding="0" border="0">
                                                                    <tr valign="top">
                                                                        <td width="140">&nbsp;</td>
                                                                        <td width="670" align="left">
                                                                            <span class="cm-button-main cm-process-items">
                                                                                <input type="submit" value="Conform">
                                                                            </span>
                                                                            <span class="cm-button-main cm-process-items">
                                                                                <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                        return false;">
                                                                            </span>
                                                                            <br><br>
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </form>
                                                            
                                                            <?php
                                                            /* SQL INJECTION */

                                                            // SQL INJECTION
                                                            $policy['sql_injection_bool'] = & $_SECURE_POLICY['POLICY']['SQL_INJECTION']['BOOL'];

                                                            if (isset($_SECURE_POLICY['POLICY']['SQL_INJECTION']['LIST']))
                                                                $policy['sql_injection_list'] = & $_SECURE_POLICY['POLICY']['SQL_INJECTION']['LIST'];
                                                            else
                                                                $policy['sql_injection_list'] = array();

                                                            $policy['sql_injection_bool'] = base64_decode($policy['sql_injection_bool']);

                                                            $print['sql_injection_bool_true_check'] = "";
                                                            $print['sql_injection_bool_false_check'] = "";
                                                            $print['sql_injection_list'] = "";

                                                            if ($policy['sql_injection_bool'] == "TRUE")
                                                                $print['sql_injection_bool_true_check'] = "checked";
                                                            else
                                                                $print['sql_injection_bool_false_check'] = "checked";

                                                            foreach ($policy['sql_injection_list'] as $key => $value)
                                                            {
                                                                $value = trim($value);

                                                                $print['sql_injection_list'] .= base64_decode($value);
                                                                $print['sql_injection_list'] .= "\n";
                                                            }
                                                            ?>
                                                            <br>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 1px solid #9fa8b3;background-color: #ddf3fa;" align="center">
                                                                        <p class="field-title">SQL Injection</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table width="780" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                <form action="admin_policy_submit.php?mode=POLICY_SQL_INJECTION" method="post">
                                                                    <tr>
                                                                        <th width="120" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td width="670">
                                                                            <input type="radio" name="policy_sql_injection" value="true" <?php echo $print['sql_injection_bool_true_check'] ?>>YES
                                                                            <input type="radio" name="policy_sql_injection" value="false" <?php echo $print['sql_injection_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120"></th>
                                                                        <td width="410" colspan="2">
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>YES - Run the SQL Injection vulnerability detection.
                                                                                                <li>NO&nbsp; - Do not run SQL Injection vulnerability detection.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120" height="30" valign="center" bgcolor="#ddf3fa">Pattern List</th>
                                                                        <td width="410">
                                                                            <textarea cols="93" rows="10" class="input-textarea-long" class="input-textarea-long" name="policy_sql_injection_list"><?php echo $print['sql_injection_list'] ?></textarea>
                                                                        </td>
                                                                    </tr>
                                                            </table>
                                                            <br>
                                                            <table width="790" height="35" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr valign="top">
                                                                    <td width="140">&nbsp;</td>
                                                                    <td width="670" align="left">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="submit" value="Conform">
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                    return false;">
                                                                        </span>
                                                                        <br><br>
                                                                    </td>
                                                                </tr>
                                                                </form>
                                                            </table>
                                                            <?php
                                                            /* SQL INJECTION */
                                                            ?>

                                                            <?php
                                                            /* XSS */

                                                            // XSS
                                                            $policy['xss_bool'] = & $_SECURE_POLICY['POLICY']['XSS']['BOOL'];

                                                            if (isset($_SECURE_POLICY['POLICY']['XSS']['LIST']))
                                                                $policy['xss_list'] = & $_SECURE_POLICY['POLICY']['XSS']['LIST'];
                                                            else
                                                                $policy['xss_list'] = array();

                                                            $policy['xss_bool'] = base64_decode($policy['xss_bool']);

                                                            $print['xss_bool_true_check'] = "";
                                                            $print['xss_bool_false_check'] = "";
                                                            $print['xss_list'] = "";

                                                            if ($policy['xss_bool'] == "TRUE")
                                                                $print['xss_bool_true_check'] = "checked";
                                                            else
                                                                $print['xss_bool_false_check'] = "checked";

                                                            foreach ($policy['xss_list'] as $key => $value)
                                                            {
                                                                $value = trim($value);

                                                                $print['xss_list'] .= base64_decode($value);
                                                                $print['xss_list'] .= "\n";
                                                            }
                                                            ?>
                                                            <br>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 1px solid #9fa8b3;background-color: #ddf3fa;" align="center">
                                                                        <p class="field-title">XSS (Cross-Site Script)</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table width="780" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                <form action="admin_policy_submit.php?mode=POLICY_XSS" method="post">
                                                                    <tr>
                                                                        <th width="120" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td width="670">
                                                                            <input type="radio" name="policy_xss" value="true" <?php echo $print['xss_bool_true_check'] ?>>YES
                                                                            <input type="radio" name="policy_xss" value="false" <?php echo $print['xss_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120"></th>
                                                                        <td width="410" colspan="2">
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>YES - Run the XSS vulnerability detection.
                                                                                                <li>NO&nbsp; - Do not run the XSS vulnerability detection.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120" height="30" bgcolor="#ddf3fa">Pattern List</th>
                                                                        <td width="410" colspan="2">
                                                                            <textarea cols="65" rows="10" class="input-textarea-long" name="policy_xss_list"><?php echo $print['xss_list'] ?></textarea>
                                                                        </td>
                                                                    </tr>
                                                            </table>
                                                            <br>
                                                            <table width="790" height="35" cellspacing="0" cellpadding="0" border="0">
                                                                <tr valign="top">
                                                                    <td width="140">&nbsp;</td>
                                                                    <td width="670" align="left">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="submit" value="Conform">
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                    return false;">
                                                                        </span>
                                                                        <br><br>
                                                                    </td>
                                                                </tr>
                                                                </form>
                                                            </table>
                                                            <?php
                                                            /* XSS */
                                                            ?>

                                                            <?php
                                                            /* WORD */

                                                            // WORD
                                                            $policy['word_bool'] = & $_SECURE_POLICY['POLICY']['WORD']['BOOL'];

                                                            if (isset($_SECURE_POLICY['POLICY']['WORD']['LIST']))
                                                                $policy['word_list'] = & $_SECURE_POLICY['POLICY']['WORD']['LIST'];
                                                            else
                                                                $policy['word_list'] = array();

                                                            $policy['word_bool'] = base64_decode($policy['word_bool']);

                                                            $print['word_bool_true_check'] = "";
                                                            $print['word_bool_false_check'] = "";
                                                            $print['word_list'] = "";

                                                            if ($policy['word_bool'] == "TRUE")
                                                                $print['word_bool_true_check'] = "checked";
                                                            else
                                                                $print['word_bool_false_check'] = "checked";

                                                            foreach ($policy['word_list'] as $key => $value)
                                                            {
                                                                $value = trim($value);

                                                                $print['word_list'] .= base64_decode($value);
                                                                $print['word_list'] .= "\n";
                                                            }
                                                            ?>
                                                            <br>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 1px solid #9fa8b3;background-color: #ddf3fa;" align="center">
                                                                        <p class="field-title">BAD WORD</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table width="780" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                <form action="admin_policy_submit.php?mode=POLICY_WORD" method="post">
                                                                    <tr>
                                                                        <th width="120" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td width="670">
                                                                            <input type="radio" name="policy_word" value="true" <?php echo $print['word_bool_true_check'] ?>>YES
                                                                            <input type="radio" name="policy_word" value="false" <?php echo $print['word_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120"></th>
                                                                        <td width="410" colspan="2">
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>YES - Run the WORD vulnerability detection.
                                                                                                <li>NO&nbsp; - Do not run the WORD vulnerability detection.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120" height="30" bgcolor="#ddf3fa">Pattern List</th>
                                                                        <td width="410" colspan="2">
                                                                            <textarea cols="65" rows="10" class="input-textarea-long" name="policy_word_list"><?php echo $print['word_list'] ?></textarea>
                                                                        </td>
                                                                    </tr>
                                                            </table>
                                                            <br>
                                                            <table width="790" height="35" cellspacing="0" cellpadding="0" border="0">
                                                                <tr valign="top">
                                                                    <td width="140">&nbsp;</td>
                                                                    <td width="670" align="left">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="submit" value="Conform">
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                    return false;">
                                                                        </span>
                                                                        <br><br>
                                                                    </td>
                                                                </tr>
                                                                </form>
                                                            </table>
                                                            <?php
                                                            /* WORD */
                                                            ?>

                                                            <?php
                                                            /* TAG */

                                                            // TAG
                                                            $policy['tag_bool'] = & $_SECURE_POLICY['POLICY']['TAG']['BOOL'];

                                                            if (isset($_SECURE_POLICY['POLICY']['TAG']['LIST']))
                                                                $policy['tag_list'] = & $_SECURE_POLICY['POLICY']['TAG']['LIST'];
                                                            else
                                                                $policy['tag_list'] = array();

                                                            $policy['tag_bool'] = base64_decode($policy['tag_bool']);

                                                            $print['tag_bool_true_check'] = "";
                                                            $print['tag_bool_false_check'] = "";
                                                            $print['tag_list'] = "";

                                                            if ($policy['tag_bool'] == "TRUE")
                                                                $print['tag_bool_true_check'] = "checked";
                                                            else
                                                                $print['tag_bool_false_check'] = "checked";

                                                            foreach ($policy['tag_list'] as $key => $value)
                                                            {
                                                                $value = trim($value);

                                                                $print['tag_list'] .= base64_decode($value);
                                                                $print['tag_list'] .= "\n";
                                                            }
                                                            ?>
                                                            <br>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 1px solid #9fa8b3;background-color: #ddf3fa;" align="center">
                                                                        <p class="field-title">TAG</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table width="780" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                <form action="admin_policy_submit.php?mode=POLICY_TAG" method="post">
                                                                    <tr>
                                                                        <th width="120" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td width="670">
                                                                            <input type="radio" name="policy_tag" value="true" <?php echo $print['tag_bool_true_check'] ?>>YES
                                                                            <input type="radio" name="policy_tag" value="false" <?php echo $print['tag_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120"></th>
                                                                        <td width="410" colspan="2">
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>YES - Run the TAG vulnerability detection.
                                                                                                <li>NO&nbsp; - Do not run the TAG vulnerability detection.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120" height="30" bgcolor="#ddf3fa">Pattern List</th>
                                                                        <td width="410" colspan="2">
                                                                            <textarea cols="65" rows="10" class="input-textarea-long" name="policy_tag_list"><?php echo $print['tag_list'] ?></textarea>
                                                                        </td>
                                                                    </tr>
                                                            </table>
                                                            <br>
                                                            <table width="790" height="35" cellspacing="0" cellpadding="0" border="0">
                                                                <tr valign="top">
                                                                    <td width="140">&nbsp;</td>
                                                                    <td width="670" align="left">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="submit" value="Conform">
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                    return false;">
                                                                        </span>
                                                                        <br><br>
                                                                    </td>
                                                                </tr>
                                                                </form>
                                                            </table>
                                                            <?php
                                                            /* TAG */
                                                            ?>

                                                            <?php
                                                            /* IP */

                                                            // IP
                                                            $policy['ip_bool'] = & $_SECURE_POLICY['POLICY']['IP']['BOOL'];
                                                            $policy['ip_allow'] = & $_SECURE_POLICY['POLICY']['IP']['ALLOW'];
                                                            $policy['ip_deny'] = & $_SECURE_POLICY['POLICY']['IP']['DENY'];

                                                            if (isset($_SECURE_POLICY['POLICY']['IP']['LIST']))
                                                                $policy['ip_list'] = & $_SECURE_POLICY['POLICY']['IP']['LIST'];
                                                            else
                                                                $policy['ip_list'] = array();

                                                            $policy['ip_bool'] = base64_decode($policy['ip_bool']);
                                                            $policy['ip_allow'] = base64_decode($policy['ip_allow']);
                                                            $policy['ip_deny'] = base64_decode($policy['ip_deny']);

                                                            $print['ip_bool_true_check'] = "";
                                                            $print['ip_bool_false_check'] = "";
                                                            $print['ip_base_allow_check'] = "";
                                                            $print['ip_base_deny_check'] = "";
                                                            $print['ip_list'] = "";

                                                            if ($policy['ip_bool'] == "TRUE")
                                                                $print['ip_bool_true_check'] = "checked";
                                                            else
                                                                $print['ip_bool_false_check'] = "checked";

                                                            if ($policy['ip_allow'] == "TRUE")
                                                                $print['ip_base_allow_check'] = "checked";
                                                            else
                                                            if ($policy['ip_deny'] == "TRUE")
                                                                $print['ip_base_deny_check'] = "checked";

                                                            foreach ($policy['ip_list'] as $key => $value)
                                                            {
                                                                $value = trim($value);

                                                                $print['ip_list'] .= base64_decode($value);
                                                                $print['ip_list'] .= "\n";
                                                            }
                                                            ?>
                                                            <br>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 1px solid #9fa8b3;background-color: #ddf3fa;" align="center">
                                                                        <p class="field-title">IP Address</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table width="780" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                <form action="admin_policy_submit.php?mode=POLICY_IP" method="post">
                                                                    <tr>
                                                                        <th width="120" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td width="670">
                                                                            <input type="radio" name="policy_ip" value="true" <?php echo $print['ip_bool_true_check'] ?>>YES
                                                                            <input type="radio" name="policy_ip" value="false" <?php echo $print['ip_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120"></th>
                                                                        <td width="410" colspan="2">
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>YES - Run the IP detection.
                                                                                                <li>NO&nbsp; - Do not run the IP detection.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120" height="30" bgcolor="#ddf3fa">Apply Base</th>
                                                                        <td width="670">
                                                                            <input type="radio" name="policy_ip_base" value="allow" <?php echo $print['ip_base_allow_check'] ?>>White List
                                                                            <input type="radio" name="policy_ip_base" value="deny" <?php echo $print['ip_base_deny_check'] ?>>Black List (default)
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120"></th>
                                                                        <td width="410" colspan="2">
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>White List - Only allows access to the following list.
                                                                                                <li>Black List - Block access to the following list.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="120" height="30" bgcolor="#ddf3fa">Pattern List</th>
                                                                        <td width="410" colspan="2">
                                                                            <textarea cols="65" rows="10" class="input-textarea-long" name="policy_ip_list"><?php echo $print['ip_list'] ?></textarea>
                                                                        </td>
                                                                    </tr>
                                                            </table>
                                                            <br>
                                                            <table width="790" height="35" cellspacing="0" cellpadding="0" border="0">
                                                                <tr valign="top">
                                                                    <td width="140">&nbsp;</td>
                                                                    <td width="670" align="left">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="submit" value="Conform">
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                    return false;">
                                                                        </span>
                                                                        <br><br>
                                                                    </td>
                                                                </tr>
                                                                </form>
                                                            </table>
                                                            <?php
                                                            /* IP */
                                                            ?>

                                                            <?php
                                                            /* FILE */

                                                            // FILENAME
                                                            $policy['filename_bool'] = & $_SECURE_POLICY['POLICY']['FILENAME']['BOOL'];
                                                            $policy['filename_allow'] = & $_SECURE_POLICY['POLICY']['FILENAME']['ALLOW'];
                                                            $policy['filename_deny'] = & $_SECURE_POLICY['POLICY']['FILENAME']['DENY'];

                                                            if (isset($_SECURE_POLICY['POLICY']['FILENAME']['LIST']))
                                                                $policy['filename_list'] = & $_SECURE_POLICY['POLICY']['FILENAME']['LIST'];
                                                            else
                                                                $policy['filename_list'] = array();

                                                            $policy['filename_bool'] = base64_decode($policy['filename_bool']);
                                                            $policy['filename_allow'] = base64_decode($policy['filename_allow']);
                                                            $policy['filename_deny'] = base64_decode($policy['filename_deny']);

                                                            $print['filename_bool_true_check'] = "";
                                                            $print['filename_bool_false_check'] = "";
                                                            $print['filename_base_allow_check'] = "";
                                                            $print['filename_base_deny_check'] = "";
                                                            $print['filename_list'] = "";

                                                            if ($policy['filename_bool'] == "TRUE")
                                                                $print['filename_bool_true_check'] = "checked";
                                                            else
                                                                $print['filename_bool_false_check'] = "checked";

                                                            if ($policy['filename_allow'] == "TRUE")
                                                                $print['filename_base_allow_check'] = "checked";
                                                            else
                                                            if ($policy['filename_deny'] == "TRUE")
                                                                $print['filename_base_deny_check'] = "checked";

                                                            foreach ($policy['filename_list'] as $key => $value)
                                                            {
                                                                $value = trim($value);

                                                                $print['filename_list'] .= base64_decode($value);
                                                                $print['filename_list'] .= "\n";
                                                            }

                                                            // FILETYPE
                                                            $policy['filetype_bool'] = & $_SECURE_POLICY['POLICY']['FILETYPE']['BOOL'];
                                                            $policy['filetype_allow'] = & $_SECURE_POLICY['POLICY']['FILETYPE']['ALLOW'];
                                                            $policy['filetype_deny'] = & $_SECURE_POLICY['POLICY']['FILETYPE']['DENY'];

                                                            if (isset($_SECURE_POLICY['POLICY']['FILETYPE']['LIST']))
                                                                $policy['filetype_list'] = & $_SECURE_POLICY['POLICY']['FILETYPE']['LIST'];
                                                            else
                                                                $policy['filetype_list'] = array();

                                                            $policy['filetype_bool'] = base64_decode($policy['filetype_bool']);
                                                            $policy['filetype_allow'] = base64_decode($policy['filetype_allow']);
                                                            $policy['filetype_deny'] = base64_decode($policy['filetype_deny']);

                                                            $print['filetype_bool_true_check'] = "";
                                                            $print['filetype_bool_false_check'] = "";
                                                            $print['filetype_base_allow_check'] = "";
                                                            $print['filetype_base_deny_check'] = "";
                                                            $print['filetype_list'] = "";

                                                            if ($policy['filetype_bool'] == "TRUE")
                                                                $print['filetype_bool_true_check'] = "checked";
                                                            else
                                                                $print['filetype_bool_false_check'] = "checked";

                                                            if ($policy['filetype_allow'] == "TRUE")
                                                                $print['filetype_base_allow_check'] = "checked";
                                                            else
                                                            if ($policy['filetype_deny'] == "TRUE")
                                                                $print['filetype_base_deny_check'] = "checked";

                                                            foreach ($policy['filetype_list'] as $key => $value)
                                                            {
                                                                $value = trim($value);

                                                                $print['filetype_list'] .= base64_decode($value);
                                                                $print['filetype_list'] .= "\n";
                                                            }

                                                            // FILESIZE
                                                            $policy['filesize_bool'] = & $_SECURE_POLICY['POLICY']['FILESIZE']['BOOL'];
                                                            $policy['filesize_min_size'] = & $_SECURE_POLICY['POLICY']['FILESIZE']['MIN_SIZE'];
                                                            $policy['filesize_max_size'] = & $_SECURE_POLICY['POLICY']['FILESIZE']['MAX_SIZE'];

                                                            $policy['filesize_bool'] = base64_decode($policy['filesize_bool']);
                                                            $print['filesize_min_size'] = base64_decode($policy['filesize_min_size']);
                                                            $print['filesize_max_size'] = base64_decode($policy['filesize_max_size']);

                                                            $print['filesize_bool_true_check'] = "";
                                                            $print['filesize_bool_false_check'] = "";

                                                            if ($policy['filesize_bool'] == "TRUE")
                                                                $print['filesize_bool_true_check'] = "checked";
                                                            else
                                                                $print['filesize_bool_false_check'] = "checked";
                                                            ?>
                                                            <br>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 1px solid #9fa8b3;background-color: #ddf3fa;" align="center">
                                                                        <p class="field-title">File</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <form action="admin_policy_submit.php?mode=POLICY_FILE" method="post">
                                                                <table width="780" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                    <tr>
                                                                        <th width="80" height="30" rowspan="3" bgcolor="#ddf3fa">File Name</th>
                                                                        <th width="90" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td>
                                                                            <input type="radio" name="policy_filename" value="true" <?php echo $print['filename_bool_true_check'] ?>>YES
                                                                            <input type="radio" name="policy_filename" value="false" <?php echo $print['filename_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Apply Base</th>
                                                                        <td>
                                                                            <input type="radio" name="policy_filename_base" value="allow" <?php echo $print['filename_base_allow_check'] ?>>White List
                                                                            <input type="radio" name="policy_filename_base" value="deny" <?php echo $print['filename_base_deny_check'] ?>>Black List
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Pattern List</th>
                                                                        <td>
                                                                            <textarea cols="50" rows="5" class="input-textarea-long" name="policy_filename_list"><?php echo $print['filename_list'] ?></textarea>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="80" height="30" rowspan="3" bgcolor="#ddf3fa">File Type</th>
                                                                        <th width="90" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td>
                                                                            <input type="radio" name="policy_filetype" value="true" <?php echo $print['filetype_bool_true_check'] ?>>YES
                                                                            <input type="radio" name="policy_filetype" value="false" <?php echo $print['filetype_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa" bgcolor="#ddf3fa">Apply Base</th>
                                                                        <td>
                                                                            <input type="radio" name="policy_filetype_base" value="allow" <?php echo $print['filetype_base_allow_check'] ?>>White List (default)
                                                                            <input type="radio" name="policy_filetype_base" value="deny" <?php echo $print['filetype_base_deny_check'] ?>>Black List
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Pattern List</th>
                                                                        <td>
                                                                            <textarea cols="50" rows="5" class="input-textarea-long" name="policy_filetype_list"><?php echo $print['filetype_list'] ?></textarea>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="80" height="30" rowspan="3" bgcolor="#ddf3fa">File Size</th>
                                                                        <th width="90" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td>
                                                                            <input type="radio" name="policy_filesize" value="true" <?php echo $print['filesize_bool_true_check'] ?>>YES
                                                                            <input type="radio" name="policy_filesize" value="false" <?php echo $print['filesize_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Min Size</th>
                                                                        <td>
                                                                            <input type="text" name="policy_filesize_min_size" class="input-text" size="20" value="<?php echo $print['filesize_min_size'] ?>"> (Unit: Byte)
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th height="30" bgcolor="#ddf3fa">Max Size</th>
                                                                        <td>
                                                                            <input type="text" name="policy_filesize_max_size" class="input-text" size="20" value="<?php echo $print['filesize_max_size'] ?>"> (Unit: Byte)
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <br>
                                                                <table width="790" height="50" cellspacing="0" cellpadding="0" border="0">
                                                                    <tr valign="top">
                                                                        <td width="140">&nbsp;</td>
                                                                        <td width="670" align="left">
                                                                            <span class="cm-button-main cm-process-items">
                                                                                <input type="submit" value="Conform">
                                                                            </span>
                                                                            <span class="cm-button-main cm-process-items">
                                                                                <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                        return false;">
                                                                            </span>
                                                                            <br><br>
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </form>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr bgcolor="#A0A0A0">
                            <td width="100%" height="50" colspan="2" align="center">
                                <?php include_once("admin_bottom.php"); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
