<?php
$exception['check_authorized'] = TRUE;

include_once("admin_lib.php");
require_once '../models/config.php';

$error_msg['admin_id'] = "Input admin ID.";
$error_msg['admin_password'] = "Input password.";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" HREF="style.css" type="text/css" title="style">
        <?php include_once("admin_title.php"); ?>
    </head>
    <body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="#D0D0D0">
        <script language="javascript">
            function nextstep() {
                var len = document.login.admin_id.value.length;
                if (len == 0) {
                    alert("<?php echo $error_msg['admin_id'] ?>");
                    document.login.admin_id.focus();
                    return false;
                }
                len = document.login.admin_password.value.length;
                if (len == 0) {
                    alert("<?php echo $error_msg['admin_password'] ?>");
                    document.login.admin_password.focus();
                    return false;
                }
                document.login.submit();
            }
        </script>
        <br><br><br><br><br><br><br><br>
        <center>
            <table width="350" height="80" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td width="100%" height="80" align="left">
                        &nbsp;<img src="img/logo.png" border="0" alt="LOGO"><br><br>
                        <font color="#0099cc" size="2"><b>&nbsp;&nbsp;Security Administration panel</b></font>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="300" border="0">
                            <tr>
                                <td class="login-page">
                                    <div id="main_column_login" class="clear">
                                        <div class="login-wrap">
                                            <form name="login" action="admin_login_submit.php" method="post" onsubmit="return nextstep();">
                                            <!--<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="main_login_form" class="cm-form-highlight cm-skip-check-items">-->
                                                <div class="login-content">
                                                    <div class="clear-form-field">
                                                        <p><label for="username" class="cm-required cm-email">Admin ID:</label></p>
                                                        <input id="username" type="text" name="admin_id" size="20" value="" class="input-text cm-focus" tabindex="1" />
                                                    </div>
                                                    <div class="clear-form-field">
                                                        <p><label for="password" class="cm-required">Password:</label></p>
                                                        <input type="password" id="password" name="admin_password" size="20" value="" class="input-text" tabindex="2" />
                                                    </div>
                                                    <div class="buttons-container nowrap">
                                                        <div class="float-right">
                                                            <span  class="submit-button cm-button-main "><input type="submit" name="dispatch[auth.login]" value="Sign in" tabindex="3" />&nbsp;&nbsp;&nbsp;</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!--main_column_login-->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
    </body>
</html>
