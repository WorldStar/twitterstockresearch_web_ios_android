<?php

include_once("admin_lib.php");
check_authorized();

init_page();

if (isset($_GET) && isset($_GET['mode']))
    $page['mode'] = $_GET['mode'];

if ($page['mode'] == "LOG_MODIFY")
{
    clear_submit();
    /*
      if (isset($_POST['log_filename']))
      $submit['log_filename'] = $_POST['log_filename'];
     */
    if (isset($_POST['log_bool']))
        $submit['log_bool'] = $_POST['log_bool'];

    if (isset($_POST['log_mode']))
        $submit['log_mode'] = $_POST['log_mode'];

    if (isset($_POST['log_list_count']))
        $submit['log_list_count'] = $_POST['log_list_count'];

    if (isset($_POST['log_charset']))
        $submit['log_charset'] = $_POST['log_charset'];

    $check['submit'] = array("log_bool", "log_mode", "log_list_count", "log_charset",);
    check_submit($check['submit']);
    
	
    if ($submit['log_bool'] == "true")
        $_SECURE_POLICY['CONFIG']['LOG']['BOOL'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['CONFIG']['LOG']['BOOL'] = base64_encode("FALSE");

    if ($submit['log_mode'] == "simple")
    {
        $_SECURE_POLICY['CONFIG']['LOG']['SIMPLE'] = base64_encode("TRUE");
        $_SECURE_POLICY['CONFIG']['LOG']['DETAIL'] = base64_encode("FALSE");
    }
    else
    if ($submit['log_mode'] == "detail")
    {
        $_SECURE_POLICY['CONFIG']['LOG']['SIMPLE'] = base64_encode("FALSE");
        $_SECURE_POLICY['CONFIG']['LOG']['DETAIL'] = base64_encode("TRUE");
    }

    if ($submit['log_charset'] == "UTF-8")
        $_SECURE_POLICY['CONFIG']['LOG']['CHARSET']['UTF-8'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['CONFIG']['LOG']['CHARSET']['UTF-8'] = base64_encode("FALSE");

    if ($submit['log_charset'] == "eucKR")
        $_SECURE_POLICY['CONFIG']['LOG']['CHARSET']['eucKR'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['CONFIG']['LOG']['CHARSET']['eucKR'] = base64_encode("FALSE");

    $submit['log_list_count'] = trim($submit['log_list_count']);

    $_SECURE_POLICY['CONFIG']['LOG']['LIST_COUNT'] = base64_encode($submit['log_list_count']);

    write_policy();

    html_msgmove("Modified log setting.", "admin_log.php");

    exit;
}

if ($page['mode'] == "LOG_DELETE")
{
    clear_submit();

    if (isset($_GET['log_filename']))
        $submit['log_filename'] = $_GET['log_filename'];

    $check['submit'] = array("log_filename",);
    check_submit($check['submit']);

    $log['filename'] = "./log/" . $submit['log_filename'];

    if (file_exists($log['filename']))
        unlink($log['filename']);

    html_msgmove("Deleted log file.", "admin_log.php");

    exit;
}
?>
