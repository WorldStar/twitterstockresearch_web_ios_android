<?php
include_once("admin_lib.php");
check_authorized();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" HREF="style.css" type="text/css" title="style">
        <script language=javascript>
            <!--
          function log_delete_submit(log_filename)
            {
                ret = confirm("You delete?");
                if (!ret)
                    return;
                location.href = 'admin_log_submit.php?mode=LOG_DELETE&log_filename=' + log_filename;
            }
//-->
        </script>
    </head>
    <body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="#D0D0D0">
        <?php include_once("admin_title.php"); ?>
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
            <tr> 
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tr bgcolor="#CACACA">
                            <td width="100%" height="80" colspan="2">
                                <?php include_once("admin_top.php"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="160" bgcolor="#f3f3f3">
                                <?php include_once("admin_menu.php"); ?>
                            </td>
                            <td width="100%" bgcolor="#f3f3f3" valign="top">
                                <br><br>
                                <div id="main_column" class="clear">
                                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tr valign="top">
                                            <td width="100%">
                                                <table width="100%" height="100%" cellspacing="20" cellpadding="0" border="0" align="center">
                                                    <tr valign="top">
                                                        <td width="100%">
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Log setting</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>

                                                            <br>
                                                            <table width="790" cellspacing="10" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="100%" style="line-height:160%" nowrap>
                                                                        <b>Note: Please check the log file size often, and backup that.</b><br>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <?php
                                                            $policy['log_bool'] = "FALSE";
                                                            $policy['log_filename'] = "";
                                                            $policy['log_simple'] = "";
                                                            $policy['log_detail'] = "";
                                                            $policy['log_list_count'] = "";
                                                            $policy['log_charset_utf8'] = "";
                                                            $policy['log_charset_euckr'] = "";

                                                            if (isset($_SECURE_POLICY['CONFIG']['LOG']['BOOL']))
                                                                $policy['log_bool'] = & $_SECURE_POLICY['CONFIG']['LOG']['BOOL'];

                                                            if (isset($_SECURE_POLICY['CONFIG']['LOG']['FILENAME']))
                                                                $policy['log_filename'] = & $_SECURE_POLICY['CONFIG']['LOG']['FILENAME'];

                                                            if (isset($_SECURE_POLICY['CONFIG']['LOG']['SIMPLE']))
                                                                $policy['log_simple'] = & $_SECURE_POLICY['CONFIG']['LOG']['SIMPLE'];

                                                            if (isset($_SECURE_POLICY['CONFIG']['LOG']['DETAIL']))
                                                                $policy['log_detail'] = & $_SECURE_POLICY['CONFIG']['LOG']['DETAIL'];

                                                            if (isset($_SECURE_POLICY['CONFIG']['LOG']['LIST_COUNT']))
                                                                $policy['log_list_count'] = & $_SECURE_POLICY['CONFIG']['LOG']['LIST_COUNT'];

                                                            if (isset($_SECURE_POLICY['CONFIG']['LOG']['CHARSET']['UTF-8']))
                                                                $policy['log_charset_utf8'] = & $_SECURE_POLICY['CONFIG']['LOG']['CHARSET']['UTF-8'];

                                                            if (isset($_SECURE_POLICY['CONFIG']['LOG']['CHARSET']['UTF-8']))
                                                                $policy['log_charset_euckr'] = & $_SECURE_POLICY['CONFIG']['LOG']['CHARSET']['eucKR'];

                                                            $print['log_filename'] = base64_decode($policy['log_filename']);
                                                            $print['log_bool_true_check'] = "";
                                                            $print['log_bool_false_check'] = "";
                                                            $print['log_simple_check'] = "";
                                                            $print['log_detail_check'] = "";
                                                            $print['log_charset_utf8_check'] = "";
                                                            $print['log_charset_euckr_check'] = "";

                                                            if (base64_decode($policy['log_bool']) == "TRUE")
                                                                $print['log_bool_true_check'] = "checked";
                                                            else
                                                                $print['log_bool_false_check'] = "checked";

                                                            if (base64_decode($policy['log_simple']) == "TRUE")
                                                                $print['log_simple_check'] = "checked";
                                                            else
                                                                $print['log_detail_check'] = "checked";

                                                            if (base64_decode($policy['log_charset_utf8']) == "TRUE")
                                                                $print['log_charset_utf8_check'] = "checked";
                                                            else
                                                            if (base64_decode($policy['log_charset_euckr']) == "TRUE")
                                                                $print['log_charset_euckr_check'] = "checked";

                                                            $print['log_list_count'] = base64_decode($policy['log_list_count']);
                                                            ?>
                                                            <br>
                                                            <form action="admin_log_submit.php?mode=LOG_MODIFY" method="post">
                                                                <table width="600" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                    <tr>
                                                                        <th width="150" height="30" align="right">Apply Status</th>
                                                                        <td>
                                                                            <input type="radio" name="log_bool" value="true" <?php echo $print['log_bool_true_check'] ?>>Logging
                                                                            <input type="radio" name="log_bool" value="false" <?php echo $print['log_bool_false_check'] ?>>None
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td>
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>Logging - Record logs (default).
                                                                                                <li>None - No record logs.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="150" height="30" align="right">Log Method</th>
                                                                        <td>
                                                                            <input type="radio" name="log_mode" value="simple" <?php echo $print['log_simple_check'] ?>>Simple
                                                                            <input type="radio" name="log_mode" value="detail" <?php echo $print['log_detail_check'] ?>>Detail
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td>                                                                            
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>Simple - Record simply logs (default).<br>
                                                                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                                    (REMOTE_ADDR - [Date] REQUEST_URL: Message)
                                                                                                <li>Detail - Secure Module Record detailed logs.<br>
                                                                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                                    (REMOTE_ADDR - [Date] REQUEST_URL: Message: ...)
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="150" height="30" align="right">CHARSET for logs</th>
                                                                        <td>
                                                                            <input type="radio" name="log_charset" value="UTF-8" <?php echo $print['log_charset_utf8_check'] ?>>UTF-8 (default)
                                                                            <input type="radio" name="log_charset" value="eucKR" <?php echo $print['log_charset_euckr_check'] ?>>Other
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td>
                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>UTF-8 - Save the string in UTF-8 (default).
                                                                                                <li>Other - Save the string by browser.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="150" height="30" align="right">Count of log lists</th>
                                                                        <td>
                                                                            <input type="text" name="log_list_count" class="input-text" value="<?php echo $print['log_list_count'] ?>">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <br>
                                                                <table width="475" height="50" cellspacing="0" cellpadding="0" border="0" align='center'>
                                                                    <tr valign="top">
                                                                        <td width="175">&nbsp;</td>
                                                                        <td width="300">
                                                                            <span class="cm-button-main cm-process-items">
                                                                                <input type="submit" value="Confirm">
                                                                            </span>
                                                                            <span class="cm-button-main cm-process-items">
                                                                                <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                        return false;">
                                                                            </span>
                                                                            <br><br>
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </form>
                                                            <br>
                                                            <table width="780" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Log Lists</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table width="770" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                <tr height="30">
                                                                    <th width="50" bgcolor="#ddf3fa">No</th>
                                                                    <th width="300" bgcolor="#ddf3fa">Log File</th>
                                                                    <th width="130" bgcolor="#ddf3fa">File Size</th>
                                                                    <th width="150" bgcolor="#ddf3fa">Latest Time</th>
                                                                    <th width="50" bgcolor="#ddf3fa">Delete</th>
                                                                </tr>
                                                                <?php
                                                                $log['dirname'] = "./log";

                                                                if (is_dir($log['dirname']))
                                                                {

                                                                    if ($dh = opendir($log['dirname']))
                                                                    {

                                                                        while (($file = readdir($dh)) !== FALSE)
                                                                        {
                                                                            if (!strstr($file, $print['log_filename']))
                                                                                continue;

                                                                            $logs[$file]['filename'] = $file;
                                                                            $logs[$file]['filesize'] = number_format(filesize($log['dirname'] . "/" . $file));
                                                                            $logs[$file]['filemtime'] = date("F d Y H:i:s", (filemtime($log['dirname'] . "/" . $file)));
                                                                        }
                                                                        closedir($dh);
                                                                    }
                                                                }

                                                                $print['log_num'] = 0;
                                                                $print['log_total_count'] = 0;

                                                                if (isset($logs))
                                                                {

                                                                    krsort($logs);

                                                                    $print['log_total_count'] = count($logs);

                                                                    foreach ($logs as $log)
                                                                    {
                                                                        if ($print['log_num'] >= $print['log_list_count'])
                                                                            break;

                                                                        $print['filename'] = htmlentities($log['filename'], ENT_QUOTES, "UTF-8");
                                                                        $print['filesize'] = htmlentities($log['filesize'], ENT_QUOTES, "UTF-8");
                                                                        $print['filemtime'] = htmlentities($log['filemtime'], ENT_QUOTES, "UTF-8");
                                                                        ?>
                                                                        <tr height="25" bgcolor="#FFFFFF">
                                                                            <td align="center">
                                                                                <?php echo ++$print['log_num'] ?>
                                                                            </td>
                                                                            <td align="center">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td><a href="admin_download.php?filename=<?php echo $print['filename'] ?>&filepath=./log/<?php echo $print['filename'] ?>"><?php echo $print['filename'] ?></a></td>
                                                                                        <td><a href="admin_download.php?filename=<?php echo $print['filename'] ?>&filepath=./log/<?php echo $print['filename'] ?>"><button>Download</button></a></td>
                                                                                    </tr>
                                                                                </table>
                                                                            <td align="center"><?php echo $print['filesize'] ?> Bytes</td>
                                                                            <td align="center"><?php echo $print['filemtime'] ?></td>
                                                                            <td align="center"><input type="button" value="Delete" onclick="log_delete_submit('<?php echo $print['filename'] ?>');
                                                                                    return false;"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="7"><hr color="#ddf3fa"></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </table>
                                                            <table width="800" height="40">
                                                                <tr>
                                                                    <td width="100%" height="100%" align="center">Recorded total  <b><?php echo $print['log_total_count'] ?></b> logs.</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr bgcolor="#A0A0A0">
                            <td width="100%" height="50" colspan="2" align="center">
                                <?php include_once("admin_bottom.php"); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
