<?php

include_once("admin_lib.php");
check_authorized();

init_page();

clear_submit();

if (isset($_GET['filename']))
    $submit['filename'] = $_GET['filename'];

if (isset($_GET['filepath']))
    $submit['filepath'] = $_GET['filepath'];

$check['submit'] = array("filename", "filepath",);
check_submit($check['submit']);

$submit['filename'] = trim($submit['filename']);
$submit['filepath'] = trim($submit['filepath']);

$check['script_filename'] = getenv("SCRIPT_FILENAME");
$check['script_filename'] = delete_directory_traverse($check['script_filename']);

$piecies = explode("/", $check['script_filename']);

$check['filepath'] = "";
$count = count($piecies) - 1;

for ($i = 0; $i < $count; $i++)
    $check['filepath'] .= $piecies[$i] . "/";

$submit['filepath'] = trim($submit['filepath']);

$check['filepath_length'] = strlen($submit['filepath']);
check_length("File Path", $check['filepath_length'], 1, 256);

$submit['filepath'] = $check['filepath'] . $submit['filepath'];

$submit['filepath'] = delete_directory_traverse($submit['filepath']);

if (!file_exists($submit['filepath']))
    exit;

$fileinfo = stat($submit['filepath']);
$filename = basename($submit['filepath']);

file_download($submit['filename'], $submit['filepath'], $fileinfo['size']);

exit;
?>
