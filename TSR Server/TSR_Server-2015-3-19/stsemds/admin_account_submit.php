<?php

include_once("admin_lib.php");
check_authorized();

init_page();

//$page['mode']
if (isset($_GET) && isset($_GET['mode']))
    $page['mode'] = $_GET['mode'];

if ($page['mode'] == "CONFIG_ACCOUNT")
{
    clear_submit();

    if (isset($_POST['admin_id']))
        $submit['admin_id'] = $_POST['admin_id'];

    if (isset($_POST['admin_password']))
        $submit['admin_password'] = $_POST['admin_password'];

    if (isset($_POST['admin_repassword']))
        $submit['admin_repassword'] = $_POST['admin_repassword'];

    if (isset($_POST['admin_old_password']))
        $submit['admin_old_password'] = $_POST['admin_old_password'];

    $check['submit'] = array("admin_id", "admin_password", "admin_repassword", "admin_old_password",);
    check_submit($check['submit']);

    if ($submit['admin_password'] != $submit['admin_repassword'])
        html_msgback("The password do not match.");

    $check['admin_id_length'] = strlen($submit['admin_id']);
    check_length("Admin ID", $check['admin_id_length'], 4, 16);

    $check['admin_password_length'] = strlen($submit['admin_password']);
    check_length("Password", $check['admin_password_length'], 8, 32);

    $check['admin_old_password_length'] = strlen($submit['admin_old_password']);
    check_length("Old password", $check['admin_old_password_length'], 8, 32);

    $policy['admin_old_password'] = & $_SECURE_POLICY['CONFIG']['ADMIN']['PASSWORD'];
    $policy['admin_old_password'] = base64_decode($policy['admin_old_password']);

    $submit['admin_old_password'] = md5(md5($submit['admin_old_password']));

    if ($submit['admin_old_password'] != $policy['admin_old_password'])
        html_msgback("The password do not match.");

    $_SECURE_POLICY['CONFIG']['ADMIN']['ID'] = base64_encode($submit['admin_id']);
    $_SECURE_POLICY['CONFIG']['ADMIN']['PASSWORD'] = base64_encode(md5(md5($submit['admin_password'])));

    write_policy();

    html_msgmove("Successfully.", "admin_account.php");

    exit;
}
?>
