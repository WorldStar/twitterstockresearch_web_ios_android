<?php

include_once("admin_lib.php");
check_authorized();

init_page();

if (isset($_GET) && isset($_GET['mode']))
    $page['mode'] = $_GET['mode'];

if ($page['mode'] == "PAGE_INSERT")
{
    clear_submit();

    if (isset($_POST['page_name']))
        $submit['page_name'] = $_POST['page_name'];

    if (isset($_POST['page_bool']))
        $submit['page_bool'] = $_POST['page_bool'];

    if (isset($_POST['page_base']))
        $submit['page_base'] = $_POST['page_base'];

    $check['submit'] = array("page_name", "page_bool", "page_base",);
    check_submit($check['submit']);

    $submit['page_name'] = trim($submit['page_name']);

    $submit['page_name'] = delete_directory_traverse($submit['page_name']);

    $check['page_name_length'] = strlen($submit['page_name']);
    check_length("Page Name", $check['page_name_length'], 4, 64);

    if (isset($_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]))
        html_msgback($submit['page_name'] . " Page exist.");

    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['BOOL'] = base64_encode("FALSE");
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['ALLOW'] = base64_encode("FALSE");
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['DENY'] = base64_encode("FALSE");

    if ($submit['page_bool'] == "true")
        $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['BOOL'] = base64_encode("TRUE");

    if ($submit['page_base'] == "allow")
        $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['ALLOW'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['DENY'] = base64_encode("TRUE");

    ksort($_SECURE_POLICY['ADVANCE']['PAGE']);

    write_policy();

    html_msgmove(" Successfully added the page.", "admin_advance.php");

    exit;
}

if ($page['mode'] == "PAGE_MODIFY")
{
    clear_submit();

    if (isset($_POST['page_name']))
        $submit['page_name'] = $_POST['page_name'];

    if (isset($_POST['page_bool']))
        $submit['page_bool'] = $_POST['page_bool'];

    if (isset($_POST['page_base']))
        $submit['page_base'] = $_POST['page_base'];

    $check['submit'] = array("page_name", "page_bool", "page_base",);
    check_submit($check['submit']);

    $submit['page_name'] = trim($submit['page_name']);

    $submit['page_name'] = delete_directory_traverse($submit['page_name']);

    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['BOOL'] = base64_encode("FALSE");
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['ALLOW'] = base64_encode("FALSE");
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['DENY'] = base64_encode("FALSE");

    if ($submit['page_bool'] == "true")
        $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['BOOL'] = base64_encode("TRUE");

    if ($submit['page_base'] == "allow")
        $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['ALLOW'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['DENY'] = base64_encode("TRUE");

    ksort($_SECURE_POLICY['ADVANCE']['PAGE']);

    write_policy();

    html_msgmove(" Page modified successfully.", "admin_advance.php");

    exit;
}

if ($page['mode'] == "PAGE_DELETE")
{
    clear_submit();

    if (isset($_GET['page_name']))
        $submit['page_name'] = $_GET['page_name'];

    $check['submit'] = array("page_name",);
    check_submit($check['submit']);

    if (isset($_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]))
        unset($_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]);

    write_policy();

    html_msgmove("Page deleted.", "admin_advance.php");

    exit;
}

if ($page['mode'] == "PAGE_MODIFY_DETAIL")
{
    clear_submit();

    if (isset($_POST['page_name']))
        $submit['page_name'] = $_POST['page_name'];

    if (isset($_POST['page_bool']))
        $submit['page_bool'] = $_POST['page_bool'];

    if (isset($_POST['page_base']))
        $submit['page_base'] = $_POST['page_base'];

    $check['submit'] = array("page_name", "page_bool", "page_base",);
    check_submit($check['submit']);

    $submit['page_name'] = trim($submit['page_name']);

    $submit['page_name'] = delete_directory_traverse($submit['page_name']);

    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['BOOL'] = base64_encode("FALSE");
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['ALLOW'] = base64_encode("FALSE");
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['DENY'] = base64_encode("FALSE");

    if ($submit['page_bool'] == "true")
        $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['BOOL'] = base64_encode("TRUE");

    if ($submit['page_base'] == "allow")
        $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['ALLOW'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['DENY'] = base64_encode("TRUE");

    ksort($_SECURE_POLICY['ADVANCE']['PAGE']);

    write_policy();

    html_msgmove("Page modified.", "admin_advance_detail.php?page_name=" . $submit['page_name']);

    exit;
}

if ($page['mode'] == "VAR_INSERT")
{
    clear_submit();

    if (isset($_POST['page_name']))
        $submit['page_name'] = $_POST['page_name'];

    if (isset($_POST['var_name']))
        $submit['var_name'] = $_POST['var_name'];

    if (isset($_POST['var_format']))
        $submit['var_format'] = $_POST['var_format'];

    if (isset($_POST['var_min_length']))
        $submit['var_min_length'] = $_POST['var_min_length'];

    if (isset($_POST['var_max_length']))
        $submit['var_max_length'] = $_POST['var_max_length'];

    if (isset($_POST['var_method_get']))
        $submit['var_method_get'] = "TRUE";
    else
        $submit['var_method_get'] = "FALSE";

    if (isset($_POST['var_method_post']))
        $submit['var_method_post'] = "TRUE";
    else
        $submit['var_method_post'] = "FALSE";

    if (isset($_POST['var_target_sql_injection']))
        $submit['var_target_sql_injection'] = "TRUE";
    else
        $submit['var_target_sql_injection'] = "FALSE";

    if (isset($_POST['var_target_xss']))
        $submit['var_target_xss'] = "TRUE";
    else
        $submit['var_target_xss'] = "FALSE";

    if (isset($_POST['var_target_word']))
        $submit['var_target_word'] = "TRUE";
    else
        $submit['var_target_word'] = "FALSE";

    if (isset($_POST['var_target_tag']))
        $submit['var_target_tag'] = "TRUE";
    else
        $submit['var_target_tag'] = "FALSE";

    $check['submit'] = array("page_name", "var_name", "var_format",
        "var_min_length", "var_max_length",);
    check_submit($check['submit']);

    $submit['var_name'] = trim($submit['var_name']);
    $submit['var_format'] = trim($submit['var_format']);
    $submit['var_format'] = trim($submit['var_format']);
    $submit['var_min_length'] = trim($submit['var_min_length']);
    $submit['var_max_length'] = trim($submit['var_max_length']);
    $submit['var_method_get'] = trim($submit['var_method_get']);
    $submit['var_method_post'] = trim($submit['var_method_post']);
    $submit['var_target_sql_injection'] = trim($submit['var_target_sql_injection']);
    $submit['var_target_xss'] = trim($submit['var_target_xss']);
    $submit['var_target_word'] = trim($submit['var_target_word']);
    $submit['var_target_tag'] = trim($submit['var_target_tag']);

    if (get_magic_quotes_gpc())
    {
        $submit['var_name'] = stripslashes($submit['var_name']);
        $submit['var_format'] = stripslashes($submit['var_format']);
        $submit['var_min_length'] = stripslashes($submit['var_min_length']);
        $submit['var_max_length'] = stripslashes($submit['var_max_length']);
        $submit['var_method_get'] = stripslashes($submit['var_method_get']);
        $submit['var_method_post'] = stripslashes($submit['var_method_post']);
        $submit['var_target_sql_injection'] = stripslashes($submit['var_target_sql_injection']);
        $submit['var_target_xss'] = stripslashes($submit['var_target_xss']);
        $submit['var_target_word'] = stripslashes($submit['var_target_word']);
        $submit['var_target_tag'] = stripslashes($submit['var_target_tag']);
    }

    if (!isset($_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]))
        html_msgback($submit['page_name'] . " Page no exist.");

    $check['var_name_length'] = strlen($submit['var_name']);
    check_length("Parameter Name", $check['var_name_length'], 1, MAX_VAR_LENGTH);

    $policy['page'] = & $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']];

    if (isset($policy['page']['LIST'][$submit['var_name']]))
        html_msgback($submit['var_name'] . " Parameter exist.");

    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['FORMAT'] = base64_encode($submit['var_format']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['MIN_LENGTH'] = base64_encode($submit['var_min_length']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['MAX_LENGTH'] = base64_encode($submit['var_max_length']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['METHOD']['GET'] = base64_encode($submit['var_method_get']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['METHOD']['POST'] = base64_encode($submit['var_method_post']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['TARGET']['SQL_INJECTION'] = base64_encode($submit['var_target_sql_injection']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['TARGET']['XSS'] = base64_encode($submit['var_target_xss']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['TARGET']['WORD'] = base64_encode($submit['var_target_word']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['TARGET']['TAG'] = base64_encode($submit['var_target_tag']);

    write_policy();

    html_msgmove("Parameter added successfully.", "admin_advance_detail.php?page_name=" . $submit['page_name']);

    exit;
}

if ($page['mode'] == "VAR_MODIFY")
{
    clear_submit();

    if (isset($_POST['page_name']))
        $submit['page_name'] = $_POST['page_name'];

    if (isset($_POST['var_name']))
        $submit['var_name'] = $_POST['var_name'];

    if (isset($_POST['var_format']))
        $submit['var_format'] = $_POST['var_format'];

    if (isset($_POST['var_min_length']))
        $submit['var_min_length'] = $_POST['var_min_length'];

    if (isset($_POST['var_max_length']))
        $submit['var_max_length'] = $_POST['var_max_length'];

    if (isset($_POST['var_method_get']))
        $submit['var_method_get'] = "TRUE";
    else
        $submit['var_method_get'] = "FALSE";

    if (isset($_POST['var_method_post']))
        $submit['var_method_post'] = "TRUE";
    else
        $submit['var_method_post'] = "FALSE";

    if (isset($_POST['var_target_sql_injection']))
        $submit['var_target_sql_injection'] = "TRUE";
    else
        $submit['var_target_sql_injection'] = "FALSE";

    if (isset($_POST['var_target_xss']))
        $submit['var_target_xss'] = "TRUE";
    else
        $submit['var_target_xss'] = "FALSE";

    if (isset($_POST['var_target_word']))
        $submit['var_target_word'] = "TRUE";
    else
        $submit['var_target_word'] = "FALSE";

    if (isset($_POST['var_target_tag']))
        $submit['var_target_tag'] = "TRUE";
    else
        $submit['var_target_tag'] = "FALSE";

    $check['submit'] = array("page_name", "var_name", "var_format",
        "var_min_length", "var_max_length",);
    check_submit($check['submit']);

    $submit['var_format'] = trim($submit['var_format']);
    $submit['var_min_length'] = trim($submit['var_min_length']);
    $submit['var_max_length'] = trim($submit['var_max_length']);
    $submit['var_method_get'] = trim($submit['var_method_get']);
    $submit['var_method_post'] = trim($submit['var_method_post']);
    $submit['var_target_sql_injection'] = trim($submit['var_target_sql_injection']);
    $submit['var_target_xss'] = trim($submit['var_target_xss']);
    $submit['var_target_word'] = trim($submit['var_target_word']);
    $submit['var_target_tag'] = trim($submit['var_target_tag']);

    if (get_magic_quotes_gpc())
    {
        $submit['var_format'] = stripslashes($submit['var_format']);
        $submit['var_min_length'] = stripslashes($submit['var_min_length']);
        $submit['var_max_length'] = stripslashes($submit['var_max_length']);
        $submit['var_method_get'] = stripslashes($submit['var_method_get']);
        $submit['var_method_post'] = stripslashes($submit['var_method_post']);
        $submit['var_target_sql_injection'] = stripslashes($submit['var_target_sql_injection']);
        $submit['var_target_xss'] = stripslashes($submit['var_target_xss']);
        $submit['var_target_word'] = stripslashes($submit['var_target_word']);
        $submit['var_target_tag'] = stripslashes($submit['var_target_tag']);
    }

    if (!isset($_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]))
        html_msgback($submit['page_name'] . " Page no exist.");

    $check['var_name_length'] = strlen($submit['var_name']);
    check_length("Parameter Name", $check['var_name_length'], 1, MAX_VAR_LENGTH);

    $policy['page'] = & $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']];

    if (!isset($policy['page']['LIST']))
        html_msgback($submit['var_name'] . " Parameter no exist.");

    if (!isset($policy['page']['LIST'][$submit['var_name']]))
        html_msgback($submit['var_name'] . " Parameter no exist.");


    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['FORMAT'] = base64_encode($submit['var_format']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['MIN_LENGTH'] = base64_encode($submit['var_min_length']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['MAX_LENGTH'] = base64_encode($submit['var_max_length']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['METHOD']['GET'] = base64_encode($submit['var_method_get']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['METHOD']['POST'] = base64_encode($submit['var_method_post']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['TARGET']['SQL_INJECTION'] = base64_encode($submit['var_target_sql_injection']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['TARGET']['XSS'] = base64_encode($submit['var_target_xss']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['TARGET']['WORD'] = base64_encode($submit['var_target_word']);
    $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]['TARGET']['TAG'] = base64_encode($submit['var_target_tag']);

    write_policy();

    html_msgmove("Parameter modified.", "admin_advance_detail.php?page_name=" . $submit['page_name']);

    exit;
}

if ($page['mode'] == "VAR_DELETE")
{
    clear_submit();

    if (isset($_GET['page_name']))
        $submit['page_name'] = $_GET['page_name'];

    if (isset($_GET['var_name']))
        $submit['var_name'] = $_GET['var_name'];

    $check['submit'] = array("page_name", "var_name",);
    check_submit($check['submit']);

    if (isset($_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]))
        unset($_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]['LIST'][$submit['var_name']]);

    write_policy();

    html_msgmove("Parameter deleted.", "admin_advance_detail.php?page_name=" . $submit['page_name']);

    exit;
}
?>
