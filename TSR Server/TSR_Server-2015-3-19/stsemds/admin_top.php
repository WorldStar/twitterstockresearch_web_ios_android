<?php
include_once("version.php");
include_once("admin_lib.php");
check_authorized();

if (isset($_SECURE_POLICY['CONFIG']['ADMIN']['LASTMODIFIED']))
    $policy['last_modified'] = $_SECURE_POLICY['CONFIG']['ADMIN']['LASTMODIFIED'];
else
    $policy['last_modified'] = base64_encode("0");

$policy['last_modified'] = base64_decode($policy['last_modified']);

$print['last_modified'] = date("D M j G:i:s Y", $policy['last_modified']);
$print['last_modified'] = htmlentities($print['last_modified'], ENT_QUOTES, "UTF-8");
?>
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" bgcolor='#0099cc'>
    <tr valign="bottom">
        <td width="50%">&nbsp;
            <a href="admin.php"><img src="img/logo.png" alt="LOGO" border="0"></a><br><br>
        </td>
        <td width="50%" align="right">
            <table width="100%" height="100%" cellspacing="0" cellpadding="0">
                <tr align="right">
                    <td>
                        <font color="black"><b>Twitter Stock Research Web Security Module</b></font>&nbsp;&nbsp;                        
                        <a href="admin_logout_submit.php"><font color="black"><b>Log out</b></font></a>&nbsp;&nbsp;&nbsp;
                        <br><br>
                        <font color="black">Last modified: <?php echo $print['last_modified'] ?></font>&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
