<?php

require_once 'admin_util.php';

if (defined("_SECURE_ADMIN_LIB_PHP_"))
    return;

else
{
    ini_set("display_errors", "0");

    /* _ADMIN_LIB_PHP_ routine */

    define("SECURE_BASE_MODULE_NAME", "Twitter Stock Research Secure Module");
    define("MAX_VAR_LENGTH", 65535);
    define("MAX_FILESIZE", 1073741824); // 1GB
    // $HTTP_POST_VARS : 4.1.0
    $php_ver = explode(".", phpversion());
    if ($php_ver[0] < 4 || $php_ver[0] == 4 && $php_ver[1] < 1)
    {
        $_GET = & $HTTP_GET_VARS;
        $_POST = & $HTTP_POST_VARS;
        $_COOKIE = & $HTTP_COOKIE_VARS;
        $_SESSION = & $HTTP_SESSION_VARS;
        $_FILES = & $HTTP_POST_FILES;
        $_ENV = & $HTTP_ENV_VARS;
        $_SERVER = & $HTTP_SERVER_VARS;
    }

    session_start();

    if (file_exists("policy.php"))
        include_once("policy.php");

    function write_policy($array = NULL, $vpath = "\$_SECURE_POLICY", $step = 0, $fd = NULL)
    {
        global $_SECURE_POLICY;

        $_SECURE_POLICY['CONFIG']['ADMIN']['LASTMODIFIED'] = base64_encode(time());

        $check['os'] = getenv("OS");
        if (!strstr($check['os'], "Windows"))
        {

            $fstat['secure_module_dir'] = stat("./");

            $fstat['secure_module_dir'] = decoct($fstat['secure_module_dir']['mode'] & 0000777);

//            if ($fstat['secure_module_dir'] != "707")
//                html_msgback("Do not have permission to write in the secure directory.");
        }

        if (!$array && !$step)
            $array = & $_SECURE_POLICY;

        if (!is_array($array))
            return;

        $count = count($array);
        $original = count($array);

        if ($step == 0)
        {
            $fd = fopen("policy.tmp.php", "w");
            if (!$fd)
                html_msgback("policy.tmp.php: Can not make the temp policy file.");

            fwrite($fd, "<?php\n\n");
            fwrite($fd, "//@UTF-8 policy.php\n\n");
            fwrite($fd, "if (defined(\"_SECURE_POLICY_PHP_\"))\n");
            fwrite($fd, "\treturn;\n\n");
            fwrite($fd, "else \n");
            fwrite($fd, "{\n");
            fwrite($fd, "/* _SECURE_POLICY_PHP_ routine */\n");
        }

        foreach ($array as $key => $value)
        {
            if ($count == $original)
                $step ++;

            if (!is_array($array[$key]))
                fwrite($fd, $vpath . "']['" . $key . "'] = \"" . $value . "\";\n");

            if (is_array($array[$key]))
            {

                unset($imsi);
                if ($step == 1)
                    $imsi = $vpath . "['" . $key;
                else
                    $imsi = $vpath . "']['" . $key;

                write_policy($array[$key], $imsi, $step, $fd);
            }

            $count --;
        }

        if ($step == 1)
        {
            fwrite($fd, "/* End of _SECURE_POLICY_PHP */\n");
            fwrite($fd, "}\n\n");
            fwrite($fd, "/* End of policy.php */\n");
            fwrite($fd, "?>");

            fclose($fd);

            if (file_exists("policy.php"))
                copy("policy.php", "policy.bak.php");

            copy("policy.tmp.php", "policy.php");
            unlink("policy.tmp.php");

            if (file_exists("policy.php"))
                chmod("policy.php", 0604);

            if (file_exists("policy.bak.php"))
                chmod("policy.bak.php", 0604);
        }

        return;
    }

    function print_policy($array = NULL, $vpath = "\$_SECURE_POLICY", $step = 0)
    {
        global $_SECURE_POLICY;

        if (!$array && !$step)
            $array = & $_SECURE_POLICY;

        if (!is_array($array))
            return;

        $count = count($array);
        $original = count($array);

        if (!$step)
            print("<b>Secure Policy Source View<br><br></b>\n");

        foreach ($array as $key => $value)
        {
            if ($count == $original)
                $step ++;

            if (!is_array($array[$key]))
                print($vpath . "']['" . $key . "'] = \"" . htmlentities(base64_decode($value), ENT_QUOTES, "UTF-8") . "\"<br>\n");

            if (is_array($array[$key]))
            {

                unset($imsi);
                if ($step == 1)
                    $imsi = $vpath . "['" . $key;
                else
                    $imsi = $vpath . "']['" . $key;

                print_policy($array[$key], $imsi, $step);
            }

            $count --;
        }

        return;
    }

    function print_policy_tree($array = NULL, $step = 0)
    {
        global $_SECURE_POLICY;

        if (!$array && !$step)
            $array = & $_SECURE_POLICY;

        if (!is_array($array))
            return;

        $count = count($array);
        $original = count($array);

        if (!$step)
            print("<b>Secure Policy Tree View<br><br></b>\n");

        foreach ($array as $key => $value)
        {
            if ($count == $original)
                $step ++;

            if ($key != "PASSWORD")
            {
                for ($i = 0; $i < $step; $i++)
                    print("&nbsp; &nbsp; &nbsp; &nbsp; ");

                if (!is_array($array[$key]))
                    print("$key = " . htmlentities(base64_decode($value), ENT_QUOTES, "UTF-8") . "<br>\n");
            }

            if (is_array($array[$key]))
            {
                print("<b>- $key</b><br>\n");
                print_policy_tree($array[$key], $step);
            }

            $count --;
        }

        return;
    }

    function check_length($check_name, $length, $min, $max)
    {
        if ($length < $min)
            html_msgback($check_name . ": Input min " . $min . " chracters.");

        if ($length > $max)
            html_msgback($check_name . ": Input max " . $max . " chracters.");

        return;
    }

    function print_error($error)
    {
        print($error);
    }

    function init_page()
    {
        if (isset($GLOBALS['page']))
            unset($page);

        if (isset($GLOBALS['print']))
            unset($print);

        if (isset($GLOBALS['submit']))
            unset($submit);

        if (isset($GLOBALS['policy']))
            unset($policy);

        return;
    }

    function check_installed()
    {
        if (!file_exists("policy.php"))
            html_msgmove("Do not install the secure module.", "install.php");

        return;
    }

    function check_authorized()
    {
        global $_SESSION;

        $auth['remote_addr'] = getenv("REMOTE_ADDR");
        $auth['user_agent'] = getenv("HTTP_USER_AGENT");

        $auth['key'] = "auth_token_" . $auth['remote_addr'];
        $auth['value'] = md5($auth['user_agent']);

        if (!isset($_SESSION))
            html_msgmove("You do not authenticated as administrator.", "admin_login.php");

        if (!isset($_SESSION[$auth['key']]))
            html_msgmove("You do not authenticated as administrator.", "admin_login.php");

        if ($_SESSION[$auth['key']] != $auth['value'])
            html_msgmove("You do not authenticated as administrator.", "admin_login.php");

        return;
    }

    function check_submit($key_array)
    {
        if (!isset($GLOBALS['submit']))
        {
            html_msgback("Entered the invalid variable.");
            return;
        }

        if (!is_array($GLOBALS['submit']))
            return;

        if (!is_array($key_array))
        {
            if (!isset($GLOBALS['submit'][$key_array]))
                html_msgback($key_array . "Parameter is not declared.");
            return;
        }

        for ($i = 0; isset($key_array[$i]); $i++)
        {
            $key = $key_array[$i];
            if (!isset($GLOBALS['submit'][$key]))
                html_msgback($key . "Parameter is not declared.");
        }

        return;
    }

    function clear_submit()
    {
        if (!isset($GLOBALS['submit']))
            return;

        if (!is_array($GLOBALS['submit']))
            unset($GLOBALS['submit']);

        foreach ($GLOBALS['submit'] as $key => $value)
            unset($GLOBALS['submit'][$key]);

        return;
    }

    function ie50_51_60_check($user_agent)
    {
        if (preg_match("(MSIE 5.0|MSIE 5.1|MSIE 6.0)i", $user_agent))
        {
            return TRUE;
        }
        return FALSE;
    }

    function ie55_check($user_agent)
    {
        if (preg_match("/MSIE 5.5/i", $user_agent))
        {
            return TRUE;
        }
        return FALSE;
    }

    function file_download($filename, $filepath, $filesize)
    {
        $user_agent = getenv("HTTP_USER_AGENT");

        if (!file_exists($filepath))
            html_msgback("File do not exist.");

        if (ie50_51_60_check($user_agent))
        {
            header("Cache-Control: public");
            Header("Content-Disposition: attachment; filename=" . $filename . ";");
            Header("Content-type: application/x-force-download");
        }
        else
        if (ie55_check($user_agent))
        {
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            Header("Content-Disposition: inline; filename=" . $filename . ";");
        }
        else
        {
            header("Cache-Control: public");
            Header("Content-Disposition: attachment; filename=" . $filename . ";");
            Header("Content-type: application/octet-stream");
        }

        header("Expires: " . gmdate("D, d M Y H:i:s", mktime(date("H") + 2, date("i"), date("s"), date("m"), date("d"), date("Y"))) . " GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Content-Length: " . (string) $filesize);
        Header("Content-Transfer-Encoding: binary");

        readfile($filepath);

        return;
    }

    function delete_directory_traverse($path)
    {
        return util_delete_directory_traverse($path);
        /*
          // "/../" -> "/"
          while (preg_match("#/\.\./#i", $path))
          $path = preg_replace("#/\.\./#i", "/", $path);

          // "./" -> ""
          while (preg_match("#\./#i", $path))
          $path = preg_replace("#\./#i", "", $path);

          // "//" -> "/"
          while (preg_match("#//#i", $path))
          $path = preg_replace("#//#i", "/", $path);

          // "\..\" -> "\"
          while (preg_match("/\\\\\.\.\\\\/i", $path))
          $path = preg_replace("/\\\\\.\.\\\\/i", "\\", $path);

          // ".\" -> ""
          while (preg_match("/\.\\\\/i", $path))
          $path = preg_replace("/\.\\\\/i", "", $path);

          // "\\" -> "\"
          while (preg_match("/\\\\\\\\/", $path))
          $path = preg_replace("/\\\\\\\\/i", "\\", $path);

          return $path;
         */
    }

    /* HTML */

    function html_msg($msg)
    {
        global $_SECURE_POLICY;

        print("<html>\n");
        print(" <head>");
        print("   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
        print(" </head>");
        print(" <script> alert(\"$msg\"); </script>");
        print("</html>\n");
    }

    function html_move($url, $target = "")
    {
        if ($target)
            $target .= ".";

        print("<html>\n");
        print(" <head>");
        print("   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
        print(" </head>");
        print(" <script> " . $target . "location.href='$url'; </script>");
        print("</html>\n");

        exit;
    }

    function html_close()
    {
        print("<html>\n");
        print(" <head>");
        print("   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
        print(" </head>");
        print("<script> window.close(); </script>");
        print("</html>\n");

        exit;
    }

    function html_back()
    {
        print("<html>\n");
        print(" <head>");
        print("   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
        print(" </head>");
        print(" <script> history.back(-1); </script>");
        print("</html>\n");

        exit;
    }

    function html_msgmove($msg, $url, $target = "")
    {
        html_msg($msg);
        html_move($url, $target);
    }

    function html_msgback($msg)
    {
        html_msg($msg);
        html_back();
    }

    function html_msgclose($msg)
    {
        html_msg($msg);
        html_close();
    }

    /* HTML */

    if (!isset($exception['check_installed']))
        check_installed();
    else
    if (!$exception['check_installed'])
        check_installed();

    /*
      if (!isset($exception['check_authorized']))
      {
      check_authorized(1);
      }
      else
      if (!$exception['check_authorized'])
      {
      check_authorized(2);
      }
     */

    /* End of _SECURE_ADMIN_LIB_PHP_ */
}

/* End of admin_lib.php */
?>
