<?php
include_once("admin_lib.php");
check_authorized();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" HREF="style.css" type="text/css" title="style">
    </head>
    <body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="#D0D0D0">
        <?php include_once("admin_title.php"); ?>
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
            <tr> 
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr bgcolor="#CACACA">
                            <td width="100%" height="80" colspan="2">
                                <?php include_once("admin_top.php"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="160" bgcolor="#f3f3f3">
                                <?php include_once("admin_menu.php"); ?>
                            </td>
                            <td width="100%" bgcolor="#f3f3f3" valign="top">
                                <br><br>
                                <div id="main_column" class="clear">
                                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr valign="top">
                                            <td width="100%">
                                                <table width="100%" height="100%" cellspacing="20" cellpadding="0" border="0">
                                                    <tr valign="top">
                                                        <td width="100%" style="line-height:100%" nowrap>

                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Secure module configuration</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>                                                            

                                                            <?php
                                                            if (isset($_SECURE_POLICY['CONFIG']['ADMIN']['MODULE_NAME']))
                                                                $policy['admin_module_name'] = & $_SECURE_POLICY['CONFIG']['ADMIN']['MODULE_NAME'];
                                                            else
                                                                $policy['admin_module_name'] = base64_encode(SECURE_BASE_MODULE_NAME);

                                                            $print['admin_module_name'] = base64_decode($policy['admin_module_name']);

                                                            $policy['mode_enforcing'] = & $_SECURE_POLICY['CONFIG']['MODE']['ENFORCING'];
                                                            $policy['mode_permissive'] = & $_SECURE_POLICY['CONFIG']['MODE']['PERMISSIVE'];
                                                            $policy['mode_disabled'] = & $_SECURE_POLICY['CONFIG']['MODE']['DISABLED'];

                                                            $policy['mode_enforcing'] = base64_decode($policy['mode_enforcing']);
                                                            $policy['mode_permissive'] = base64_decode($policy['mode_permissive']);
                                                            $policy['mode_disabled'] = base64_decode($policy['mode_disabled']);

                                                            $print['mode_enforcing_select'] = "";
                                                            $print['mode_permissive_select'] = "";
                                                            $print['mode_disabled_select'] = "";

                                                            if ($policy['mode_enforcing'] == "TRUE")
                                                                $print['mode_enforcing_select'] = "selected";

                                                            if ($policy['mode_permissive'] == "TRUE")
                                                                $print['mode_permissive_select'] = "selected";

                                                            if ($policy['mode_disabled'] == "TRUE")
                                                                $print['mode_disabled_select'] = "selected";

                                                            $policy['alert_alert'] = & $_SECURE_POLICY['CONFIG']['ALERT']['ALERT'];
                                                            $policy['alert_message'] = & $_SECURE_POLICY['CONFIG']['ALERT']['MESSAGE'];
                                                            $policy['alert_stealth'] = & $_SECURE_POLICY['CONFIG']['ALERT']['STEALTH'];

                                                            $policy['alert_alert'] = base64_decode($policy['alert_alert']);
                                                            $policy['alert_message'] = base64_decode($policy['alert_message']);
                                                            $policy['alert_stealth'] = base64_decode($policy['alert_stealth']);

                                                            $print['alert_alert_select'] = "";
                                                            $print['alert_message_select'] = "";
                                                            $print['alert_stealth_select'] = "";

                                                            if ($policy['alert_alert'] == "TRUE")
                                                                $print['alert_alert_select'] = "selected";

                                                            if ($policy['alert_message'] == "TRUE")
                                                                $print['alert_message_select'] = "selected";

                                                            if ($policy['alert_stealth'] == "TRUE")
                                                                $print['alert_stealth_select'] = "selected";
                                                            ?>
                                                            <table cellspacing="2" cellpadding="5" border="0">
                                                                <form action="admin_config_submit.php?mode=CONFIG_BASIC" method="post">
                                                                    <tr>
                                                                        <th width="150" height="30" align="right">Module name</th>
                                                                        <td>
                                                                            <input type="text" name="admin_module_name" class="input-text" size="48" maxlength="64" value="<?php echo $print['admin_module_name'] ?>">
                                                                        </td>
                                                                    </tr>
                                                            </table>

                                                            <table width="800" cellspacing="2" cellpadding="5" border="0">
                                                                <tr>
                                                                    <th width="150" height="30" align="right">Execution mode</th>
                                                                    <td>
                                                                        <select type="radio" name="config_mode">
                                                                            <option value="enforcing" <?php echo $print['mode_enforcing_select'] ?>>enforcing
                                                                            <option value="permissive" <?php echo $print['mode_permissive_select'] ?>>permissive
                                                                            <option value="disabled" <?php echo $print['mode_disabled_select'] ?>>disabled
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>
                                                                        <table width="100%" height="50" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                            <tr>
                                                                                <td bgcolor="#FFFFFF" style="line-height:120%" nowrap>
                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <li>enforcing - Applying the secure module.
                                                                                            <li>permissive - Permissive the secure module, but it does not apply (default).
                                                                                            <li>disabled - Do not apply the secure module.
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th width="150" height="30" align="right">Notification method</th>
                                                                    <td>
                                                                        <select type="radio" name="config_alert">
                                                                            <option value="alert" <?php echo $print['alert_alert_select'] ?>>alert
                                                                            <option value="message"  <?php echo $print['alert_message_select'] ?>>message
                                                                            <option value="stealth" <?php echo $print['alert_stealth_select'] ?>>stealth
                                                                        </select>
                                                                    </td>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>
                                                                        <table width="100%" height="50" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                            <tr>
                                                                                <td bgcolor="#FFFFFF" style="line-height:120%" nowrap>
                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            <li>Alert - Execution results notifications alert.
                                                                                            <li>Message - Enforcement results in a message notification.
                                                                                            <li>Stealth - Do not notify any results (default).
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="800" height="1">
                                                                <tr>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                            <table width="475" height="35" cellspacing="0" cellpadding="0" border="0">
                                                                <tr valign="top">
                                                                    <td width="175">&nbsp;</td>
                                                                    <td width="300">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="submit" value="Conform">
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                    return false;">
                                                                        </span>
                                                                        <br><br>
                                                                    </td>
                                                                </tr>
                                                                </form>
                                                            </table>
                                                            <br><br>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0">
                                                                <tr>                                                                    
                                                                    <td width="2"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Site setting</p>
                                                                    </td>
                                                                    <td width="2"></td>
                                                                </tr>
                                                            </table>

                                                            <?php
                                                            if (isset($_SECURE_POLICY['CONFIG']['SITE']['BOOL']))
                                                                $policy['site_bool'] = & $_SECURE_POLICY['CONFIG']['SITE']['BOOL'];

                                                            if (isset($policy['site_bool']))
                                                                $policy['site_bool'] = base64_decode($policy['site_bool']);
                                                            else
                                                                $policy['site_bool'] = "TRUE";

                                                            $print['site_bool_true_check'] = "";
                                                            $print['site_bool_false_check'] = "";

                                                            if ($policy['site_bool'] == "TRUE")
                                                                $print['site_bool_true_check'] = "checked";
                                                            else
                                                                $print['site_bool_false_check'] = "checked";
                                                            ?>
                                                            <br>
                                                            <table width="800" cellspacing="2" cellpadding="5" border="0">
                                                                <form action="admin_config_submit.php?mode=CONFIG_SITE" method="post">
                                                                    <tr>
                                                                        <th width="150" height="30" align="right">Site status</th>
                                                                        <td>
                                                                            <input type="radio" name="config_site_bool" value="true" <?php echo $print['site_bool_true_check'] ?>>Open (default)
                                                                            <input type="radio" name="config_site_bool" value="false" <?php echo $print['site_bool_false_check'] ?>>Closed
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td>
                                                                            <table width="100%" height="50" cellspacing="0" cellpadding="10" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>Open - The site running normally..
                                                                                                <li>Closed - The site be locked.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                            </table>

                                                            <table width="475" height="35" cellspacing="0" cellpadding="0" border="0">
                                                                <tr valign="top">
                                                                    <td width="175">&nbsp;</td>
                                                                    <td width="300">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="submit" value="Conform">
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                    return false;">
                                                                        </span>
                                                                        <br><br>
                                                                    </td>
                                                                </tr>
                                                                </form>
                                                            </table>

                                                            <?php ?>
                                                            <br>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td width="2"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Apply Parameters</p>
                                                                    </td><td width="2"></td>
                                                                </tr>
                                                            </table>

                                                            <?php
                                                            $policy['target_get'] = & $_SECURE_POLICY['CONFIG']['TARGET']['GET'];
                                                            $policy['target_post'] = & $_SECURE_POLICY['CONFIG']['TARGET']['POST'];
                                                            $policy['target_cookie'] = & $_SECURE_POLICY['CONFIG']['TARGET']['COOKIE'];
                                                            $policy['target_file'] = & $_SECURE_POLICY['CONFIG']['TARGET']['FILE'];

                                                            $policy['target_get'] = base64_decode($policy['target_get']);
                                                            $policy['target_post'] = base64_decode($policy['target_post']);
                                                            $policy['target_cookie'] = base64_decode($policy['target_cookie']);
                                                            $policy['target_file'] = base64_decode($policy['target_file']);

                                                            $print['target_get_true_check'] = "";
                                                            $print['target_get_false_check'] = "";
                                                            $print['target_post_true_check'] = "";
                                                            $print['target_post_false_check'] = "";
                                                            $print['target_cookie_true_check'] = "";
                                                            $print['target_cookie_false_check'] = "";
                                                            $print['target_file_true_check'] = "";
                                                            $print['target_file_false_check'] = "";

                                                            if ($policy['target_get'] == "TRUE")
                                                                $print['target_get_true_check'] = "checked";
                                                            else
                                                                $print['target_get_false_check'] = "checked";

                                                            if ($policy['target_post'] == "TRUE")
                                                                $print['target_post_true_check'] = "checked";
                                                            else
                                                                $print['target_post_false_check'] = "checked";

                                                            if ($policy['target_cookie'] == "TRUE")
                                                                $print['target_cookie_true_check'] = "checked";
                                                            else
                                                                $print['target_cookie_false_check'] = "checked";

                                                            if ($policy['target_file'] == "TRUE")
                                                                $print['target_file_true_check'] = "checked";
                                                            else
                                                                $print['target_file_false_check'] = "checked";
                                                            ?>
                                                            <br>
                                                            <table cellspacing="2" cellpadding="5" border="0">
                                                                <form action="admin_config_submit.php?mode=CONFIG_TARGET" method="post">
                                                                    <tr>
                                                                        <th width="130" height="30" align="right">GET Params</th>
                                                                        <td>
                                                                            <input type="radio" name="target_get" value="true" <?php echo $print['target_get_true_check'] ?>>Yes
                                                                            <input type="radio" name="target_get" value="false" <?php echo $print['target_get_false_check'] ?>>No
                                                                        </td>
                                                                        <th width="130" height="30" align="right">POST Params</th>
                                                                        <td width="150">
                                                                            <input type="radio" name="target_post" value="true" <?php echo $print['target_post_true_check'] ?>>Yes
                                                                            <input type="radio" name="target_post" value="false" <?php echo $print['target_post_false_check'] ?>>No
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="130" height="30" align="right">FILE Params</th>
                                                                        <td width="150">
                                                                            <input type="radio" name="target_file" value="true" <?php echo $print['target_file_true_check'] ?>>Yes
                                                                            <input type="radio" name="target_file" value="false" <?php echo $print['target_file_false_check'] ?>>No
                                                                        </td>
                                                                        <th width="130" height="30" align="right">COOKIE Params</th>
                                                                        <td>
                                                                            <input type="radio" name="target_cookie" value="true" <?php echo $print['target_cookie_true_check'] ?>>Yes
                                                                            <input type="radio" name="target_cookie" value="false" <?php echo $print['target_cookie_false_check'] ?>>No
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="100" height="30"></th>
                                                                        <td width="630" colspan="3">
                                                                            <table width="100%" height="50" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                                <tr>
                                                                                    <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                        <div class="statistics-box inventory">
                                                                                            <div class="statistics-body">
                                                                                                <li>Yes - Apply the secure policy in parameters.
                                                                                                <li>No&nbsp; - Do not apply the secure policy in parameters.
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                            </table>
                                                            <br>
                                                            <table width="475" height="50" cellspacing="0" cellpadding="0" border="0">
                                                                <tr valign="top">
                                                                    <td width="175">&nbsp;</td>
                                                                    <td width="300">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="submit" value="Conform">
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="reset();
                                                                                    return false;">
                                                                        </span>
                                                                        <br><br>
                                                                    </td>
                                                                </tr>
                                                                </form>
                                                            </table>
                                                            <?php ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr bgcolor="#A0A0A0">
                            <td width="100%" height="50" colspan="2" align="center">
                                <?php include_once("admin_bottom.php"); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
