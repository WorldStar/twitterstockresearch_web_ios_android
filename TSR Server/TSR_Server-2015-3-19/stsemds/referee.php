<?php

if (!defined("__SECURE_MODULE_BASE_DIR__"))
    return;

/* check if exist policy file */
if (!file_exists(__SECURE_MODULE_BASE_DIR__ . "/policy.php"))
    return;

include_once(__SECURE_MODULE_BASE_DIR__ . "/policy.php");

require_once 'admin_util.php';

function referee_alert($msg)
{
    global $_SECURE_POLICY, $_SERVER;

    /* if exist alert */
    if (!isset($_SECURE_POLICY['CONFIG']['ALERT']))
        return;

    /* get alert policy */
    $policy['config'] = & $_SECURE_POLICY['CONFIG']['ADMIN']['MODULE_NAME'];
    $policy['config_title'] = base64_decode($policy['config']);

    $policy['alert'] = & $_SECURE_POLICY['CONFIG']['ALERT'];
    $policy['alert_alert'] = base64_decode($policy['alert']['ALERT']);
    $policy['alert_message'] = base64_decode($policy['alert']['MESSAGE']);
    $policy['alert_stealth'] = base64_decode($policy['alert']['STEALTH']);

    // if stealth mode
    if ($policy['alert_stealth'] == "TRUE")
    {
        echo "";
        return;
    }


    $page = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];

    // if message mode
    if ($policy['alert_message'] == "TRUE")
    {
        /* echo "
          <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">
          <html>
          <head>
          <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
          </head>
          <body bgcolor=\"#FFFFFF\">
          <center><br><br><br><img src=\"".$error_img."\"></center>
          </body>
          </html>"; */
        include "error.html";

        return;
    }

    // if alert mode
    if ($policy['alert_alert'] == "TRUE")
    {
        $alert_msg_utf8 = "\\-- Notification -- \\n\\n";
        $alert_msg_utf8 .= "Blocked by secure module.\\n\\n";
        $alert_msg_utf8 .= "--- Blocked page: ---\\n\\n" . $page . "\\n\\n";
        $alert_msg_utf8 .= "--- Reason for Blocking ---\\n\\n" . $msg . "\\n\\n";

        $alert_msg_euckr = iconv("UTF-8", "eucKR", $alert_msg_utf8);

        $alert_msg_js = "<script language=\"javascript\">\n";
        $alert_msg_js .= "	var SecureBrowserDetect = {\n";
        $alert_msg_js .= "		init: function () {\n";
        $alert_msg_js .= "			this.browser = this.searchString(this.dataBrowser) || \"An unknown browser\";\n";
        $alert_msg_js .= "			this.version = this.searchVersion(navigator.userAgent)\n";
        $alert_msg_js .= "				|| this.searchVersion(navigator.appVersion)\n";
        $alert_msg_js .= "				|| \"an unknown version\";\n";
        $alert_msg_js .= "			this.OS = this.searchString(this.dataOS) || \"an unknown OS\";\n";
        $alert_msg_js .= "		},\n";
        $alert_msg_js .= "		searchString: function (data) {\n";
        $alert_msg_js .= "			for (var i=0;i<data.length;i++) {\n";
        $alert_msg_js .= "				var dataString = data[i].string;\n";
        $alert_msg_js .= "				var dataProp = data[i].prop;\n";
        $alert_msg_js .= "				this.versionSearchString = data[i].versionSearch || data[i].identity;\n";
        $alert_msg_js .= "				if (dataString) {\n";
        $alert_msg_js .= "					if (dataString.indexOf(data[i].subString) != -1)\n";
        $alert_msg_js .= "						return data[i].identity;\n";
        $alert_msg_js .= "				}\n";
        $alert_msg_js .= "				else if (dataProp)\n";
        $alert_msg_js .= "					return data[i].identity;\n";
        $alert_msg_js .= "			}\n";
        $alert_msg_js .= "		},\n";
        $alert_msg_js .= "		searchVersion: function (dataString) {\n";
        $alert_msg_js .= "			var index = dataString.indexOf(this.versionSearchString);\n";
        $alert_msg_js .= "			if (index == -1) return;\n";
        $alert_msg_js .= "			return parseFloat(dataString.substring(index+this.versionSearchString.length+1));\n";
        $alert_msg_js .= "		},\n";
        $alert_msg_js .= "		dataBrowser: [\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.userAgent,\n";
        $alert_msg_js .= "				subString: \"Chrome\",\n";
        $alert_msg_js .= "				identity: \"Chrome\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{       string: navigator.userAgent,\n";
        $alert_msg_js .= "				subString: \"OmniWeb\",\n";
        $alert_msg_js .= "				versionSearch: \"OmniWeb/\",\n";
        $alert_msg_js .= "				identity: \"OmniWeb\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.vendor,\n";
        $alert_msg_js .= "				subString: \"Apple\",\n";
        $alert_msg_js .= "				identity: \"Safari\",\n";
        $alert_msg_js .= "				versionSearch: \"Version\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				prop: window.opera,\n";
        $alert_msg_js .= "				identity: \"Opera\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.vendor,\n";
        $alert_msg_js .= "				subString: \"iCab\",\n";
        $alert_msg_js .= "				identity: \"iCab\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.vendor,\n";
        $alert_msg_js .= "				subString: \"KDE\",\n";
        $alert_msg_js .= "				identity: \"Konqueror\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.userAgent,\n";
        $alert_msg_js .= "				subString: \"Firefox\",\n";
        $alert_msg_js .= "				identity: \"Firefox\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.vendor,\n";
        $alert_msg_js .= "				subString: \"Camino\",\n";
        $alert_msg_js .= "				identity: \"Camino\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{	       // for newer Netscapes (6+)\n";
        $alert_msg_js .= "				string: navigator.userAgent,\n";
        $alert_msg_js .= "				subString: \"Netscape\",\n";
        $alert_msg_js .= "				identity: \"Netscape\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.userAgent,\n";
        $alert_msg_js .= "				subString: \"MSIE\",\n";
        $alert_msg_js .= "				identity: \"Explorer\",\n";
        $alert_msg_js .= "				versionSearch: \"MSIE\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.userAgent,\n";
        $alert_msg_js .= "				subString: \"Gecko\",\n";
        $alert_msg_js .= "				identity: \"Mozilla\",\n";
        $alert_msg_js .= "				versionSearch: \"rv\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{	       // for older Netscapes (4-)\n";
        $alert_msg_js .= "				string: navigator.userAgent,\n";
        $alert_msg_js .= "				subString: \"Mozilla\",\n";
        $alert_msg_js .= "				identity: \"Netscape\",\n";
        $alert_msg_js .= "				versionSearch: \"Mozilla\"\n";
        $alert_msg_js .= "			}\n";
        $alert_msg_js .= "		],\n";
        $alert_msg_js .= "		dataOS : [\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.platform,\n";
        $alert_msg_js .= "				subString: \"Win\",\n";
        $alert_msg_js .= "				identity: \"Windows\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.platform,\n";
        $alert_msg_js .= "				subString: \"Mac\",\n";
        $alert_msg_js .= "				identity: \"Mac\"\n";
        $alert_msg_js .= "			},\n";
        $alert_msg_js .= "			{\n";
        $alert_msg_js .= "				string: navigator.platform,\n";
        $alert_msg_js .= "				subString: \"Linux\",\n";
        $alert_msg_js .= "				identity: \"Linux\"\n";
        $alert_msg_js .= "			}\n";
        $alert_msg_js .= "		]\n";
        $alert_msg_js .= "\n";
        $alert_msg_js .= "	};\n";
        $alert_msg_js .= "\n";
        $alert_msg_js .= "	SecureBrowserDetect.init();\n";
        $alert_msg_js .= "\n";
        $alert_msg_js .= "	var SecureBrowser = '';\n";
        $alert_msg_js .= "	if (SecureBrowserDetect.browser == \"Explorer\")\n";
        $alert_msg_js .= "		SecureBrowser = document.charset;\n";
        $alert_msg_js .= "	else\n";
        $alert_msg_js .= "		SecureBrowser = document.characterSet;\n";
        $alert_msg_js .= "\n";
        $alert_msg_js .= "	if (SecureBrowser == \"UTF-8\" || SecureBrowser == \"utf-8\") \n";
        $alert_msg_js .= "		alert('" . $alert_msg_utf8 . "');\n";
        $alert_msg_js .= "	else \n";
        $alert_msg_js .= "		alert('" . $alert_msg_euckr . "');\n";
        $alert_msg_js .= "\n";
        $alert_msg_js .= "	history.back(-1);\n";
        $alert_msg_js .= "\n";
        $alert_msg_js .= "</script>\n";

        print($alert_msg_js);

        return;
    }
}

/* referee logging function
 *
 * Param: 
 *    $msg - log msg
 *
 * return:
 */

function referee_logger($msg)
{
    global $_SECURE_POLICY;

    /* check if exist log policy */
    if (!isset($_SECURE_POLICY['CONFIG']['LOG']))
        return;

    /* get log policy */
    $policy['log'] = & $_SECURE_POLICY['CONFIG']['LOG'];
    $policy['log_bool'] = base64_decode($policy['log']['BOOL']);
    $policy['log_filename'] = base64_decode($policy['log']['FILENAME']);

    /* check if record log */
    if ($policy['log_bool'] == "FALSE")
        return;

    // "<" -> "&lt;"
    $msg = util_htmlencode_lt($msg);
    // ">" -> "&gt;"
    $msg = util_htmlencode_gt($msg);

    /* record log */
    $log['filename'] = __SECURE_MODULE_BASE_DIR__;
    $log['filename'] .= "/log/" . date("Ymd") . "-" . $policy['log_filename'];

    if (!file_exists($log['filename']))
        $log['openmode'] = "w";
    else
        $log['openmode'] = "a+";

    $fd = fopen($log['filename'], $log['openmode']);
    if ($fd)
    {
        fwrite($fd, $msg, strlen($msg));
        fclose($fd);
    }

    return;
}

/* Check regular expression
 *
 */

function referee_eregi($regexp, $str)
{
    global $_SECURE_POLICY;

    $regexp = trim($regexp);
    $str = trim($str);

    if (!strlen($regexp) || !strlen($str))
        return NULL;

    $str = referee_urldecode($str);
    $str = referee_unhtmlentities($str);
    $str = referee_htmldecode($str);

    // delete slashes
    if (get_magic_quotes_gpc())
        $str = stripslashes($str);

    // delete %00, %0a
    $str = referee_delete_special_characters($str);

    // check regular expression
    $esc_regexp = str_replace("/", "\/", $regexp);

    if (preg_match("/$esc_regexp/", iconv("eucKR", "UTF-8", $str)))
        return $regexp;

    /* UTF-8 */
    if (preg_match("/$esc_regexp/", $str))
        return $regexp;

    return NULL;
}

function referee_urldecode($str)
{
    /* HTTP URL Decoding */

    // ULl Decoding  ex., '+' -> ' '
    $str = urldecode($str);

    // RAW ULl Decoding  ex., "%20"' -> ' '
    $str = rawurldecode($str);

    return $str;
}

// For users prior to PHP 4.3.0 you may do this:
function referee_unhtmlentities($str)
{
    // replace numeric entities
    $str = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $str);
    $str = preg_replace('~&#([0-9]+);~e', 'chr("\\1")', $str);

    $trans_tbl = get_html_translation_table(HTML_ENTITIES);
    $trans_tbl = array_flip($trans_tbl);

    return strtr($str, $trans_tbl);
}

function referee_htmldecode($str)
{
    /* ASCII Entities Decoding */

    // Hex encoding '&#x20;' -> ' '
    while (preg_match('~&#x([0-9a-f]+);~ei', $str))
        $str = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $str);

    // Hex encoding without semicolons '&#x20' -> ' '
    while (preg_match('~&#x([0-9a-f]+)~ei', $str))
        $str = preg_replace('~&#x([0-9a-f]+)~ei', 'chr(hexdec("\\1"))', $str);

    // UTF-8 Unicode encoding '&#32;' -> ' '
    while (preg_match('~&#([0-9]+);~e', $str))
        $str = preg_replace('~&#([0-9]+);~e', 'chr("\\1")', $str);

    // UTF-8 Unicode encoding without semicolons '&#32' -> ' '
    while (preg_match('~&#([0-9]+)~e', $str))
        $str = preg_replace('~&#([0-9]+)~e', 'chr("\\1")', $str);

    return $str;
}

function referee_delete_special_characters($str)
{
    while (preg_match('/\x00/', $str))
        $str = preg_replace('/\x00/', '', $str);

    while (preg_match('/\x0a/', $str))
        $str = preg_replace('/\x0a/', '', $str);

    return $str;
}

function referee_delete_directory_traverse($path)
{
    /* Delete directory traverse attack.. */

    $path = referee_urldecode($path);

    return util_delete_directory_traverse($path);

    /*
      // "/../" -> "/"
      while (preg_match("#/\.\./#i", $path))
      $path = preg_replace("#/\.\./#i", "/", $path);

      // "./" -> ""
      while (preg_match("#\./#i", $path))
      $path = preg_replace("#\./#i", "", $path);

      // "//" -> "/"
      while (preg_match("#//#i", $path))
      $path = preg_replace("#//#i", "/", $path);

      // "\..\" -> "\"
      while (preg_match("#/\.\./#i", $path))
      $path = preg_replace("#\\\.\.\\#i", "\\", $path);

      // ".\" -> ""
      while (preg_match("#\./#i", $path))
      $path = preg_replace("#\.\\#i", "", $path);

      // "\\" -> "\"
      while (preg_match("//", $path))
      $path = preg_replace("#\\\\#i", "\\", $path);

      return $path;
     */
}

function referee_error_handler($policy_type, $rule, $method, $key, $value, $msg = "")
{
    global $_SECURE_POLICY, $_SERVER;

    if (!isset($_SECURE_POLICY['CONFIG']['ALERT']))
        return;

    if (!isset($_SECURE_POLICY['CONFIG']['LOG']))
        return;

    $policy['log'] = & $_SECURE_POLICY['CONFIG']['LOG'];
    $policy['log_bool'] = base64_decode($policy['log']['BOOL']);
    $policy['log_simple'] = base64_decode($policy['log']['SIMPLE']);
    $policy['log_detail'] = base64_decode($policy['log']['DETAIL']);

    $policy['mode'] = & $_SECURE_POLICY['CONFIG']['MODE'];
    $policy['mode_enforcing'] = base64_decode($policy['mode']['ENFORCING']);
    $policy['mode_permissive'] = base64_decode($policy['mode']['PERMISSIVE']);

    $policy['log_charset']['eucKR'] = "";
    $policy['log_charset']['UTF-8'] = "";
    if (isset($_SECURE_POLICY['CONFIG']['LOG']['CHARSET']))
    {
        $policy['log_charset'] = $_SECURE_POLICY['CONFIG']['LOG']['CHARSET'];
        $policy['log_charset']['UTF-8'] = base64_decode($policy['log_charset']['UTF-8']);
        $policy['log_charset']['eucKR'] = base64_decode($policy['log_charset']['eucKR']);
    }

    $value = util_remove_crlf($value);

    if ($policy['log_simple'] == "TRUE")
    {
        $log['simple'] = $_SERVER['REMOTE_ADDR'] . " - [" . date("d/M/Y:H:i:s O") . "] ";

        if (!iconv("UTF-8", "eucKR", $value))
        {
            if ($policy['log_charset']['UTF-8'] == "TRUE")
            {
                $log['simple'] .= iconv("eucKR", "UTF-8", $_SERVER['PHP_SELF'] . ": ");
                $log['simple'] .= iconv("eucKR", "UTF-8", $key . " = " . substr($value, 0, 64) . ": ");
                $log['simple'] .= $msg . "\n";
            }
            else
            {
                $log['simple'] .= $_SERVER['PHP_SELF'] . ": ";
                $log['simple'] .= $key . " = " . substr($value, 0, 64) . ": ";
                $log['simple'] .= iconv("UTF-8", "eucKR", $msg . "\n");
            }
        }
        else
        {
            if ($policy['log_charset']['UTF-8'] == "TRUE")
            {
                $log['simple'] .= $_SERVER['PHP_SELF'] . ": ";
                $log['simple'] .= $key . " = " . substr($value, 0, 64) . ": ";
                $log['simple'] .= $msg . "\n";
            }
            else
            {
                $log['simple'] .= iconv("UTF-8", "eucKR", $_SERVER['PHP_SELF'] . ": ");
                $log['simple'] .= iconv("UTF-8", "eucKR", $key . " = " . substr($value, 0, 64) . ": ");
                $log['simple'] .= iconv("UTF-8", "eucKR", $msg . "\n");
            }
        }

        referee_logger($log['simple']);
    }
    else
    if ($policy['log_detail'] == "TRUE")
    {
        $log['detail'] = $_SERVER['REMOTE_ADDR'] . " - [" . date("d/M/Y:H:i:s O") . "] ";

        if (!iconv("UTF-8", "eucKR", $value))
        {
            if ($policy['log_charset']['UTF-8'] == "TRUE")
            {
                $log['detail'] .= iconv("eucKR", "UTF-8", $_SERVER['PHP_SELF'] . ": ");
                $log['detail'] .= iconv("eucKR", "UTF-8", $key . " = " . substr($value, 0, 64) . ": ");
                $log['detail'] .= $msg . "\n";
            }
            else
            {
                $log['detail'] .= $_SERVER['PHP_SELF'] . ": ";
                $log['detail'] .= $key . " = " . substr($value, 0, 64) . ": ";
                $log['detail'] .= iconv("UTF-8", "eucKR", $msg . "\n");
            }
        }
        else
        {
            if ($policy['log_charset']['UTF-8'] == "TRUE")
            {
                $log['detail'] .= $_SERVER['PHP_SELF'] . ": ";
                $log['detail'] .= $key . " = " . substr($value, 0, 64) . ": ";
                $log['detail'] .= $msg . "\n";
            }
            else
            {
                $log['detail'] .= iconv("UTF-8", "eucKR", $_SERVER['PHP_SELF'] . ": ");
                $log['detail'] .= iconv("UTF-8", "eucKR", $key . " = " . substr($value, 0, 64) . ": ");
                $log['detail'] .= iconv("UTF-8", "eucKR", $msg . "\n");
            }
        }

        if ($policy['log_charset']['UTF-8'] == "TRUE")
        {
            $log['detail'] .= " -> [Method: " . $method . "]\n";
            $log['detail'] .= " -> [Policy: " . $policy_type . "]\n";
            $log['detail'] .= " -> [Pattern: " . $rule . "]\n";
        }
        else
        {
            $log['detail'] .= iconv("UTF-8", "eucKR", " -> [Method: " . $method . "]\n");
            $log['detail'] .= iconv("UTF-8", "eucKR", " -> [Policy: " . $policy_type . "]\n");
            $log['detail'] .= iconv("UTF-8", "eucKR", " -> [Pattern: " . $rule . "]\n");
        }

        referee_logger($log['detail']);
    }

    if ($policy['mode_enforcing'] == "TRUE")
    {
        referee_alert($msg . ": " . $key);
        exit;
    }

    if ($policy['mode_permissive'] == "TRUE")
        return;

    return;
}

function referee_detect_sql_injection($string)
{
    global $_SECURE_POLICY;

    if (!isset($_SECURE_POLICY['POLICY']['SQL_INJECTION']))
        return;

    $policy['sql_injection'] = & $_SECURE_POLICY['POLICY']['SQL_INJECTION'];

    $policy['sql_injection_bool'] = $policy['sql_injection']['BOOL'];
    $policy['sql_injection_bool'] = base64_decode($policy['sql_injection_bool']);

    if ($policy['sql_injection_bool'] == "FALSE")
        return;

    $policy['sql_injection_list'] = $policy['sql_injection']['LIST'];

    foreach ($policy['sql_injection_list'] as $regexp)
    {
        $regexp = base64_decode($regexp);
        $regexp = referee_eregi($regexp, $string);
        if ($regexp)
            return $regexp;
    }

    return FALSE;
}

function referee_detect_xss($string)
{
    global $_SECURE_POLICY;

    if (!isset($_SECURE_POLICY['POLICY']['XSS']))
        return;

    $policy['xss'] = & $_SECURE_POLICY['POLICY']['XSS'];

    $policy['xss_bool'] = $policy['xss']['BOOL'];
    $policy['xss_bool'] = base64_decode($policy['xss_bool']);

    if ($policy['xss_bool'] == "FALSE")
        return;

    $policy['xss_list'] = $policy['xss']['LIST'];

    foreach ($policy['xss_list'] as $regexp)
    {
        $regexp = base64_decode($regexp);
        $regexp = referee_eregi($regexp, $string);
        if ($regexp)
            return $regexp;
    }

    return FALSE;
}

function referee_detect_word($string)
{
    global $_SECURE_POLICY;

    if (!isset($_SECURE_POLICY['POLICY']['WORD']))
        return;

    $policy['word'] = & $_SECURE_POLICY['POLICY']['WORD'];

    $policy['word_bool'] = $policy['word']['BOOL'];
    $policy['word_bool'] = base64_decode($policy['word_bool']);

    if ($policy['word_bool'] == "FALSE")
        return;

    $policy['word_list'] = $policy['word']['LIST'];

    foreach ($policy['word_list'] as $regexp)
    {
        $regexp = base64_decode($regexp);
        $regexp = referee_eregi($regexp, strtoupper($string));
        if ($regexp)
            return $regexp;
    }

    return FALSE;
}

function referee_detect_tag($string)
{
    global $_SECURE_POLICY;

    if (!isset($_SECURE_POLICY['POLICY']['TAG']))
        return;

    $policy['tag'] = & $_SECURE_POLICY['POLICY']['TAG'];

    $policy['tag_bool'] = $policy['tag']['BOOL'];
    $policy['tag_bool'] = base64_decode($policy['tag_bool']);

    if ($policy['tag_bool'] == "FALSE")
        return;

    $policy['tag_list'] = $policy['tag']['LIST'];

    foreach ($policy['tag_list'] as $regexp)
    {
        $regexp = base64_decode($regexp);
        $regexp = referee_eregi($regexp, $string);
        if ($regexp)
            return $regexp;
    }

    return FALSE;
}

function referee_check_ip_policy()
{
    global $_SECURE_POLICY;

    // get client ip
    $check['ip'] = getenv("REMOTE_ADDR");

    if (isset($_SECURE_POLICY['POLICY']['IP']))
        $policy['ip'] = & $_SECURE_POLICY['POLICY']['IP'];

    /* check */
    if (!isset($policy['ip']))
        return;

    $policy['ip_bool'] = base64_decode($policy['ip']['BOOL']);
    $policy['ip_allow'] = base64_decode($policy['ip']['ALLOW']);
    $policy['ip_deny'] = base64_decode($policy['ip']['DENY']);

    if ($policy['ip_bool'] == "FALSE")
        return;

    if (!isset($policy['ip']['LIST']))
        return;

    $policy['ip_list'] = $policy['ip']['LIST'];

    $ip_exists = FALSE;
    foreach ($policy['ip_list'] as $ip_regexp)
    {
        $ip_regexp = base64_decode($ip_regexp);
        // if IP, change from . to \.
        if (referee_eregi($ip_regexp, $check['ip']))
            $ip_exists = TRUE;
    }

    // white list
    if ($policy['ip_allow'] == "TRUE")
    {
        if (!$ip_exists)
            referee_error_handler("IP Policy", "check remote_ip", "IP", stripslashes($check['ip']), "", "Unauthorized access attempts from IP range.");
    }
    else
    // black list
    if ($policy['ip_deny'] == "TRUE")
    {
        if ($ip_exists)
            referee_error_handler("IP정책", "check remote_ip", "IP", stripslashes($check['ip']), "", " Access attempts in the Blocked IP range.");
    }

    return;
}

function referee_check_file_policy()
{
    global $_SECURE_POLICY, $_FILE;

    if (!isset($_SECURE_POLICY['CONFIG']['TARGET']))
        return;

    if (!isset($_SECURE_POLICY['CONFIG']['TARGET']['FILE']))
        return;

    $policy['file_bool'] = base64_decode($_SECURE_POLICY['CONFIG']['TARGET']['FILE']);

    if ($policy['file_bool'] == "FALSE")
        return;

    // no exist file 
    if (!isset($_FILES))
        return;

    // get file policy
    $policy['filename'] = "";
    if (isset($_SECURE_POLICY['POLICY']['FILENAME']))
        $policy['filename'] = & $_SECURE_POLICY['POLICY']['FILENAME'];

    $policy['filetype'] = "";
    if (isset($_SECURE_POLICY['POLICY']['FILETYPE']))
        $policy['filetype'] = & $_SECURE_POLICY['POLICY']['FILETYPE'];

    $policy['filesize'] = "";
    if (isset($_SECURE_POLICY['POLICY']['FILESIZE']))
        $policy['filesize'] = & $_SECURE_POLICY['POLICY']['FILESIZE'];

    /* apply */
    $policy['filename_bool'] = base64_decode($policy['filename']['BOOL']);
    $policy['filename_allow'] = base64_decode($policy['filename']['ALLOW']);
    $policy['filename_deny'] = base64_decode($policy['filename']['DENY']);

    /* apply file type */
    $policy['filetype_bool'] = base64_decode($policy['filetype']['BOOL']);
    $policy['filetype_allow'] = base64_decode($policy['filetype']['ALLOW']);
    $policy['filetype_deny'] = base64_decode($policy['filetype']['DENY']);

    /* apply file size */
    $policy['filesize_bool'] = base64_decode($policy['filesize']['BOOL']);
    $policy['filesize_min_size'] = base64_decode($policy['filesize']['MIN_SIZE']);
    $policy['filesize_max_size'] = base64_decode($policy['filesize']['MAX_SIZE']);

    foreach ($_FILES as $var => $fileinfo)
    {
        $check['filename'] = $fileinfo['name'];
        $check['filevar'] = $var;
        $check['filetype'] = array_pop(explode('.', $fileinfo['name']));
        $check['filesize'] = $fileinfo['size'];

        // check file upload
        if ($fileinfo['error'])
            return;

        // apply file name policy
        if ($policy['filename_bool'] == "TRUE")
        {

            if (isset($policy['filename']['LIST']))
            {
                $policy['filename_list'] = $policy['filename']['LIST'];

                $filename_exists = FALSE;
                foreach ($policy['filename_list'] as $filename)
                {
                    $filename_regexp = base64_decode($filename);
                    if (referee_eregi($filename_regexp, $check['filename']))
                        $filename_exists = TRUE;
                }
            }

            // White list
            if ($policy['filename_allow'] == "TRUE")
            {
                if (!$filename_exists)
                    referee_error_handler("File Policy", "check filename", "FILE", $check['filevar'], $check['filename'], "Filename was not allowed.");
            }
            else
            // White list
            if ($policy['filename_deny'] == "TRUE")
            {
                if ($filename_exists)
                    referee_error_handler("File Policy", "check filename", "FILE", $check['filevar'], $check['filename'], "Filename was blocked.");
            }
        }

        // file execution
        if ($policy['filetype_bool'] == "TRUE")
        {

            if (isset($policy['filetype']['LIST']))
            {
                $policy['filetype_list'] = $policy['filetype']['LIST'];

                $filetype_exists = FALSE;
                foreach ($policy['filetype_list'] as $filetype)
                {
                    $filetype_regexp = base64_decode($filetype);
                    if (referee_eregi($filetype_regexp, $check['filetype']))
                        $filetype_exists = TRUE;
                }
            }

            // White list
            if ($policy['filetype_allow'] == "TRUE")
            {
                if (!$filetype_exists)
                    referee_error_handler("File Policy", "check filetype", "FILE", $check['filevar'], $check['filename'], "File execution was not allowed.");
            }
            else
            // White list
            if ($policy['filetype_deny'] == "TRUE")
            {
                if ($filetype_exists)
                    referee_error_handler("File Policy", "check filetype", "FILE", $check['filevar'], $check['filename'], "File execution was blocked.");
            }
        }

        // file size
        if ($policy['filesize_bool'] == "TRUE")
        {

            // min
            if ($check['filesize'] < $policy['filesize_min_size'])
                referee_error_handler("File Policy", ">=" . $policy['filesize_min_size'], "FILE", $check['filevar'], $check['filesize'], "File is too small.");

            // max
            if ($check['filesize'] > $policy['filesize_max_size'])
                referee_error_handler("File Policy", "<=" . $policy['filesize_max_size'], "FILE", $check['filevar'], $check['filesize'], "File is too large.");
        }
    }

    return;
}

function referee_check_site_policy()
{
    global $_SECURE_POLICY, $_SERVER;

    /* site policy */
    if (!isset($_SECURE_POLICY['CONFIG']['SITE']['BOOL']))
        return;

    // get site policy
    $policy['site'] = & $_SECURE_POLICY['CONFIG']['SITE']['BOOL'];
    $policy['site'] = base64_decode($policy['site']);

    // apply
    if ($policy['site'] == "FALSE")
        referee_error_handler("Site Policy", "check site-policy", "SITE", "Blocked", "", "Site blocked.");

    return;
}

function referee_check_dos_policy()
{
    global $_SECURE_POLICY, $_SERVER, $mysqli;

    // get dos policy
    $policy['dos'] = "";
    if (isset($_SECURE_POLICY['POLICY']['DOS']))
        $policy['dos'] = & $_SECURE_POLICY['POLICY']['DOS'];

    /* apply */
    $policy['dos_bool'] = base64_decode($policy['dos']['BOOL']);
    $policy['dos_peroid'] = base64_decode($policy['dos']['PERIOD']);
    $policy['dos_hurdle'] = base64_decode($policy['dos']['HURDLE']);

    if ($policy['dos_bool'] === "FALSE")
        return;

    // Log current request

    $nowsec = time();
    $rip = $_SERVER['REMOTE_ADDR'];
    $wanted = $_SERVER['REQUEST_URI'];

    // insert new access
    save_page_access("recent", $nowsec, $rip, $wanted);

    // How many requests in the test time period
    $keeptime = intval($policy['dos_peroid']);
    $hurdle = intval($policy['dos_hurdle']);

    $nn = $nowsec - $keeptime;
    $balloon = get_access_count($rip, $nn);

    // $hurdle pages per $keeptime seconds - aggressive!
    if ($balloon > $hurdle)
    {
        save_page_access("warned", $nowsec, $rip, $wanted);

        // Keep me waiting!
        sleep(10);

        // apply
        referee_error_handler("DOS Policy", "check dos-policy", "DOS", "Blocked", "", "Page blocked(DOS).");
    }

    return;
}

function save_page_access($table_name, $nowsec, $rip, $wanted)
{
    global $mysqli;
    $stmt = $mysqli->prepare("INSERT INTO " . $table_name . " (
                                    tstamp,
                                    remoteaddr,
                                    calledfor
                                    )
                                    VALUES (
                                    ?,?,?
                                    )");

    $stmt->bind_param("sss", $nowsec, $rip, $wanted);
    $stmt->execute();
    $stmt->close();
}

function get_access_count($rip, $tstamp)
{
    global $mysqli;
    
    $type = 1;
    $stmt = $mysqli->prepare("SELECT 
                    COUNT(tstamp)
                    FROM recent
                    WHERE 
                    type = ? AND 
                    remoteaddr = ? AND
                    tstamp > ?");
    $stmt->bind_param("isi", $type, $rip, $tstamp);
    $stmt->execute();
    $stmt->bind_result($count);

    $row = array();
    while ($stmt->fetch())
    {
        $row = array('balloon' => $count);
    }
    $stmt->close();

    return intval($row['balloon']);
}

function referee_check_login_policy()
{
    global $_SECURE_POLICY;
    
    // get dos policy
    $policy['login'] = "";
    if (isset($_SECURE_POLICY['POLICY']['LOGIN']))
        $policy['login'] = & $_SECURE_POLICY['POLICY']['LOGIN'];

    /* apply */
    $policy['login_bool'] = base64_decode($policy['login']['BOOL']);
    $policy['peroid'] = base64_decode($policy['login']['PERIOD']);
    $policy['secadmin_hurdle'] = base64_decode($policy['login']['SECADMIN']);
    $policy['admin_hurdle'] = base64_decode($policy['login']['ADMIN']);
    $policy['user_hurdle'] = base64_decode($policy['login']['USER']);

    if ($policy['login_bool'] === "FALSE")
        return TRUE;
    
    $rip = $_SERVER['REMOTE_ADDR'];
    $wanted = $_SERVER['REQUEST_URI'];

    // How many requests in the test time period
    $nowsec = time();
    $keeptime = intval($policy['peroid']);
    $secadmin_hurdle = intval($policy['secadmin_hurdle']);
    $admin_hurdle = intval($policy['admin_hurdle']);
    $user_hurdle = intval($policy['user_hurdle']);

    $nn = $nowsec - $keeptime;
    
    // check security admin login    
    $balloon = get_login_count($rip, $nn, 2);
    // $hurdle pages per $keeptime seconds - aggressive!
    if ($balloon >= $secadmin_hurdle)
    {
        // apply
        referee_error_handler("Login Policy", "check security module login", "Login", "Blocked", "", "Page blocked(Login).\r\nYou have exceeded the login counts.\r\nPlease try again in a few minutes.");
    }
    
    // check security admin page login    
    $balloon = get_login_count($rip, $nn, 3);
    // $hurdle pages per $keeptime seconds - aggressive!
    if ($balloon >= $admin_hurdle)
    {
        // apply
        referee_error_handler("Login Policy", "check admin panel login", "Login", "Blocked", "", "Page blocked(Login).\r\nYou have exceeded the login counts.\r\nPlease try again in a few minutes.");
    }
    
    // check security admin page login    
    $balloon = get_login_count($rip, $nn, 4);
    // $hurdle pages per $keeptime seconds - aggressive!
    if ($balloon >= $user_hurdle)
    {
        // apply
        referee_error_handler("Login Policy", "check user login", "Login", "Blocked", "", "Page blocked(Login).\r\nYou have exceeded the login counts.\r\nPlease try again in a few minutes.");
    }
}

function get_login_count($rip, $tstamp, $type)
{
    global $mysqli;
    $stmt = $mysqli->prepare("SELECT 
                    COUNT(tstamp)
                    FROM recent
                    WHERE 
                    type = ? AND 
                    remoteaddr = ? AND
                    tstamp > ?");
    $stmt->bind_param("isi", $type, $rip, $tstamp);
    $stmt->execute();
    $stmt->bind_result($count);

    $row = array();
    while ($stmt->fetch())
    {
        $row = array('balloon' => $count);
    }
    $stmt->close();

    return intval($row['balloon']);
}

function referee_check_basic_policy()
{
    global $_SECURE_POLICY, $_SERVER;

    // get page names
    $check['page_name'] = getenv("SCRIPT_NAME");
    $check['page_name'] = referee_delete_directory_traverse($check['page_name']);

    if (isset($_SECURE_POLICY['ADVANCE']['PAGE'][$check['page_name']]))
        $policy['page'] = & $_SECURE_POLICY['ADVANCE']['PAGE'][$check['page_name']];

    /* apply policy */
    if (isset($policy['page']))
        return;

    /* GET method */
    if (isset($_SECURE_POLICY['CONFIG']['TARGET']))
    {

        if (isset($_SECURE_POLICY['CONFIG']['TARGET']['GET']))
        {

            $policy['get_bool'] = base64_decode($_SECURE_POLICY['CONFIG']['TARGET']['GET']);

            if ($policy['get_bool'] == "TRUE")
            {

                foreach ($_GET as $key => $value)
                {
                    // SQL_INJECTION
                    $pattern = referee_detect_sql_injection($value);
                    if ($pattern)
                        referee_error_handler("Main Policy", $pattern, "GET", $key, $value, "SQL_Injection attack detected.");

                    // XSS 
                    $pattern = referee_detect_xss($value);
                    if ($pattern)
                        referee_error_handler("Main Policy", $pattern, "GET", $key, $value, "XSS attack detected.");

                    // WORD 

                    $pattern = referee_detect_word($value);
                    if ($pattern)
                        referee_error_handler("Main Policy", $pattern, "GET", $key, $value, "Bad word detected.");

                    // TAG 
                    $pattern = referee_detect_tag($value);
                    if ($pattern)
                        referee_error_handler("Main Policy", $pattern, "GET", $key, $value, "Bad TAG detected.");
                }
            }
        }
    }

    /* POST */
    if (isset($_SECURE_POLICY['CONFIG']['TARGET']))
    {

        if (isset($_SECURE_POLICY['CONFIG']['TARGET']['POST']))
        {

            $policy['post_bool'] = base64_decode($_SECURE_POLICY['CONFIG']['TARGET']['POST']);

            if ($policy['post_bool'] == "TRUE")
            {

                foreach ($_POST as $key => $value)
                {
                    // SQL_INJECTION 
                    $pattern = referee_detect_sql_injection($value);
                    if ($pattern)
                        referee_error_handler("Main Policy", $pattern, "POST", $key, $value, "SQL_Injection attack detected.");

                    // XSS 
                    $pattern = referee_detect_xss($value);
                    if ($pattern)
                        referee_error_handler("Main Policy", $pattern, "POST", $key, $value, "XSS attack detected.");

                    // WORD 
                    $pattern = referee_detect_word($value);
                    if ($pattern)
                        referee_error_handler("Main Policy", $pattern, "POST", $key, $value, "Bad word detected.");

                    // TAG 
                    $pattern = referee_detect_tag($value);
                    if ($pattern)
                        referee_error_handler("Main Policy", $pattern, "POST", $key, $value, "Bad TAG detected.");
                }
            }
        }
    }

    return;
}

function referee_check_cookie_policy()
{
    global $_SECURE_POLICY, $_COOKIE;

    /* COOKIE */
    if (!isset($_SECURE_POLICY['CONFIG']['TARGET']))
        return;

    if (!isset($_SECURE_POLICY['CONFIG']['TARGET']['COOKIE']))
        return;

    $policy['cookie_bool'] = base64_decode($_SECURE_POLICY['CONFIG']['TARGET']['COOKIE']);

    if ($policy['cookie_bool'] == "FALSE")
        return;

    foreach ($_COOKIE as $key => $value)
    {
        // SQL_INJECTION 
        $pattern = referee_detect_sql_injection($value);
        if ($pattern)
            referee_error_handler("Main Policy", $pattern, "COOKIE", $key, $value, "SQL_Injection attack detected.");

        // XSS 
        $pattern = referee_detect_xss($value);
        if ($pattern)
            referee_error_handler("Main Policy", $pattern, "COOKIE", $key, $value, "XSS attack detected.");

        // WORD 
        $pattern = referee_detect_word($value);
        if ($pattern)
            referee_error_handler("Main Policy", $pattern, "COOKIE", $key, $value, "Bad word detected.");

        // TAG 
        $pattern = referee_detect_tag($value);
        if ($pattern)
            referee_error_handler("Main Policy", $pattern, "COOKIE", $key, $value, "Bad TAG detected.");
    }

    return;
}

function referee_check_advance_policy()
{
    global $_SECURE_POLICY;

    // get page name
    $check['page_name'] = getenv("SCRIPT_NAME");
    $check['page_name'] = referee_delete_directory_traverse($check['page_name']);

    if (isset($_SECURE_POLICY['ADVANCE']['PAGE'][$check['page_name']]))
        $policy['page'] = & $_SECURE_POLICY['ADVANCE']['PAGE'][$check['page_name']];

    /* apply */
    if (!isset($policy['page']))
        return;

    /* apply arrow/block page policy */
    $policy['page_bool'] = base64_decode($policy['page']['BOOL']);
    $policy['page_allow'] = base64_decode($policy['page']['ALLOW']);
    $policy['page_deny'] = base64_decode($policy['page']['DENY']);

    // if FALSE, block
    if ($policy['page_bool'] == "FALSE")
        referee_error_handler("Advanced Policy", "check page", "PAGE", $check['page_name'], "", "Tried to access blocked page.");

    if (!isset($_SECURE_POLICY['CONFIG']['TARGET']))
        return;

    /* GET */
    if (isset($_SECURE_POLICY['CONFIG']['TARGET']['GET']))
    {

        $policy['get_bool'] = base64_decode($_SECURE_POLICY['CONFIG']['TARGET']['GET']);

        if ($policy['get_bool'] == "TRUE")
        {

            foreach ($_GET as $key => $value)
            {
                // White list
                if ($policy['page_allow'] == "TRUE")
                {
                    if (!isset($policy['page']['LIST'][$key]))
                        referee_error_handler("Advanced Policy", "check white-list", "GET", $key, "", "Tried to access variable that is not allowed.");
                }
                else
                // White list
                if ($policy['page_deny'] == "TRUE")
                {
                    if (isset($policy['page']['LIST'][$key]))
                        referee_error_handler("Advanced Policy", "check black-list", "GET", $key, "", "Tried to access variable that is blocked.");
                }

                // get variable
                if (!isset($policy['page']['LIST'][$key]))
                    continue;

                $policy['var'] = $policy['page']['LIST'][$key];

                // GET method 
                $policy['get_bool'] = base64_decode($policy['var']['METHOD']['GET']);
                if ($policy['get_bool'] == "FALSE")
                    referee_error_handler("Advanced Policy", "check method", "GET", $key, "", "Tried to access method that is blocked.");

                /* apply parameter value */
                $policy['format'] = "";
                if (isset($policy['var']['FORMAT']))
                    $policy['format'] = $policy['var']['FORMAT'];

                $policy['format'] = trim($policy['format']);

                if (strlen($policy['format']) > 0)
                {
                    $policy['format'] = base64_decode($policy['format']);

                    if (!referee_eregi($policy['format'], $value))
                        referee_error_handler("Advanced Policy", $policy['format'], "GET", $key, $value, "The parameter value is not allowed.");
                }

                /* length */
                $submit[$key]['length'] = strlen($value);

                // min
                if (isset($policy['var']['MIN_LENGTH']))
                {
                    $policy['min_length'] = base64_decode($policy['var']['MIN_LENGTH']);

                    if ($submit[$key]['length'] < $policy['min_length'])
                        referee_error_handler("Advanced Policy", ">=" . $policy['min_length'], "GET", $key, $value, "The parameter value is too short.");
                }
                // max
                if (isset($policy['var']['MAX_LENGTH']))
                {
                    $policy['max_length'] = base64_decode($policy['var']['MAX_LENGTH']);

                    if ($submit[$key]['length'] > $policy['max_length'])
                        referee_error_handler("Advanced Policy", "<=" . $policy['max_length'], "GET", $key, $value, "The parameter value is too long.");
                }

                // SQL_INJECTION 
                $policy['sql_injection_bool'] = "FALSE";
                if (isset($policy['var']['TARGET']['SQL_INJECTION']))
                    $policy['sql_injection_bool'] = base64_decode($policy['var']['TARGET']['SQL_INJECTION']);

                if ($policy['sql_injection_bool'] == "TRUE")
                {
                    $pattern = referee_detect_sql_injection($value);
                    if ($pattern)
                        referee_error_handler("Advanced Policy", $pattern, "GET", $key, $value, "SQL_Injection attack detected.");
                }

                // XSS 
                $policy['xss'] = "FALSE";
                if (isset($policy['var']['TARGET']['XSS']))
                    $policy['xss'] = base64_decode($policy['var']['TARGET']['XSS']);

                if ($policy['xss'] == "TRUE")
                {
                    referee_detect_xss($value);
                    if ($pattern)
                        referee_error_handler("Advanced Policy", $pattern, "GET", $key, $value, "XSS attack detected.");
                }

                // WORD 
                $policy['word'] = "FALSE";
                if (isset($policy['var']['TARGET']['WORD']))
                    $policy['word'] = base64_decode($policy['var']['TARGET']['WORD']);

                if ($policy['word'] == "TRUE")
                {
                    $pattern = referee_detect_word($value);
                    if ($pattern)
                        referee_error_handler("Advanced Policy", $pattern, "GET", $key, $value, "Bad word detected.");
                }

                // TAG 
                $policy['tag'] = "FALSE";
                if (isset($policy['var']['TARGET']['TAG']))
                    $policy['tag'] = base64_decode($policy['var']['TARGET']['TAG']);

                if ($policy['tag'] == "TRUE")
                {
                    $pattern = referee_detect_tag($value);
                    if ($pattern)
                        referee_error_handler("Advanced Policy", $pattern, "GET", $key, $value, "Bad TAG detected.");
                }
            }
        }
    }

    /* POST */
    if (isset($_SECURE_POLICY['CONFIG']['TARGET']['POST']))
    {

        $policy['post_bool'] = base64_decode($_SECURE_POLICY['CONFIG']['TARGET']['POST']);

        if ($policy['post_bool'] == "TRUE")
        {

            foreach ($_POST as $key => $value)
            {

                // White list
                if ($policy['page_allow'] == "TRUE")
                {
                    if (!isset($policy['page']['LIST'][$key]))
                        referee_error_handler("Advanced Policy", "check white-list", "POST", $key, "", "Tried to access variable that is not allowed.");
                }
                else
                // White list
                if ($policy['page_deny'] == "TRUE")
                {
                    if (isset($policy['page']['LIST'][$key]))
                        referee_error_handler("Advanced Policy", "check black-list", "POST", $key, "", "Tried to access variable that is blocked.");
                }

                // get variable
                if (!isset($policy['page']['LIST'][$key]))
                    continue;

                $policy['var'] = $policy['page']['LIST'][$key];

                // POST 
                $policy['post_bool'] = base64_decode($policy['var']['METHOD']['POST']);
                if ($policy['post_bool'] == "FALSE")
                    referee_error_handler("Advanced Policy", "check method", "POST", $key, "", "Tried to access method that is blocked.");

                /* apply */
                $policy['format'] = "";
                if ($policy['var']['FORMAT'])
                    $policy['format'] = $policy['var']['FORMAT'];

                $policy['format'] = trim($policy['format']);

                if (strlen($policy['format']) > 0)
                {
                    $policy['format'] = base64_decode($policy['format']);

                    if ($value !== "")
                    {
                        if (!referee_eregi($policy['format'], $value))
                            referee_error_handler("Advanced Policy", $policy['format'], "POST", $key, $value, "The parameter value is not allowed.");
                    }
                }

                /* length */
                $submit[$key]['length'] = strlen($value);

                // min
                if (isset($policy['var']['MIN_LENGTH']))
                {
                    $policy['min_length'] = base64_decode($policy['var']['MIN_LENGTH']);

                    if ($submit[$key]['length'] < $policy['min_length'])
                        referee_error_handler("Advanced Policy", ">=" . $policy['min_length'], "POST", $key, $value, "The parameter value is too short.");
                }
                // max
                if (isset($policy['var']['MAX_LENGTH']))
                {
                    $policy['max_length'] = base64_decode($policy['var']['MAX_LENGTH']);

                    if ($submit[$key]['length'] > $policy['max_length'])
                        referee_error_handler("Advanced Policy", "<=" . $policy['max_length'], "POST", $key, $value, "The parameter value is too long.");
                }

                // SQL_INJECTION 
                $policy['sql_injection_bool'] = "FALSE";
                if (isset($policy['var']['TARGET']['SQL_INJECTION']))
                    $policy['sql_injection_bool'] = base64_decode($policy['var']['TARGET']['SQL_INJECTION']);

                if ($policy['sql_injection_bool'] == "TRUE")
                {
                    $pattern = referee_detect_sql_injection($value);
                    if ($pattern)
                        referee_error_handler("Advanced Policy", $pattern, "POST", $key, $value, "SQL_Injection attack detected.");
                }

                // XSS 
                $policy['xss'] = "FALSE";
                if (isset($policy['var']['TARGET']['XSS']))
                    $policy['xss'] = base64_decode($policy['var']['TARGET']['XSS']);

                if ($policy['xss'] == "TRUE")
                {
                    referee_detect_xss($value);
                    if ($pattern)
                        referee_error_handler("Advanced Policy", $pattern, "POST", $key, $value, "XSS attack detected.");
                }

                // WORD 
                $policy['word'] = "FALSE";
                if (isset($policy['var']['TARGET']['WORD']))
                    $policy['word'] = base64_decode($policy['var']['TARGET']['WORD']);

                if ($policy['word'] == "TRUE")
                {
                    $pattern = referee_detect_word($value);
                    if ($pattern)
                        referee_error_handler("Advanced Policy", $pattern, "POST", $key, $value, "Bad word detected.");
                }

                // TAG 
                $policy['tag'] = "FALSE";
                if (isset($policy['var']['TARGET']['TAG']))
                    $policy['tag'] = base64_decode($policy['var']['TARGET']['TAG']);

                if ($policy['tag'] == "TRUE")
                {
                    $pattern = referee_detect_tag($value);
                    if ($pattern)
                        referee_error_handler("Advanced Policy", $pattern, "POST", $key, $value, "bad TAG detected.");
                }
            }
        }
    }

    return;
}

/* Secure Referee main function */

function referee_main()
{
    // site policy access
    referee_check_site_policy();

    // dos policy check
    referee_check_dos_policy();
    
    // login policy check
    referee_check_login_policy();

    // IP access
    referee_check_ip_policy();

    // advanced policy(per page)
    referee_check_advance_policy();

    // main policy
    referee_check_basic_policy();

    // COOKIE
    referee_check_cookie_policy();

    // FILE
    referee_check_file_policy();


    return;
}

if (!isset($_SECURE_POLICY['CONFIG']['MODE']['ENFORCING']))
    return;

/* if Secure DISABLED, exit */
$policy['disabled'] = & $_SECURE_POLICY['CONFIG']['MODE']['DISABLED'];
$policy['disabled'] = base64_decode($policy['disabled']);

if ($policy['disabled'] == "TRUE")
    return;

/* call main function */
referee_main();

/* End of referee.php */
?>
