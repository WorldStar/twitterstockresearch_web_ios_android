<?php

include_once("admin_lib.php");
check_authorized();

init_page();

if (isset($_GET) && isset($_GET['mode']))
    $page['mode'] = $_GET['mode'];


if ($page['mode'] == "CONFIG_BASIC")
{
    clear_submit();

    if (isset($_POST['admin_module_name']))
        $submit['admin_module_name'] = $_POST['admin_module_name'];

    if (isset($_POST['config_mode']))
        $submit['config_mode'] = $_POST['config_mode'];

    if (isset($_POST['config_alert']))
        $submit['config_alert'] = $_POST['config_alert'];

    $check['submit'] = array("admin_module_name", "config_mode", "config_alert",);
    check_submit($check['submit']);

    $check['module_name_length'] = strlen($submit['admin_module_name']);
    check_length("Module Name", $check['module_name_length'], 4, 64);

    $submit['admin_module_name'] = trim($submit['admin_module_name']);

    if (get_magic_quotes_gpc())
        $submit['admin_module_name'] = stripslashes($submit['admin_module_name']);

    $_SECURE_POLICY['CONFIG']['ADMIN']['MODULE_NAME'] = base64_encode($submit['admin_module_name']);

    $_SECURE_POLICY['CONFIG']['MODE']['ENFORCING'] = base64_encode("FALSE");
    $_SECURE_POLICY['CONFIG']['MODE']['PERMISSIVE'] = base64_encode("FALSE");
    $_SECURE_POLICY['CONFIG']['MODE']['DISABLED'] = base64_encode("FALSE");

    if ($submit['config_mode'] == "enforcing")
        $_SECURE_POLICY['CONFIG']['MODE']['ENFORCING'] = base64_encode("TRUE");

    if ($submit['config_mode'] == "permissive")
        $_SECURE_POLICY['CONFIG']['MODE']['PERMISSIVE'] = base64_encode("TRUE");

    if ($submit['config_mode'] == "disabled")
        $_SECURE_POLICY['CONFIG']['MODE']['DISABLED'] = base64_encode("TRUE");

    $_SECURE_POLICY['CONFIG']['ALERT']['ALERT'] = base64_encode("FALSE");
    $_SECURE_POLICY['CONFIG']['ALERT']['MESSAGE'] = base64_encode("FALSE");
    $_SECURE_POLICY['CONFIG']['ALERT']['STEALTH'] = base64_encode("FALSE");

    if ($submit['config_alert'] == "alert")
        $_SECURE_POLICY['CONFIG']['ALERT']['ALERT'] = base64_encode("TRUE");

    if ($submit['config_alert'] == "message")
        $_SECURE_POLICY['CONFIG']['ALERT']['MESSAGE'] = base64_encode("TRUE");

    if ($submit['config_alert'] == "stealth")
        $_SECURE_POLICY['CONFIG']['ALERT']['STEALTH'] = base64_encode("TRUE");

    write_policy();

    html_msgmove("Configure info modified.", "admin_config.php#basic");

    exit;
}

if ($page['mode'] == "CONFIG_SITE")
{
    clear_submit();

    if (isset($_POST['config_site_bool']))
        $submit['config_site_bool'] = $_POST['config_site_bool'];

    $check['submit'] = array("config_site_bool",);
    check_submit($check['submit']);

    if ($submit['config_site_bool'] == "true")
        $_SECURE_POLICY['CONFIG']['SITE']['BOOL'] = base64_encode("TRUE");
    else
        $_SECURE_POLICY['CONFIG']['SITE']['BOOL'] = base64_encode("FALSE");

    write_policy();

    html_msgmove("Configure info modified.", "admin_config.php#site");

    exit;
}

if ($page['mode'] == "CONFIG_TARGET")
{
    clear_submit();

    if (isset($_POST['target_get']))
        $submit['target_get'] = $_POST['target_get'];

    if (isset($_POST['target_post']))
        $submit['target_post'] = $_POST['target_post'];

    if (isset($_POST['target_cookie']))
        $submit['target_cookie'] = $_POST['target_cookie'];

    if (isset($_POST['target_file']))
        $submit['target_file'] = $_POST['target_file'];

    $check['submit'] = array("target_get", "target_post",
        "target_cookie", "target_file",);
    check_submit($check['submit']);

    $_SECURE_POLICY['CONFIG']['TARGET']['GET'] = base64_encode("FALSE");
    $_SECURE_POLICY['CONFIG']['TARGET']['POST'] = base64_encode("FALSE");
    $_SECURE_POLICY['CONFIG']['TARGET']['FILE'] = base64_encode("FALSE");
    $_SECURE_POLICY['CONFIG']['TARGET']['COOKIE'] = base64_encode("FALSE");

    if ($submit['target_get'] == "true")
        $_SECURE_POLICY['CONFIG']['TARGET']['GET'] = base64_encode("TRUE");

    if ($submit['target_post'] == "true")
        $_SECURE_POLICY['CONFIG']['TARGET']['POST'] = base64_encode("TRUE");

    if ($submit['target_cookie'] == "true")
        $_SECURE_POLICY['CONFIG']['TARGET']['COOKIE'] = base64_encode("TRUE");

    if ($submit['target_file'] == "true")
        $_SECURE_POLICY['CONFIG']['TARGET']['FILE'] = base64_encode("TRUE");

    write_policy();

    html_msgmove("Configure info modified.", "admin_config.php#target");

    exit;
}
?>
