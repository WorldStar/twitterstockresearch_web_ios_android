<?php
include_once("admin_lib.php");

if (isset($_SECURE_POLICY['CONFIG']['ADMIN']['MODULE_NAME']))
    $policy['title'] = $_SECURE_POLICY['CONFIG']['ADMIN']['MODULE_NAME'];
else
    $policy['title'] = base64_encode(SECURE_BASE_MODULE_NAME);

$print['title'] = base64_decode($policy['title']);
?>
<title>Secure Module</title>
