<?php

include_once("admin_lib.php");
check_authorized();

// unset session
session_unset();

html_msgmove("Administrators have been logged out.", "admin_login.php");

exit;
?>
