<?php
include_once("admin_lib.php");
check_authorized();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" HREF="style.css" type="text/css" title="style">
    </head>
    <body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="#D0D0D0">
        <?php include_once("admin_title.php"); ?>
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
            <tr> 
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tr bgcolor="#f3f3f3">
                            <td width="100%" height="80" colspan="2">
                                <?php include_once("admin_top.php"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="160" bgcolor="#f3f3f3">
                                <?php include_once("admin_menu.php"); ?>
                            </td>
                            <td width="100%" bgcolor="#f3f3f3" valign="top">
                                <br><br>
                                <div id="main_column" class="clear">
                                    <table width="800" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tr valign="top">
                                            <td width="100%">
                                                <table width="100%" height="100%" cellspacing="20" cellpadding="0" border="0" align="center">
                                                    <tr valign="top">
                                                        <td width="100%">

                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Backup Policy</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>

                                                            <br>
                                                            <table width="790" cellspacing="10" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="100%" style="line-height:160%" nowrap>
                                                                        <b>Note: When you back up your files often, it will help.</b><br>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <?php
                                                            $print['policy_filename'] = "policy.php";
                                                            $print['policy_down_filename'] = date("Ymd") . "-policy.bak";

                                                            $print['policy_filesize'] = number_format(filesize($print['policy_filename']));
                                                            $print['policy_filemtime'] = date("F d Y H:i:s", (filemtime($print['policy_filename'])));
                                                            ?>
                                                            <table cellspacing="2" cellpadding="5" border="0" align="center">
                                                                <tr>
                                                                    <th width="150" height="30" bgcolor="#ddf3fa" align="right">Policy File Name</th>
                                                                    <td width="475">
                                                                        <?php echo $print['policy_filename'] ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th width="150" height="30" bgcolor="#ddf3fa" align="right">File Size</th>
                                                                    
                                                                </tr>
                                                                <tr>
                                                                    <th width="150" height="30" bgcolor="#ddf3fa" align="right">Latest Modify</th>
                                                                    <td width="475">
                                                                        <?php echo $print['policy_filemtime'] ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                                                            <tr>
                                                                                <td bgcolor="#FFFFFF" style="line-height:100%" nowrap>
                                                                                    <div class="statistics-box inventory">
                                                                                        <div class="statistics-body">
                                                                                            Download as the name "Year.Month.Day-policy.bak".<br>
                                                                                            (ex. 20140328-policy.bak)
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table width="475" height="50" cellspacing="0" cellpadding="0" border="0">
                                                                <tr valign="top">
                                                                    <td width="175">&nbsp;</td>
                                                                    <td width="300">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <a href="admin_download.php?filename=<?php echo $print['policy_down_filename'] ?>&filepath=<?php echo $print['policy_filename'] ?>"">
                                                                                <input type="button" class="cm-confirm cm-process-items" value="Download">
                                                                            </a>
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="history.back(-1);
                                                                                    return false;">
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr bgcolor="#A0A0A0">
                            <td width="100%" height="50" colspan="2" align="center">
                                <?php include_once("admin_bottom.php"); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
