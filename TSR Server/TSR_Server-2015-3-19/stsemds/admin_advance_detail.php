<?php
include_once("admin_lib.php");
check_authorized();

init_page();

if (isset($_GET) && isset($_GET['mode']))
    $page['mode'] = $_GET['mode'];

if (!isset($_GET['page_name']))
    html_msgback("Page name is not entered..");

$submit['page_name'] = $_GET['page_name'];
$submit['return_pos'] = "";

if (isset($_GET['pos']))
    $submit['return_pos'] = $_GET['pos'];

if (!isset($_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']]))
    html_msgback("Invalid page name.");

$policy['page'] = & $_SECURE_POLICY['ADVANCE']['PAGE'][$submit['page_name']];

$print['return_pos'] = $submit['return_pos'];
$print['page_name'] = $submit['page_name'];

$print['page_bool_true_check'] = "checked";
$print['page_bool_false_check'] = "";
$print['page_base_allow_check'] = "checked";
$print['page_base_deny_check'] = "";

if (base64_decode($policy['page']['BOOL']) == "TRUE")
    $print['page_bool_true_check'] = "checked";
else
    $print['page_bool_false_check'] = "checked";

if (base64_decode($policy['page']['ALLOW']) == "TRUE")
    $print['page_base_allow_check'] = "checked";
else
if (base64_decode($policy['page']['DENY']) == "TRUE")
    $print['page_base_deny_check'] = "checked";

$error_msg['modify_confirm'] = "Do you want to modify?";
$error_msg['delete_confirm'] = "Do you want to delete reallys?";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="StyleSheet" HREF="style.css" type="text/css" title="style">
        <script language=javascript>
            <!--
          function page_move()
            {
                location.href = 'admin_advance.php#<?php echo $print['return_pos'] ?>';
            }

            function page_view2(url)
            {
                window.open(url, "page_view");
            }

            function var_modify_confirm()
            {
                ret = confirm("<?php echo $error_msg['modify_confirm'] ?>");
                return ret;
            }

            function var_delete_submit(var_name)
            {
                ret = confirm("<?php echo $error_msg['delete_confirm'] ?>");
                if (!ret)
                    return;
                location.href = 'admin_advance_submit.php?mode=VAR_DELETE&page_name=<?php echo $print['page_name'] ?>&var_name=' + var_name;
            }
//-->
        </script>
    </head>
    <body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" bgcolor="#D0D0D0">
        <?php include_once("admin_title.php"); ?>
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
            <tr> 
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tr bgcolor="#CACACA">
                            <td width="100%" height="80" colspan="2">
                                <?php include_once("admin_top.php"); ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="160" bgcolor="#f3f3f3">
                                <?php include_once("admin_menu.php"); ?>
                            </td>
                            <td width="100%" bgcolor="#f3f3f3" valign="top">
                                <br><br>
                                <div id="main_column" class="clear">
                                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                        <tr valign="top">
                                            <td width="100%">
                                                <table width="100%" height="100%" cellspacing="20" cellpadding="0" border="0" align="center">
                                                    <tr valign="top">
                                                        <td width="100%">
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Page Management Settings</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>

                                                            <br>
                                                            <table width="790" cellspacing="10" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="100%" style="line-height:160%" nowrap>
                                                                        <b>Note: Set the details policy per page.</b><br>
                                                                    </td>
                                                                </tr>
                                                            </table>


                                                            <table width="780" height="25" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td align="right">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="&nbsp;To List&nbsp;" onclick="page_move();">
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>


                                                            <table width="800" cellspacing="2" cellpadding="5" border="0" align="center">
                                                                <form name="page_modify_form" action="admin_advance_submit.php?mode=PAGE_MODIFY_DETAIL" method="post">
                                                                    <tr>
                                                                        <th height="30" rowspan="4" bgcolor="#ddf3fa" align="right">Page Management</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="100" height="30" bgcolor="#ddf3fa">Page Name</th>
                                                                        <td width="300">
                                                                            <input type="hidden" name="page_name" value="<?php echo $print['page_name'] ?>">
                                                                            <table width="300" height="25" cellspacing="0" cellpadding="2" border="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="text" size="50" class="input-text-disabled" value="<?php echo $print['page_name'] ?>" disabled>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td width="200">
                                                                            <span class="cm-button-main cm-process-items">
                                                                                <input type="button" class="cm-confirm cm-process-items" value="View Page" onclick="page_view2('<?php echo $print['page_name'] ?>');">
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="100" height="30" bgcolor="#ddf3fa">Apply Status</th>
                                                                        <td width="480" colspan="2">
                                                                            <input type="radio" name="page_bool" value="true" <?php echo $print['page_bool_true_check'] ?>>YES (default)
                                                                            <input type="radio" name="page_bool" value="allow" <?php echo $print['page_bool_false_check'] ?>>NO
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="100" height="30" bgcolor="#ddf3fa">Apply Base</th>
                                                                        <td width="480" colspan="2">
                                                                            <input type="radio" name="page_base" value="allow" <?php echo $print['page_base_allow_check'] ?>>White List (default)
                                                                            <input type="radio" name="page_base" value="deny" <?php echo $print['page_base_deny_check'] ?>>Black List
                                                                        </td>
                                                                    </tr>
                                                            </table>
                                                            <br>
                                                            <table width="475" height="35" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr valign="top">
                                                                    <td width="175">&nbsp;</td>
                                                                    <td width="300">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="submit" value="Confirm">
                                                                        </span>
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="page_move();">
                                                                        </span>
                                                                        <br>
                                                                    </td>
                                                                </tr>
                                                                </form>
                                                            </table>
                                                            <br>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="5"></td>
                                                                    <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                        <p class="mainbox-title">Parameter Management Setting</p>
                                                                    </td>
                                                                    <td width="5"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tr>
                                                                    <td width="80" align="right">
                                                                        <span class="cm-button-main cm-process-items">
                                                                            <a href="admin_advance_detail.php?page_name=<?php echo $print['page_name'] ?>&mode=INSERT#insert_form">
                                                                                <input type="button" class="cm-confirm cm-process-items" value="Add parameter">
                                                                            </a>
                                                                        </span>                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="780" cellspacing="2" cellpadding="0" border="0" align="center">
                                                                <tr height="25">
                                                                    <th width="30" rowspan="2" bgcolor="#ddf3fa"><font color="#606060">No</font></th>
                                                                    <th width="200" colspan="2" bgcolor="#ddf3fa"><font color="#606060">Parameter Name</font></th>
                                                                    <th width="100" bgcolor="#ddf3fa"><font color="#606060">Method</font></th>
                                                                    <th width="250" bgcolor="#ddf3fa"><font color="#606060">Check Items</font></th>
                                                                    <th width="50" rowspan="2" bgcolor="#ddf3fa"><font color="#606060">Modify</font></th>
                                                                    <th width="50" rowspan="2" bgcolor="#ddf3fa"><font color="#606060">Delete</font></th>
                                                                </tr>
                                                                <tr height="25">
                                                                    <th colspan="2" bgcolor="#ddf3fa"><font color="#606060">Param Form (Regular Expressions)</font></th>
                                                                    <th colspan="2" bgcolor="#ddf3fa"><font color="#606060">Min/Max Length</font></th>
                                                                </tr>
                                                                <?php
                                                                $print['var_count'] = 0;

                                                                if (isset($policy['page']['LIST']))
                                                                {
                                                                    $policy['var_list'] = $policy['page']['LIST'];

                                                                    $print['var_count'] = count($policy['var_list']);
                                                                    $print['var_num'] = 1;

                                                                    foreach ($policy['var_list'] as $key => $value)
                                                                    {
                                                                        if ($value == "")
                                                                            continue;

                                                                        $policy['var'] = $policy['var_list'][$key];

                                                                        $print['var_name'] = $key;
                                                                        $print['var_format'] = "";
                                                                        $print['var_min_length'] = "";
                                                                        $print['var_max_length'] = "";
                                                                        $print['var_method_get_check'] = "";
                                                                        $print['var_method_post_check'] = "";
                                                                        $print['var_target_sql_injection_check'] = "";
                                                                        $print['var_target_xss_check'] = "";
                                                                        $print['var_target_word_check'] = "";
                                                                        $print['var_target_tag_check'] = "";

                                                                        if (isset($policy['var']['FORMAT']))
                                                                            $print['var_format'] = base64_decode($policy['var']['FORMAT']);

                                                                        if (isset($policy['var']['MAX_LENGTH']))
                                                                            $print['var_min_length'] = base64_decode($policy['var']['MIN_LENGTH']);

                                                                        if (isset($policy['var']['MIN_LENGTH']))
                                                                            $print['var_max_length'] = base64_decode($policy['var']['MAX_LENGTH']);

                                                                        $print['var_name'] = htmlentities($print['var_name'], ENT_QUOTES, "UTF-8");
                                                                        $print['var_format'] = htmlentities($print['var_format'], ENT_QUOTES, "UTF-8");
                                                                        $print['var_max_length'] = htmlentities($print['var_max_length'], ENT_QUOTES, "UTF-8");
                                                                        $print['var_min_length'] = htmlentities($print['var_min_length'], ENT_QUOTES, "UTF-8");

                                                                        if (isset($policy['var']['METHOD']['GET']) && base64_decode($policy['var']['METHOD']['GET']) == "TRUE")
                                                                            $print['var_method_get_check'] = "checked";

                                                                        if (isset($policy['var']['METHOD']['POST']) && base64_decode($policy['var']['METHOD']['POST']) == "TRUE")
                                                                            $print['var_method_post_check'] = "checked";

                                                                        if (isset($policy['var']['TARGET']['SQL_INJECTION']) && base64_decode($policy['var']['TARGET']['SQL_INJECTION']) == "TRUE")
                                                                            $print['var_target_sql_injection_check'] = "checked";

                                                                        if (isset($policy['var']['TARGET']['XSS']) && base64_decode($policy['var']['TARGET']['XSS']) == "TRUE")
                                                                            $print['var_target_xss_check'] = "checked";

                                                                        if (isset($policy['var']['TARGET']['WORD']) && base64_decode($policy['var']['TARGET']['WORD']) == "TRUE")
                                                                            $print['var_target_word_check'] = "checked";

                                                                        if (isset($policy['var']['TARGET']['TAG']) && base64_decode($policy['var']['TARGET']['TAG']) == "TRUE")
                                                                            $print['var_target_tag_check'] = "checked";
                                                                        ?>
                                                                        <form name="var_modify_form" action="admin_advance_submit.php?mode=VAR_MODIFY" method="post">
                                                                            <input type="hidden" name="page_name" value="<?php echo $print['page_name'] ?>">
                                                                            <input type="hidden" name="var_name" value="<?php echo $print['var_name'] ?>">
                                                                            <tr height="25" bgcolor="#FFFFFF">
                                                                                <td rowspan="2" align="center">
                                                                                    <a name="<?php echo $print['var_num'] ?>"></a><?php echo $print['var_num'] ?>
                                                                                </td>
                                                                                <td width="50" align="right"><font size="1em"><b>Name</b></font></td>
                                                                                <td>&nbsp;<font color="#606060"><?php echo $print['var_name'] ?></font>
                                                                                </td>
                                                                                <td align="center">
                                                                                    <input type="checkbox" name="var_method_get" <?php echo $print['var_method_get_check'] ?>><font size="1em"><b>GET</b></font>
                                                                                    <input type="checkbox" name="var_method_post" <?php echo $print['var_method_post_check'] ?>><font size="1em"><b>POST</b></font>
                                                                                </td>
                                                                                <td align="center">
                                                                                    <input type="checkbox" name="var_target_sql_injection" <?php echo $print['var_target_sql_injection_check'] ?>><font size="1em"><b>SQL Injection</b></font>
                                                                                    <input type="checkbox" name="var_target_xss" <?php echo $print['var_target_xss_check'] ?>><font size="1em"><b>XSS</b></font>
                                                                                    <input type="checkbox" name="var_target_word" <?php echo $print['var_target_word_check'] ?>><font size="1em"><b>WORD</b></font>
                                                                                    <input type="checkbox" name="var_target_tag" <?php echo $print['var_target_tag_check'] ?>><font size="1em"><b>TAG</b></font>
                                                                                </td>
                                                                                <td rowspan="2" align="center" valign="middle">
                                                                                    <input type="button" value="Modify" onclick="ret = var_modify_confirm();
                                                                                            if (!ret)
                                                                                                return false;
                                                                                            else
                                                                                                submit();">
                                                                                </td>
                                                                                <td rowspan="2" align="center">
                                                                                    <input type="button" value="Delete" onclick="var_delete_submit('<?php echo $print['var_name'] ?>');
                                                                                            return false;">
                                                                                </td>
                                                                            </tr>
                                                                            <tr height="30" bgcolor="#FFFFFF">
                                                                                <td width="50" align="right"><font size="1em"><b>Format</b></font></td>
                                                                                <td>&nbsp;<input type="text" name="var_format" size="25" class="input-text" value="<?php echo $print['var_format'] ?>">
                                                                                </td>
                                                                                <td colspan="2" bgcolor="#FFFFFF" align="center">
                                                                                    <font size="1em"><b>Min Len</b></font> <input type="text" name="var_min_length" class="input-text" size="10" value="<?php echo $print['var_min_length'] ?>"> &nbsp;
                                                                                    <font size="1em"><b>Max Len</b></font> <input type="text" name="var_max_length" class="input-text" size="10" value="<?php echo $print['var_max_length'] ?>">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="7"><hr color="#ddf3fa"></td>
                                                                            </tr>
                                                                        </form>
                                                                        <?php
                                                                        $print['var_num'] ++;
                                                                    }
                                                                }
                                                                ?>
                                                            </table>
                                                            <br>
                                                            <table width="800" height="40">
                                                                <tr>
                                                                    <td width="100%" height="100%" align="center">Set the total <b><?php echo $print['var_count'] ?></b> parameters.</td>
                                                                </tr>
                                                            </table>
                                                            <?php
                                                            if (isset($page['mode']) && $page['mode'] == "INSERT")
                                                            {
                                                                ?>
                                                                <br>
                                                                <table width="790" height="30" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                    <tr>
                                                                        <td width="5"></td>
                                                                        <td height="100%" style="border-bottom: 2px solid #9fa8b3;background-color: #f3f3f3;" align="center">
                                                                            <p class="mainbox-title">Add new parameter</p>
                                                                        </td>
                                                                        <td width="5"></td>
                                                                    </tr>
                                                                </table>
                                                                <br>
                                                                <form name="var_modify_form" action="admin_advance_submit.php?mode=VAR_INSERT" method="post">
                                                                    <table width="780" cellspacing="2" cellpadding="0" border="0" align="center">
                                                                        <input type="hidden" name="page_name" value="<?php echo $print['page_name'] ?>">
                                                                        <tr height="25" bgcolor="#FFFFFF">
                                                                            <td width="60" align="right"><font size="1em"><b>Name</b></font>&nbsp;</td>
                                                                            <td>&nbsp;<input type="text" class="input-text" name="var_name" size="40"></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" name="var_method_get" checked><font size="1em"><b>GET</b></font>
                                                                                <input type="checkbox" name="var_method_post" checked><font size="1em"><b>POST</b></font>
                                                                            </td>
                                                                            <td align="center" valign='middle'>
                                                                                <input type="checkbox" name="var_target_sql_injection"><font size="1em"><b>SQL Injection</b></font>
                                                                                <input type="checkbox" name="var_target_xss"><font size="1em"><b>XSS</b></font>
                                                                                <input type="checkbox" name="var_target_word"><font size="1em"><b>WORD</b></font>
                                                                                <input type="checkbox" name="var_target_tag"><font size="1em"><b>TAG</b></font>
                                                                            </td>
                                                                        </tr>
                                                                        <tr height="30" bgcolor="#FFFFFF">
                                                                            <td align="right"><font size="1em"><b>Format</b></font>&nbsp;</td>
                                                                            <td>&nbsp;<input type="text" name="var_format" class="input-text" size="40">
                                                                            </td>
                                                                            <td colspan="2" bgcolor="#FFFFFF" align="center">
                                                                                <font size="1em"><b>Min Length</b></font> <input type="text" name="var_min_length" class="input-text" size="15" value="0"> &nbsp;
                                                                                <font size="1em"><b>Max Length</b></font> <input type="text" name="var_max_length" class="input-text" size="15" value="<?php echo MAX_VAR_LENGTH ?>">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <br>
                                                                    <table width="475" height="50" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                        <tr valign="top">
                                                                            <td width="175">&nbsp;</td>
                                                                            <td width="300">
                                                                                <span class="cm-button-main cm-process-items">
                                                                                    <input type="submit" value="Confirm">
                                                                                </span>
                                                                                <span class="cm-button-main cm-process-items">
                                                                                    <input type="button" class="cm-confirm cm-process-items" value="Cancel" onclick="page_move();">
                                                                                </span>
                                                                                <br>
                                                                            </td>
                                                                        </tr>

                                                                    </table>
                                                                </form>
                                                                <?php
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" bgcolor="#000000" colspan="2"></td>
                        </tr>
                        <tr bgcolor="#A0A0A0">
                            <td width="100%" height="50" colspan="2" align="center">
                                <?php include_once("admin_bottom.php"); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
