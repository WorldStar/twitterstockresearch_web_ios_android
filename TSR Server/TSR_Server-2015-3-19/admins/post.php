<?php

require_once '../models/config.php';

$type = 1;
$page = 1;

$xtea = new XTEA(NULL);
// param
if (isset($_REQUEST['34b9c']) && $_REQUEST['34b9c'] != null)
{
    $type = $_REQUEST['34b9c'];
    $type = intval($type);
    if ($type === 0)
    {
        $type = 1;
    }
}

$page_link = PARAM_PAGE . "=";

$signout_link = PARAM_TYPE . "=" . base64_encode($xtea->encrypt('1'));
$chpass_link = PARAM_TYPE . "=" . base64_encode($xtea->encrypt('2'));
$sendmail_link = PARAM_TYPE . "=" . base64_encode($xtea->encrypt('3'));
$loglist_link = PARAM_TYPE . "=" . base64_encode($xtea->encrypt('4'));

$usr = new UserManager();

switch ($type)
{
    case 1: // delete user
        $uids = split(',', $_POST['seled_uids']);

        if ($_POST['req'] === 'delete')
        {
            foreach ($uids as $uid)
            {
                if ($uid === '')
                {
                    continue;
                }

                $usr->deleteUser($uid);
            }
        }
        else if ($_POST['req'] === 'sendmail')
        {
            $_SESSION['uids'] = $uids;
            Navigator::redirect("index.php?" . $sendmail_link);
        }
        break;

    case 2: // login
        $user_type = 3; // site administrator
        $nowsec = time();
        $rip = $_SERVER['REMOTE_ADDR'];
        $wanted = $_SERVER['REQUEST_URI'];

        $res = $usr->loginAd($_POST);
        if ($res === SUCCESS)
        {
            delete_login_info($rip, $user_type);
        }
        else
        {
            // insert new login
            save_login_access("recent", $nowsec, $rip, $wanted, $user_type);
            ExceptionHandler::needToShowErrMsg($res);
        }
            
        Navigator::redirect("index.php?" . $chpass_link);
        break;

    case 3: // log out
        $usr->signoutAd();
        Navigator::redirect("index.php?" . $signout_link);
        break;

    case 4: // change password
        $res = $usr->changeAdminPass($_POST);
        if ($res === SUCCESS)
        {
            $usr->signoutAd();
        }
        else
        {
            ExceptionHandler::needToShowErrMsg($res);
        }

        Navigator::redirect("index.php?" . $signout_link);
        break;

    case 5: // send email
        $usr->sendMailAll($_SESSION['uids'], $_POST['subject'], $_POST['message']);
        Navigator::redirect("index.php?" . $signout_link);
        break;
}

if($type === 5)
{
    exit();
}

if (isset($_POST['req']) && $_POST['req'] === 'sendmail')
{
    Navigator::redirect("index.php?" . $sendmail_link);
}
else
{
    if (isset($_POST['page']))
    {
        $redirect = PARAM_TYPE . "=" . base64_encode($xtea->encrypt($_POST['34b9c'])) . "&" . PARAM_PAGE . "=" . base64_encode($xtea->encrypt($_POST['page']));
        Navigator::redirect("index.php?" . $redirect);
    }
    else
    {
        $redirect = PARAM_TYPE . "=" . base64_encode($xtea->encrypt($_POST['34b9c']));
        Navigator::redirect("index.php?" . $redirect);
    }
}

function save_login_access($table_name, $nowsec, $rip, $wanted, $type)
{
    global $mysqli;
    $stmt = $mysqli->prepare("INSERT INTO " . $table_name . " (
                                    tstamp,
                                    remoteaddr,
                                    calledfor,
                                    type
                                    )
                                    VALUES (
                                    ?,?,?,?
                                    )");

    $stmt->bind_param("sssi", $nowsec, $rip, $wanted, $type);
    $stmt->execute();
    $stmt->close();
}
/*
function get_login_count($rip, $tstamp, $type)
{
    global $mysqli;
    $stmt = $mysqli->prepare("SELECT 
                    COUNT(tstamp)
                    FROM recent
                    WHERE 
                    type = ? AND 
                    remoteaddr = ? AND
                    tstamp > ?");
    $stmt->bind_param("isi", $type, $rip, $tstamp);
    $stmt->execute();
    $stmt->bind_result($count);

    $row = array();
    while ($stmt->fetch())
    {
        $row = array('balloon' => $count);
    }
    $stmt->close();

    return intval($row['balloon']);
}
*/
function delete_login_info($rip, $type)
{
    global $mysqli;
    $stmt = $mysqli->prepare("DELETE 
                    FROM recent
                    WHERE 
                    type = ? AND 
                    remoteaddr = ?");
    $stmt->bind_param("is", $type, $rip);
    $stmt->execute();
    $stmt->bind_result($count);
    $stmt->close();
}

function check_login_count($rip, $type)
{
    global $_SECURE_POLICY;

    // get dos policy
    $policy['login'] = "";
    if (isset($_SECURE_POLICY['POLICY']['LOGIN']))
        $policy['login'] = & $_SECURE_POLICY['POLICY']['LOGIN'];

    /* apply */
    $policy['login_bool'] = base64_decode($policy['login']['BOOL']);
    $policy['peroid'] = base64_decode($policy['login']['PERIOD']);
    $policy['hurdle'] = base64_decode($policy['login']['ADMIN']);

    if ($policy['login_bool'] === "FALSE")
        return TRUE;

    // How many requests in the test time period
    $nowsec = time();
    $keeptime = intval($policy['peroid']);
    $hurdle = intval($policy['hurdle']);

    $nn = $nowsec - $keeptime;
    $balloon = get_login_count($rip, $nn, $type);

    // $hurdle pages per $keeptime seconds - aggressive!
    if ($balloon >= $hurdle)
    {
        return "You have exceeded the login counts.<br>Please try again in a few minutes.";
    }

    return TRUE;
}

?>