<?php
require_once("../models/config.php");

//if (!securePage($_SERVER['PHP_SELF']))
//{
//    die();
//}
//Prevent the user visiting the logged in page if he/she is already logged in
if (empty($_POST) && !isset($_SESSION['3d91n7s']))
{
    Home::showAdmin();
    return;
}

// title
$title = "Logs";

// current dir
$curr_path = "../";

// Total user count
$total = 0;

// Get parameter values
$mode = getParameter('mode', "list");
$page = getParameter('page', 1);
$sort_by = getParameter('sort_by', 'id');
$sort_order = getParameter('sort_order', 'desc');
$items_per_page = getParameter('items_per_page', 10);
$search_start = getParameter('time_from', "");
$search_end = getParameter('time_to', "");

$where = "id != '0'";
if ($search_start !== "" && $search_end !== "")
{
//    $where .= " AND reg_stamp >= '" . strtotime($search_start) . "'";
//    $where .= " AND reg_stamp <= '" . strtotime($search_end . " 24:00:00") . "'";
    $where .= " AND reg_stamp >= '" . getSecondTime($search_start . " 00:00:00") . "'";
    $where .= " AND reg_stamp <= '" . getSecondTime($search_end . " 23:59:60") . "'";
}
else
{
    if ($search_start !== "")
    {
//        $where .= " AND reg_stamp >= '" . strtotime($search_start . " 00:00:00") . "'";
//        $where .= " AND reg_stamp <= '" . strtotime($search_start . " 24:00:00") . "'";
        $where .= " AND reg_stamp >= '" . getSecondTime($search_start . " 00:00:00") . "'";
        $where .= " AND reg_stamp <= '" . getSecondTime($search_start . " 23:59:60") . "'";
    }
    if ($search_end !== "")
    {
//        $where .= " AND reg_stamp >= '" . strtotime($search_end . " 00:00:00") . "'";
//        $where .= " AND reg_stamp <= '" . strtotime($search_end . " 24:00:00") . "'";
        $where .= " AND reg_stamp >= '" . strtotime($search_end . " 00:00:00") . "'";
        $where .= " AND reg_stamp <= '" . strtotime($search_end . " 23:59:60") . "'";
    }
}

//if ($search_start !== "")
//{
//    $where .= " AND reg_stamp >= '" . strtotime($search_start) . "'";
//}
//if ($search_end !== "")
//{
//    $where .= " AND reg_stamp <= '" . strtotime($search_end) . "'";
//}

$total = getTotalCount("sys_logs", $where);

$cpage = intval($page);
$totalpage = intval($total / $items_per_page);
if ($totalpage * $items_per_page !== $total)
{
    $totalpage = $totalpage + 1;
}

if ($cpage == 1)
{
    $cline = 0;
}
else
{
    $cline = ($cpage * $items_per_page) - $items_per_page;
}

$limit = $cline + $items_per_page;

if ($limit >= $total)
{
    $limit = $total;
}

$scale1 = $limit - $cline;

// added parameters
$addParams = "page=" . $page . "&sort_by=" . $sort_by . "&sort_order=" . $sort_order . "&items_per_page=" . $items_per_page . "&time_from=" . $search_start . "&time_to=" . $search_end;

// Get page navigation string
$added = "&sort_by=" . $sort_by . "&sort_order=" . $sort_order . "&items_per_page=" . $items_per_page . "&time_from=" . $search_start . "&time_to=" . $search_end;
$pageNavigation = getNavigationPaging(10, intval($page), intval($totalpage), $curr_path . "admin/syslogs_list.php?page=", $added);

// Set link urls
$listLink = $curr_path . "admin/syslogs_list.php?" . $addParams;
//$addLink = $curr_path . "admin/admin_register.php?" . $addParams;
//$editLink = $curr_path . "admin/admin_profile.php?mode=edit&" . $addParams;
//$deleteLink = $curr_path . "admin/admin_list.php?mode=delete&" . $addParams;
//$csvLink = $curr_path . "admin/save_csv.php?rating=2&" . $addParams;

$addParams = "page=1&sort_by=" . $sort_by . "&sort_order=" . $sort_order . "&time_from=" . $search_start . "&time_to=" . $search_end;
$listPerLink = $curr_path . "admin/syslogs_list.php?" . $addParams;
$csvLink = $curr_path . "admin/syslogs_save_csv.php?" . $addParams;

// Fetch information for all users
$logsClass = new SysLogs();
$logData = $logsClass->fetchAllSysLogs($cline, $items_per_page, $sort_by, $sort_order, $search_start, $search_end);

//require_once($curr_path . "models/header.php");
//require_once($curr_path . "models/topmenu.php");
?>
<table cellpadding="0" cellspacing="0" border="0" class="content-table">
    <tr valign="top">
        <td width="1px" class="side-menu">
            <div id="right_column">
            </div>
        </td>
        <td class="content">
            <div class="mainbox-breadcrumbs">
            </div>

            <div id="main_column" class="clear">

                <div class="cm-notification-container ">
                </div>
                <div>
                    <div class="clear mainbox-title-container">
                        <h1 class="mainbox-title float-left">
                            <?php echo $title; ?>
                        </h1>
                    </div>
                    <div class="mainbox-body" >

                        <div class="clear" id="ds_1598117836">
                            <div class="section-border">
                                <form name="user_search_form" action="<?php echo $listLink; ?>" method="get" class="">
                                    <div class="nowrap">
                                        <table cellpadding="0" cellspacing="0" border="0" class="search-header">
                                            <tbody>
                                                <tr>
                                                    <td class="search-field">
                                                        <script>
                                                            $(function() {
                                                                var dates = $("#from, #to ").datepicker({
                                                                    dateFormat: 'yy-mm-dd',
                                                                    showMonthAfterYear: true,
                                                                    onSelect: function(selectedDate) {
                                                                        var option = this.id == "from" ? "minDate" : "maxDate",
                                                                                instance = $(this).data("datepicker"),
                                                                                date = $.datepicker.parseDate(
                                                                                        instance.settings.dateFormat ||
                                                                                        $.datepicker._defaults.dateFormat,
                                                                                        selectedDate, instance.settings);
                                                                        dates.not(this).datepicker("option", option, date);
                                                                    }
                                                                });
                                                            });
                                                        </script>

                                                        <style>
                                                            .ui-datepicker{ font-size: 12px; width: 180px; }
                                                            .ui-datepicker select.ui-datepicker-month{ width:30%; font-size: 11px; }
                                                            .ui-datepicker select.ui-datepicker-year{ width:40%; font-size: 11px; }
                                                        </style>

                                                        <label>Select dates:</label>

                                                        <div class="break nowrap">
                                                            <input type="text" class="input-text" id="from" name="time_from" value="<?php echo $search_start; ?>" />                                                            
                                                            &nbsp;&nbsp;-&nbsp;&nbsp;
                                                            <input type="text" class="input-text" id="to" name="time_to" value="<?php echo $search_end; ?>" />
                                                        </div>
                                                    </td>
                                                    <td class="buttons-container">
                                                        <span class="submit-button "><input type="submit" name="dispatch[logs.manage]" value="Search"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <script type="text/javascript" src="<?php echo $curr_path; ?>models/js/period_selector.js"></script>
                                    <script type="text/javascript">
                                                            //<![CDATA[
                                                            $(function() {
                                                                $('#period_selects').cePeriodSelector({
                                                                    from: 'f_date',
                                                                    to: 't_date'
                                                                });
                                                            });
                                                            //]]>
                                    </script>


                                    <script type="text/javascript">
//<![CDATA[
                                        lang.object_exists = 'Object of the same name already exists. Do you want to overwrite it?';

                                        function fn_check_views(input_id, views_id)
                                        {
                                            var match = true;
                                            var sbm_button = $(':submit:first', $('#' + input_id).parents('form:first'));
                                            $('.cm-view-name', $('#' + views_id)).each(function() {
                                                if ($(this).text().toLowerCase() == $('#' + input_id).val().toLowerCase()) {
                                                    match = confirm(lang.object_exists);
                                                    if (match) {
                                                        $('<input type="hidden" name="update_view_id" value="' + $(this).attr('rev') + '" />').appendTo($('#' + input_id).parent());
                                                    }
                                                    return false;
                                                }
                                            });
                                            if (match) {
                                                sbm_button.attr('name', sbm_button.attr('name').substr(0, sbm_button.attr('name').length - 1) + '.save_view]');
                                                sbm_button.trigger('click');
                                            }
                                        }

//]]>
                                    </script>
                                </form>
                            </div>
                        </div>


                        <div id="CSS_Table_Example">
                            <form action="<?php echo $listLink; ?>" method="post" name="userlist_form" id="userlist_form" class="">
                                <div id="pagination_contents">
                                    <script type="text/javascript" src="<?php echo $curr_path; ?>models/js/jquery.history.js"></script>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        $(function() {
                                            $.initHistory();
                                        });
                                        //]]>
                                    </script>

                                    <div class="pagination clear cm-pagination-wraper top-pagination">

                                        <div class="float-right">

                                            <?php echo $pageNavigation; ?>

                                            <span class="pagination-total-items">&nbsp;Total items:&nbsp;</span><span><?php echo $total; ?>&nbsp;/</span>

                                            <div class="tools-container inline">
                                                <div class="tools-content inline">
                                                    <a class="cm-combo-on cm-combination pagination-selector" id="sw_tools_list_pagination_381469667"><?php echo $items_per_page; ?></a>
                                                    <div id="tools_list_pagination_381469667" class="cm-tools-list popup-tools hidden cm-popup-box cm-smart-position">
                                                        <ul>
                                                            <li class="strong">Items per page:</li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=10" rev="pagination_contents">10</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=20" rev="pagination_contents">20</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=30" rev="pagination_contents">30</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=40" rev="pagination_contents">40</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=50" rev="pagination_contents">50</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=60" rev="pagination_contents">60</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=70" rev="pagination_contents">70</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=80" rev="pagination_contents">80</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=90" rev="pagination_contents">90</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=100" rev="pagination_contents">100</a></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table sortable">
                                        <tr>
                                            <th width="1%">
                                                &nbsp;
                                            </th>
                                            <th width="15%">
                                                &nbsp;&nbsp;<b>Time</b>
                                            </th>
                                            <th width="15%">
                                                &nbsp;&nbsp;<b>User</b>
                                            </th>
                                            <th width="20%">
                                                &nbsp;&nbsp;<b>Type</b>
                                            </th>
                                            <th width="50%">
                                                &nbsp;&nbsp;<b>Content</b>
                                            </th>
                                        </tr>

                                        <?php foreach ($logData as $v1) : ?>
                                            <tr class=" ">
                                                <td class="center cm-no-hide-input">
                                                    <?php if (intval($v1['result']) === 1) : ?>
                                                        <img src="<?php echo $curr_path ?>models/css/images/icons/notification_icon_n.png" />
                                                    <?php else : ?>
                                                        <img src="<?php echo $curr_path ?>models/css/images/icons/notification_icon_w.png" />
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php echo date("j M, Y h:i:s A", $v1['reg_stamp']); ?>
                                                </td>
                                                <td>
                                                    <span><?php echo $v1['user_name']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $v1['action']; ?></span>
                                                </td>
                                                <td>
                                                    <span><?php echo $v1['content']; ?></span>
                                                </td>
                                            </tr>

                                        <?php endforeach; ?>

                                        <?php if ($total === 0) : ?>
                                            <tr class="no-items">
                                                <td class="center cm-no-hide-input" colspan="8">
                                                    <p>No data found.</p>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                    </table>

                                    <div class="table-tools">
                                        &nbsp;
                                    </div>

                                    <script type="text/javascript">
                                        //<![CDATA[
                                        $(function() {
                                            $.initHistory();
                                        });
                                        //]]>
                                    </script>

                                    <div class="pagination clear cm-pagination-wraper">

                                        <div class="float-right">

                                            <?php echo $pageNavigation; ?>

                                            <span class="pagination-total-items">&nbsp;Total items:&nbsp;</span><span><?php echo $total; ?>&nbsp;/</span>

                                            <div class="tools-container inline">
                                                <div class="tools-content inline">
                                                    <a class="cm-combo-on cm-combination pagination-selector" id="sw_tools_list_pagination_381469668"><?php echo $items_per_page; ?></a>
                                                    <div id="tools_list_pagination_381469668" class="cm-tools-list popup-tools hidden cm-popup-box cm-smart-position">
                                                        <ul>
                                                            <li class="strong">Items per page:</li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=10" rev="pagination_contents">10</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=20" rev="pagination_contents">20</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=30" rev="pagination_contents">30</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=40" rev="pagination_contents">40</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=50" rev="pagination_contents">50</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=60" rev="pagination_contents">60</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=70" rev="pagination_contents">70</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=80" rev="pagination_contents">80</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=90" rev="pagination_contents">90</a></li>
                                                            <li><a href="<?php echo $listPerLink; ?>&items_per_page=100" rev="pagination_contents">100</a></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <!--pagination_contents-->
                                </div>

                                <div class="buttons-container buttons-bg">
                                    <div class="float-left">
                                        <span class="cm-button-main cm-process-items">
                                            <input type="button" onclick="window.open('<?php echo $csvLink; ?>');"  value="Export CSV" />
                                        </span>
                                    </div>	
                                </div>

                            </form>
                            <!--content_manage_users-->
                        </div>
                    </div>
                </div>
            </div>
            <!--main_column-->
        </td>
    </tr>
</table>

<?php
//require_once($curr_path . "models/bottom.php");
?>
