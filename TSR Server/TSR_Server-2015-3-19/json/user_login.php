<?php
  require_once '../models/config.php';
  if (file_get_contents('php://input')) {
    // Get the JSON Array
    $json = file_get_contents('php://input');
    // Lets parse through the JSON Array and get our individual values
    // in the form of an array
    $parsedJSON = json_decode($json, true);

    // Check to verify keys are set then define local variable, 
    // or handle however you would normally in PHP.
    // If it isn't set we can either define a default value
    // ('' in this case) or do something else
    $username = (isset($parsedJSON['username'])) ? $parsedJSON['username'] : '';
    $userpass = (isset($parsedJSON['userpass'])) ? $parsedJSON['userpass'] : '';
	$usr = new UserManager();
    
	$regdata = array( 'username' => $username, 'password' => $userpass );
	$res = $usr->mobile_login($regdata);

	if ($res === SUCCESS)
	{
		$regdata = array( 'username' => $username, 'userpass' => $userpass, 'registered' => "success" );
	}else if($res == UNCONFIRMED_EMAIL){
		$regdata = array( 'username' => $username, 'userpass' => $userpass, 'registered' => "The email is not verified yet. Please check your mail box and verify it by clicking the link." );
	}else if($res == WRONG_EMAIL){
		$regdata = array( 'username' => $username, 'userpass' => $userpass, 'registered' => "User name or email is wrong. Pleae try again." );
	}else if($res == WRONG_PASSWORD){
		$regdata = array( 'username' => $username, 'userpass' => $userpass, 'registered' => "Password is wrong. Pleae try again." );
	}else
	{
		$regdata = array( 'username' => $username, 'userpass' => $userpass, 'registered' => "failed" );
	}
    // Or we could just use the array we have as is
    /*$sql = "UPDATE `table` SET 
                `par_1` = '" . $parsedJSON['par_1'] . "',
                `par_2` = '" . $parsedJSON['par_2'] . "',
                `par_3` = '" . $parsedJSON['par_3'] . "'
            WHERE `action` = '" . $parsedJSON['action'] . "'";*/
	header('Content-Type: application/json');
	//$data2 = array( 'name' => 'God', 'age' => -1 );
	echo json_encode($regdata);
  }else{
  // Get the JSON Array
    //$json = file_get_contents('php://input');
    // Lets parse through the JSON Array and get our individual values
    // in the form of an array
    $parsedJSON = array( 'username' => "sin2@test.com", 'userpass' => 'sin1234' );

    $username = (isset($parsedJSON['username'])) ? $parsedJSON['username'] : '';
    $userpass = (isset($parsedJSON['userpass'])) ? $parsedJSON['userpass'] : '';
	$usr = new UserManager();
    
	$regdata = array( 'username' => $username, 'password' => $userpass );
	$res = $usr->mobile_login($regdata);

	if ($res === SUCCESS)
	{
		$regdata = array( 'username' => $username, 'userpass' => $userpass, 'registered' => "success" );
	}else if($res == UNCONFIRMED_EMAIL){
		$regdata = array( 'username' => $username, 'userpass' => $userpass, 'registered' => "The email is not verified yet. Please check your mail box and verify it by clicking the link." );
	}else if($res == WRONG_EMAIL){
		$regdata = array( 'username' => $username, 'userpass' => $userpass, 'registered' => "User name or email is wrong. Pleae try again." );
	}else if($res == WRONG_PASSWORD){
		$regdata = array( 'username' => $username, 'userpass' => $userpass, 'registered' => "Password is wrong. Pleae try again." );
	}else
	{
		$regdata = array( 'username' => $username, 'userpass' => $userpass, 'registered' => "failed" );
	}
    // Or we could just use the array we have as is
    
	header('Content-Type: application/json');
	//$data2 = array( 'name' => 'God', 'age' => -1 );
	echo json_encode($regdata);
	}
?>