<?php
  require_once '../models/config.php';
  if (file_get_contents('php://input')) {
    // Get the JSON Array
    $json = file_get_contents('php://input');
    // Lets parse through the JSON Array and get our individual values
    // in the form of an array
    $parsedJSON = json_decode($json, true);

    // Check to verify keys are set then define local variable, 
    // or handle however you would normally in PHP.
    // If it isn't set we can either define a default value
    // ('' in this case) or do something else
    $username = (isset($parsedJSON['username'])) ? $parsedJSON['username'] : '';
    $usermail = (isset($parsedJSON['usermail'])) ? $parsedJSON['usermail'] : '';
    $userpass = (isset($parsedJSON['userpass'])) ? $parsedJSON['userpass'] : '';
    $userverify = (isset($parsedJSON['verify'])) ? $parsedJSON['verify'] : '';
	$usr = new UserManager();
    if($userverify != "" && $userverify != null){
		$regdata = array( 'usernamesignup' => $username, 'emailsignup' => $usermail, 'passwordsignup' => $userpass, 'userverify' => $userverify );
	    $res = $usr->addMobileUser($regdata);
		if($res == SUCCESS){
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "success" );
		}else if($res == DUPLICATED_EMAIL){
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "The email is already in use. Please try with other email." );
		}else{
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "failed" );
		}
	}else{
		$regdata = array( 'usernamesignup' => $username, 'emailsignup' => $usermail, 'passwordsignup' => $userpass );
		$res = $usr->mobile_register($regdata);

		if ($res === SUCCESS)
		{
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "success" );
		}else if($res == DUPLICATED_USERNAME){
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "The user name is already in use. Please try with other user name." );
		}else if($res == DUPLICATED_EMAIL){
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "The email is already in use. Please try with other email." );
		}else if($res == SENT_CONFIRM_MAIL_FAILED){
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "Your mail address can't be confirmed. Try again with aother email address." );
		}else
		{
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "failed" );
		}
	}
    // Or we could just use the array we have as is
    /*$sql = "UPDATE `table` SET 
                `par_1` = '" . $parsedJSON['par_1'] . "',
                `par_2` = '" . $parsedJSON['par_2'] . "',
                `par_3` = '" . $parsedJSON['par_3'] . "'
            WHERE `action` = '" . $parsedJSON['action'] . "'";*/
	header('Content-Type: application/json');
	//$data2 = array( 'name' => 'God', 'age' => -1 );
	echo json_encode($regdata);
  }else{
	
    $parsedJSON = array( 'username' => "sin2", 'usermail' => "sin2@test.com", 'userpass' => "sin1234", "verify" => "");
    // Check to verify keys are set then define local variable, 
    // or handle however you would normally in PHP.
    // If it isn't set we can either define a default value
    // ('' in this case) or do something else
    $username = (isset($parsedJSON['username'])) ? $parsedJSON['username'] : '';
    $usermail = (isset($parsedJSON['usermail'])) ? $parsedJSON['usermail'] : '';
    $userpass = (isset($parsedJSON['userpass'])) ? $parsedJSON['userpass'] : '';
    $userverify = (isset($parsedJSON['verify'])) ? $parsedJSON['verify'] : '';
	
	$usr = new UserManager();
    if($userverify != "" && $userverify != null){
		$regdata = array( 'usernamesignup' => $username, 'emailsignup' => $usermail, 'passwordsignup' => $userpass, 'userverify' => $userverify );
	    
		$res = $usr->addMobileUser($regdata);
		if($res == SUCCESS){
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "success" );
		}else if($res == DUPLICATED_EMAIL){
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "The email is already in use. Please try with other email." );
		}else{
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "failed" );
		}
	}else{
		$regdata = array( 'usernamesignup' => $username, 'emailsignup' => $usermail, 'passwordsignup' => $userpass );
		
		$res = $usr->mobile_register($regdata);
		
		if ($res === SUCCESS)
		{
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "success" );
		}else if($res == DUPLICATED_USERNAME){
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "The user name is already in use. Please try with other user name." );
		}else if($res == DUPLICATED_EMAIL){
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "The email is already in use. Please try with other email." );
		}else if($res == SENT_CONFIRM_MAIL_FAILED){
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "Your mail address can't be confirmed. Try again with aother email address." );
		}else
		{
			$regdata = array( 'username' => $username, 'usermail' => $usermail, 'userpass' => $userpass, 'registered' => "failed" );
		}
	}
    // Or we could just use the array we have as is
    
	header('Content-Type: application/json');
	//$data2 = array( 'name' => 'God', 'age' => -1 );
	echo json_encode($regdata);

  }
?>