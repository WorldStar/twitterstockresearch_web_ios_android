<?php

require_once 'models/config.php';
/*
  $run = POST_SIGN_OUT;

  $xtea = new XTEA(NULL);

  if (isset($_REQUEST['VXorNVlnQU1sNmc4MkI2Vlp6M3dHUT09']) && $_REQUEST['VXorNVlnQU1sNmc4MkI2Vlp6M3dHUT09'] !== NULL)
  {
  $run = $_REQUEST['VXorNVlnQU1sNmc4MkI2Vlp6M3dHUT09'];
  $run = $xtea->decrypt(base64_decode($run));
  $run = intval($run);
  if($run === 0)
  {
  $run = POST_SIGN_OUT;
  }
  }
 */

$xtea = new XTEA(NULL);

$toregist_link = PARAM_GOTO . "=" . base64_encode($xtea->encrypt('toregist'));
$toprofile_link = PARAM_GOTO . "=" . base64_encode($xtea->encrypt('profile'));
$tochpass_link = PARAM_GOTO . "=" . base64_encode($xtea->encrypt('tochpass'));
$toforgot_link = PARAM_GOTO . "=" . base64_encode($xtea->encrypt('toforgot'));

$command = 4;
if (isset($_REQUEST['b5fc5']) && $_REQUEST['b5fc5'] !== NULL)
{
    $command = intval($_REQUEST['b5fc5']);
}

//switch ($_REQUEST['b5fc5'])
switch ($command)
{
    case 1: // registeration
        $usr = new UserManager();
        $res = $usr->register($_POST);

        if ($res === SUCCESS)
        {
            //$_SESSION['login'] = $_POST['usernamesignup'];
            ExceptionHandler::needToShowErrMsg(SENT_CONFIRM_MAIL);

            Navigator::redirect("index.php");
        }
        else
        {
            //$_SESSION['login'] = "";
            ExceptionHandler::needToShowErrMsg($res);
//die($res);
            Navigator::redirect("index.php?" . $toregist_link);
        }

        break;
    /* 	case 2: // confirm email
      $usr = new UserManager();
      $res = $usr->confirmEmail($_GET);

      if($res === SUCCESS) {
      //$_SESSION['login'] = $_POST['usernamesignup'];
      ExceptionHandler::needToShowErrMsg(EMAIL_CONFIRM_SUCCESS);

      Navigator::redirect("index.php");
      }
      else {
      //$_SESSION['login'] = "";
      ExceptionHandler::needToShowErrMsg($res);
      //die($res);
      Navigator::redirect("index.php#toregister");
      }

      Navigator::redirect("index.php");

      break;
     */
    case 2: // confirm email
        $usr = new UserManager();
        $res = $usr->confirmEmail($_GET);

        if ($res === SUCCESS)
        {
            //ExceptionHandler::needToShowErrMsg(EMAIL_CONFIRM_SUCCESS);
        }
        else
        {
            ExceptionHandler::needToShowErrMsg($res);
        }

        Navigator::redirect("index.php");

        break;
    case 3: // login
        $user_type = 4; // site user
        $nowsec = time();
        $rip = $_SERVER['REMOTE_ADDR'];
        $wanted = $_SERVER['REQUEST_URI'];

        $usr = new UserManager();
        $res = $usr->login($_POST);

        if ($res === SUCCESS)
        {
            delete_login_info($rip, $user_type);
        }
        else
        {
            // insert new login
            save_login_access("recent", $nowsec, $rip, $wanted, $user_type);
            ExceptionHandler::needToShowErrMsg($res);
        }
        
        Navigator::redirect("index.php");

        break;
    case 4: // sign out
        $usr = new UserManager();
        $res = $usr->signout();

        Navigator::redirect("index.php");

        break;
    case 5: // save profile
        $usr = new UserManager();
        $res = $usr->saveProfile($_POST);

        if ($res === SUCCESS)
        {
            
        }
        else
        {
            ExceptionHandler::needToShowErrMsg($res);
        }

        Navigator::redirect("index.php?" . $toprofile_link);

        break;
    case 6: // change pass
        $usr = new UserManager();
        $res = $usr->changeuserpass($_POST);

        if ($res === SUCCESS)
        {
            ExceptionHandler::needToShowErrMsg("Password was changed successfully.");
            Navigator::redirect("index.php?" . $toprofile_link);
        }
        else
        {
            ExceptionHandler::needToShowErrMsg($res);
            Navigator::redirect("index.php?" . $tochpass_link);
        }

        break;
    case 8: // reset password
        $usr = new UserManager();
        $res = $usr->forgotPassword($_POST);

        if ($res === SUCCESS)
        {
            //$_SESSION['login'] = $_POST['usernamesignup'];
            ExceptionHandler::needToShowErrMsg(CONFIRM_PASSWORD);

            Navigator::redirect("index.php");
        }
        else
        {
            //$_SESSION['login'] = "";
            ExceptionHandler::needToShowErrMsg($res);
//die($res);
            Navigator::redirect("index.php?" . $toforgot_link);
        }
    case 9: // stock research
        
        break;
}

function save_login_access($table_name, $nowsec, $rip, $wanted, $type)
{
    global $mysqli;
    $stmt = $mysqli->prepare("INSERT INTO " . $table_name . " (
                                    tstamp,
                                    remoteaddr,
                                    calledfor,
                                    type
                                    )
                                    VALUES (
                                    ?,?,?,?
                                    )");

    $stmt->bind_param("sssi", $nowsec, $rip, $wanted, $type);
    $stmt->execute();
    $stmt->close();
}
/*
function get_login_count($rip, $tstamp, $type)
{
    global $mysqli;
    $stmt = $mysqli->prepare("SELECT 
                    COUNT(tstamp)
                    FROM recent
                    WHERE 
                    type = ? AND 
                    remoteaddr = ? AND
                    tstamp > ?");
    $stmt->bind_param("isi", $type, $rip, $tstamp);
    $stmt->execute();
    $stmt->bind_result($count);

    $row = array();
    while ($stmt->fetch())
    {
        $row = array('balloon' => $count);
    }
    $stmt->close();

    return intval($row['balloon']);
}
*/
function delete_login_info($rip, $type)
{
    global $mysqli;
    $stmt = $mysqli->prepare("DELETE 
                    FROM recent
                    WHERE 
                    type = ? AND 
                    remoteaddr = ?");
    $stmt->bind_param("is", $type, $rip);
    $stmt->execute();
    $stmt->bind_result($count);
    $stmt->close();
}

function check_login_count($rip, $type)
{
    global $_SECURE_POLICY;

    // get dos policy
    $policy['login'] = "";
    if (isset($_SECURE_POLICY['POLICY']['LOGIN']))
        $policy['login'] = & $_SECURE_POLICY['POLICY']['LOGIN'];

    /* apply */
    $policy['login_bool'] = base64_decode($policy['login']['BOOL']);
    $policy['peroid'] = base64_decode($policy['login']['PERIOD']);
    $policy['hurdle'] = base64_decode($policy['login']['USER']);

    if ($policy['login_bool'] === "FALSE")
        return TRUE;

    // How many requests in the test time period
    $nowsec = time();
    $keeptime = intval($policy['peroid']);
    $hurdle = intval($policy['hurdle']);

    $nn = $nowsec - $keeptime;
    $balloon = get_login_count($rip, $nn, $type);

    // $hurdle pages per $keeptime seconds - aggressive!
    if ($balloon >= $hurdle)
    {
        return "You have exceeded the login counts.<br>Please try again in a few minutes.";
    }
    
    return TRUE;
}
?>

