//
//  AutoFillTableViewController.h
//  TSR
//
//  Created by Polaris on 1/12/15.
//  Copyright (c) 2015 Nihao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoFillTableViewController : UITableViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property UITextField *toTextField;
@property NSMutableArray * staffTableArray;
@property BOOL serverIdle;
- (void)createTableView:(CGRect)Position;

@end
