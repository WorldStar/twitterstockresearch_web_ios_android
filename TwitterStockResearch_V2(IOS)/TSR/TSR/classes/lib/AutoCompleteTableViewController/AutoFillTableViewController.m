//
//  AutoFillTableViewController.m
//  TSR
//
//  Created by Polaris on 1/12/15.
//  Copyright (c) 2015 Nihao. All rights reserved.
//

#import "AutoFillTableViewController.h"
#import "Global.h"

@implementation AutoFillTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
   
    if (self) {
        self.toTextField.delegate = self;
        self.staffTableArray = [[NSMutableArray alloc] initWithCapacity:1];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.scrollEnabled = YES;
        self.tableView.hidden = YES;
        self.serverIdle = YES;
        [self.tableView setBackgroundColor:[UIColor whiteColor]];
    }
    return self;
}

- (void)createTableView:(CGRect)Position {
    Position.origin.y += Position.size.height+1;
    Position.size.height *=3 ;
    self.tableView = [[UITableView alloc] initWithFrame:
                      Position style:UITableViewStylePlain];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.toTextField) {
        self.tableView.hidden = NO;
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring stringByReplacingCharactersInRange:range withString:string];
        [self searchAutocompleteEntriesWithSubstring:substring];
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"return" object:self];
    [textField resignFirstResponder];
    self.tableView.hidden = YES;
    return YES;
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring
{
    if (self.serverIdle == NO) return;
    self.serverIdle = NO;
    substring = [substring lowercaseString];
    substring = [substring stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    //--------------------  WEB SERVER URL ---------------------------------------------
#ifdef USE_REAL_SERVER
    NSString *autoCompleteUrl = [[NSString alloc] initWithFormat: @"http://autoc.finance.yahoo.com/autoc?query=%@&callback=YAHOO.Finance.SymbolSuggest.ssCallback", substring];
#else
    NSString *autoCompleteUrl = [[NSString alloc] initWithFormat: @"file:///Users/admin/Documents/TSR/TSRTests/autocomplete.txt"];
#endif
    
    //---------------------------------------------------------------------------------
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:autoCompleteUrl parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *result = operation.responseString;
         NSRange range;
         @try {
             range.location = [result rangeOfString:@"("].location + 1;
             range.length = result.length - range.location - 1;
             result = [result substringWithRange:range];
             SBJSON *parser = [[SBJSON alloc] init];
             NSArray *jsonArray = [[[parser objectWithString:result] valueForKeyPath:@"ResultSet"]objectForKey:@"Result"];
             NSArray *autoCompleteArray = [[NSArray alloc] init];
             
             for (int i = 0 ; i < jsonArray.count ; i ++) {
                 NSString *item = [NSString stringWithFormat:@"%@,%@(%@)",[jsonArray[i] objectForKey:@"symbol"],[jsonArray[i] objectForKey:@"name"],[jsonArray[i] objectForKey:@"exch"]];
                 autoCompleteArray = [autoCompleteArray arrayByAddingObject:item];
             }
             if (![substring isEqualToString:@""])
             {
                 self.staffTableArray = [NSMutableArray arrayWithArray:autoCompleteArray];
                 [self.tableView reloadData];
             }
         }
         @catch (NSException *exception) {
             NSLog(@"autocomplete error:%@",exception.description);
         }
         @finally {
             
         }
         self.serverIdle = YES;
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"%@",error.description);
             self.serverIdle = YES;

         }];
}

#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    return self.staffTableArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    cell.textLabel.text = [self.staffTableArray objectAtIndex:indexPath.row];

    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    self.toTextField.text = selectedCell.textLabel.text;
    [self.toTextField resignFirstResponder];
    self.tableView.hidden = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"return" object:self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}
@end
