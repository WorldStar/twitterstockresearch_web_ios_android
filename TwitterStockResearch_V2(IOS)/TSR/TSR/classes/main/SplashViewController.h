//
//  SplashViewController.h
//  TSR
//
//  Created by Polaris on 12/22/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *_lblTitle;

@end
