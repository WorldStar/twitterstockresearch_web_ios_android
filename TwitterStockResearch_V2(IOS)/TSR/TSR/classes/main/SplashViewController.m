//
//  SplashViewController.m
//  TSR
//
//  Created by Polaris on 12/22/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "SplashViewController.h"
#import "TabbarController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

@synthesize _lblTitle;

-(void)viewDidLoad
{
    [super viewDidLoad];
  //  _lblTitle.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self performSelector:@selector(nextView) withObject:nil afterDelay:3.0];
}

-(void)nextView
{
    TabbarController* viewController= [self.storyboard instantiateViewControllerWithIdentifier:@"tabbar"];
    [self presentViewController:viewController animated:YES completion:nil];
}

@end
