//
//  TabbarController.m
//  TSR
//
//  Created by Polaris on 12/23/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "TabbarController.h"
#import "Global.h"

@interface TabbarController ()

@end

@implementation TabbarController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.selectedIndex=1;
    [self.tabBar setTintColor:[UIColor whiteColor]];
   // UITabBarItem *item0 = [self.tabBar.items objectAtIndex:0];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithWhite:1.0 alpha:0.3], UITextAttributeTextColor,[UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14], UITextAttributeFont,nil] forState:UIControlStateNormal];
  
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor,[UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14], UITextAttributeFont,nil] forState:UIControlStateSelected];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
