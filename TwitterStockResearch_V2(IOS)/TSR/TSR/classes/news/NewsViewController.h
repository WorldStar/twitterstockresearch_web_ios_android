//
//  NewsViewController.h
//  TSR
//
//  Created by Polaris on 12/31/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UIScrollView *_scrollView;
@property (retain, nonatomic) IBOutlet UIButton *_btnSearch;
@property (retain, nonatomic) IBOutlet UITableView *_tableView;
@property (retain, nonatomic) NSMutableArray *_news;
@property (retain, nonatomic) IBOutlet UITextField *_txtSearch;
@property (weak, nonatomic) IBOutlet UILabel *_lblNewsTitle;

- (IBAction)searchClick:(id)sender;

@end
