//
//  NewsViewController.m
//  TSR
//
//  Created by Polaris on 12/31/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "NewsViewController.h"
#import "DetailViewController.h"
#import "Global.h"
#import "XMLParser.h"

@interface NewsViewController ()

@end

@implementation NewsViewController

@synthesize _scrollView,_tableView,_news, _btnSearch;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _news=[[NSMutableArray alloc] init];
    _tableView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.29 alpha:1];
    [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, [UIScreen mainScreen].bounds.size.height - 144)];
    self._lblNewsTitle.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:16];
}

- (IBAction)searchClick:(id)sender {
    if (self._txtSearch != nil) return;
    self._txtSearch = [[Global sharedGlobal] insertSearchTextField:self position:CGRectMake(10, 66, 300, 30)];
    self._txtSearch.text = [Global sharedGlobal].stockSymbol;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newsSearch) name:@"return" object:nil];
    [_scrollView setFrame:CGRectMake(_scrollView.frame.origin.x, 98, _scrollView.frame.size.width, _scrollView.frame.size.height)];
    [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, [UIScreen mainScreen].bounds.size.height - 144 - 30)];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self._news count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"newsCell"];
    
    UILabel *newsTitle = (UILabel *)[cell viewWithTag:100];
    newsTitle.text = [self._news[indexPath.row] leafForKey:@"title"];
    newsTitle.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    
    UILabel *newsDate = (UILabel *)[cell viewWithTag:101];
    newsDate.text =  [self._news[indexPath.row] leafForKey:@"pubDate"];
    newsDate.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:10];
    
    UILabel *newsContent = (UILabel *)[cell viewWithTag:102];
    newsContent.text =  [self._news[indexPath.row] leafForKey:@"description"];
    newsContent.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:12];

    UILabel *newsMore = (UILabel *)[cell viewWithTag:103];
    newsMore.text = @"Read More >>";
    newsMore.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:12];

    return cell;
}

#pragma mark -
#pragma mark UIViewController delegate

- (void)viewWillAppear:(BOOL)animated
{
    if([Global sharedGlobal].isLogined){
        self._scrollView.hidden = NO;
        self._btnSearch.hidden = NO;
        self._txtSearch.hidden = NO;
        [self getNews:[Global sharedGlobal].stockSymbol];
    }else{
        [Global showAlertTips:@"Please login."];
        self._scrollView.hidden = YES;
        self._btnSearch.hidden = YES;
        self._txtSearch.hidden = YES;
    }
}


#pragma mark -
#pragma mark UITableViewDelegate

// the table's selection has changed, switch to that item's UIViewController
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailView"];
    detailViewController._url = [self._news[indexPath.row] leafForKey:@"link"];
    [self addChildViewController:detailViewController];
    [self.view addSubview:detailViewController.view];
    [detailViewController didMoveToParentViewController:self];
    
    [self._tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)getNews:(NSString *)symbol
{
    if (symbol == nil || [symbol isEqualToString:@""] == true) {
        return;
    }
    [Global sharedGlobal].stockSymbol = symbol;
    [ MBProgressHUD showHUDAddedTo : self.view animated : YES ] ;
    symbol = [[symbol componentsSeparatedByString:@","] firstObject];
    symbol = [symbol stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
   
    //--------------------  WEB SERVER URL ---------------------------------------------

#ifdef USE_REAL_SERVER
    NSString *Url = [[NSString alloc] initWithFormat: @"https://feeds.finance.yahoo.com/rss/2.0/headline?s=%@&region=US&lang=en-US", symbol];
#else
    NSString *Url = [[NSString alloc] initWithFormat: @"http://192.168.0.107/NewStockTwits/json/%@_headline.xml", symbol];
#endif
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:25];
    [manager GET:Url parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
         TreeNode *xmlData = [[XMLParser sharedInstance] parseXMLFromData:operation.responseData];
         self._news = [xmlData objectsForKey:@"item"];
         self._lblNewsTitle.text = [xmlData objectForKey:@"title"].leafvalue;
         [self._tableView reloadData];
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
         self._news = nil;
         [self._tableView reloadData];
         [Global showAlertTips: @"Can not get news information.\nPlease try again." ] ;
         NSLog(@"%@", error.description);
     }];
    

}

- (void)newsSearch
{
    NSString *symbol = [self _txtSearch].text;
    if( symbol == nil || [symbol isEqualToString:@""] == true )
    {
        [Global showAlertTips: @"Please input stock symbol!" ] ;
        return;
    }
    [self getNews:symbol];
}


@end
