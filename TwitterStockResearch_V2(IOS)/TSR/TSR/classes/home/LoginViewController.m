//
//  LoginViewController.m
//  TSR
//
//  Created by Polaris on 12/22/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "LoginViewController.h"
#import "JoinViewController.h"
#import "SearchViewController.h"
#import "VerifyViewController.h"
#import "Global.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

@synthesize _txtName, _txtPassword;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self._txtSearch = [[Global sharedGlobal] insertSearchTextField:self position:CGRectMake(48, 115, 225, 30)];
    self._txtSearch.enabled = NO;
    UIImageView *imageUser = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mail.png"]];
    _txtName.leftView = imageUser;
    _txtName.leftViewMode = UITextFieldViewModeAlways;
    _txtName.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    _txtName.delegate = self;	// let us be the delegate so we know when the keyboard's "Done" button is pressed

    UIImageView *imagePassword = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password.png"]];
    _txtPassword.leftView = imagePassword;
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;
    _txtPassword.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    _txtPassword.delegate = self;	// let us be the delegate so we know when the keyboard's "Done" button is pressed
    
    _txtName.text = [Global sharedGlobal].loginEmail;
    _txtPassword.text = [Global sharedGlobal].password;
    if( ![_txtName.text isEqualToString:@""] && ![_txtPassword.text isEqualToString:@""] )  [self processLogin];
}

#pragma mark - UITextField Delegate
- ( BOOL ) textFieldShouldReturn : ( UITextField* ) _textField
{
    if(_textField == _txtName) {
        [_txtPassword becomeFirstResponder] ;
        return YES ;
    }
    if(_textField == _txtPassword) {
        [_txtPassword resignFirstResponder] ;
        return YES ;
    }
    return YES ;
}

- (IBAction)loginClick:(id)sender {
    [Global sharedGlobal].stockSymbol = [self _txtSearch].text;
    [NSTimer scheduledTimerWithTimeInterval:0.01f target:self selector:@selector(processLogin) userInfo:nil repeats:NO];
}

- (void)processLogin
{
    NSString    *username = [_txtName text];
    NSString    *password = [_txtPassword text];
    
    if( username == nil || [username isEqualToString:@""] == true )
    {
        [Global showAlertTips: @"Please input your email address." ] ;
        return;
    }
    
    if (![self checkEmail]) {
        [_txtName becomeFirstResponder];
        return;
    }
    
    if( password == nil || [password isEqualToString:@""] == true )
    {
        [Global showAlertTips: @"Please input your password!" ] ;
        return;
    }
    
    [ MBProgressHUD showHUDAddedTo : self.view animated : YES ] ;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:25];

    NSDictionary *params = @ {@"username" :username, @"userpass" :password };
    
    //--------------------  WEB SERVER URL ---------------------------------------------
#ifdef USE_REAL_SERVER
   [manager POST:@"https://www.twitterstockresearch.com/json/user_login.php" parameters:params
#else
    [manager POST:@"http://192.168.0.107/NewStockTwits/json/user_login.php" parameters:params
#endif
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
         
         if ([responseObject[@"registered"]  isEqual: @"success"])
         {
             [Global sharedGlobal].loginEmail = username;
             [Global sharedGlobal].password = password;
             [[Global sharedGlobal] SaveParam];
             
             SearchViewController *searchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"searchView"];
             [self addChildViewController:searchViewController];
             [self.view addSubview:searchViewController.view];
             [searchViewController didMoveToParentViewController:self];
         }
         else if ([responseObject[@"registered"]  isEqual: @"The email is not verified yet. Please check your mail box and verify it by clicking the link."])
         {
             [Global sharedGlobal].loginEmail = username;
             [Global sharedGlobal].password = password;
             [[Global sharedGlobal] SaveParam];

             [Global showAlertTips: @"Please check your mail box and verify it by clicking the link."] ;
             VerifyViewController *verifyViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyView"];
             [self addChildViewController:verifyViewController];
             [self.view addSubview:verifyViewController.view];
             [verifyViewController didMoveToParentViewController:self];
         }
         else if ([responseObject[@"registered"]  isEqual: @"failed"])
         {
             [Global showAlertTips: @"Failed response from server. Please try again." ] ;
         }
         else
         {
             [Global showAlertTips: responseObject[@"registered"] ] ;
         }
     }
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
         [Global showAlertTips: @"Failed. Please check network and try again." ] ;
         
     }];
}

- (IBAction)joinClick:(id)sender {
    
    JoinViewController *joinViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"joinView"];
    [self addChildViewController:joinViewController];
    [self.view addSubview:joinViewController.view];
    [joinViewController didMoveToParentViewController:self];
}

- ( BOOL ) checkEmail
{
    BOOL            filter = YES ;
    NSString*       filterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" ;
    NSString*       laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*" ;
    NSString*       emailRegex = filter ? filterString : laxString ;
    NSPredicate*    emailTest = [ NSPredicate predicateWithFormat : @"SELF MATCHES %@", emailRegex ] ;
    
    if( [ emailTest evaluateWithObject : _txtName.text ] == NO )
    {
        [Global showAlertTips: @"Please input your valid email address." ] ;
        return NO ;
    }
    
    return YES ;
}

@end
