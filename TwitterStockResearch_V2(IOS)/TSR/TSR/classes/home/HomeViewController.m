//
//  HomeViewController.m
//  TSR
//
//  Created by Polaris on 12/26/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "HomeViewController.h"
#import "LoginViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    LoginViewController *loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginView"];
    [self addChildViewController:loginViewController];
    [self.view addSubview:loginViewController.view];
    [loginViewController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
