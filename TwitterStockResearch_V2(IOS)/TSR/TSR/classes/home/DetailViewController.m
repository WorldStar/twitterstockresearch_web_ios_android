//
//  DetailViewController.m
//  TSR
//
//  Created by Polaris on 12/29/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "DetailViewController.h"
#import "Global.h"
#import "UIWebView+AFNetworking.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize _webView;

- (void)viewDidLoad
{
    [super viewDidLoad];

  //  self._url = @"http://192.168.0.107/NewStockTwits/";
    //[ MBProgressHUD showHUDAddedTo : self.view animated : YES ] ;
    /*
    [self._webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self._url]]
                      MIMEType:@"text/html"
              textEncodingName:nil
                      progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite){
                 } success:^NSData *(NSHTTPURLResponse *response, NSData *data) {
                     //[ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
                     return data;
                 } failure:^(NSError *error){
                     //[ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
                     //[Global showAlertTips: @"Can not get news information.\nPlease try again." ] ;
                 }];*/
    [self._webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self._url]]];
    self._webView.hidden = NO;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if(![Global sharedGlobal].isLogined){
        [self backClick:nil];
    }
	
}

- (IBAction)backClick:(id)sender {
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

@end
