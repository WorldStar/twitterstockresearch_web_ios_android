//
//  SearchViewController.h
//  TSR
//
//  Created by Polaris on 12/27/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UITextField *_txtSearch;

- (IBAction)researchClick:(id)sender;
- (IBAction)logoutClick:(id)sender;

@end
