//
//  JoinViewController.h
//  TSR
//
//  Created by Polaris on 12/26/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JoinViewController : UIViewController <UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UITextField *_txtName;
@property (retain, nonatomic) IBOutlet UITextField *_txtEmail;
@property (retain, nonatomic) IBOutlet UITextField *_txtPassword;
@property (retain, nonatomic) IBOutlet UITextField *_txtConfirm;
@property (retain, nonatomic) IBOutlet UIView *_contentView;

- (IBAction)joinClick:(id)sender;
- (IBAction)backClick:(id)sender;

@end
