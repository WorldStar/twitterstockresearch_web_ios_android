//
//  SearchViewController.m
//  TSR
//
//  Created by Polaris on 12/27/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "SearchViewController.h"
#import "ResultViewController.h"
#import "Global.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [Global sharedGlobal].isLogined = TRUE;
    self._txtSearch = [[Global sharedGlobal] insertSearchTextField:self position:CGRectMake(48, 150, 225, 30)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self._txtSearch.text = [Global sharedGlobal].stockSymbol;
}

- (IBAction)researchClick:(id)sender {
    [[self _txtSearch] resignFirstResponder];
    NSString *symbol = [self _txtSearch].text;
    if( symbol == nil || [symbol isEqualToString:@""] == true )
    {
        [Global showAlertTips: @"Please input stock symbol!" ] ;
        return;
    }
    [Global sharedGlobal].stockSymbol = symbol;
    
    ResultViewController *resultViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"resultView"];
    [self addChildViewController:resultViewController];
    [self.view addSubview:resultViewController.view];
    [resultViewController didMoveToParentViewController:self];
}

- (IBAction)logoutClick:(id)sender {
    [Global sharedGlobal].username = @"";
    [Global sharedGlobal].loginEmail = @"";
    [Global sharedGlobal].password = @"";
    [[Global sharedGlobal] SaveParam];
    [Global sharedGlobal].isLogined = false;
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
}

@end
