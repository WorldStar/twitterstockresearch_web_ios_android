//
//  DetailViewController.h
//  TSR
//
//  Created by Polaris on 12/29/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UIWebView *_webView;
@property (nonatomic) NSString * _url;

- (IBAction)backClick:(id)sender;

@end
