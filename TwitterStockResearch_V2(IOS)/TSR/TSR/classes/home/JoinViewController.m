//
//  JoinViewController.m
//  TSR
//
//  Created by Polaris on 12/26/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "JoinViewController.h"
#import "VerifyViewController.h"
#import "Global.h"

@interface JoinViewController ()

@end

@implementation JoinViewController

@synthesize _txtName, _txtEmail, _txtPassword, _txtConfirm, _contentView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIImageView *imageEmail = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mail.png"]];
    _txtEmail.leftView = imageEmail;
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
    _txtEmail.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    _txtEmail.delegate = self;	// let us be the delegate so we know when the keyboard's "Done" button is pressed
    
    UIImageView *imageUser = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user.png"]];
    _txtName.leftView = imageUser;
    _txtName.leftViewMode = UITextFieldViewModeAlways;
    _txtName.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    _txtName.delegate = self;	// let us be the delegate so we know when the keyboard's "Done" button is pressed
    
    UIImageView *imagePassword = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password.png"]];
    _txtPassword.leftView = imagePassword;
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;
    _txtPassword.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    _txtPassword.delegate = self;	// let us be the delegate so we know when the keyboard's "Done" button is pressed
    
    UIImageView *imageConfirm = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password.png"]];
    _txtConfirm.leftView = imageConfirm;
    _txtConfirm.leftViewMode = UITextFieldViewModeAlways;
    _txtConfirm.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    _txtConfirm.delegate = self;	// let us be the delegate so we know when the keyboard's "Done" button is pressed

}

#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([UIScreen mainScreen].bounds.size.height == 568) return;
    [UIView beginAnimations:nil context:nil];
    [self._contentView setFrame:CGRectMake(0,64, self._contentView.frame.size.width, self._contentView.frame.size.height)];
    [UIView setAnimationDuration:1.0];
    [UIView commitAnimations];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)_textField {
    if(_textField == _txtName ) {
        [_txtEmail becomeFirstResponder] ;
        return YES ;
    }
    
    if (_textField == _txtEmail) {
        [_txtPassword becomeFirstResponder];
        return YES;
    }
    if (_textField == _txtPassword) {
        [_txtConfirm becomeFirstResponder];
        return YES;
    }
    
    if(_textField == _txtConfirm) {
        [_txtConfirm resignFirstResponder] ;
        if ([UIScreen mainScreen].bounds.size.height == 568) return YES;
        [UIView beginAnimations:nil context:nil];
        [self._contentView setFrame:CGRectMake(0, 103, self._contentView.frame.size.width, self._contentView.frame.size.height)];
        [UIView setAnimationDuration:1.0];
        [UIView commitAnimations];
        
        return YES ;
    }
    
    return YES ;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- ( BOOL ) checkEmail
{
    BOOL            filter = YES ;
    NSString*       filterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" ;
    NSString*       laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*" ;
    NSString*       emailRegex = filter ? filterString : laxString ;
    NSPredicate*    emailTest = [ NSPredicate predicateWithFormat : @"SELF MATCHES %@", emailRegex ] ;
    
    if( [ emailTest evaluateWithObject : _txtEmail.text ] == NO )
    {
        [Global showAlertTips: @"Please input your valid email address." ] ;
        return NO ;
    }
    
    return YES ;
}

- (IBAction)joinClick:(id)sender {
    NSString *username = _txtName.text;
    NSString *usermail = _txtEmail.text;
    NSString *password = _txtPassword.text;
    NSString *confirm = _txtConfirm.text;
    if (username == nil || [username isEqualToString:@""]) {
        [Global showAlertTips: @"Please input your name." ] ;
        [_txtName becomeFirstResponder];
        return;
    }
    
    if (![self checkEmail]) {
        [_txtEmail becomeFirstResponder];
        return;
    }
    
    if (password == nil || [password isEqualToString:@""]) {
        [Global showAlertTips: @"Please input your password." ] ;
        [_txtPassword becomeFirstResponder];
        return;
    }
    if (![password isEqualToString:confirm]) {
        [Global showAlertTips: @"Password does not match." ] ;
        [_txtPassword becomeFirstResponder];
        return;
    }
   
    [ MBProgressHUD showHUDAddedTo : self.view animated : YES ] ;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:25];

    NSDictionary *params = @ {@"username" :username, @"usermail" :usermail, @"userpass" :password };
    
    //--------------------  WEB SERVER URL ---------------------------------------------
#ifdef USE_REAL_SERVER
   [manager POST:@"https://www.twitterstockresearch.com/json/user_request.php" parameters:params
#else
    [manager POST:@"http://192.168.0.107/NewStockTwits/json/user_request.php" parameters:params
#endif
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
         
         if ([responseObject[@"registered"]  isEqual: @"success"])
         {
             [Global sharedGlobal].username = username;
             [Global sharedGlobal].loginEmail = usermail;
             [Global sharedGlobal].password = password;
             [[Global sharedGlobal] SaveParam];
             
             VerifyViewController *verifyViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyView"];
             [self.parentViewController addChildViewController:verifyViewController];
             [self.parentViewController.view addSubview:verifyViewController.view];
             [verifyViewController didMoveToParentViewController:self.parentViewController];
             
             [self willMoveToParentViewController:nil];
             [self.view removeFromSuperview];
             [self removeFromParentViewController];
         }
         else if ([responseObject[@"registered"]  isEqual: @"failed"])
         {
             [Global showAlertTips: @"Failed response from server. Please try again." ] ;
         }
         else
         {
             [Global showAlertTips: responseObject[@"registered"] ] ;
         }
     }
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
         [Global showAlertTips: @"Failed. Please check network and try again." ] ;
         
     }];
}

- (IBAction)backClick:(id)sender {
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

@end
