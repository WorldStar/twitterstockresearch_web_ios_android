//
//  VerifyViewController.m
//  TSR
//
//  Created by Polaris on 12/27/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "VerifyViewController.h"
#import "SearchViewController.h"
#import "Global.h"

@interface VerifyViewController ()

@end

@implementation VerifyViewController

@synthesize _txtVerify;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImageView *imageConfirm = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mail.png"]];
    _txtVerify.leftView = imageConfirm;
    _txtVerify.leftViewMode = UITextFieldViewModeAlways;
    _txtVerify.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    _txtVerify.delegate = self;	// let us be the delegate so we know when the keyboard's "Done" button is pressed
}

#pragma mark - UITextField Delegate
- ( BOOL ) textFieldShouldReturn : ( UITextField* ) _textField
{
    if(_textField == _txtVerify) {
        [_txtVerify resignFirstResponder] ;
    }
    return YES ;
}

- (IBAction)confirmClick:(id)sender {

    NSString *username = [Global sharedGlobal].username;
    NSString *usermail = [Global sharedGlobal].loginEmail;
    NSString *password = [Global sharedGlobal].password;
    NSString *verifycode = self._txtVerify.text;
    
    if( verifycode == nil || [verifycode isEqualToString:@""] == true )
    {
        [Global showAlertTips: @"Please input your verify code." ] ;
        return;
    }
    
    [self._txtVerify resignFirstResponder];

    [ MBProgressHUD showHUDAddedTo : self.view animated : YES ] ;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:25];

    NSDictionary *params = @ {@"username" :username, @"usermail" :usermail, @"userpass" :password, @"verify" :verifycode };
    
    //--------------------  WEB SERVER URL ---------------------------------------------
#ifdef USE_REAL_SERVER
   [manager POST:@"https://www.twitterstockresearch.com/json/user_request.php" parameters:params
#else
    [manager POST:@"http://192.168.0.107/NewStockTwits/json/user_request.php" parameters:params
#endif
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
         
         if ([responseObject[@"registered"]  isEqual: @"success"])
         {
             SearchViewController *searchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"searchView"];
             [self.parentViewController addChildViewController:searchViewController];
             [self.parentViewController.view addSubview:searchViewController.view];
             [searchViewController didMoveToParentViewController:self.parentViewController];
             
             [self willMoveToParentViewController:nil];
             [self.view removeFromSuperview];
             [self removeFromParentViewController];

         }
         else if ([responseObject[@"registered"]  isEqual: @"failed"])
         {
             [Global showAlertTips: @"Invalid verify code. Please try again." ] ;
         }
         else
         {
             [Global showAlertTips: responseObject[@"registered"] ] ;
         }
     }
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
         [Global showAlertTips: @"Failed. Please check network and try again." ] ;
         
     }];
}

- (IBAction)backClick:(id)sender {
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

@end
