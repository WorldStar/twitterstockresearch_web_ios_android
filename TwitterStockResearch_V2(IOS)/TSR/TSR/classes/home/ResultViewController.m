//
//  ResultViewController.m
//  TSR
//
//  Created by Polaris on 12/28/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "ResultViewController.h"
#import "DetailViewController.h"
#import "CCSAreaChart.h"
#import "CCSLineData.h"
#import "CCSTitledLine.h"
#import "Global.h"
#import "STTwitter.h"

@interface ResultViewController ()

@end

@implementation ResultViewController

@synthesize _scrollView, _tableView, _tweetNews, _graphView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self._stockInfo = nil;
    self._graphInfo = nil;
    self._tweetNews = [[NSMutableArray alloc] init];

    _tableView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.29 alpha:1];
    self._lblYahooTitle.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:16];
    self._lblTweetNews.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:16];
    self._lblTime.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:13];
    self._lblCurrentPrice.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblDailyChangeTitle.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblDailyChange.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblOpen.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblClose.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblHigh.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblLow.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    
    [self getStockInfo];
    [self getTweetsNews];
}
- (void)stockSearch
{
    NSString *symbol = [self _txtSearch].text;
    if( symbol == nil || [symbol isEqualToString:@""] == true )
    {
        [Global showAlertTips: @"Please input stock symbol!" ] ;
        return;
    }
    [Global sharedGlobal].stockSymbol = symbol;
    self._segment.selectedSegmentIndex = 0;
    [self getStockInfo];
    [self getTweetsNews];
}

- (IBAction)searchClick:(id)sender {
    if (self._txtSearch != nil) return;
    self._txtSearch = [[Global sharedGlobal] insertSearchTextField:self position:CGRectMake(10, 66, 300, 30)];
    self._txtSearch.text = [Global sharedGlobal].stockSymbol;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stockSearch) name:@"return" object:nil];
    [_scrollView setFrame:CGRectMake(0, 97, _scrollView.frame.size.width, _scrollView.frame.size.height - 30)];
    [UIView setAnimationDuration:1.0];
    [UIView commitAnimations];

}

- (IBAction)backClick:(id)sender {
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (IBAction)switchTimeline:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    switch (selectedSegment) {
        case 0:
            [self getGraphInfo:@"1d"];
            break;
        case 1:
            [self getGraphInfo:@"5d"];
            break;
        case 2:
            [self getGraphInfo:@"1m"];
            break;
        case 3:
            [self getGraphInfo:@"6m"];
            break;
        case 4:
            [self getGraphInfo:@"1y"];
            break;
        case 5:
            [self getGraphInfo:@"5y"];
            break;
        default:
            break;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    [_scrollView setFrame:CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, [UIScreen mainScreen].bounds.size.height - 114)];
 
    CGRect frame = tableView.frame;
    frame.size.height = [self._tweetNews count]*tableView.rowHeight;
    tableView.frame = frame;
    
    CGSize size = CGSizeMake( frame.size.width, frame.origin.y + frame.size.height );
    _scrollView.contentSize = size;
    return [self._tweetNews count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"newsCell"];
    
    UILabel *newsTitle = (UILabel *)[cell viewWithTag:100];
    newsTitle.text =[NSString stringWithFormat:@"%@@%@",[[self._tweetNews[indexPath.row] valueForKeyPath:@"user"] objectForKey:@"name"],[[self._tweetNews[indexPath.row] valueForKeyPath:@"user"] objectForKey:@"screen_name"]];
    newsTitle.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    
    UILabel *newsContent = (UILabel *)[cell viewWithTag:101];
    newsContent.text = [self._tweetNews[indexPath.row] objectForKey:@"text"];
    newsContent.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:12];
    
    UIImageView *img = (UIImageView *)[cell viewWithTag:102];
    //--------------------  WEB SERVER URL ---------------------------------------------
#ifdef USE_REAL_SERVER
  img.image =[UIImage imageWithContentsOfFile:[[self._tweetNews[indexPath.row] valueForKeyPath:@"user"] objectForKey:@"profile_image_url"]];
#else
    NSString *ImageURL = @"http://192.168.0.107/NewStockTwits/json/logo5.png";
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
    img.image = [UIImage imageWithData:imageData];
#endif
    return cell;
}

- (void)drawGraph
{
    [_graphView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    _graphView.backgroundColor = [UIColor whiteColor];
    
    if (self._graphInfo == nil || [self._graphInfo isEqualToString:@""] == true ) {
        [Global showAlertTips: @"Can not get stock's chart data from yahoo. please try again."] ;
        return;
    }

    NSMutableArray *linedata = [[NSMutableArray alloc] init];
    NSMutableArray *singlelinedatas1 = [[NSMutableArray alloc] init];
    
    NSMutableArray *graphData = [NSMutableArray arrayWithArray:[self._graphInfo componentsSeparatedByString:@"\n"]];
    for (int i = 0; i < graphData.count; i++) {
        if ([graphData[i] isEqualToString:@""] == true) [graphData removeObjectAtIndex:i];
    }
    
    NSArray *value = [[NSArray alloc] init];
    NSDate *stockDate;
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT-08:00"]];
    
    @try {
        NSString *range = [[[[[graphData firstObject] componentsSeparatedByString:@"range="] objectAtIndex:1] componentsSeparatedByString:@"/"] objectAtIndex:0];
        
        int j;
        for ( j=0; j<graphData.count; j++) {
            if ([[[graphData[j] componentsSeparatedByString:@":"] objectAtIndex:0] isEqualToString:@"volume"] == true){
                break;
            }
        }
        for ( int i=j+1; i<graphData.count; i++) {
            value = [graphData[i] componentsSeparatedByString:@","];
            if (value.count < 2) continue;
            if ([range isEqualToString:@"1d"] == true) {
                stockDate = [NSDate dateWithTimeIntervalSince1970:[value[0] intValue]];
                [dateFormatter setDateFormat:@"hh:mm a"] ;
                [singlelinedatas1 addObject:[[CCSLineData alloc] initWithValue:[value[4] floatValue] date:[dateFormatter stringFromDate:stockDate]]];
            }else if ([range isEqualToString:@"5d"] == true) {
                stockDate = [NSDate dateWithTimeIntervalSince1970:[value[0] intValue]];
                [dateFormatter setDateFormat:@"EEEE"] ;
                [singlelinedatas1 addObject:[[CCSLineData alloc] initWithValue:[value[4] floatValue] date:[dateFormatter stringFromDate:stockDate]]];
            }else if ([range isEqualToString:@"1m"] == true) {
                [dateFormatter setDateFormat:@"yyyyMMdd"];
                stockDate = [dateFormatter dateFromString:value[0]];
                [dateFormatter setDateFormat:@"MMM dd"] ;
                [singlelinedatas1 addObject:[[CCSLineData alloc] initWithValue:[value[4] floatValue] date:[dateFormatter stringFromDate:stockDate]]];
            }else if ([range isEqualToString:@"6m"] == true) {
                [dateFormatter setDateFormat:@"yyyyMMdd"];
                stockDate = [dateFormatter dateFromString:value[0]];
                [dateFormatter setDateFormat:@"MMM dd"] ;
                [singlelinedatas1 addObject:[[CCSLineData alloc] initWithValue:[value[4] floatValue] date:[dateFormatter stringFromDate:stockDate]]];
            }else if ([range isEqualToString:@"1y"] == true) {
                [dateFormatter setDateFormat:@"yyyyMMdd"];
                stockDate = [dateFormatter dateFromString:value[0]];
                [dateFormatter setDateFormat:@"MMM yyyy"] ;
                [singlelinedatas1 addObject:[[CCSLineData alloc] initWithValue:[value[4] floatValue] date:[dateFormatter stringFromDate:stockDate]]];
            }else if ([range isEqualToString:@"5y"] == true) {
                [dateFormatter setDateFormat:@"yyyyMMdd"];
                stockDate = [dateFormatter dateFromString:value[0]];
                [dateFormatter setDateFormat:@"MMM yyyy"] ;
                [singlelinedatas1 addObject:[[CCSLineData alloc] initWithValue:[value[4] floatValue] date:[dateFormatter stringFromDate:stockDate]]];
            }else{
                [Global showAlertTips: @"Can not get stock's chart data from yahoo. please try again."] ;
                return;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"graphInfo parsing error:%@",exception.description);
    }
    @finally {

    }
    
    [_graphView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    _graphView.backgroundColor = [UIColor whiteColor];

    CCSTitledLine *singleline1 = [[CCSTitledLine alloc] init];
    singleline1.data = singlelinedatas1;
    singleline1.color = [UIColor blueColor];
    singleline1.title = @"chartLine1";
    
    [linedata addObject:singleline1];
    
    CCSAreaChart *linechart = [[CCSAreaChart alloc] initWithFrame:CGRectMake(0, 0, _graphView.frame.size.width, _graphView.frame.size.height)];
    
    linechart.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    linechart.linesData = linedata;
    linechart.longitudeNum = 5;
    linechart.backgroundColor = [UIColor clearColor];
    linechart.lineWidth = 0.5;
    linechart.areaAlpha = 0.2;
    linechart.displayCrossYOnTouch = NO;
    linechart.displayCrossXOnTouch = NO;
    linechart.latitudeFont = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:8];
    linechart.longitudeFont = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:8];
    linechart.longitudeFontSize = 8;
    linechart.latitudeFontSize = 8;
    linechart.axisMarginLeft = 25;
    linechart.axisMarginBottom = 10;
    linechart.axisMarginTop = 0;
    linechart.axisMarginRight = 0;
    
    [self._graphView addSubview:linechart];
}

- (void)showStockInfo
{
    @try {
        if (self._stockInfo == nil) {
            if (self._graphInfo == nil) {
                self._lblCurrentPrice.text = @"Current Price:  ";
                self._lblDailyChange.text = @"";
                self._lblOpen.text = @"Open:  ";
                self._lblClose.text = @"Close:  ";
                self._lblHigh.text = @"High :  ";
                self._lblLow.text = @"Low :  ";
                [Global showAlertTips: @"Can not get stock's real data from yahoo. Please try again."] ;
            }else{
                NSMutableArray *graphData = [NSMutableArray arrayWithArray:[self._graphInfo componentsSeparatedByString:@"\n"]];
                for (int i = 0; i < graphData.count; i++) {
                    if ([graphData[i] isEqualToString:@""] == true) [graphData removeObjectAtIndex:i];
                }
                NSArray *value = [[NSArray alloc] init];
                double closeValue = 0, currentValue = 0, dailyChangeValue = 0, changeInPercent = 0;
                for (int i=0; i<graphData.count; i++) {
                    value = [graphData[i] componentsSeparatedByString:@":"];
                    if (value.count < 2) continue;
                    if ([value[0] hasPrefix:@"previous_close"] == true){
                        self._lblClose.text = [@"Close:  " stringByAppendingString:value[1]];
                        closeValue = [value[1] doubleValue];
                    }else if ([value[0] isEqualToString:@"open"] == true){
                        self._lblOpen.text = [@"Open:  " stringByAppendingString:[[value[1] componentsSeparatedByString:@","] objectAtIndex:1]];
                    }else if ([value[0] isEqualToString:@"high"] == true){
                        self._lblHigh.text = [@"High :  " stringByAppendingString:[[value[1] componentsSeparatedByString:@","] objectAtIndex:1]];
                    }else if ([value[0] isEqualToString:@"low"] == true){
                        self._lblLow.text = [@"Low :  " stringByAppendingString:[[value[1] componentsSeparatedByString:@","] objectAtIndex:0]];
                    }else if ([value[0] isEqualToString:@"Company-Name"] == true){
                        self._lblYahooTitle.text = [NSString stringWithFormat:@"%@,%@",[Global sharedGlobal].stockSymbol, value[1]];
                    }
                }
            
                self._lblCurrentPrice.text = [@"Current Price:  " stringByAppendingString:[[[graphData lastObject] componentsSeparatedByString:@","] objectAtIndex:4]];
                currentValue = [[[[graphData lastObject] componentsSeparatedByString:@","] objectAtIndex:4] doubleValue];
                dailyChangeValue = currentValue - closeValue;
                if (closeValue != 0) changeInPercent = dailyChangeValue / closeValue * 100;
                if (dailyChangeValue > 0)
                {
                    self._lblDailyChange.textColor = [UIColor greenColor];
                }
                else
                {
                    self._lblDailyChange.textColor = [UIColor colorWithRed:249.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1.0];
                }
                self._lblDailyChange.text =[NSString stringWithFormat:@"%.2f{%.2f}", dailyChangeValue, changeInPercent];
            }
        }else{
            self._lblYahooTitle.text =  [NSString stringWithFormat:@"%@,%@",[Global sharedGlobal].stockSymbol, [[self._stockInfo valueForKeyPath:@"query.results.quote"] objectForKey:@"Name"]];
            self._lblCurrentPrice.text = [@"Current Price:  " stringByAppendingString:[[self._stockInfo valueForKeyPath:@"query.results.quote"]objectForKey:@"LastTradePriceOnly"]];
            if ([[[self._stockInfo valueForKeyPath:@"query.results.quote"]objectForKey:@"ChangeRealtime"] floatValue] > 0)
            {
                self._lblDailyChange.textColor = [UIColor greenColor];
            }
            else
            {
                self._lblDailyChange.textColor = [UIColor colorWithRed:249.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1.0];
            }
            self._lblDailyChange.text =[NSString stringWithFormat:@"%@{%@}",[[self._stockInfo valueForKeyPath:@"query.results.quote"]objectForKey:@"ChangeRealtime"],[[self._stockInfo valueForKeyPath:@"query.results.quote"]objectForKey:@"ChangeinPercent"]];
            self._lblOpen.text = [@"Open:  " stringByAppendingString:[[self._stockInfo valueForKeyPath:@"query.results.quote"]objectForKey:@"Open"]];
            self._lblClose.text = [@"Close:  " stringByAppendingString:[[self._stockInfo valueForKeyPath:@"query.results.quote"]objectForKey:@"PreviousClose"]];
            self._lblHigh.text = [@"High :  " stringByAppendingString:[[self._stockInfo valueForKeyPath:@"query.results.quote"]objectForKey:@"DaysHigh"]];
            self._lblLow.text = [@"Low :  " stringByAppendingString:[[self._stockInfo valueForKeyPath:@"query.results.quote"]objectForKey:@"DaysLow"]];
        }
        NSArray *title = [[Global sharedGlobal].stockSymbol componentsSeparatedByString:@","];
        if (title.count > 1) self._lblYahooTitle.text = [Global sharedGlobal].stockSymbol;
    }
    @catch (NSException *exception) {
        NSLog(@"stockInfo parsing error:%@",exception.description);
    }
    @finally {

    }
 
    return;
}

- (void)getStockInfo
{
    NSString *symbol = [[[Global sharedGlobal].stockSymbol componentsSeparatedByString:@","] firstObject];
    symbol = [symbol stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
   
    //--------------------  WEB SERVER URL ---------------------------------------------
#ifdef USE_REAL_SERVER
   NSString *stockInfoUrl = [[NSString alloc] initWithFormat: @"https://query.yahooapis.com/v1/public/yql?q=select%%20%%2a%%20from%%20yahoo.finance.quotes%%20where%%20symbol%%20in%%20%%28%%22%@%%22%%29%%0A%%09%%09&env=http%%3A%%2F%%2Fdatatables.org%%2Falltables.env&format=json", symbol];
#else
    NSString *stockInfoUrl = [[NSString alloc] initWithFormat: @"http://192.168.0.107/NewStockTwits/json/chart/%@",symbol];
#endif
    [ MBProgressHUD showHUDAddedTo : self.view animated : YES ] ;

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:25];
    [manager GET:stockInfoUrl parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
         
         NSString *result = operation.responseString;
         SBJSON *parser = [[SBJSON alloc] init];
         self._stockInfo = [parser objectWithString:result];
         [self getGraphInfo:@"1d"];
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
        NSLog(@"%@",error.description);
        self._stockInfo = nil;
        [self getGraphInfo:@"1d"];
    }];
}

- (void)getGraphInfo:(NSString *)range
{
    NSString *symbol = [[[Global sharedGlobal].stockSymbol componentsSeparatedByString:@","] firstObject];
    symbol = [symbol stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    if( range == nil || [range isEqualToString:@""] == true )   range = @"1d";
   
    //--------------------  WEB SERVER URL ---------------------------------------------
#ifdef USE_REAL_SERVER
   NSString *graphInfoUrl = [[NSString alloc] initWithFormat: @"https://chartapi.finance.yahoo.com/instrument/1.0/%@/chartdata;type=quote;range=%@/csv", symbol, range];
#else
    NSString *graphInfoUrl = [[NSString alloc] initWithFormat: @"http://192.168.0.107/NewStockTwits/json/chart/%@_%@.txt", symbol, range];
#endif
    [ MBProgressHUD showHUDAddedTo : self.view animated : YES ] ;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:25];
    [manager GET:graphInfoUrl parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         self._graphInfo = operation.responseString;
         if ([range isEqualToString:@"1d"] == true) [self showStockInfo];
         [self drawGraph];
         [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             self._graphInfo = nil;
             if ([range isEqualToString:@"1d"] == true) [self showStockInfo];
             [self drawGraph];
             [ MBProgressHUD hideHUDForView : self.view animated : YES ] ;
        }];
}

- (void)getTweetsNews
{
#ifdef USE_REAL_SERVER
    NSString *symbol = [[[Global sharedGlobal].stockSymbol componentsSeparatedByString:@","] firstObject];
    symbol = [symbol stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    STTwitterAPI *twitter = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:@"howb20GutmTtvUnM1qcKyHATE" consumerSecret:@"IcXOqOSBFkBwPyKxJNkOGayfSwtPdxi3pqNgPiw8q8Oo6Tw8wI"];
    
    [twitter verifyCredentialsWithSuccessBlock:^(NSString *bearerToken) {
        
        [twitter getSearchTweetsWithQuery:symbol successBlock:^(NSDictionary *searchMetadata, NSArray *statuses) {
            
            self._tweetNews = [NSMutableArray arrayWithArray:statuses];
            [self._tableView reloadData];
        } errorBlock:^(NSError *error) {
            self._tweetNews = nil;
            [self._tableView reloadData];
            NSLog(@"%@", error.debugDescription);

        }];
        
    } errorBlock:^(NSError *error) {
        
        NSLog(@"%@", error.debugDescription);
        
    }];
    
#else
    
  
    //--------------------  WEB SERVER URL ---------------------------------------------

    NSString *tweetUrl = [[NSString alloc] initWithFormat: @"file:///Users/admin/Documents/TSR/TSRTests/tweet.txt"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //  manager.securityPolicy.allowInvalidCertificates = YES;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:25];
    [manager GET:tweetUrl parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *result = operation.responseString;
         SBJSON *parser = [[SBJSON alloc] init];
         NSArray *jsonArray = [[parser objectWithString:result] valueForKeyPath:@"statuses"];
         self._tweetNews = [NSMutableArray arrayWithArray:jsonArray];
         [self._tableView reloadData];
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"%@",error.description);
         }];
#endif
}

@end
