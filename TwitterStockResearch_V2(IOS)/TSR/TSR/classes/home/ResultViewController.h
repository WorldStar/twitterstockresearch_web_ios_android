//
//  ResultViewController.h
//  TSR
//
//  Created by Polaris on 12/28/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) IBOutlet UIScrollView *_scrollView;
@property (retain, nonatomic) IBOutlet UITableView *_tableView;
@property (weak, nonatomic) IBOutlet UIView *_graphView;
@property (weak, nonatomic) IBOutlet UILabel *_lblCurrentPrice;
@property (weak, nonatomic) IBOutlet UILabel *_lblDailyChange;
@property (weak, nonatomic) IBOutlet UILabel *_lblDailyChangeTitle;
@property (weak, nonatomic) IBOutlet UILabel *_lblOpen;
@property (weak, nonatomic) IBOutlet UILabel *_lblClose;
@property (weak, nonatomic) IBOutlet UILabel *_lblHigh;
@property (weak, nonatomic) IBOutlet UILabel *_lblLow;
@property (weak, nonatomic) IBOutlet UILabel *_lblYahooTitle;
@property (weak, nonatomic) IBOutlet UILabel *_lblTime;
@property (weak, nonatomic) IBOutlet UILabel *_lblTweetNews;
@property (weak, nonatomic) IBOutlet UISegmentedControl *_segment;
@property (retain, nonatomic) IBOutlet UITextField *_txtSearch;

@property (nonatomic, retain) NSDictionary *_stockInfo;
@property (nonatomic, retain) NSString *_graphInfo;
@property (retain, nonatomic) NSMutableArray *_tweetNews;

- (IBAction)searchClick:(id)sender;
- (IBAction)backClick:(id)sender;
- (IBAction)switchTimeline:(id)sender;

@end
