//
//  VerifyViewController.h
//  TSR
//
//  Created by Polaris on 12/27/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyViewController : UIViewController <UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UITextField *_txtVerify;

- (IBAction)confirmClick:(id)sender;
- (IBAction)backClick:(id)sender;

@end
