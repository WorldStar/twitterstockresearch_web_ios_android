//
//  LoginViewController.h
//  TSR
//
//  Created by Polaris on 12/22/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UITextField *_txtPassword;
@property (retain, nonatomic) IBOutlet UITextField *_txtName;
@property (retain, nonatomic) IBOutlet UITextField *_txtSearch;

- (IBAction)loginClick:(id)sender;
- (IBAction)joinClick:(id)sender;

@end
