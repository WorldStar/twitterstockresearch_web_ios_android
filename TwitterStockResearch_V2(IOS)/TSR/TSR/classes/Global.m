
#import "Global.h"

@implementation Global

Global* _sharedGlobal = nil;

+ (Global*)sharedGlobal
{
    if( _sharedGlobal == nil )
    {
        _sharedGlobal = [[Global alloc] init];
        [_sharedGlobal LoadParam];
    }
    
    return _sharedGlobal;
}


- (void)LoadParam
{
    NSUserDefaults  *defaults = [NSUserDefaults standardUserDefaults];
	BOOL            bInstalled = [defaults boolForKey:@"Launch Application"];
    
	if( !bInstalled )
    {
		[defaults setBool:YES forKey:@"Launch Application"];
        
        self.username = @"";
        self.loginEmail = @"";
        self.password = @"";
		[self SaveParam];
	}
	else
    {
        self.username = [defaults valueForKey:@"username"];
        self.loginEmail = [defaults valueForKey:@"loginEmail"];
        self.password = [defaults valueForKey:@"Password"];
	}
    
    self.isLogined = false;
    self.stockSymbol = nil;
    self.marketsInfo = [[NSArray alloc] initWithObjects:
                    [[NSMutableDictionary alloc] initWithObjects:@[@"^dji", @"DOW Jones", @""] forKeys:@[@"symbol", @"title", @"info"]],
                    [[NSMutableDictionary alloc] initWithObjects:@[@"^GSPC", @"S&P", @""] forKeys:@[@"symbol", @"title", @"info"]],
                    [[NSMutableDictionary alloc] initWithObjects:@[@"^IXIC", @"NASDAQ", @""] forKeys:@[@"symbol", @"title", @"info"]], nil];
}

- (UITextField *)insertSearchTextField:(UIViewController *)into position:(CGRect)frame
{
    UITextField * _txtSearch = [[UITextField alloc] init];
    _txtSearch.borderStyle = UITextBorderStyleRoundedRect;
    _txtSearch.textColor = [UIColor blackColor];
    _txtSearch.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    _txtSearch.placeholder = @"Search by stock symbol";
    _txtSearch.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _txtSearch.backgroundColor = [UIColor whiteColor];
    _txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;	// has a clear 'x' button to the right
    UIImageView *imageSearch = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search.png"]];
    _txtSearch.leftView = imageSearch;
    _txtSearch.leftViewMode = UITextFieldViewModeAlways;
    _txtSearch.layer.cornerRadius = 15;
    self.autoFillTableViewController = [[AutoFillTableViewController alloc] initWithStyle:UITableViewStylePlain];
    self.autoFillTableViewController.toTextField = _txtSearch;
    _txtSearch.delegate = self.autoFillTableViewController;
    _txtSearch.frame = frame;
    [self.autoFillTableViewController createTableView:frame];
    self.autoFillTableViewController.tableView.hidden = YES;
    [into addChildViewController:self.autoFillTableViewController];
    [into.view addSubview:self.autoFillTableViewController.tableView];
    [into.view bringSubviewToFront:self.autoFillTableViewController.tableView];
    [into.view addSubview: _txtSearch];
    return _txtSearch;
}

- (void)SaveParam
{
    NSUserDefaults  *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setValue:self.username forKey:@"username"];
    [defaults setValue:self.loginEmail forKey:@"loginEmail"];
    [defaults setValue:self.password forKey:@"Password"];

    [defaults synchronize];
}

+ (void)showAlertTips:(NSString *)_message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:_message
                                                        delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [ alertView show ] ;
}
@end