
#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "JSON.h"
#import "AutoFillTableViewController.h"

#define USE_REAL_SERVER

@interface Global : NSObject

@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSString *loginEmail;
@property (nonatomic, retain) NSString *password;
@property (nonatomic) BOOL isLogined;

@property (nonatomic, retain) NSString *stockSymbol;
@property (nonatomic, retain) NSArray *marketsInfo;
@property AutoFillTableViewController *autoFillTableViewController;

+ (Global*)sharedGlobal;
- (void)SaveParam;
+ (void)showAlertTips:(NSString *)_message;
- (UITextField *)insertSearchTextField:(UIViewController *)into position:(CGRect)frame;

@end