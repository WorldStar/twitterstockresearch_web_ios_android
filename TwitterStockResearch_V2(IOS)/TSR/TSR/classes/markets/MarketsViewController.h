//
//  MarketsViewController.h
//  TSR
//
//  Created by Polaris on 12/31/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketsViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *_contentView;
@property (strong, nonatomic) IBOutlet UIView *_graphView;
@property (strong, nonatomic) IBOutlet UILabel *_lblBtnTitle;
@property (strong, nonatomic) IBOutlet UILabel *_lblTime;
@property (strong, nonatomic) IBOutlet UILabel *_lblSymbol1Title;
@property (strong, nonatomic) IBOutlet UILabel *_lblSymbol1Value;
@property (strong, nonatomic) IBOutlet UILabel *_lblSymbol1Change;
@property (strong, nonatomic) IBOutlet UILabel *_lblSymbol2Title;
@property (strong, nonatomic) IBOutlet UILabel *_lblSymbol2Value;
@property (strong, nonatomic) IBOutlet UILabel *_lblSymbol2Change;
@property (strong, nonatomic) IBOutlet UILabel *_lblSymbol3Title;
@property (strong, nonatomic) IBOutlet UILabel *_lblSymbol3Value;
@property (strong, nonatomic) IBOutlet UILabel *_lblSymbol3Change;
@property (strong, nonatomic) IBOutlet UISegmentedControl *_segment;

@property (nonatomic)  int _replyCount;

- (IBAction)switchTimeline:(id)sender;
- (IBAction)switchCountry:(id)sender;

@end
