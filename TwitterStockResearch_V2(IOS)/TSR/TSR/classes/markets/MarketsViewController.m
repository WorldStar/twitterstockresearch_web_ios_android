//
//  MarketsViewController.m
//  TSR
//
//  Created by Polaris on 12/31/14.
//  Copyright (c) 2014 Nihao. All rights reserved.
//

#import "MarketsViewController.h"
#import "CCSAreaChart.h"
#import "CCSLineData.h"
#import "CCSTitledLine.h"
#import "Global.h"

@interface MarketsViewController ()

@end

@implementation MarketsViewController

@synthesize _graphView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
    self._lblBtnTitle.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:18];
    self._lblTime.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:13];
    self._lblSymbol1Title.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblSymbol1Value.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblSymbol1Change.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblSymbol2Title.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblSymbol2Value.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblSymbol2Change.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblSymbol3Title.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblSymbol3Value.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
    self._lblSymbol3Change.font = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:14];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   
    if( [Global sharedGlobal].isLogined ){
        self._contentView.hidden = NO;
        [self getMarketsInfo:@"1d"];
        self._segment.selectedSegmentIndex = 0;
    }else{
        [Global showAlertTips:@"Please login."];
        self._contentView.hidden = YES;
    }
}

- (void)getMarketsInfo:(NSString *)range
{
    self._replyCount = 0;
    if( range == nil || [range isEqualToString:@""] == true )   range = @"1d";
    [ MBProgressHUD showHUDAddedTo : self.parentViewController.view animated : YES ];
    for (int i=0; i<3; i++)
    {
        NSString *symbol = [[Global sharedGlobal].marketsInfo[i] objectForKey:@"symbol"];
        symbol = [symbol stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
   
        //--------------------  WEB SERVER URL ---------------------------------------------
#ifdef USE_REAL_SERVER
        NSString *graphUrl = [[NSString alloc] initWithFormat: @"https://chartapi.finance.yahoo.com/instrument/1.0/%@/chartdata;type=quote;range=%@/csv", symbol, range];
#else
        NSString *graphUrl = [[NSString alloc] initWithFormat: @"http://192.168.0.107/NewStockTwits/json/chart/%@_%@.txt", symbol,range];
#endif
         AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
         manager.requestSerializer = [AFJSONRequestSerializer serializer];
         manager.responseSerializer = [AFHTTPResponseSerializer serializer];
         [manager.requestSerializer setTimeoutInterval:25];
         [manager GET:graphUrl parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [[Global sharedGlobal].marketsInfo[i] setValue:operation.responseString forKey:@"info"];
             self._replyCount+=1;
             if (self._replyCount >= 3) {
                 [self drawGraph];
                 [ MBProgressHUD hideHUDForView : self.parentViewController.view animated : YES ] ;
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [[Global sharedGlobal].marketsInfo[i] setValue:@"" forKey:@"info"];
             self._replyCount+=1;
             if (self._replyCount >= 3) {
                 [self drawGraph];
                 [ MBProgressHUD hideHUDForView : self.parentViewController.view animated : YES ] ;
             }
         }];
    }
}

- (void)drawGraph
{
    
    [_graphView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    _graphView.backgroundColor = [UIColor whiteColor];
    NSMutableArray *linedata = [[NSMutableArray alloc] init];
    self._replyCount = 0;
    for (int i=0; i<3; i++)
    {
        if ([[[Global sharedGlobal].marketsInfo[i] objectForKey:@"info"] isEqualToString:@""] == true)
        {
            switch (i) {
                case 0:
                    if ([self._lblSymbol1Value.text isEqualToString:@"Loading..."]) {
                        self._lblSymbol1Value.text =  @"Invalid stock information.";
                    }
                    break;
                case 1:
                    if ([self._lblSymbol2Value.text isEqualToString:@"Loading..."]) {
                        self._lblSymbol2Value.text =  @"Invalid stock information.";
                    }
                    break;
                case 2:
                    if ([self._lblSymbol3Value.text isEqualToString:@"Loading..."]) {
                        self._lblSymbol3Value.text =  @"Invalid stock information.";
                    }
                    break;
                    
                default:
                    break;
            }
            self._replyCount ++;
            if (self._replyCount >= 3) {
                return;
            }
            continue;
        }
        
        NSMutableArray *singlelinedatas1 = [[NSMutableArray alloc] init];
        

        NSMutableArray *graphData = [NSMutableArray arrayWithArray:[[[Global sharedGlobal].marketsInfo[i] objectForKey:@"info"] componentsSeparatedByString:@"\n"]];
        
        for (int i = 0; i < graphData.count; i++) {
            if ([graphData[i] isEqualToString:@""] == true) [graphData removeObjectAtIndex:i];
        }

        NSArray *value = [[NSArray alloc] init];
        NSDate *stockDate;
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init] ;
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT-08:00"]];
     
        @try {
            NSString *range = [[[[[graphData firstObject] componentsSeparatedByString:@"range="] objectAtIndex:1] componentsSeparatedByString:@"/"] objectAtIndex:0];
            double closeValue = 0.0, currentValue = 0.0, dailyChangeValue = 0.0;
            
            if ([graphData count] < 18)
            {
                switch (i) {
                    case 0:
                        self._lblSymbol1Value.text = @"Invalid stock information.";
                        break;
                    case 1:
                        self._lblSymbol2Value.text =  @"Invalid stock information.";
                        break;
                    case 2:
                        self._lblSymbol3Value.text =  @"Invalid stock information.";
                        break;
                        
                    default:
                        break;
                }
                continue;
            }
            
            //----------------------------------   get closeValue and dailyChangeValue
            for (int i=0; i<graphData.count; i++) {
                value = [graphData[i] componentsSeparatedByString:@":"];
                if (value.count < 2) continue;
                if ([value[0] hasPrefix:@"previous_close"] == true){
                    closeValue = [value[1] doubleValue];
                    break;
                }
            }
            currentValue = [[[[graphData lastObject] componentsSeparatedByString:@","] objectAtIndex:4] doubleValue];
            if (closeValue == 0) {
                NSLog(@"Warning: previous close value no found.");
                continue;
            }
            dailyChangeValue = currentValue - closeValue;
            //---------------------------------   display stock information--------------------------
            if ([range isEqualToString:@"1d"] == true)
            {
                switch (i) {
                    case 0:
                        self._lblSymbol1Title.text = [[Global sharedGlobal].marketsInfo[i] objectForKey:@"title"];
                        self._lblSymbol1Value.text = [NSString stringWithFormat:@"%.2f", currentValue];
                        self._lblSymbol1Change.text =[NSString stringWithFormat:@"%.2f (%.2f%%)", dailyChangeValue, dailyChangeValue / closeValue * 100];
                        
                        if (dailyChangeValue > 0)
                        {
                            self._lblSymbol1Change.textColor = [UIColor greenColor];
                        }
                        else{
                            self._lblSymbol1Change.textColor = self._lblSymbol1Title.textColor;
                        }
                        break;
                        
                    case 1:
                        self._lblSymbol2Title.text = [[Global sharedGlobal].marketsInfo[i] objectForKey:@"title"];
                        self._lblSymbol2Value.text = [NSString stringWithFormat:@"%.2f", currentValue];
                        self._lblSymbol2Change.text =[NSString stringWithFormat:@"%.2f (%.2f%%)", dailyChangeValue, dailyChangeValue / closeValue * 100];
                        
                        if (dailyChangeValue > 0)
                        {
                            self._lblSymbol2Change.textColor = [UIColor greenColor];
                        }
                        else{
                            self._lblSymbol2Change.textColor = self._lblSymbol2Title.textColor;
                        }
                        break;
                        
                    case 2:
                        self._lblSymbol3Title.text = [[Global sharedGlobal].marketsInfo[i] objectForKey:@"title"];
                        self._lblSymbol3Value.text = [NSString stringWithFormat:@"%.2f", currentValue];
                        self._lblSymbol3Change.text =[NSString stringWithFormat:@"%.2f (%.2f%%)", dailyChangeValue, dailyChangeValue / closeValue * 100];
                        
                        if (dailyChangeValue > 0)
                        {
                            self._lblSymbol3Change.textColor = [UIColor greenColor];
                        }
                        else{
                            self._lblSymbol3Change.textColor = self._lblSymbol3Title.textColor;
                        }
                        break;
                    default:
                        break;
                }
            }
            //------------------------------------------------------------------------------------------------
            int j;
            for ( j=0; j<graphData.count; j++) {
                if ([[[graphData[j] componentsSeparatedByString:@":"] objectAtIndex:0] isEqualToString:@"volume"] == true){
                    break;
                }
            }
            //---------------------------------------   get graph data    -------------------------------------
            for ( int i=j+1; i<graphData.count; i++) {
                value = [graphData[i] componentsSeparatedByString:@","];
                if ([range isEqualToString:@"1d"] == true) {
                    stockDate = [NSDate dateWithTimeIntervalSince1970:[value[0] intValue]];
                    [dateFormatter setDateFormat:@"hh:mm a"] ;
                }else if ([range isEqualToString:@"5d"] == true) {
                    stockDate = [NSDate dateWithTimeIntervalSince1970:[value[0] intValue]];
                    [dateFormatter setDateFormat:@"EEEE"] ;
                }else if ([range isEqualToString:@"1m"] == true) {
                    [dateFormatter setDateFormat:@"yyyyMMdd"];
                    stockDate = [dateFormatter dateFromString:value[0]];
                    [dateFormatter setDateFormat:@"MMM dd"] ;
                }else if ([range isEqualToString:@"3m"] == true) {
                    [dateFormatter setDateFormat:@"yyyyMMdd"];
                    stockDate = [dateFormatter dateFromString:value[0]];
                    [dateFormatter setDateFormat:@"MMM dd"] ;
                }else if ([range isEqualToString:@"6m"] == true) {
                    [dateFormatter setDateFormat:@"yyyyMMdd"];
                    stockDate = [dateFormatter dateFromString:value[0]];
                    [dateFormatter setDateFormat:@"MMM dd"] ;
                }else if ([range isEqualToString:@"1y"] == true) {
                    [dateFormatter setDateFormat:@"yyyyMMdd"];
                    stockDate = [dateFormatter dateFromString:value[0]];
                    [dateFormatter setDateFormat:@"MMM yyyy"] ;
                }else{
                    [Global showAlertTips: @"Failed. Please check stock's range." ] ;
                    return;
                }
                currentValue = [value[4] floatValue];
                dailyChangeValue = (currentValue - closeValue) / closeValue * 100 ;
                [singlelinedatas1 addObject:[[CCSLineData alloc] initWithValue:dailyChangeValue * 100 + 10000 date:[dateFormatter stringFromDate:stockDate]]];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"market data parsing error:%@",exception.description);
        }
        @finally {
            
        }
        CCSTitledLine *singleline = [[CCSTitledLine alloc] init];
        singleline.data = singlelinedatas1;
        if (i == 0) {
            singleline.color = self._lblSymbol1Title.textColor;
        }else if (i == 1){
            singleline.color = self._lblSymbol2Title.textColor;
        }else {
            singleline.color = self._lblSymbol3Title.textColor;
        }
        [linedata addObject:singleline];
    }
    
    CCSAreaChart *linechart = [[CCSAreaChart alloc] initWithFrame:CGRectMake(0, 0, _graphView.frame.size.width, _graphView.frame.size.height)];
    
    linechart.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    if (linedata.count == 0) return;
    linechart.linesData = linedata;
  //  linechart.maxValue = 20000;
  //  linechart.minValue = 0;
    linechart.longitudeNum = 5;
    linechart.backgroundColor = [UIColor clearColor];
    linechart.lineWidth = 1.0;
    linechart.areaAlpha = 0.1;
    
    linechart.displayCrossYOnTouch = NO;
    linechart.displayCrossXOnTouch = NO;
    linechart.latitudeFont = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:8];
    linechart.longitudeFont = [UIFont fontWithName:@"HelveticaNeueLTStd-Cn" size:8];
    linechart.longitudeFontSize = 8;
    linechart.latitudeFontSize = 8;
    linechart.axisMarginLeft = 25;
    linechart.axisMarginBottom = 10;
    linechart.axisMarginTop = 0;
    linechart.axisMarginRight = 0;
    linechart.axisYPercent = YES;

    [self._graphView addSubview:linechart];
}

- (IBAction)switchTimeline:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
   // self._segment.selectedSegmentIndex = -1;
    switch (selectedSegment) {
        case 0:
            [self getMarketsInfo:@"1d"];
            break;
        case 1:
            [self getMarketsInfo:@"5d"];
            break;
        case 2:
            [self getMarketsInfo:@"1m"];
            break;
        case 3:
            [self getMarketsInfo:@"3m"];
            break;
        case 4:
            [self getMarketsInfo:@"6m"];
            break;
        case 5:
            [self getMarketsInfo:@"1y"];
            break;
        default:
            break;
    }
}

- (IBAction)switchCountry:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    self._segment.selectedSegmentIndex = 0;
    switch (selectedSegment) {
        case 0:
            [[Global sharedGlobal].marketsInfo[0] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^dji", @"DOW Jones", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            [[Global sharedGlobal].marketsInfo[1] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^GSPC", @"S&P", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            [[Global sharedGlobal].marketsInfo[2] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^IXIC", @"NASDAQ", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            
            self._lblSymbol1Title.text = @"DOW Jones";
            self._lblSymbol2Title.text = @"S&P";
            self._lblSymbol3Title.text = @"NASDAQ";
            break;
        case 1:
            [[Global sharedGlobal].marketsInfo[0] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^GDAXI", @"DAX", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            [[Global sharedGlobal].marketsInfo[1] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^FCHI", @"CAC 40", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            [[Global sharedGlobal].marketsInfo[2] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^IBEX", @"IBEX", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            
            self._lblSymbol1Title.text = @"DAX";
            self._lblSymbol2Title.text = @"CAC 40";
            self._lblSymbol3Title.text = @"IBEX";
            break;
        case 2:
            [[Global sharedGlobal].marketsInfo[0] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^FTSE", @"FTSE 100", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            [[Global sharedGlobal].marketsInfo[1] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^FTMC", @"FTSE 250", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            [[Global sharedGlobal].marketsInfo[2] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^FTAI", @"AIM", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            
            self._lblSymbol1Title.text = @"FTSE 100";
            self._lblSymbol2Title.text = @"FTSE 250";
            self._lblSymbol3Title.text = @"AIM";
            break;
        case 3:
            [[Global sharedGlobal].marketsInfo[0] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^N225", @"Nikkei 225", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            [[Global sharedGlobal].marketsInfo[1] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"^HSI", @"Hang Seng", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            [[Global sharedGlobal].marketsInfo[2] setValuesForKeysWithDictionary:[[NSMutableDictionary alloc] initWithObjects:@[@"000001.SS", @"SSE Composite", @""] forKeys:@[@"symbol", @"title", @"info"]]];
            
            self._lblSymbol1Title.text = @"Nikkei 225";
            self._lblSymbol2Title.text = @"Hang Seng";
            self._lblSymbol3Title.text = @"SSE Composite";
            break;
        default:
            break;
    }
    self._lblSymbol1Value.text = @"Loading...";
    self._lblSymbol1Change.text = @"";
    self._lblSymbol2Value.text = @"Loading...";
    self._lblSymbol2Change.text = @"";
    self._lblSymbol3Value.text = @"Loading...";
    self._lblSymbol3Change.text = @"";

    [self getMarketsInfo:@"1d"];
}
@end
